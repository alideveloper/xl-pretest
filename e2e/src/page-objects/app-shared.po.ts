/*
 * Use the Page Object pattern to define the page under test.
 * See docs/coding-guide/e2e-tests.md for more info.
 */

import { browser, element, by } from 'protractor';

import { environment } from '@environments/environment';

export class AppSharedPage {
  async navigateAndSetLanguage() {
    // Forces default language
    await this.navigateTo();
    await browser.executeScript(() =>
      localStorage.setItem(
        environment.domain + '-language',
        environment.defaultLanguage
      )
    );
  }

  async navigateTo() {
    await browser.get('/');
  }
}
