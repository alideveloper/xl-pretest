import { Injectable } from '@angular/core';

interface Scripts {
  name: string;
  src: string;
}
export const ScriptStore: Scripts[] = [
  {
    name: 'pdfMake',
    src: 'https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.60/pdfmake.min.js',
  },
  {
    name: 'vfsFonts',
    src: 'https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.60/vfs_fonts.js',
  },
  {
    name: 'exceljs',
    src: 'https://cdnjs.cloudflare.com/ajax/libs/exceljs/4.1.1/exceljs.min.js',
  },
];

@Injectable({
  providedIn: 'root',
})
export class AppScript {
  private scripts: any = {};

  constructor() {
    ScriptStore.forEach((script: any) => {
      this.scripts[script.name] = {
        loaded: false,
        src: script.src,
      };
    });
  }

  load(...scripts: string[]) {
    const promises: any[] = [];
    scripts.forEach((script) => promises.push(this.loadScript(script)));
    return Promise.all(promises);
  }

  loadScript(name: string, scripts: any[] = this.scripts) {
    return new Promise((resolve, reject) => {
      // resolve if already loaded
      if (scripts[name].loaded) {
        console.log('Loaded', name);
        resolve({ script: name, loaded: true, status: 'Already Loaded' });
      } else {
        // load script
        const script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = scripts[name].src;
        script.onload = () => {
          scripts[name].loaded = true;
          resolve({ script: name, loaded: true, status: 'Loaded' });
        };
        console.log('Loaded', name);
        script.onerror = (error: any) =>
          resolve({ script: name, loaded: false, status: 'Loaded' });
        document.getElementsByTagName('head')[0].appendChild(script);
      }
    });
  }
}
