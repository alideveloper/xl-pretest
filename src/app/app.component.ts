import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';

import { Logger, I18nService, untilDestroyed } from '@app/core';

import { Title } from '@angular/platform-browser';

import { TranslateService } from '@ngx-translate/core';
import { NgxFaviconService } from 'ngx-favicon';

import { merge } from 'rxjs';
import { filter, map, switchMap } from 'rxjs/operators';

import { environment } from '@environments/environment';

import { FirebaseService } from '@app/@firebase/firebase.service';
import { GlobalConstants } from '@app/shared/constant/global.constant';
import { AppData } from '@app/app.data';
import { AppService } from '@app/app.service';
import { FirebaseConfigModel } from '@shared/model/other/firebase-config.model';
import { Site } from '@shared/model/panel/site.model';
import { Loading } from '@shared/model/other/loading.model';
import { Whatsapp } from '@shared/model/other/whatsapp.model';
import { AppScript } from '@app/app.script';

const log = new Logger('App');

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {
  site: Site;
  loading: Loading;

  dateNow: any = new Date();

  whatsapp: Whatsapp = {
    phone: '6285280871948',
    title: 'Your Message...',
  };

  layout: any = environment.layout;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private titleService: Title,
    private translateService: TranslateService,
    private _firebaseService: FirebaseService,
    private i18nService: I18nService,
    private ngxFaviconService: NgxFaviconService<any>,
    private appData: AppData,
    private appService: AppService,
    private appScript: AppScript
  ) {}

  async ngOnInit() {
    this.loading = GlobalConstants.loading;
    this.getSites();
    this.appService.getProjects();
    this.appService.getDomains();

    this.appScript.load('pdfMake', 'vfsFonts');

    // Setup logger
    if (environment.production) {
      Logger.enableProductionMode();
    }

    log.debug('init');

    // Setup translations
    this.i18nService.init(
      environment.defaultLanguage,
      environment.supportedLanguages
    );

    this.setCategoryAccount();

    const onNavigationEnd = this.router.events.pipe(
      filter((event) => event instanceof NavigationEnd)
    );

    // Change page title on navigation or language change, based on route data
    merge(this.translateService.onLangChange, onNavigationEnd)
      .pipe(
        map(() => {
          let route = this.activatedRoute;
          while (route.firstChild) {
            route = route.firstChild;
          }
          return route;
        }),
        filter((route) => route.outlet === 'primary'),
        switchMap((route) => route.data),
        untilDestroyed(this)
      )
      .subscribe((event) => {
        const title = event.title;
        if (title) {
          this.titleService.setTitle(this.translateService.instant(title));
        }
      });
  }

  setCategoryAccount(): any {
    this.appData.categoryAccount = [
      {
        category: 'Income',
        type: 'Credit',
        icon: '',
        business: false,
      },
      {
        category: 'Expense',
        type: 'Debit',
        icon: '',
        business: false,
      },
      {
        category: 'Loan',
        type: 'Credit',
        icon: '',
        business: false,
      },
      {
        category: 'Debt',
        type: 'Debit',
        icon: '',
        business: false,
      },
      {
        category: 'Capital',
        type: 'Debit',
        icon: '',
        business: true,
      },
      {
        category: 'Asset',
        type: 'Debit',
        icon: '',
        business: true,
      },
    ];
  }

  async getSites(): Promise<any> {
    this.loading.isLoading = true;
    return new Promise(async (resolve, reject) => {
      const configFirebase: FirebaseConfigModel = {
        collection: {
          status: true,
          name: 'globals',
          document: {
            status: true,
            name: 'Master',
            collection: {
              status: true,
              name: 'sites',
              document: {
                status: false,
                name: null,
                document: null,
              },
              orderBy: {
                object: 'created',
                order: 'desc',
              },
              query: [
                {
                  object: 'project',
                  condition: '==',
                  value: environment.projectId,
                },
              ],
            },
          },
        },
      };

      const data: any = await this._firebaseService.handleCollection(
        null,
        configFirebase
      );
      data.subscribe((response: any) => {
        if (response && response.length > 0) {
          localStorage.setItem(
            environment.domain + '-site',
            JSON.stringify(response[0])
          );
          this.site = response[0];
          this.titleService.setTitle(this.site.name);
          this.setFaviconByUrl(this.site.favicon.downloadURL);
          this.whatsapp.phone = this.site.phoneNumber;
          this.loading.isLoading = false;
        }
      });
    });
  }

  public setFaviconByUrl(faviconUrl: string): void {
    this.ngxFaviconService.setFaviconByUrl(faviconUrl);
  }

  ngOnDestroy() {
    this.i18nService.destroy();
  }
}
