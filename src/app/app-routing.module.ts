import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { Shell } from '@app/shell/shell.service';

import { AuthGuard } from '@app/shared/services/auth/guard/auth.guard';
import { AuthNonGuard } from '@app/shared/services/auth/guard/auth-non.guard';
import { AuthFreeGuard } from '@app/shared/services/auth/guard/auth-free.guard';

const routes: Routes = [
  Shell.childRootRoutes([
    {
      path: '',
      redirectTo: 'dashboard',
      pathMatch: 'full',
    },
    {
      path: 'dashboard',
      canActivate: [AuthGuard],
      loadChildren: () =>
        import('@pages/dashboard/dashboard.module').then(
          (m) => m.DashboardModule
        ),
    },
    {
      path: 'dashboard-staff',
      canActivate: [AuthGuard],
      loadChildren: () =>
        import('@pages/dashboard-staff/dashboard.module').then(
          (m) => m.DashboardStaffModule
        ),
    },
    {
      path: 'home',
      canActivate: [AuthGuard],
      loadChildren: () =>
        import('@pages/home/home.module').then((m) => m.HomeModule),
    },
    {
      path: 'master',
      canActivate: [AuthGuard],
      loadChildren: () =>
        import('@pages/master/master.module').then((m) => m.MasterModule),
    },
    {
      path: 'manage',
      canActivate: [AuthGuard],
      loadChildren: () =>
        import('@pages/manage/manage.module').then((m) => m.ManageModule),
    },
    {
      path: 'reporting',
      canActivate: [AuthGuard],
      loadChildren: () =>
        import('@pages/reporting/reporting.module').then(
          (m) => m.ReportingModule
        ),
    },
    {
      path: 'settings',
      canActivate: [AuthGuard],
      loadChildren: () =>
        import('@pages/settings/settings.module').then((m) => m.SettingsModule),
    },
    {
      path: '**',
      redirectTo: '404',
    },
  ]),
  {
    path: '404',
    loadChildren: () =>
      import('@errors/404/error-404.module').then((m) => m.Error404Module),
    canActivate: [AuthFreeGuard],
  },
  {
    path: '500',
    loadChildren: () =>
      import('@errors/500/error-500.module').then((m) => m.Error500Module),
    canActivate: [AuthFreeGuard],
  },

  // Fallback when no prior route is matched
  { path: '**', redirectTo: '404', pathMatch: 'full' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      preloadingStrategy: PreloadAllModules,
      scrollPositionRestoration: 'enabled',
    }),
  ],
  exports: [RouterModule],
  providers: [],
})
export class AppRoutingModule {}
