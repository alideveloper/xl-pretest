import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CurrencyPipe } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { TranslateModule } from '@ngx-translate/core';
import { NgxLoadingModule } from 'ngx-loading';
import { ToastrModule } from 'ngx-toastr';
import { FileUploadModule } from '@iplab/ngx-file-upload';
import { CountdownModule } from 'ngx-countdown';
import { NgPipesModule } from 'ngx-pipes';
import { AgmCoreModule } from '@agm/core';
export const mapsConfig = {
  apiKey: 'AIzaSyDT-6gmS7iBnQZAy1h3ABVK_6nmlt9ConU',
  libraries: ['places'],
};
import { NgxFaviconModule } from 'ngx-favicon';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgxSkeletonModule } from 'ngx-skeleton';
import { QuillModule } from 'ngx-quill';
import { QrCodeModule } from 'ng-qrcode';
import { NgxBarcodeModule } from 'ngx-barcode';
import { NgxBarcodeScannerModule } from '@eisberg-labs/ngx-barcode-scanner';
import { NgxBootstrapSliderModule } from 'ngx-bootstrap-slider';
import { CurrencyMaskInputMode, NgxCurrencyModule } from 'ngx-currency';
export const customCurrencyMaskConfig = {
  align: 'right',
  allowNegative: true,
  allowZero: true,
  decimal: ',',
  precision: 2,
  prefix: 'Rp ',
  suffix: '',
  thousands: '.',
  nullable: true,
  inputMode: CurrencyMaskInputMode.FINANCIAL,
};
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { NgxWhastappButtonModule } from 'ngx-whatsapp-button';
import { NgxScrollTopModule } from 'ngx-scrolltop';
import { WebBluetoothModule } from '@manekinekko/angular-web-bluetooth';

import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import {
  firebaseConfigProd,
  firebaseConfigStaging,
} from '@firebase/firebase.config';

import { AppRoutingModule } from './app-routing.module';
import { CoreModule, ApplicationConfigurationService } from '@app/core';

import { SharedModule } from '@app/shared';
import { ShellModule } from '@shell/shell.module';
import { AuthModule } from '@auth/auth.module';
import { MaintenanceModule } from '@others/maintenance/maintenence.module';
import { ComingsoonModule } from '@others/comingsoon/comingsoon.module';

import { AppComponent } from './app.component';

import {
  getBsDatepickerConfiguration,
  getBsModulesForRoot,
} from '@bootstrap/bootstrap.module';

import {
  PerfectScrollbarModule,
  PERFECT_SCROLLBAR_CONFIG,
  PerfectScrollbarConfigInterface,
} from 'ngx-perfect-scrollbar';
const defaultPerfectScrollbarConfiguration: PerfectScrollbarConfigInterface = {
  suppressScrollX: true,
};

import { NgxMaskModule, IConfig } from 'ngx-mask';
const maskConfig: Partial<IConfig> = {
  validation: false,
};

import { NgxPatternModule } from 'ngx-pattern';

const initializeApp = (_config: ApplicationConfigurationService) => {
  return () => _config.initialize();
};

import { environment } from '@environments/environment';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    TranslateModule.forRoot(),
    NgxLoadingModule.forRoot({}),

    NgxFaviconModule.forRoot<any>({
      faviconElementId: 'favicon',
      defaultUrl: 'favicon.ico',
      custom: {},
    }),
    NgxPaginationModule,
    NgxSkeletonModule,
    QuillModule.forRoot(),
    NgxBarcodeModule.forRoot(),
    NgxBarcodeScannerModule,
    QrCodeModule,
    WebBluetoothModule.forRoot({
      enableTracing: true,
    }),

    ToastrModule.forRoot({
      maxOpened: 5,
      autoDismiss: true,
      preventDuplicates: true,
    }),
    FileUploadModule,
    CountdownModule,
    AgmCoreModule.forRoot(mapsConfig),

    AngularFireModule.initializeApp(
      environment.production ? firebaseConfigProd : firebaseConfigStaging
    ),
    AngularFireAuthModule,
    AngularFirestoreModule,
    // AngularFirestoreModule.enablePersistence(),

    CoreModule,
    ShellModule,
    SharedModule,
    AuthModule,
    PerfectScrollbarModule,
    NgxWhastappButtonModule,
    NgxScrollTopModule,
    NgPipesModule,
    NgxCurrencyModule.forRoot(customCurrencyMaskConfig),
    NgxBootstrapSliderModule,

    NgxMaskModule.forRoot(maskConfig),
    NgxPatternModule,

    // Ngx Bootstrap
    ...getBsModulesForRoot(),

    MaintenanceModule,
    ComingsoonModule,

    AppRoutingModule, // must be imported as the last module as it contains the fallback route
  ],
  declarations: [AppComponent],
  providers: [
    // App Initializer
    {
      provide: APP_INITIALIZER,
      useFactory: initializeApp,
      deps: [ApplicationConfigurationService],
      multi: true,
    },

    // PerfectScrollbar Configuration
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: defaultPerfectScrollbarConfiguration,
    },

    // Ngx-Bootstrap Datepicker Default Configuration
    {
      provide: BsDatepickerConfig,
      useFactory: getBsDatepickerConfiguration,
    },

    CurrencyPipe,
    // {
    //   provide: RouteReuseStrategy,
    //   useClass: AARouteReuseStrategy
    // }
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
