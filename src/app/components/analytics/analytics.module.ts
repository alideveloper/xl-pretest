import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardsModule } from '@components/cards/cards.module';
import { ProgressModule } from '@components/progress/progress.module';

import { TranslateModule } from '@ngx-translate/core';

import { ProgressCardComponent } from './progress-card/progress-card.component';
import { StatCardComponent } from './stat-card/stat-card.component';
import { ValueProgressComponent } from './value-progress/value-progress.component';
import { ValueWidgetComponent } from './value-widget/value-widget.component';

const exports = [
  ProgressCardComponent,
  StatCardComponent,
  ValueProgressComponent,
  ValueWidgetComponent,
];

@NgModule({
  declarations: [...exports],
  imports: [
    CommonModule,
    ProgressModule,
    CardsModule,
    TranslateModule.forChild(),
  ],
  exports,
})
export class AnalyticsModule {}
