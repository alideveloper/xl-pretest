import { Component, OnInit, Input } from '@angular/core';
import { ColorScheme } from '@app/core';

@Component({
  selector: 'app-value-widget',
  templateUrl: './value-widget.component.html',
  styleUrls: ['./value-widget.component.scss'],
})
export class ValueWidgetComponent implements OnInit {
  @Input()
  value: number;

  @Input()
  format: string;

  @Input()
  percent: number;

  @Input()
  percentActive: boolean;

  @Input()
  title: string;

  @Input()
  scheme: ColorScheme;

  constructor() {}

  ngOnInit() {}
}
