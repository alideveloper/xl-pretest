import { Component, ViewEncapsulation, ChangeDetectorRef } from '@angular/core';

import { GlobalService } from '@app/shared/services/global.service';

import { TextComponent } from '@components/text/text.component';

@Component({
  selector: 'component-text-extend',
  templateUrl: './../../text/text.component.html',
  styleUrls: ['./../../text/text.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class TextExtendComponent extends TextComponent {
  constructor(public global: GlobalService, public cdRef: ChangeDetectorRef) {
    super(global, cdRef);
  }
}
