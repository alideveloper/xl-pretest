import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FontawesomeExtendComponent } from './fontawesome.component';

import { SharedModule } from '@shared/shared.module';

@NgModule({
  declarations: [FontawesomeExtendComponent],
  imports: [CommonModule, SharedModule],
  exports: [FontawesomeExtendComponent],
  providers: [],
})
export class FontawesomeExtendModule {}
