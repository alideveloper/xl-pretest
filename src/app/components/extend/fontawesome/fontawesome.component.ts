import { Component } from '@angular/core';

import { GlobalService } from '@shared/services/global.service';

import { IconComponent } from '@components/icons/components/icon/icon.component';

@Component({
  selector: 'component-fontawesome-extend',
  templateUrl: './../../icons/components/icon/icon.component.html',
  styleUrls: ['./../../icons/components/icon/icon.component.scss'],
})
export class FontawesomeExtendComponent extends IconComponent {
  constructor(public global: GlobalService) {
    super(global);
  }
}
