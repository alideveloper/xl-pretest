import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SVGExtendComponent } from './svg.component';

import { SharedModule } from '@shared/shared.module';

@NgModule({
  declarations: [SVGExtendComponent],
  imports: [CommonModule, SharedModule],
  exports: [SVGExtendComponent],
  providers: [],
})
export class SVGExtendModule {}
