import { Component } from '@angular/core';

import { GlobalService } from '@shared/services/global.service';

import { SVGComponent } from '@components/svg/svg.component';

@Component({
  selector: 'component-svg-extend',
  templateUrl: './../../svg/svg.component.html',
  styleUrls: ['./../../svg/svg.component.scss'],
})
export class SVGExtendComponent extends SVGComponent {
  constructor(public global: GlobalService) {
    super(global);
  }
}
