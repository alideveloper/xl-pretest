import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ImageExtendComponent } from './image.component';

import { SharedModule } from '@shared/shared.module';

import { SVGExtendModule } from '@components/extend/svg/svg.module';
import { FontawesomeExtendModule } from '@components/extend/fontawesome/fontawesome.module';

@NgModule({
  declarations: [ImageExtendComponent],
  imports: [
    CommonModule,
    SharedModule,

    SVGExtendModule,
    FontawesomeExtendModule,
  ],
  exports: [ImageExtendComponent],
  providers: [],
})
export class ImageExtendModule {}
