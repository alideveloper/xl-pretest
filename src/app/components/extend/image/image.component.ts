import { Component } from '@angular/core';

import { GlobalService } from '@shared/services/global.service';

import { ImageComponent } from '@components/image/image.component';

@Component({
  selector: 'component-image-extend',
  templateUrl: './../../image/image.component.html',
  styleUrls: ['./../../image/image.component.scss'],
})
export class ImageExtendComponent extends ImageComponent {
  constructor(public global: GlobalService) {
    super(global);
  }
}
