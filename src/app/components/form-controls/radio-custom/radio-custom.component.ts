import {
  Component,
  OnInit,
  Input,
  DoCheck,
  Output,
  EventEmitter,
  ChangeDetectorRef,
} from '@angular/core';

import { Label } from '@app/shared/types/label';
import { State } from '@app/shared/types/state';
import { Validation } from '@app/shared/types/validation';

import { GlobalService } from '@app/shared/services/global.service';
import { GlobalValidation } from '@app/shared/services/global.validation';

import { ColorScheme } from '@app/core';

@Component({
  selector: 'component-radio-custom',
  templateUrl: './radio-custom.component.html',
  styleUrls: ['./radio-custom.component.scss'],
})
export class RadioCustomComponent implements OnInit, DoCheck {
  @Input() name: any;
  @Input() style: any;
  @Input() styleClass: string;
  @Input() state: State;
  @Input() validation: Validation;
  @Input() parentBinding: any;
  @Input() bindingObject: any;
  @Input() bindingData: any;
  @Input() required: boolean;
  @Input() defaultValue: any;
  @Input() formText: any;

  @Input() schema: any;
  @Input() value: any;

  valueRadio: any;

  validationInput = false;

  @Output() callback: any = new EventEmitter();
  @Output() callbackAction: any = new EventEmitter();

  ColorScheme = ColorScheme;

  constructor(
    public global: GlobalService,
    public globalValidation: GlobalValidation,
    private cdRef: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    this.initData();
  }

  ngDoCheck(): void {
    this.cdRef.detectChanges();

    if (this.required) {
      const validation = {
        required: this.required,
        ...this.validation,
      };

      const validationCheck = this.globalValidation.checkValidation(
        validation,
        this.value[this.parentBinding][this.bindingObject]
      );

      this.validationInput = !validationCheck.status;
      this.schema.validationInput = this.validationInput;
      if (this.validationInput) {
        this.formText.invalid.text = validationCheck.text;
      }
    }

    if (this.schema.state) {
      if (this.schema.state.disabled) {
        this.schema.state.disabled = this.global.disabledConditions(
          this.schema,
          this.value
        );
        this.cdRef.detectChanges();
      }

      this.schema.state.hide = this.global.showHideConditions(
        this.schema,
        this.value
      );
      this.cdRef.detectChanges();

      if (this.schema.state.hide) {
        this.validationInput = false;
        this.schema.validationInput = false;
      }
    }
  }

  initData(): void {
    if (this.schema) {
      this.name = this.schema.name;
      this.style = this.schema.style;
      this.styleClass = this.schema.styleClass;
      this.state = this.schema.state;
      this.required = this.schema.required;
      this.formText = this.schema.formText;
      this.validation = this.schema.validation;
      this.defaultValue = this.schema.defaultValue;
      this.bindingData = this.schema.bindingData;

      if (this.schema.bindingObject) {
        this.bindingObject = this.schema.bindingObject;
      }
      if (this.schema.parentBinding) {
        this.parentBinding = this.schema.parentBinding;
      }

      if (this.value) {
        if (!this.value[this.parentBinding]) {
          this.value[this.parentBinding] = {};
        }
        this.onCallback({
          [this.parentBinding]: this.value[this.parentBinding],
        });
      }

      if (this.defaultValue) {
        this.value[this.parentBinding][this.bindingObject] = this.value[
          this.bindingData
        ][0].value;
      }
    }
  }

  callbackRadio(event: any) {
    console.log(event.value);
    this.value[this.parentBinding][this.bindingObject] = event.value;
    console.log(event);
    console.log(this.value);
    this.cdRef.detectChanges();
  }

  onChange(event: any): void {
    // console.log(event);
    // console.log(this.value);
  }

  onCallback(event: any) {
    this.callback.emit(event);
  }

  onCallbackAction(event: any) {
    this.callbackAction.emit(event);
  }
}
