import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RadioCustomComponent } from './radio-custom.component';

import { SharedModule } from '@shared/shared.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
  declarations: [RadioCustomComponent],
  imports: [CommonModule, SharedModule, FontAwesomeModule],
  exports: [RadioCustomComponent],
  providers: [],
})
export class RadioCustomModule {}
