import {
  Component,
  OnInit,
  Input,
  forwardRef,
  HostBinding,
  Output,
  EventEmitter,
} from '@angular/core';
import { BaseControlValueAccessor } from '@app/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-toggle',
  templateUrl: './toggle.component.html',
  styleUrls: ['./toggle.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ToggleComponent),
      multi: true,
    },
  ],
})
export class ToggleComponent extends BaseControlValueAccessor<boolean>
  implements OnInit {
  /**
   * The checked state of the checkbox
   */
  @Input()
  get checked() {
    return this._valueComponent;
  }
  set checked(value: boolean) {
    this._valueComponent = value;
  }

  @Input()
  id: string;

  @Input()
  theme: 'light' | 'ios' = 'light';

  // tslint:disable-next-line: no-output-on-prefix
  @Output() onChanged: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor() {
    super();
  }

  ngOnInit() {}

  toggle() {
    this._valueComponent = !this._valueComponent;
    this.onChanged.emit(this._valueComponent);
    this.onChange(this._valueComponent);
  }
}
