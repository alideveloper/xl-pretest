import {
  Component,
  OnInit,
  Input,
  forwardRef,
  ChangeDetectorRef,
  AfterViewInit,
  Output,
  EventEmitter,
} from '@angular/core';
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import { BaseRadio } from '@components/form-controls/radio/base-radio';
import { ColorScheme } from '@app/core';

@Component({
  selector: 'app-radio-card',
  templateUrl: './radio-card.component.html',
  styleUrls: ['./radio-card.component.scss'],
  providers: [
    {
      provide: BaseRadio,
      useExisting: forwardRef(() => RadioCardComponent),
    },
  ],
})
export class RadioCardComponent extends BaseRadio implements OnInit {
  check = faCheck;

  @Input() checked: any;

  @Input()
  showMark: boolean;

  @Input() value: any;

  @Input()
  theme: ColorScheme = ColorScheme.Primary;

  @Output() callbackAction: any = new EventEmitter();

  constructor() {
    super();
  }

  hostClasses() {
    const classes = super.hostClasses();

    return [...classes, `radio-${this.theme}`];
  }

  ngOnInit() {}

  callback(event: any, value: any) {
    console.log(event, value);
    this.toggle(event);
    this.callbackAction.emit(value);
  }
}
