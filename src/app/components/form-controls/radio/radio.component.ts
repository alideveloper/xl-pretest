import {
  Component,
  OnInit,
  Input,
  DoCheck,
  Output,
  EventEmitter,
  ChangeDetectorRef,
  forwardRef,
} from '@angular/core';

import { Radio, FormText } from '@app/shared/types/radio';
import { Label } from '@app/shared/types/label';
import { State } from '@app/shared/types/state';
import { Validation } from '@app/shared/types/validation';

import { GlobalService } from '@app/shared/services/global.service';
import { GlobalValidation } from '@app/shared/services/global.validation';

import { BaseRadio } from './base-radio';

@Component({
  selector: 'app-radio',
  templateUrl: './radio.component.html',
  styleUrls: ['./radio.component.scss'],
  providers: [
    {
      provide: BaseRadio,
      useExisting: forwardRef(() => RadioComponent),
    },
  ],
})
export class RadioComponent extends BaseRadio implements OnInit, DoCheck {
  @Input() label: Label;
  @Input() labelSubtitle: Label;
  @Input() style: any;
  @Input() styleClass: string;
  @Input() shadow: boolean;
  @Input() state: State;
  @Input() data: any = [];
  @Input() disabled = false;
  @Input() readonly = false;
  @Input() required = false;
  @Input() name: string;
  @Input() formText: FormText;
  @Input() validation: Validation;
  @Input() bindingObject: any;
  @Input() parentBinding: any;
  @Input() defaultValue: any;

  @Input() schema: Radio;
  @Input() value: any;

  validationInput = false;

  @Output() callback: any = new EventEmitter();
  @Output() callbackAction: any = new EventEmitter();

  valueRadio: any;

  constructor(
    public global: GlobalService,
    public globalValidation: GlobalValidation,
    protected dcRef: ChangeDetectorRef
  ) {
    super();
  }

  ngOnInit(): void {
    this.initData();

    this.initDefaultData();
  }

  ngDoCheck(): void {
    if (
      this.schema &&
      this.schema.state &&
      this.schema.state.hasOwnProperty('hide') &&
      this.schema.state.hasOwnProperty('hideCondition') &&
      this.schema.state.hideCondition.length > 0
    ) {
      // this.schema.state.hide = this.global.showHideCondition(
      //   this.schema?.state?.hideCondition
      // );
    }

    if (this.required) {
      const validation = {
        required: this.required,
        ...this.validation,
      };

      if (
        this.value[this.parentBinding] &&
        this.value[this.parentBinding][this.bindingObject]
      ) {
        const validationCheck = this.globalValidation.checkValidation(
          validation,
          this.value[this.parentBinding][this.bindingObject]
        );

        this.validationInput = !validationCheck.status;
        this.schema.validationInput = this.validationInput;
        if (this.validationInput) {
          this.formText.invalid.text = validationCheck.text;
        }
      }
    }
  }

  initDefaultData(): void {
    if (this.data && this.data.length > 0 && this.required) {
      const findData = this.data.filter((item: any) => item.checked === true);

      if (findData && findData.length > 0) {
        this.validationInput = false;
        this.schema.validationInput = false;
      } else {
        this.validationInput = true;
        this.schema.validationInput = true;
      }
    }
  }

  initData(): void {
    if (this.schema) {
      this.label = this.schema.label;
      this.labelSubtitle = this.schema.labelSubtitle;
      this.style = this.schema.style;
      this.styleClass = this.schema.styleClass;
      this.state = this.schema.state;
      this.data = this.schema.data;
      this.disabled = this.schema.disabled;
      this.required = this.schema.required;
      this.readonly = this.schema.readonly;
      this.shadow = this.schema.shadow;
      this.name = this.schema.name;
      this.formText = this.schema.formText;
      this.validation = this.schema.validation;
      this.defaultValue = this.schema.defaultValue;

      if (this.schema.bindingObject) {
        this.bindingObject = this.schema.bindingObject;
      }
      if (this.schema.parentBinding) {
        this.parentBinding = this.schema.parentBinding;
      }

      if (this.defaultValue && this.data && this.data.length > 0) {
        this.value[this.parentBinding][this.bindingObject] = this.data[0].value;
      }

      if (this.value) {
        if (this.value[this.parentBinding][this.bindingObject]) {
          this.onValidationRadio(
            this.value[this.parentBinding][this.bindingObject]
          );
        }
        if (!this.value[this.parentBinding]) {
          this.value[this.parentBinding] = {};
        }
        this.onCallback({
          [this.parentBinding]: this.value[this.parentBinding],
        });
      }
    }
  }

  onChange(event: any): void {
    this.onValidationRadio(event);
  }

  onValidationRadio(event: any) {
    if (
      (this.required && event === undefined) ||
      (this.required && event === null)
    ) {
      this.validationInput = true;
      this.schema.validationInput = true;
      return;
    }
    this.validationInput = false;
    this.schema.validationInput = false;
    // this.onCallback(event);
  }

  onCallback(event: any) {
    this.callback.emit(event);
  }

  onCallbackAction(event: any) {
    this.callbackAction.emit(event);
  }
}
