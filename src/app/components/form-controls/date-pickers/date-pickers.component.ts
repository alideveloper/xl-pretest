import {
  Component,
  OnInit,
  Input,
  DoCheck,
  Output,
  EventEmitter,
  ChangeDetectorRef,
} from '@angular/core';

import { DatePicker, Append, FormText } from '@app/shared/types/datepicker';
import { Label } from '@app/shared/types/label';
import { State } from '@app/shared/types/state';
import { Validation } from '@app/shared/types/validation';

import { GlobalService } from '@app/shared/services/global.service';
import { GlobalValidation } from '@app/shared/services/global.validation';

import { FormBuilder, FormGroup } from '@angular/forms';
import { faCalendarAlt } from '@fortawesome/free-regular-svg-icons';
import {
  BsDatepickerConfig,
  DatepickerDateCustomClasses,
} from 'ngx-bootstrap/datepicker';
import { ThemeColor } from '@app/core';

const moment = require('moment');
moment();

@Component({
  selector: 'app-date-pickers',
  templateUrl: './date-pickers.component.html',
  styleUrls: ['./date-pickers.component.scss'],
})
export class DatePickersComponent implements OnInit, DoCheck {
  today = new Date();

  color: ThemeColor;

  @Input() label: Label;
  @Input() labelSubtitle: Label;
  @Input() style: any;
  @Input() pipe: any;
  @Input() styleClass: string;
  @Input() styleClassDate: string;
  @Input() state: State;
  @Input() type = '';
  @Input() readonly = false;
  @Input() required = false;
  @Input() roundedInput = false;
  @Input() placeholder: string;
  @Input() append: Append;
  @Input() formText: FormText;
  @Input() validation: Validation;
  @Input() bindingObject: any;
  @Input() bindingObjectRange: any;
  @Input() parentBinding: any;
  @Input() disabledDates: Date[];
  @Input() defaultValue: any;
  @Input() isRange: boolean;
  @Input() minDate: any;
  @Input() maxDate: any;
  @Input() dateInputFormat: any;

  @Input() schema: DatePicker;
  @Input() value: any;
  @Input() values: any;

  validationInput = false;

  @Output() callback: any = new EventEmitter();
  @Output() callbackAction: any = new EventEmitter();

  dateValue: any;

  constructor(
    public global: GlobalService,
    public globalValidation: GlobalValidation,
    private cdRef: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    this.initData();
  }

  ngDoCheck(): void {
    this.cdRef.detectChanges();

    if (this.required) {
      const validation = {
        required: this.required,
        ...this.validation,
      };

      let validationCheck = this.globalValidation.checkValidation(
        validation,
        this.value[this.parentBinding][this.bindingObject]
      );

      if (validationCheck.status && this.pipe && this.pipe.length > 0) {
        validationCheck = this.globalValidation.checkPipeValidation(
          this.pipe,
          this.value[this.parentBinding][this.bindingObject],
          this.value
        );
      }

      this.validationInput = !validationCheck.status;
      this.schema.validationInput = this.validationInput;
      if (this.validationInput) {
        this.formText.invalid.text = validationCheck.text;
      }
    }

    if (this.schema.state) {
      if (this.schema.state.disabled) {
        this.schema.state.disabled = this.global.disabledConditions(
          this.schema,
          this.value
        );
        this.cdRef.detectChanges();
      }

      this.schema.state.hide = this.global.showHideConditions(
        this.schema,
        this.value
      );
      this.cdRef.detectChanges();

      if (this.schema.state.hide) {
        this.validationInput = false;
        this.schema.validationInput = false;
      }
    }
  }

  initData(): void {
    if (this.schema) {
      this.label = this.schema.label;
      this.labelSubtitle = this.schema.labelSubtitle;
      this.pipe = this.schema.pipe;
      this.style = this.schema.style;
      this.styleClass = this.schema.styleClass;
      this.styleClassDate = this.schema.styleClassDate;
      this.state = this.schema.state;
      this.type = this.schema.type;
      this.placeholder = this.schema.placeholder;
      this.required = this.schema.required;
      this.readonly = this.schema.readonly;
      this.roundedInput = this.schema.roundedInput;
      this.append = this.schema.append;
      this.formText = this.schema.formText;
      this.validation = this.schema.validation;
      this.defaultValue = this.schema.defaultValue;
      this.isRange = this.schema.isRange;
      if (this.schema.minDate) this.minDate = this.schema.minDate;
      if (this.schema.maxDate) this.maxDate = this.schema.maxDate;
      if (this.schema.disabledDates) {
        this.disabledDates = this.schema.disabledDates;
      }
      this.dateInputFormat = this.schema.dateInputFormat;

      if (this.schema.bindingObject) {
        this.bindingObject = this.schema.bindingObject;
      }
      if (this.schema.bindingObjectRange) {
        this.bindingObjectRange = this.schema.bindingObjectRange;
      }
      if (this.schema.parentBinding) {
        this.parentBinding = this.schema.parentBinding;
      }

      if (this.minDate === 'dateNow') {
        this.minDate = this.today;
      }

      if (this.defaultValue) {
        if (this.isRange) {
          this.dateValue = [this.today, this.today];
          this.value[this.parentBinding][this.bindingObject] = this.today;
          this.value[this.parentBinding][this.bindingObjectRange] = this.today;
        } else {
          this.dateValue = this.today;
          this.value[this.parentBinding][this.bindingObject] = this.today;
        }
      } else {
        if (this.value[this.parentBinding][this.bindingObject]) {
          if (this.value[this.parentBinding][this.bindingObject]?.seconds) {
            this.dateValue = new Date(
              this.value[this.parentBinding][this.bindingObject].seconds * 1000
            );
          }
        }
      }

      if (this.value) {
        if (!this.value[this.parentBinding]) {
          this.value[this.parentBinding] = {};
        }
        this.onCallback({
          [this.parentBinding]: this.value[this.parentBinding],
        });
      }
    }
  }

  onChange(event: any): void {
    if (this.isRange) {
      this.value[this.parentBinding][this.bindingObject] = this.dateValue[0];
      this.value[this.parentBinding][
        this.bindingObjectRange
      ] = this.dateValue[1];
    } else {
      this.value[this.parentBinding][this.bindingObject] = this.dateValue;
    }
  }

  onCallback(event: any) {
    this.callback.emit(event);
  }

  onCallbackAction(event: any) {
    this.callbackAction.emit(event);
  }
}
