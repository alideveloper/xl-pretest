import { Component, OnInit } from '@angular/core';
import { I18nService } from '@app/core';
import { faChevronDown } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-button-language-selector',
  templateUrl: './button-language-selector.component.html',
  styleUrls: ['./button-language-selector.component.scss'],
})
export class ButtonLanguageSelectorComponent implements OnInit {
  chevronDown = faChevronDown;

  constructor(private i18nService: I18nService) {}

  ngOnInit() {}

  setLanguage(language: any) {
    this.i18nService.language = language.value;
  }

  get currentLanguage(): string {
    return this.i18nService.language;
  }

  get languages(): any[] {
    return this.i18nService.supportedLanguages;
  }
}
