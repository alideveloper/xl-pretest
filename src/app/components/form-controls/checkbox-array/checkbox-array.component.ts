import {
  Component,
  OnInit,
  DoCheck,
  Input,
  forwardRef,
  HostBinding,
  Output,
  EventEmitter,
  ChangeDetectorRef,
} from '@angular/core';
import { BaseControlValueAccessor, Logger } from '@app/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';

import { Checkbox, FormText } from '@app/shared/types/checkbox';
import { Label } from '@app/shared/types/label';
import { State } from '@app/shared/types/state';
import { Validation } from '@app/shared/types/validation';

import { GlobalService } from '@app/shared/services/global.service';
import { GlobalValidation } from '@app/shared/services/global.validation';

const logger = new Logger('CheckboxArrayComponent');

@Component({
  selector: 'app-checkbox-array',
  templateUrl: './checkbox-array.component.html',
  styleUrls: ['./checkbox-array.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CheckboxArrayComponent),
      multi: true,
    },
  ],
})
export class CheckboxArrayComponent extends BaseControlValueAccessor<boolean>
  implements OnInit, DoCheck {
  @Input()
  get checked() {
    return this._valueComponent;
  }
  set checked(value: boolean) {
    this._valueComponent = value;
  }

  @Input() label: Label;
  @Input() labelSubtitle: Label;
  @Input() result: any;
  @Input() style: any;
  @Input() styleClass: string;
  @Input() shadow: boolean;
  @Input() state: State;
  @Input() data: any = [];
  @Input() dataAPI: any;
  @Input() json = false;
  @Input() disabled = false;
  @Input() readonly = false;
  @Input() required = false;
  @Input() bindingData: any;
  @Input() name: string;
  @Input() formText: FormText;
  @Input() validation: Validation;
  @Input() bindingObject: any;
  @Input() parentBinding: any;
  @Input() defaultValue: any;
  @Input() currentId: any;
  @HostBinding('class.form-check-inline')
  @Input()
  inline: boolean;

  @Input() schema: Checkbox;
  @Input() value: any;

  validationInput: boolean;

  @Output() callback: any = new EventEmitter();
  @Output() callbackAction: any = new EventEmitter();
  // tslint:disable-next-line: no-output-on-prefix
  @Output() onChanged: EventEmitter<boolean> = new EventEmitter<boolean>();

  valueCheckbox: any = [];

  flag: any = {
    load: false,
  };

  constructor(
    public global: GlobalService,
    public globalValidation: GlobalValidation,
    protected dcRef: ChangeDetectorRef
  ) {
    super('checkbox');
  }

  ngOnInit(): void {
    this.initData();

    this.initDefaultData();
  }

  ngDoCheck(): void {
    if (
      this.schema &&
      this.schema.state &&
      this.schema.state.hasOwnProperty('hide') &&
      this.schema.state.hasOwnProperty('hideCondition') &&
      this.schema.state.hideCondition.length > 0
    ) {
      // this.schema.state.hide = this.global.showHideCondition(
      //   this.schema?.state?.hideCondition
      // );
    }

    if (this.bindingData && !this.flag.load) {
      this.data = Object.assign([], this.value[this.bindingData]);
      console.log('checkbox', this.data);
      console.log(this.value);
      console.log(this.bindingData);
      if (this.dataAPI && this.data && this.data.length > 0) {
        let index = 0;
        const dataAPI = Object.assign({}, this.dataAPI);
        console.log('checkbox2', this.data);
        this.data.map((itemData: any) => {
          index++;
          if (dataAPI.id) {
            if (dataAPI.id === 'index') {
              itemData.id = 'index-' + index;
            }
          }
          if (dataAPI.value) itemData.value = itemData[dataAPI.value];
          if (dataAPI.checked) itemData.checked = dataAPI.checked;
          if (this.value[this.parentBinding][this.bindingObject]) {
            if (this.result && this.result.type === 'data') {
              const find = this.value[this.parentBinding][
                this.bindingObject
              ].findIndex((item: any) => {
                if (item === itemData[this.result.key]) {
                  return item;
                }
              });

              if (find !== -1) {
                itemData.checked = true;
              }
            }
          }
          if (dataAPI.label) itemData.label = dataAPI.label;
          if (dataAPI.text) itemData.text = itemData[dataAPI.text];
          if (dataAPI.name) itemData.name = dataAPI.name;
        });

        if (this.defaultValue && this.data && this.data.length > 0) {
          this.data[0].checked = true;
          this.valueCheckbox.push(this.data[0]);
        } else {
          this.valueCheckbox = this.data.filter((item: any) => item.checked);
          this.onValidationCheckbox();
        }
        this.flag.load = true;
      }
    }
  }

  initDefaultData(): void {
    if (this.data && this.data.length > 0 && this.required) {
      const findData = this.data.filter((item: any) => item.checked === true);

      if (findData && findData.length > 0) {
        this.validationInput = false;
        this.schema.validationInput = false;
      } else {
        this.validationInput = true;
        this.schema.validationInput = true;
      }
    }
  }

  initData(): void {
    if (this.schema) {
      this.label = this.schema.label;
      this.labelSubtitle = this.schema.labelSubtitle;
      this.style = this.schema.style;
      this.styleClass = this.schema.styleClass;
      this.state = this.schema.state;
      this.bindingData = this.schema.bindingData;
      this.dataAPI = this.schema.dataAPI;
      this.data = this.schema.data;
      this.result = this.schema.result;
      this.disabled = this.schema.disabled;
      this.required = this.schema.required;
      this.json = this.schema.json;
      this.readonly = this.schema.readonly;
      this.shadow = this.schema.shadow;
      this.name = this.schema.name;
      this.formText = this.schema.formText;
      this.validation = this.schema.validation;
      this.defaultValue = this.schema.defaultValue;
      this.currentId = this.schema.currentId;

      if (this.schema.bindingObject) {
        this.bindingObject = this.schema.bindingObject;
      }
      if (this.schema.parentBinding) {
        this.parentBinding = this.schema.parentBinding;
      }

      if (this.value) {
        if (this.value[this.parentBinding][this.bindingObject]) {
          if (this.json) {
            try {
            } catch (error) {}
          } else {
          }
        }
        if (this.value[this.parentBinding][this.bindingObject]) {
          this.onValidationCheckbox(
            this.value[this.parentBinding][this.bindingObject]
          );
        }
      }
    }
  }

  onChangeModel(event: any, item: any): void {
    const value = event.target.checked;
    const type = value ? 'push' : 'remove';
    let findIndex;
    if (!value) {
      findIndex = this.valueCheckbox.findIndex(
        (itemFilter: any) => itemFilter.id === item.id
      );
    }
    this.onChangeCheckbox(item, type, findIndex);
    this.onValidationCheckbox(event);
  }

  onChangeCheckbox(item: any, type: any, index: any): void {
    if (type === 'push') {
      this.valueCheckbox.push(item);
    } else {
      this.valueCheckbox.splice(index, 1);
    }

    if (this.result && this.result.type === 'data') {
      const resultData: any = [];
      if (this.valueCheckbox && this.valueCheckbox.length > 0) {
        this.valueCheckbox.forEach((itemSub: any) => {
          resultData.push(itemSub[this.result.key]);
        });
      }

      if (this.json) {
        try {
          this.value[this.parentBinding][this.bindingObject] = JSON.stringify(
            resultData
          );
        } catch (error) {}
      } else {
        this.value[this.parentBinding][this.bindingObject] = Object.assign(
          [],
          resultData
        );
      }
      this.dcRef.detectChanges();
    }
  }

  onValidationCheckbox(event?: any) {
    if (
      (this.required && this.valueCheckbox.length === 0) ||
      (this.required && !this.valueCheckbox)
    ) {
      this.validationInput = true;
      this.schema.validationInput = true;
      return;
    }
    this.validationInput = false;
    this.schema.validationInput = false;
    // this.onCallback(event);
  }

  onCallback(event: any) {
    this.callback.emit(event);
  }

  onCallbackAction(event: any) {
    this.callbackAction.emit(event);
  }

  toggle() {
    this.checked = !this.checked;
    this.onChanged.emit(this.checked);
    // this.onChangeModel(this.checked);
  }
}
