import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';

import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { IconsModule } from '@components/icons/icons.module';
import { DatePickersModule } from '@app/components/date-pickers/date-pickers.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgOptionHighlightModule } from '@ng-select/ng-option-highlight';

import { CheckboxComponent } from './checkbox/checkbox.component';
import { CheckboxArrayComponent } from './checkbox-array/checkbox-array.component';
import { ButtonComponent } from './button/button.component';
import { ButtonLanguageSelectorComponent } from './button-language-selector/button-language-selector.component';

import { ToggleComponent } from './toggle/toggle.component';
import { RadioCardComponent } from './radio-card/radio-card.component';
import { DatePickersComponent } from './date-pickers/date-pickers.component';
import { CardsModule } from '../cards/cards.module';
import { RadioComponent } from './radio/radio.component';
import { RadioGroupComponent } from './radio-group/radio-group.component';
import { RadioCustomComponent } from './radio-custom/radio-custom.component';

const exports = [
  CheckboxComponent,
  CheckboxArrayComponent,
  ButtonComponent,
  ButtonLanguageSelectorComponent,
  ToggleComponent,
  RadioCardComponent,
  DatePickersComponent,
  RadioComponent,
  RadioGroupComponent,
  RadioCustomComponent,
];

const exportsModule = [FormsModule, ReactiveFormsModule];

@NgModule({
  declarations: [...exports],
  imports: [
    CommonModule,
    IconsModule,
    DatePickersModule,
    BsDropdownModule,
    TranslateModule,
    CardsModule,
    NgSelectModule,
    NgOptionHighlightModule,
    ...exportsModule,
  ],
  exports: [...exports, ...exportsModule],
})
export class FormControlsModule {}
