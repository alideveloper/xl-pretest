import {
  Component,
  OnInit,
  Input,
  DoCheck,
  ViewEncapsulation,
  NgZone,
  ElementRef,
  ViewChild,
  Output,
  EventEmitter,
  AfterViewInit,
} from '@angular/core';
import { FormControl } from '@angular/forms';

import { Maps, Search, Callback, Address } from '@app/shared/types/maps';
import { Label } from '@app/shared/types/label';
import { State } from '@app/shared/types/state';

import { GlobalService } from '@app/shared/services/global.service';

import { MapsAPILoader } from '@agm/core';
declare let google: any;

import { MapsService } from '@app/shared/services/maps/maps.service';

@Component({
  selector: 'component-maps',
  templateUrl: './maps.component.html',
  styleUrls: ['./maps.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class MapsComponent implements OnInit, DoCheck, AfterViewInit {
  constructor(
    public global: GlobalService,
    private mapsService: MapsService,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone
  ) {}
  @ViewChild('searchLocationAutoComplete', { static: false })
  set searchLocationAutoComplete(elRef: ElementRef) {
    this.searchLocationAutoCompleteElRef = elRef;
    // if (this.searchLocationAutoCompleteElRef && this.searchLocationAutoCompleteElRef.nativeElement) {
    //   searchLocationAutoComplete = this.searchLocationAutoCompleteElRef.nativeElement;
    // }
  }
  @Input() label: Label;
  @Input() labelSubtitle: Label;
  @Input() zoom: number;
  @Input() minZoon: number;
  @Input() maxZoom: number;
  @Input() parentBinding: any;
  @Input() latitude: any;
  @Input() longitude: any;
  @Input() search: Search;
  @Input() address: Address;
  @Input() callbackMaps: Callback;
  @Input() style: any;
  @Input() styleClass: string;
  @Input() state: State;
  @Input() required: boolean;
  @Input() isView = false;

  @Input() schema: Maps;
  @Input() value: any;
  @Input() values: any;

  public searchLocation: FormControl;
  @Output() callback: any = new EventEmitter();
  @Output() callbackAction: any = new EventEmitter();

  @Output() callbackActionStatic: any = new EventEmitter();

  defaultZoom: number;
  private geoCoder: any;

  private searchLocationAutoCompleteElRef: ElementRef;

  ngOnInit(): void {
    this.initData();

    this.searchLocation = new FormControl();
  }

  ngAfterViewInit(): void {
    if (this.schema) {
      this.search = this.schema.search;
    }
    setTimeout(() => {
      if (this.search && this.search.active) {
      }
      this.searchAutoComplete();
    });
  }

  ngDoCheck(): void {
    if (
      this.schema &&
      this.schema.state &&
      this.schema.state.hasOwnProperty('hide') &&
      this.schema.state.hasOwnProperty('hideCondition') &&
      this.schema.state.hideCondition.length > 0
    ) {
      // this.schema.state.hide = this.global.showHideCondition(
      //   this.schema?.state?.hideCondition
      // );
    }
  }

  initData(): void {
    if (this.schema) {
      this.label = this.schema.label;
      this.labelSubtitle = this.schema.labelSubtitle;
      this.zoom = this.schema.zoom;
      if (this.zoom) this.defaultZoom = this.zoom;
      this.maxZoom = this.schema.maxZoom;
      this.minZoon = this.schema.minZoom;
      this.latitude = this.schema.latitude;
      this.longitude = this.schema.longitude;
      this.parentBinding = this.schema.parentBinding;
      this.isView = this.schema.isView;
      this.address = this.schema.address;
      this.required = this.schema.required;

      if (this.parentBinding && this.latitude && this.longitude) {
        if (!this.value[this.parentBinding][this.latitude]) {
        }
      }

      this.search = this.schema.search;
      this.callbackMaps = this.schema.callbackMaps;
      this.style = this.schema.style;
      this.styleClass = this.schema.styleClass;
      this.state = this.schema.state;
    }
  }

  searchAutoComplete(): any {
    this.mapsAPILoader.load().then(() => {
      this.getLocation();
      this.geoCoder = new google.maps.Geocoder();
      if (this.search && this.search.active) {
        const autocomplete = new google.maps.places.Autocomplete(
          this.searchLocationAutoCompleteElRef.nativeElement,
          {
            types: ['address'],
          }
        );

        autocomplete.addListener('place_changed', () => {
          this.ngZone.run(() => {
            const place = autocomplete.getPlace();

            if (place.geometry === undefined || place.geometry === null) {
              return;
            }

            if (this.callbackMaps) {
              if (this.callbackMaps.latitude) {
                this.value[this.parentBinding][
                  this.callbackMaps.latitude
                ] = place.geometry.location.lat();
              }
              if (this.callbackMaps.longitude) {
                this.value[this.parentBinding][
                  this.callbackMaps.longitude
                ] = place.geometry.location.lng();
              }

              this.zoom = this.defaultZoom;
              this.getAddress(
                place.geometry.location.lat(),
                place.geometry.location.lng()
              );
            }
          });
        });
      }
    });
  }

  getLocation(): void {
    this.mapsService.getLocation().then((result) => {
      console.log('result', result);
      console.log(this.value[this.parentBinding]);
      if (
        this.value[this.parentBinding] &&
        !this.value[this.parentBinding][this.callbackMaps.latitude]
      ) {
        if (this.callbackMaps.latitude) {
          this.value[this.parentBinding][this.callbackMaps.latitude] =
            result.latitude;
        }
        if (this.callbackMaps.longitude) {
          this.value[this.parentBinding][this.callbackMaps.longitude] =
            result.longitude;
        }
        if (this.callbackMaps.accuracy) {
          this.value[this.parentBinding][this.callbackMaps.accuracy] =
            result.accuracy;
        }
        if (this.callbackMaps.altitude) {
          this.value[this.parentBinding][this.callbackMaps.altitude] =
            result.altitude;
        }
        if (this.callbackMaps.heading) {
          this.value[this.parentBinding][this.callbackMaps.heading] =
            result.heading;
        }
        if (this.callbackMaps.speed) {
          this.value[this.parentBinding][this.callbackMaps.speed] =
            result.speed;
        }
        if (this.callbackMaps.altitudeAccuracy) {
          this.value[this.parentBinding][this.callbackMaps.altitudeAccuracy] =
            result.altitudeAccuracy;
        }

        this.getAddress(result.latitude, result.longitude);
      }
    });
  }

  mapClick(event: any): void {
    if (!this.isView) {
      const coords = this.mapsService.mapClick(event);
      if (this.callbackMaps.latitude) {
        this.value[this.parentBinding][this.callbackMaps.latitude] =
          coords.latitude;
      }
      if (this.callbackMaps.longitude) {
        this.value[this.parentBinding][this.callbackMaps.longitude] =
          coords.longitude;
      }

      this.getAddress(coords.latitude, coords.longitude);
    }
  }

  getAddress(latitude: any, longitude: any) {
    if (!this.isView) {
      if (this.address && this.address.active) {
        this.geoCoder.geocode(
          { location: { lat: latitude, lng: longitude } },
          (results: any, status: any) => {
            if (status === 'OK') {
              if (results[0]) {
                this.zoom = this.defaultZoom;
                if (this.address.bindingObject) {
                  this.value[this.parentBinding][this.address.bindingObject] =
                    results[0].formatted_address;
                  this.callbackActionStatic.emit(
                    this.value[this.parentBinding]
                  );
                }
              } else {
                window.alert('No results found');
              }
            } else {
              window.alert('Geocoder failed due to: ' + status);
            }
          }
        );
      }
    }
  }
}
