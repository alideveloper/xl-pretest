import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MapsComponent } from './maps.component';

import { SharedModule } from '@shared/shared.module';

@NgModule({
  declarations: [MapsComponent],
  imports: [CommonModule, SharedModule],
  exports: [MapsComponent],
  providers: [],
})
export class MapsModule {}
