import { Input, Directive } from '@angular/core';
import { AvatarListItem, AvatarSize } from '@components/avatars/models/avatar';

@Directive({})
// tslint:disable-next-line: directive-class-suffix
export class AvatarList {
  @Input()
  avatars: AvatarListItem[];

  @Input()
  size: AvatarSize;

  @Input()
  display: number;

  @Input()
  showTooltip: any = true;

  get displayItems(): number {
    return this.display ? this.display : this.avatars.length;
  }

  get remainingItems(): number {
    return this.display ? this.avatars.length - this.display : 0;
  }
}
