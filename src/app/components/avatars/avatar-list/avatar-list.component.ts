import { Component, OnInit } from '@angular/core';
import { AvatarList } from '@components/avatars/base/avatar-list';

@Component({
  selector: 'app-avatar-list',
  templateUrl: './avatar-list.component.html',
  styleUrls: ['./avatar-list.component.scss'],
})
export class AvatarListComponent extends AvatarList implements OnInit {
  constructor() {
    super();
  }

  ngOnInit(): void {}
}
