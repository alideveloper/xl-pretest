import { Component, OnInit } from '@angular/core';
import { CdkStepper } from '@angular/cdk/stepper';
import { BaseWizard } from '@components/wizards/base-wizard';

@Component({
  selector: 'app-wizard-circled',
  templateUrl: './wizard-circled.component.html',
  styleUrls: ['./wizard-circled.component.scss'],
  providers: [
    { provide: BaseWizard, useExisting: WizardCircledComponent },
    { provide: CdkStepper, useExisting: WizardCircledComponent },
  ],
})
export class WizardCircledComponent extends BaseWizard implements OnInit {
  ngOnInit() {}
}
