import { Component, OnInit } from '@angular/core';
import { BaseWizardNavigation } from '@components/wizards/base-wizard-navigation';

@Component({
  selector: 'app-wizard-navigation-prev-next',
  templateUrl: './wizard-navigation-prev-next.component.html',
  styleUrls: ['./wizard-navigation-prev-next.component.scss'],
})
export class WizardNavigationPrevNextComponent extends BaseWizardNavigation
  implements OnInit {
  constructor() {
    super();
  }

  ngOnInit() {}
}
