import { Component, OnInit, Input } from '@angular/core';
import { BaseWizardNavigation } from '@components/wizards/base-wizard-navigation';
import {
  faChevronLeft,
  faChevronRight,
} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-wizard-navigation-bullet',
  templateUrl: './wizard-navigation-bullet.component.html',
  styleUrls: ['./wizard-navigation-bullet.component.scss'],
})
export class WizardNavigationBulletComponent extends BaseWizardNavigation
  implements OnInit {
  arrowLeft = faChevronLeft;
  arrowRight = faChevronRight;

  @Input()
  displayStepsNumber: any = true;

  @Input()
  steps: any[];

  constructor() {
    super();
  }

  ngOnInit() {}
}
