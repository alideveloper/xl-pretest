import { Directive, Input } from '@angular/core';
import { CdkStepper } from '@angular/cdk/stepper';

export enum WizardNavigationStyle {
  PrevNext,
  Bullet,
}

@Directive({
  // tslint:disable-next-line: directive-selector
  selector: '[prxWizard]',
  providers: [{ provide: CdkStepper, useExisting: BaseWizard }],
})
// tslint:disable-next-line: directive-class-suffix
export class BaseWizard extends CdkStepper {
  @Input()
  doneUrl: string;

  @Input()
  doneText: string;

  @Input()
  navigation: WizardNavigationStyle = WizardNavigationStyle.PrevNext;
}
