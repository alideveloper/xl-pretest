import {
  Component,
  forwardRef,
  SkipSelf,
  Optional,
  Inject,
  OnInit,
} from '@angular/core';
import {
  CdkStep,
  StepperOptions,
  STEPPER_GLOBAL_OPTIONS,
} from '@angular/cdk/stepper';
import { BaseWizard } from '@components/wizards/base-wizard';

@Component({
  selector: 'app-wizard-step',
  templateUrl: './wizard-step.component.html',
  styleUrls: ['./wizard-step.component.scss'],
  providers: [{ provide: CdkStep, useExisting: WizardStepComponent }],
})
export class WizardStepComponent extends CdkStep implements OnInit {
  constructor(
    @Inject(forwardRef(() => BaseWizard)) stepper: BaseWizard,
    @Optional() @Inject(STEPPER_GLOBAL_OPTIONS) stepperOptions?: StepperOptions
  ) {
    super(stepper, stepperOptions);
  }

  ngOnInit() {}
}
