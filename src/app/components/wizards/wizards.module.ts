import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { CdkStepperModule } from '@angular/cdk/stepper';
import { IconsModule } from '@components/icons/icons.module';
import { TranslateModule } from '@ngx-translate/core';
import { FormControlsModule } from '@components/form-controls/form-controls.module';

import { WizardNavigationComponent } from '@components/wizards/navigation/wizard-navigation/wizard-navigation.component';
import { BaseWizard } from '@components/wizards/base-wizard';

import { WizardStepComponent } from '@components/wizards/wizard-step/wizard-step.component';
import { WizardCircledComponent } from '@components/wizards/wizard-circled/wizard-circled.component';
import { WizardBulletedComponent } from '@components/wizards/wizard-bulleted/wizard-bulleted.component';
import { WizardNavigationBulletComponent } from '@components/wizards/navigation/wizard-navigation-bullet/wizard-navigation-bullet.component';
import { NavigationsModule } from '@components/navigations/navigations.module';
import { WizardNavigationPrevNextComponent } from '@components/wizards/navigation/wizard-navigation-prev-next/wizard-navigation-prev-next.component';
import { WizardTabbedComponent } from '@components/wizards/wizard-tabbed/wizard-tabbed.component';

const exports = [
  WizardCircledComponent,
  WizardBulletedComponent,
  WizardTabbedComponent,
  WizardStepComponent,
];

@NgModule({
  declarations: [
    BaseWizard,
    WizardNavigationComponent,
    WizardNavigationBulletComponent,
    WizardNavigationPrevNextComponent,
    ...exports,
  ],
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule,
    CdkStepperModule,
    IconsModule,
    NavigationsModule,
    FormControlsModule,
  ],
  exports: [...exports],
})
export class WizardsModule {}
