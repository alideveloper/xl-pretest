import {
  Component,
  OnInit,
  Input,
  ViewEncapsulation,
  HostBinding,
} from '@angular/core';

import { Link } from '@app/shared/types/link';

import { GlobalService } from '@app/shared/services/global.service';

@Component({
  selector: 'component-link',
  templateUrl: './link.component.html',
  styleUrls: ['./link.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class LinkComponent implements OnInit {
  @HostBinding('class') styleClassHost: string;

  @Input() type: any;
  @Input() bindingObject: any;
  @Input() style: any;
  @Input() styleClass: string;
  @Input() binding: boolean;
  @Input() target: any = '_blank';

  @Input() schema: Link;
  @Input() value: any;

  constructor(public global: GlobalService) {
    this.styleClassHost = 'width-full-p';
  }

  ngOnInit(): void {
    this.initData();
  }

  initData(): void {
    if (this.schema) {
      this.type = this.schema.type;
      this.bindingObject = this.schema.bindingObject;
      this.style = this.schema.style;
      this.styleClass = this.schema.styleClass;
      this.binding = this.schema.binding;
      this.target = this.schema.target;
    }
  }
}
