import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AngularSvgIconModule } from 'angular-svg-icon';

import { SVGComponent } from './svg.component';

@NgModule({
  declarations: [SVGComponent],
  imports: [CommonModule, AngularSvgIconModule.forRoot()],
  exports: [AngularSvgIconModule, SVGComponent],
  providers: [],
})
export class SVGModule {}
