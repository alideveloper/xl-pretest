import { Component, OnInit, Input } from '@angular/core';

import { GlobalService } from '@shared/services/global.service';

import { SVG } from '@app/shared/types/svg';

@Component({
  selector: 'component-svg',
  templateUrl: './svg.component.html',
  styleUrls: ['./svg.component.scss'],
})
export class SVGComponent implements OnInit {
  @Input() src: string;
  @Input() style: string;
  @Input() styleClass: string;
  @Input() binding: boolean;

  @Input() schema: SVG;
  @Input() value: any;
  @Input() values: any;

  id: any;

  constructor(public global: GlobalService) {}

  ngOnInit(): void {
    this.initData();
    this.id = new Date().getTime();
  }

  initData(): void {
    if (this.schema) {
      this.style = this.schema.style;
      this.styleClass = this.schema.styleClass;
      this.binding = this.schema.binding;

      if (this.binding) {
        console.log('binding');
        console.log(this.value);
        console.log(this.schema);
        if (this.schema.srcParent) {
          if (
            this.value &&
            this.value[this.schema.srcParent][this.schema.src]
          ) {
            this.src = this.value[this.schema.srcParent][this.schema.src];
          }
        } else {
          if (this.value && this.value[this.schema.src]) {
            this.src = this.value[this.schema.src];
          }
        }
      } else {
        this.src = this.schema.src;
      }
      console.log(this.src);
    }
  }
}
