import { Component, OnInit, Input } from '@angular/core';

import { SVG } from '@app/shared/types/svg';
import { FontAwesome } from '@app/shared/types/fontawesome';
import { FirebaseStorage } from '@app/shared/types/firebase-storage';
import { Image, URL } from '@app/shared/types/image';

import { GlobalService } from '@shared/services/global.service';

@Component({
  selector: 'component-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.scss'],
})
export class ImageComponent implements OnInit {
  @Input() type: 'SVG' | 'image' | 'FontAwesome' | 'FirebaseStorage';
  @Input() image: SVG | FontAwesome | URL | FirebaseStorage | any;
  @Input() positionImage: 'left' | 'right';
  @Input() bindingObject: any;
  @Input() style: any;
  @Input() styleClass: string;
  @Input() binding: boolean;

  @Input() schema: Image;
  @Input() value: any;
  @Input() values: any;

  constructor(public global: GlobalService) {}

  ngOnInit(): void {
    this.initData();
  }

  initData(): void {
    if (this.schema) {
      this.type = this.schema.type;
      this.image = this.schema.image;
      this.bindingObject = this.schema.bindingObject;
      this.positionImage = this.schema.positionImage;
      this.style = this.schema.style;
      this.styleClass = this.schema.styleClass;
      this.binding = this.schema.binding;
    }
  }
}
