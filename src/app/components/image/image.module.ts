import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ImageComponent } from './image.component';

import { SharedModule } from '@shared/shared.module';

import { SVGModule } from '@components/svg/svg.module';
import { FontawesomeExtendModule } from '@components/extend/fontawesome/fontawesome.module';

@NgModule({
  declarations: [ImageComponent],
  imports: [CommonModule, SharedModule, SVGModule, FontawesomeExtendModule],
  exports: [ImageComponent],
  providers: [],
})
export class ImageModule {}
