import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InputComponent } from './input.component';

import { SharedModule } from '@shared/shared.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
  declarations: [InputComponent],
  imports: [CommonModule, SharedModule, FontAwesomeModule],
  exports: [InputComponent],
  providers: [],
})
export class InputModule {}
