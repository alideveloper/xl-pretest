import {
  Component,
  OnInit,
  Input,
  DoCheck,
  Output,
  EventEmitter,
  ChangeDetectorRef,
} from '@angular/core';

import { InputModel, Append, FormText } from '@app/shared/types/input';
import { Label } from '@app/shared/types/label';
import { State } from '@app/shared/types/state';
import { Validation } from '@app/shared/types/validation';

import { GlobalService } from '@app/shared/services/global.service';
import { GlobalValidation } from '@app/shared/services/global.validation';

@Component({
  selector: 'component-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
})
export class InputComponent implements OnInit, DoCheck {
  @Input() label: Label;
  @Input() labelSubtitle: Label;
  @Input() style: any;
  @Input() pipe: any;
  @Input() styleClass: string;
  @Input() state: State;
  @Input() type = 'text';
  @Input() readonly = false;
  @Input() required = false;
  @Input() isPrice = false;
  @Input() minLength = 0;
  @Input() maxLength = 0;
  @Input() roundedInput = false;
  @Input() placeholder: string;
  @Input() onKeyUpActive = false;
  @Input() onBlurActive = false;
  @Input() append: Append;
  @Input() formText: FormText;
  @Input() validation: Validation;
  @Input() bindingObject: any;
  @Input() parentBinding: any;
  @Input() defaultValue: any;
  @Input() search: boolean;

  @Input() schema: InputModel;
  @Input() value: any;

  validationInput = false;

  @Output() callback: any = new EventEmitter();
  @Output() callbackAction: any = new EventEmitter();

  constructor(
    public global: GlobalService,
    public globalValidation: GlobalValidation,
    private cdRef: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    this.initData();
  }

  ngDoCheck(): void {
    this.cdRef.detectChanges();

    if (this.required || this.minLength || this.maxLength) {
      const validation = {
        minLength: this.minLength,
        maxLength: this.maxLength,
        required: this.required,
        ...this.validation,
      };

      let validationCheck = this.globalValidation.checkValidation(
        validation,
        this.value[this.parentBinding][this.bindingObject]
      );

      if (validationCheck.status && this.pipe && this.pipe.length > 0) {
        validationCheck = this.globalValidation.checkPipeValidation(
          this.pipe,
          this.value[this.parentBinding][this.bindingObject],
          this.value
        );
      }

      this.validationInput = !validationCheck.status;
      this.schema.validationInput = this.validationInput;
      if (this.validationInput) {
        this.formText.invalid.text = validationCheck.text;
      }
    }

    if (this.state) {
      if (this.state.disabled) {
        this.state.disabled = this.global.disabledConditions(
          this.schema,
          this.value
        );
        this.cdRef.detectChanges();
      }

      this.state.hide = this.global.showHideConditions(this.schema, this.value);
      this.cdRef.detectChanges();

      if (this.state.hide) {
        this.validationInput = false;
        this.schema.validationInput = false;
      }
    }
  }

  initData(): void {
    if (this.schema) {
      this.label = this.schema.label;
      this.labelSubtitle = this.schema.labelSubtitle;
      this.search = this.schema.search;
      this.pipe = this.schema.pipe;
      this.style = this.schema.style;
      this.styleClass = this.schema.styleClass;
      this.state = this.schema.state;
      this.type = this.schema.type;
      this.placeholder = this.schema.placeholder;
      this.required = this.schema.required;
      this.readonly = this.schema.readonly;
      this.minLength = this.schema.minLength;
      this.maxLength = this.schema.maxLength;
      this.roundedInput = this.schema.roundedInput;
      this.onKeyUpActive = this.schema.onKeyUpActive;
      this.onBlurActive = this.schema.onBlurActive;
      this.append = this.schema.append;
      this.formText = this.schema.formText;
      this.validation = this.schema.validation;
      this.defaultValue = this.schema.defaultValue;
      this.isPrice = this.schema.isPrice;

      if (this.schema.bindingObject) {
        this.bindingObject = this.schema.bindingObject;
      }
      if (this.schema.parentBinding) {
        this.parentBinding = this.schema.parentBinding;
      }

      if (this.value) {
        if (!this.value[this.parentBinding]) {
          this.value[this.parentBinding] = {};
        }
        this.onCallback({
          [this.parentBinding]: this.value[this.parentBinding],
        });
      }
    }
  }

  onKeyUp(event: any): void {
    if (this.onKeyUpActive) {
    }
  }

  onBlur(event: any): void {
    if (this.onBlurActive) {
    }
  }

  onChange(event: any): void {
    if (this.search) {
      this.onCallback(event);
    }
  }

  onCallback(event: any) {
    this.callback.emit(event);
  }

  onCallbackAction(event: any) {
    this.callbackAction.emit(event);
  }
}
