import {
  Component,
  OnInit,
  Input,
  HostBinding,
  Output,
  EventEmitter,
} from '@angular/core';
import { ColorScheme, BaseComponent } from '@app/core';
import { AlertStyle } from '@components/alerts/models/alert-style.enum';

import { Alert } from '@app/shared/types/alert';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss'],
})
export class AlertComponent extends BaseComponent implements OnInit {
  AlertStyle = AlertStyle;

  @Input()
  scheme: ColorScheme;

  @Input()
  background: ColorScheme;

  @Input()
  style: AlertStyle;

  @Input()
  withIcon: boolean;

  @Input()
  dismissible: boolean;

  @Input()
  icon: any;

  @Input() schemas: any;
  @Input() schema: Alert;
  @Input() value: any;
  @Input() values: any;

  @Output() callback: any = new EventEmitter();
  @Output() callbackAction: any = new EventEmitter();

  get fullScheme(): string {
    return `${this.scheme} ${this.scheme === ColorScheme.Light ? 'border' : ''}
    ${this.style ? 'alert-' + this.style : ''}
    ${this.background ? 'bg-' + this.background : ''}`;
  }

  constructor() {
    super();
  }

  ngOnInit() {
    this.initData();
  }

  initData(): void {
    if (this.schema) {
      this.scheme = this.schema.scheme;
      this.background = this.schema.background;
      this.style = this.schema.style;
      this.withIcon = this.schema.withIcon;
      this.dismissible = this.schema.dismissible;
      this.icon = this.schema.icon;
    }
  }
}
