import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  HostListener,
  OnDestroy,
} from '@angular/core';

import {
  Select,
  CustomTemplate,
  Append,
  FormText,
} from '@app/shared/types/select';
import { Label } from '@app/shared/types/label';
import { State } from '@app/shared/types/state';
import { Validation } from '@app/shared/types/validation';

import { AppService } from '@app/app.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { environment } from '@environments/environment';
import { AppData } from '@app/app.data';
import { Loading } from '@app/shared/model/other/loading.model';
import { LoadingService } from '@app/shared/services/loading.service';

@Component({
  selector: 'component-select-business',
  templateUrl: './select-business.component.html',
  styleUrls: ['./select-business.component.scss'],
})
export class SelectBusinessComponent implements OnInit, OnDestroy {
  selectedDataAll: string[] = [];

  @Input() label: Label;
  @Input() labelSubtitle: Label;
  @Input() parentBinding: any;
  @Input() bindingData: any;
  @Input() bindingDataGlobal: any;
  @Input() bindingObject: any;
  @Input() bindingWhere: any;
  @Input() bindingWhereFrom: any;
  @Input() bindingSearch: any;
  @Input() businessKey: any;
  @Input() labelForId: any;
  @Input() style: any;
  @Input() styleClass: string;
  @Input() state: State;
  @Input() data: any = [];
  @Input() bindLabel: string;
  @Input() grouping: any;
  @Input() bindValue: string;
  @Input() searchable: boolean;
  @Input() required: boolean;
  @Input() multiple: boolean;
  @Input() hideSelected: boolean;
  @Input() maxSelectedItems: number;
  @Input() closeOnSelect = false;
  @Input() groupBy: string;
  @Input() selectableGroup: any;
  @Input() selectableGroupAsModel: any;
  @Input() defaultValue: boolean;
  @Input() customSearch: boolean;
  @Input() selectedAll: any;
  @Input() customTemplate: CustomTemplate;
  @Input() placeholder: string;
  @Input() append: Append;
  @Input() formText: FormText = {
    valid: {
      active: true,
      text: 'Data telah Sesuai!',
    },
    invalid: {
      active: true,
      text: 'Data tidak Sesuai!',
    },
  };
  @Input() validation: Validation;
  @Input() search: boolean;

  @Input() schema: Select;
  @Input() value: any;
  @Input() values: any;

  key: any;
  keyBinding: any;
  validationInput = false;

  flag = {
    binding: false,
    callbackData: false,
  };

  where: any;
  dataStorage: any;

  hideMobile: boolean;

  @Output() callback: any = new EventEmitter();
  @Output() callbackAction: any = new EventEmitter();

  private _unsubscribeAll: Subject<any>;

  constructor(
    private _appService: AppService,
    private _appData: AppData,
    private _loadingService: LoadingService
  ) {
    this._unsubscribeAll = new Subject();
  }

  ngOnInit(): void {
    this.checkWidthBrowser();

    const selectedBusinessValueStorage = localStorage.getItem(
      environment.domain + '-selectedBusinessValue'
    );
    if (selectedBusinessValueStorage) {
      this.value = selectedBusinessValueStorage;
    }

    this._appService.onDataChanges
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((data) => {
        console.log('data', data);
        if (data.business !== this.data) {
          this.data = Object.assign([], data.business);
        }

        console.log('this.data', this.data);
        if (!this.data || (this.data && this.data.length === 0)) {
          this.data = JSON.parse(
            localStorage.getItem(environment.domain + '-business')
          );
          this._appData.business = this.data;
          if (this._appData.business && this._appData.business.length > 0) {
            this._appService.onDataChanges.next(this._appData);
          }
          this.value = null;
        }
        console.log('this.data', this.data);

        if (!this.value) {
          if (this.data && this.data.length > 0) {
            this.value = this.data[0].key;
            localStorage.setItem(
              environment.domain + '-selectedBusinessValue',
              this.value
            );
            this._appData.selectedBusinessValue = this.value;
            if (this.data[0].businessName === 'Personal') {
              const user = JSON.parse(
                localStorage.getItem(environment.domain + '-Login')
              );
              console.log('user', user);
              localStorage.setItem(
                environment.domain + '-selectedBusiness',
                JSON.stringify(user)
              );
              this._appData.selectedBusiness = user;
            } else {
              const find = this.data.filter(
                (item: any) => item.key === this.value
              );
              localStorage.setItem(
                environment.domain + '-selectedBusiness',
                JSON.stringify(find[0])
              );
              this._appData.selectedBusiness = find[0];
            }
            if (
              this._appData.selectedBusiness &&
              this._appData.selectedBusinessValue
            ) {
              this._appService.onDataChanges.next(this._appData);
            }
          }
        }
      });
  }

  async ngOnDestroy() {
    try {
      this._unsubscribeAll.next();
      this._unsubscribeAll.complete();
    } catch (error) {}
  }

  @HostListener('window:resize', ['$event'])
  onResize(event: any) {
    console.log(window.innerWidth);
    this.checkWidthBrowser();
  }

  checkWidthBrowser() {
    if (window.innerWidth > 648) {
      this.hideMobile = true;
      return;
    }
    this.hideMobile = false;
  }

  onChange(event: any): void {
    this._loadingService.loading.isLoading = true;
    setTimeout(() => {
      this._loadingService.loading.isLoading = false;
    }, 1500);
    if (this.bindingWhereFrom) {
      this.value = null;
    }
    this.onValidationSelect(event);
  }

  onValidationSelect(event: any) {
    console.log(event);
    localStorage.setItem(environment.domain + '-selectedBusinessValue', event);
    this._appData.selectedBusinessValue = event;
    if (event === 'Personal') {
      const user = JSON.parse(
        localStorage.getItem(environment.domain + '-Login')
      );
      console.log('user', user);
      localStorage.setItem(
        environment.domain + '-selectedBusiness',
        JSON.stringify(user)
      );
      this._appData.selectedBusiness = user;
    } else {
      const find = this.data.filter((item: any) => item.key === event);
      localStorage.setItem(
        environment.domain + '-selectedBusiness',
        JSON.stringify(find[0])
      );
      this._appData.selectedBusiness = find[0];
    }
    this._appService.onDataChanges.next(this._appData);
    this.onCallback(event);

    if (this.required && !event) {
      this.validationInput = true;
      return;
    }
    this.validationInput = false;
  }

  onCallback(event: any) {
    this.callback.emit(event);
  }

  onCallbackAction(event: any) {
    this.callbackAction.emit(event);
  }
}
