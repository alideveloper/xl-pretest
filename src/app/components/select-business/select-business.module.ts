import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SelectBusinessComponent } from './select-business.component';

import { SharedModule } from '@app/shared/shared.module';
import { UtilsModule } from '@app/components/utils/utils.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgOptionHighlightModule } from '@ng-select/ng-option-highlight';

@NgModule({
  declarations: [SelectBusinessComponent],
  imports: [
    CommonModule,
    SharedModule,
    UtilsModule,
    NgSelectModule,
    NgOptionHighlightModule,
  ],
  exports: [SelectBusinessComponent],
  providers: [],
})
export class SelectBusinessModule {}
