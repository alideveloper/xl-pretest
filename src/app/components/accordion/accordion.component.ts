import {
  Component,
  OnInit,
  Input,
  DoCheck,
  ViewEncapsulation,
  ChangeDetectorRef,
} from '@angular/core';

import { GlobalService } from '@app/shared/services/global.service';

@Component({
  selector: 'component-accordion',
  templateUrl: './accordion.component.html',
  styleUrls: ['./accordion.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AccordionComponent implements OnInit, DoCheck {
  @Input() style: any;
  @Input() styleClass: string;
  @Input() styleContent: any;
  @Input() styleClassContent: string;
  @Input() bindingData: any;
  @Input() title: any;
  @Input() content: any;

  @Input() schema: any;
  @Input() value: any;

  constructor(public global: GlobalService, public cdRef: ChangeDetectorRef) {}

  ngOnInit(): void {
    this.initData();
  }

  ngDoCheck(): void {}

  initData(): void {
    if (this.schema) {
      this.styleContent = this.schema.styleContent;
      this.style = this.schema.style;
      this.styleClass = this.schema.styleClass;
      this.styleClassContent = this.schema.styleClassContent;
      this.bindingData = this.schema.bindingData;
      this.title = this.schema.title;
      this.content = this.schema.content;
    }
  }
}
