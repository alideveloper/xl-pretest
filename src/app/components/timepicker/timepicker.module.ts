import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TimepickerComponent } from './timepicker.component';

import { SharedModule } from '@shared/shared.module';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
  declarations: [TimepickerComponent],
  imports: [CommonModule, SharedModule, FontAwesomeModule, TimepickerModule],
  exports: [TimepickerComponent],
  providers: [],
})
export class TimepickersModule {}
