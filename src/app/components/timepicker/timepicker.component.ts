import {
  Component,
  OnInit,
  Input,
  DoCheck,
  Output,
  EventEmitter,
  ChangeDetectorRef,
} from '@angular/core';

import { Label } from '@app/shared/types/label';
import { State } from '@app/shared/types/state';
import { Validation } from '@app/shared/types/validation';

import { GlobalService } from '@app/shared/services/global.service';
import { GlobalValidation } from '@app/shared/services/global.validation';

@Component({
  selector: 'component-timepicker',
  templateUrl: './timepicker.component.html',
  styleUrls: ['./timepicker.component.scss'],
})
export class TimepickerComponent implements OnInit, DoCheck {
  @Input() label: Label;
  @Input() labelSubtitle: Label;
  @Input() style: any;
  @Input() styleClass: string;
  @Input() state: State;
  @Input() type = 'text';
  @Input() readonly = false;
  @Input() required = false;
  @Input() minLength = 0;
  @Input() maxLength = 0;
  @Input() placeholder: string;
  @Input() any = false;
  @Input() append: any;
  @Input() formText: any;
  @Input() validation: Validation;
  @Input() bindingObject: any;
  @Input() parentBinding: any;
  @Input() defaultValue: any;
  @Input() search: boolean;

  @Input() schema: any;
  @Input() value: any;

  validationInput = false;

  flag: any = {
    load: false,
  };

  @Output() callback: any = new EventEmitter();
  @Output() callbackAction: any = new EventEmitter();

  constructor(
    public global: GlobalService,
    public globalValidation: GlobalValidation,
    private cdRef: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    this.initData();
  }

  ngDoCheck(): void {
    this.cdRef.detectChanges();

    if (this.required || this.minLength || this.maxLength) {
      const validation = {
        minLength: this.minLength,
        maxLength: this.maxLength,
        required: this.required,
        ...this.validation,
      };

      const validationCheck = this.globalValidation.checkValidation(
        validation,
        this.value[this.parentBinding][this.bindingObject]
      );

      this.validationInput = !validationCheck.status;
      this.schema.validationInput = this.validationInput;
      if (this.validationInput) {
        this.formText.invalid.text = validationCheck.text;
      }
    }

    if (this.schema.state) {
      if (this.schema.state.disabled) {
        this.schema.state.disabled = this.global.disabledConditions(
          this.schema,
          this.value
        );
        this.cdRef.detectChanges();
      }

      this.schema.state.hide = this.global.showHideConditions(
        this.schema,
        this.value
      );
      this.cdRef.detectChanges();

      if (this.schema.state.hide) {
        this.validationInput = false;
        this.schema.validationInput = false;
      }
    }
  }

  initData(): void {
    if (this.schema) {
      this.label = this.schema.label;
      this.labelSubtitle = this.schema.labelSubtitle;
      this.search = this.schema.search;
      this.style = this.schema.style;
      this.styleClass = this.schema.styleClass;
      this.state = this.schema.state;
      this.type = this.schema.type;
      this.placeholder = this.schema.placeholder;
      this.required = this.schema.required;
      this.readonly = this.schema.readonly;
      this.append = this.schema.append;
      this.formText = this.schema.formText;
      this.validation = this.schema.validation;
      this.defaultValue = this.schema.defaultValue;

      if (this.schema.bindingObject) {
        this.bindingObject = this.schema.bindingObject;
      }
      if (this.schema.parentBinding) {
        this.parentBinding = this.schema.parentBinding;
      }

      if (this.value) {
        if (!this.value[this.parentBinding]) {
          this.value[this.parentBinding] = {};
        }
        this.onCallback({
          [this.parentBinding]: this.value[this.parentBinding],
        });
      }

      if (this.defaultValue) {
        this.flag.load = true;
        console.log('GA MASUK');
        this.value[this.parentBinding][this.bindingObject] = new Date();
      } else {
        if (this.value[this.parentBinding][this.bindingObject]) {
          console.log('MASUK');
          console.log(this.value);
          this.value[this.parentBinding][this.bindingObject] = new Date(
            this.value[this.parentBinding][this.bindingObject].seconds * 1000
          );
          console.log(
            this.value[this.parentBinding][this.bindingObject].getHours()
          );

          setTimeout(() => {
            this.flag.load = true;
          }, 1000);
        }
      }
    }
  }

  onChange(event: any): void {
    if (this.search) {
      this.onCallback(event);
    }
  }

  onCallback(event: any) {
    this.callback.emit(event);
  }

  onCallbackAction(event: any) {
    this.callbackAction.emit(event);
  }
}
