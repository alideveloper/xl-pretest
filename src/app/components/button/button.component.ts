import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  DoCheck,
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  AfterViewChecked,
  HostBinding,
} from '@angular/core';

import { Button } from '@app/shared/types/button';

import { Label } from '@app/shared/types/label';
import { Image } from '@app/shared/types/image';
import { State } from '@app/shared/types/state';
import { Action } from '@app/shared/types/action';

import { GlobalService } from '@app/shared/services/global.service';
import { GlobalValidation } from '@app/shared/services/global.validation';

@Component({
  selector: 'component-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ButtonComponent implements OnInit, DoCheck, AfterViewChecked {
  @HostBinding('class') styleClassHost: string;

  @Input() text: any;
  @Input() image: Image;
  @Input() style: any;
  @Input() styleClass: string;
  @Input() state: State;
  @Input() action: any;
  @Input() disabledByValidation: boolean;

  @Input() schemas: any;
  @Input() schema: Button;
  @Input() value: any;
  @Input() values: any;

  @Output() callback: any = new EventEmitter();
  @Output() callbackAction: any = new EventEmitter();

  flag = {
    load: false,
  };

  constructor(
    public global: GlobalService,
    public globalValidation: GlobalValidation,
    private cdRef: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    this.initData();
  }

  ngDoCheck(): void {
    this.cdRef.detectChanges();

    if (this.flag.load) {
      this.schema.state.disabled = this.global.disabledConditions(
        this.schema,
        this.value
      );
      this.cdRef.detectChanges();

      this.schema.state.hide = this.global.showHideConditions(
        this.schema,
        this.value
      );
      this.cdRef.detectChanges();

      if (this.disabledByValidation) {
        this.schema.state.disabled = this.globalValidation.checkValidationAll(
          this.schemas
        );
        this.cdRef.detectChanges();
      }
    }
  }

  ngAfterViewChecked(): void {
    this.flag.load = true;
  }

  initData(): void {
    if (this.schema) {
      this.text = this.schema.text;
      this.image = this.schema.image;
      this.disabledByValidation = this.schema.disabledByValidation;
      this.style = this.schema.style;
      this.styleClass = this.schema.styleClass;
      this.state = this.schema.state;
      this.action = this.schema.action;

      if (this.schema.styleClassHost) {
        this.styleClassHost = this.schema.styleClassHost;
      }
    }

    if (!this.schema) {
      this.schema = {};
      this.schema.state = {};
      this.schema.state.disabled = false;
    }
  }

  onCallback(event: any) {
    this.callback.emit(event);
  }

  onCallbackAction(event: any) {
    let data;
    if (event.constructor.name === 'Array') {
      data = Object.assign([], event);
    } else if (event.constructor.name === 'Object') {
      data = Object.assign({}, event);
    }

    if (this.schema.action.type === 'resetPassword') {
      try {
        data =
          data[this.schema.action.bindingKey][
            this.schema.action.bindingResetPassword
          ];
      } catch (error) {}
    }
    const callback = {
      schema: this.schema.action,
      value: data,
      values: this.values,
    };
    this.callbackAction.emit(callback);
  }
}
