import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ButtonComponent } from './button.component';

import { TextExtendModule } from '@components/extend/text/text.module';
import { ImageExtendModule } from '@components/extend/image/image.module';

import { SharedModule } from '@shared/shared.module';

@NgModule({
  declarations: [ButtonComponent],
  imports: [CommonModule, SharedModule, TextExtendModule, ImageExtendModule],
  exports: [ButtonComponent],
  providers: [],
})
export class ButtonModule {}
