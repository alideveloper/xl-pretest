import {
  Component,
  OnInit,
  Input,
  DoCheck,
  Output,
  EventEmitter,
  ViewEncapsulation,
} from '@angular/core';

import {
  Select,
  CustomTemplate,
  Append,
  FormText,
} from '@app/shared/types/select';
import { Label } from '@app/shared/types/label';
import { State } from '@app/shared/types/state';
import { Validation } from '@app/shared/types/validation';

import { GlobalService } from '@app/shared/services/global.service';
import { GlobalValidation } from '@app/shared/services/global.validation';

import { cloneDeep } from 'lodash';

@Component({
  selector: 'component-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class SelectComponent implements OnInit, DoCheck {
  selectedDataAll: string[] = [];

  @Input() label: Label;
  @Input() labelSubtitle: Label;
  @Input() parentBinding: any;
  @Input() bindingData: any;
  @Input() bindingDataGlobal: any;
  @Input() bindingObject: any;
  @Input() bindingWhere: any;
  @Input() bindingWhereFrom: any;
  @Input() bindingSearch: any;
  @Input() primaryKey: any;
  @Input() labelForId: any;
  @Input() style: any;
  @Input() styleClass: string;
  @Input() state: State;
  @Input() data: any = [];
  @Input() bindLabel: string;
  @Input() grouping: any;
  @Input() bindValue: string;
  @Input() searchable: boolean;
  @Input() required: boolean;
  @Input() multiple: boolean;
  @Input() hideSelected: boolean;
  @Input() maxSelectedItems: number;
  @Input() closeOnSelect = false;
  @Input() groupBy: string;
  @Input() selectableGroup: any;
  @Input() selectableGroupAsModel: any;
  @Input() defaultValue: boolean;
  @Input() customSearch: boolean;
  @Input() selectedAll: any;
  @Input() customTemplate: CustomTemplate;
  @Input() placeholder: string;
  @Input() append: Append;
  @Input() formText: FormText;
  @Input() selectedBusiness: any;
  @Input() validation: Validation;
  @Input() search: boolean;
  @Input() clearable: any = true;

  @Input() schema: Select;
  @Input() value: any;
  @Input() values: any;

  key: any;
  keyBinding: any;
  validationInput = false;

  flag = {
    binding: false,
    callbackData: false,
  };

  where: any;
  dataStorage: any;

  @Output() callback: any = new EventEmitter();
  @Output() callbackAction: any = new EventEmitter();

  constructor(
    public global: GlobalService,
    public globalValidation: GlobalValidation
  ) {}

  ngOnInit(): void {
    this.initData();
  }

  ngDoCheck(): void {
    if (
      this.schema &&
      this.schema.state &&
      this.schema.state.hasOwnProperty('hide') &&
      this.schema.state.hasOwnProperty('hideCondition') &&
      this.schema.state.hideCondition.length > 0
    ) {
      // this.schema.state.hide = this.global.showHideCondition(
      //   this.schema?.state?.hideCondition
      // );
    }

    if (this.bindingObject && this.value && !this.flag.binding) {
      this.key = this.global.recursiveObject(this.bindingObject, this.value);
      this.flag.binding = true;
    }

    if (this.required) {
      const validation = {
        required: this.required,
        ...this.validation,
      };

      // const validationCheck = this.globalValidation.checkValidation(
      //   validation,
      //   this.value[this.parentBinding][this.bindingObject]
      // );

      // this.validationInput = !validationCheck.status;
      // this.schema.validationInput = this.validationInput;
      // if (this.validationInput) {
      //   this.formText.invalid.text = validationCheck.text;
      // }
    }

    if (this.bindingWhere) {
      if (this.where !== this.value[this.parentBinding][this.bindingWhere]) {
        this.where = this.value[this.parentBinding][this.bindingWhere];
        this.data = this.dataStorage.filter(
          (item: any) => item[this.bindingWhere] === this.where
        );
      }
    }

    if (
      this.schema &&
      this.value &&
      this.bindingData &&
      !this.flag.callbackData
    ) {
      if (this.global.recursiveData(this.bindingData, this.value)) {
        const bindingData = this.bindingData;
        if (!this.bindingDataGlobal) {
          this.data = this.global.recursiveData(this.bindingData, this.value);
        }
        this.dataStorage = Object.assign([], this.data);
        console.log('dataselect', this.data);
        if (bindingData !== this.data) this.flag.callbackData = true;
        if (this.defaultValue && this.data && this.data.length > 0) {
          this.value[this.parentBinding][this.bindingObject] = this.data[0][
            this.primaryKey
          ];
          console.log(
            'this.value[this.parentBinding][this.bindingObject]',
            this.value[this.parentBinding][this.bindingObject]
          );
          if (this.value[this.parentBinding][this.bindingObject]) {
            this.onCallback(this.value[this.parentBinding][this.bindingObject]);
          }
        }
      }
    }
  }

  initData(): void {
    if (this.schema) {
      this.label = this.schema.label;
      this.labelSubtitle = this.schema.labelSubtitle;
      this.style = this.schema.style;
      this.styleClass = this.schema.styleClass;
      this.state = this.schema.state;
      this.labelForId = this.schema.labelForId;
      this.data = this.schema.data;
      this.placeholder = this.schema.placeholder;
      this.required = this.schema.required;
      this.bindingData = this.schema.bindingData;
      this.bindingWhere = this.schema.bindingWhere;
      this.bindingWhereFrom = this.schema.bindingWhereFrom;
      this.bindingDataGlobal = this.schema.bindingDataGlobal;
      this.bindLabel = this.schema.bindLabel;
      this.bindValue = this.schema.bindValue;
      this.grouping = this.schema.grouping;
      this.search = this.schema.search;
      this.searchable = this.schema.searchable;
      if (this.schema.hasOwnProperty('clearable')) {
        this.clearable = this.schema.clearable;
      }
      this.multiple = this.schema.multiple;
      this.hideSelected = this.schema.hideSelected;
      this.maxSelectedItems = this.schema.maxSelectedItems;
      this.closeOnSelect = this.schema.closeOnSelect;
      this.groupBy = this.schema.groupBy;
      this.selectableGroup = this.schema.selectableGroup;
      this.selectableGroupAsModel = this.schema.selectableGroupAsModel;
      this.defaultValue = this.schema.defaultValue;
      this.customSearch = this.schema.customSearch;
      this.selectedAll = this.schema.selectedAll;
      this.selectedBusiness = this.schema.selectedBusiness;
      this.multiple = this.schema.multiple;
      this.bindingSearch = this.schema.bindingSearch;
      this.primaryKey = this.schema.primaryKey;
      this.customTemplate = this.schema.customTemplate;
      this.formText = this.schema.formText;
      this.validation = this.schema.validation;
      this.append = this.schema.append;
      this.formText = this.schema.formText;
      this.validation = this.schema.validation;

      if (this.schema.bindingObject) {
        this.bindingObject = this.schema.bindingObject;
      }
      if (this.schema.parentBinding) {
        this.parentBinding = this.schema.parentBinding;
      }

      if (this.bindingDataGlobal) {
        this.data = this.global.bindingDataGlobal(this.bindingDataGlobal);
        this.dataStorage = Object.assign([], this.data);
      }

      if (this.value) {
        if (!this.value[this.parentBinding]) {
          this.value[this.parentBinding] = {};
        }
        // this.onCallback({
        //   [this.parentBinding]: this.value[this.parentBinding],
        // });
      }
    }
  }

  customSearchSelect(term: string, item: any) {
    term = term.toLowerCase();
    return item[this.bindingSearch].toLowerCase().indexOf(term) > -1;
  }

  addTagSelect(key: string) {
    return { [this.primaryKey]: key, tag: true };
  }

  selectAll(): void {
    if (this.data) {
      this.selectedDataAll = this.data.map(
        (itemData: any) => itemData[this.primaryKey]
      );
    }
  }

  unselectAll(): void {
    this.selectedDataAll = [];
  }

  onChange(event: any): void {
    console.log(event);
    if (this.bindingWhereFrom) {
      this.value[this.parentBinding][this.bindingWhereFrom] = null;
    }
    this.onValidationSelect(event);
  }

  onValidationSelect(event: any) {
    if (this.required && !event) {
      console.log('masuk', event);
      this.validationInput = true;
      this.schema.validationInput = true;
      this.onCallback(event);
      return;
    }
    this.validationInput = false;
    this.schema.validationInput = false;
    if (this.search) {
      this.onCallback(event);
    }
  }

  onCallback(event: any) {
    this.callback.emit(event);
  }

  onCallbackAction(event: any) {
    this.callbackAction.emit(event);
  }
}
