import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  SimpleChange,
  SimpleChanges,
  OnChanges,
} from '@angular/core';

import { Firebase } from '@app/shared/types/firebase';

import { GlobalService } from '@app/shared/services/global.service';

import { environment } from '@environments/environment';

import { FirebaseService } from '@firebase/firebase.service';

@Component({
  selector: 'app-firebase',
  template: '',
  styleUrls: [],
})
export class FirebaseDataComponent implements OnInit, OnChanges {
  @Input() firebase: any;
  @Input() value: any = {};

  valueWhereStorage: any;

  @Output() callback: any = new EventEmitter();

  project = environment.projectId;

  constructor(
    public global: GlobalService,
    private firebaseService: FirebaseService
  ) {}

  ngOnInit(): void {
    this.handleFirebaseDataGet();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.getFirebaseData(changes);
  }

  getFirebaseData(changes?: any) {
    if (changes.firebase) {
      const currentData: SimpleChange = changes
        ? changes.firebase
        : { currentValue: this.firebase };
      this.firebase = changes ? changes.firebase.currentValue : this.firebase;

      if (currentData && currentData.currentValue) {
        this.handleFirebaseDataGet();
      }
    }
  }

  async handleCollection(result: any, item: any): Promise<any> {
    if (item.collection.status && !item.collection.document.status) {
      result = await this.firebaseService.getCollection(
        item.collection.name,
        (ref: any) =>
          this.handleRefRecursive(
            ref,
            item.collection.orderBy,
            item.query,
            item.limit
          )
      );
    } else if (item.collection.status && item.collection.document.status) {
      if (!result) {
        result = await this.firebaseService.getCollections(
          item.collection.name
        );
      }
      result = await this.handleDocumentRecursive(
        result,
        item.collection.document
      );
    }
    return result;
  }

  handleRefRecursive(ref: any, orderBy: any, query: any, limit?: any): any {
    if (orderBy && orderBy.object && orderBy.order) {
      ref = ref.orderBy(orderBy.object, orderBy.order);
    }

    if (limit) {
      ref = ref.limit(limit);
    }
    ref = this.handleWhereRecursive(ref, query);
    return ref;
  }

  handleWhereRecursive(ref: any, query: any): any {
    console.log(this.value);
    if (query && query.length > 0) {
      query.forEach((queryItem: any) => {
        if (queryItem.bindingValue) {
          if (queryItem.valueParent) {
            if (queryItem.storage) {
              this.value[queryItem.valueParent] = JSON.parse(
                localStorage.getItem(
                  environment.domain + '-' + queryItem.storage
                )
              );
            }

            console.log(this.value);
            if (
              this.value[queryItem.valueParent] &&
              this.value[queryItem.valueParent][queryItem.value]
            ) {
              ref = ref.where(
                queryItem.object,
                queryItem.condition,
                this.value[queryItem.valueParent][queryItem.value]
              );
            }
          } else {
            ref = ref.where(
              queryItem.object,
              queryItem.condition,
              this.value[queryItem.value]
            );
          }
        } else {
          ref = ref.where(
            queryItem.object,
            queryItem.condition,
            queryItem.value
          );
        }
      });
    }
    return ref;
  }

  async handleCollectionRecursive(result: any, item: any): Promise<any> {
    if (item.document && item.document.status) {
      result = await this.firebaseService.getCollectionRecursive(
        result,
        item.name
      );
      result = await this.handleDocumentRecursive(result, item.document);
    } else {
      result = await this.firebaseService.getCollectionFinalRecursive(
        result,
        item.name,
        (ref: any) =>
          this.handleRefRecursive(ref, item.orderBy, item.query, item.limit)
      );
    }
    return result;
  }

  async handleDocumentRecursive(result: any, item: any): Promise<any> {
    result = await this.firebaseService.getDocuments(
      result,
      item.name === 'PBproject' ? this.project : item.name
    );
    if (item.collection.status) {
      result = await this.handleCollectionRecursive(result, item.collection);
    }
    return result;
  }

  async handleFirebaseDataGet(): Promise<any> {
    await this.firebase.firebase.forEach(async (item: any) => {
      let length = 0;
      console.log('item', item);
      let data;
      setTimeout(
        async () => {
          data = await this.handleCollection(null, item);

          data.subscribe(async (response: any) => {
            console.log('response', response);
            if (response) {
              if (this.firebase.detail || item.detail) {
                if (response && response[0]) {
                  this.value[item.bindingKey] = response[0];
                }
                this.value[item.bindingKey].afterAPI = true;
              } else {
                this.value[item.bindingKey] = response;
              }
            }

            if (item.relation && this.value[item.bindingKey]) {
              this.value[item.bindingKey].map((itemValue: any) => {
                item.relation.forEach((itemRelation: any) => {
                  const filteritem = this.value[
                    itemRelation.bindingFirebase
                  ].filter(
                    (itemFilter: any) =>
                      itemFilter.key === itemValue[itemRelation.bindingKey]
                  );

                  if (filteritem && filteritem.length > 0) {
                    itemValue[itemRelation.bindingObject] = filteritem[0];
                  }
                });
              });
            }
            length++;

            console.log('callback');
            this.callback.emit(this.value);
          });
        },
        item.timeout ? item.timeout : 0
      );
    });
  }
}
