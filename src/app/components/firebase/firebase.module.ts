import { NgModule } from '@angular/core';

import { FirebaseDataComponent } from './firebase.component';

import { SharedModule } from '@shared/shared.module';

@NgModule({
  declarations: [FirebaseDataComponent],
  imports: [SharedModule],
  exports: [FirebaseDataComponent],
  providers: [],
})
export class FirebaseDataModule {}
