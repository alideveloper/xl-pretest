import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InnerHTMLComponent } from './innerhtml.component';

import { SharedModule } from '@shared/shared.module';

@NgModule({
  declarations: [InnerHTMLComponent],
  imports: [CommonModule, SharedModule],
  exports: [InnerHTMLComponent],
  providers: [],
})
export class InnerHTMLModule {}
