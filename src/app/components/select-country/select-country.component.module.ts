import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SelectCountryComponent } from './select-country.component';

import { SharedModule } from '@app/shared/shared.module';
import { UtilsModule } from '@app/components/utils/utils.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgOptionHighlightModule } from '@ng-select/ng-option-highlight';

@NgModule({
  declarations: [SelectCountryComponent],
  imports: [
    CommonModule,
    SharedModule,
    UtilsModule,
    NgSelectModule,
    NgOptionHighlightModule,
  ],
  exports: [SelectCountryComponent],
  providers: [],
})
export class SelectCountryModule {}
