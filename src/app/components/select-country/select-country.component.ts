import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  HostListener,
} from '@angular/core';

import {
  Select,
  CustomTemplate,
  Append,
  FormText,
} from '@app/shared/types/select';
import { Label } from '@app/shared/types/label';
import { State } from '@app/shared/types/state';
import { Validation } from '@app/shared/types/validation';

import { AppService } from '@app/app.service';
import { Subject } from 'rxjs';
import { AppData } from '@app/app.data';
import { LoadingService } from '@app/shared/services/loading.service';
import { CountriesHelper } from '@app/shared/helper/countries.helper';

@Component({
  selector: 'component-select-country',
  templateUrl: './select-country.component.html',
  styleUrls: ['./select-country.component.scss'],
})
export class SelectCountryComponent implements OnInit {
  selectedDataAll: string[] = [];

  @Input() label: Label;
  @Input() labelSubtitle: Label;
  @Input() parentBinding: any;
  @Input() bindingData: any;
  @Input() bindingDataGlobal: any;
  @Input() bindingObject: any;
  @Input() bindingWhere: any;
  @Input() bindingWhereFrom: any;
  @Input() bindingSearch: any;
  @Input() businessKey: any;
  @Input() labelForId: any;
  @Input() style: any;
  @Input() styleClass: string;
  @Input() state: State;
  @Input() data: any = [];
  @Input() bindLabel: string;
  @Input() grouping: any;
  @Input() bindValue: string;
  @Input() searchable: boolean;
  @Input() required: boolean;
  @Input() multiple: boolean;
  @Input() hideSelected: boolean;
  @Input() maxSelectedItems: number;
  @Input() closeOnSelect = false;
  @Input() groupBy: string;
  @Input() selectableGroup: any;
  @Input() selectableGroupAsModel: any;
  @Input() defaultValue: boolean;
  @Input() customSearch: boolean;
  @Input() selectedAll: any;
  @Input() customTemplate: CustomTemplate;
  @Input() placeholder: string;
  @Input() append: Append;
  @Input() formText: FormText = {
    valid: {
      active: true,
      text: 'Data telah Sesuai!',
    },
    invalid: {
      active: true,
      text: 'Data tidak Sesuai!',
    },
  };
  @Input() validation: Validation;
  @Input() search: boolean;

  @Input() schema: Select;
  @Input() value: any;
  @Input() values: any;

  key: any;
  keyBinding: any;
  validationInput = false;

  flag = {
    binding: false,
    callbackData: false,
  };

  where: any;
  dataStorage: any;

  @Output() callback: any = new EventEmitter();
  @Output() callbackAction: any = new EventEmitter();

  constructor(
    private _countriesHelper: CountriesHelper,
    private _loadingService: LoadingService
  ) {}

  ngOnInit(): void {
    this.data = [...this._countriesHelper.getCountries];
    this.dataStorage = [...this._countriesHelper.getCountries];
  }

  onChange(event: any): void {
    this._loadingService.loading.isLoading = true;
    setTimeout(() => {
      this._loadingService.loading.isLoading = false;
    }, 1500);
    if (this.bindingWhereFrom) {
      this.value = null;
    }
    this.onValidationSelect(event);
  }

  onValidationSelect(event: any) {
    this.onCallback(event);

    if (this.required && !event) {
      this.validationInput = true;
      return;
    }
    this.validationInput = false;
  }

  onCallback(event: any) {
    this.callback.emit(event);
  }

  onCallbackAction(event: any) {
    this.callbackAction.emit(event);
  }
}
