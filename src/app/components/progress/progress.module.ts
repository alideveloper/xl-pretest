import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProgressSolidComponent } from './progress-solid/progress-solid.component';
import {
  ProgressbarModule,
  ProgressbarComponent,
} from 'ngx-bootstrap/progressbar';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [ProgressSolidComponent],
  imports: [CommonModule, ProgressbarModule, TranslateModule],
  exports: [ProgressSolidComponent, ProgressbarComponent, ProgressbarModule],
})
export class ProgressModule {}
