import {
  Component,
  OnChanges,
  Input,
  ChangeDetectorRef,
  OnInit,
  DoCheck,
} from '@angular/core';

import { State } from '@app/shared/types/state';

import { GlobalService } from '@app/shared/services/global.service';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss'],
})
export class LoaderComponent implements OnInit, DoCheck {
  // Indicates if the component shows the loading bars
  @Input() isLoading = true;

  // Which template to use
  @Input() template: string;

  // Total number of bars-groups to use
  @Input() count: any = 5;

  // The message to show when there no items
  @Input() emptyMessage: string;

  // URL for when there no items on the collection
  @Input() emptyUrl: string;

  @Input() schema: any;
  @Input() value: any;

  // Private internal, used to actually iterate over the bars
  public items: any[];

  constructor(public global: GlobalService, public cdRef: ChangeDetectorRef) {}

  ngOnInit(): void {
    this.initData();
  }

  ngDoCheck(): void {
    if (!this.items || (this.items && this.items.length === 0)) {
      this.items = Array(this.count).fill(0);
    }

    if (this.schema) {
      if (this.schema.state) {
        if (this.schema.state.disabled) {
          this.schema.state.disabled = this.global.disabledConditions(
            this.schema,
            this.value
          );
          this.cdRef.detectChanges();
        }

        if (this.schema.state.hide) {
          this.schema.state.hide = this.global.showHideConditions(
            this.schema,
            this.value
          );
        }
        this.cdRef.detectChanges();
      }
    }
  }

  initData(): void {
    if (this.schema) {
      this.count = this.schema.count;
      this.isLoading = this.schema.isLoading;
      this.template = this.schema.template;
      this.emptyUrl = this.schema.emptyUrl;
      this.emptyMessage = this.schema.emptyMessage;
    }
  }
}
