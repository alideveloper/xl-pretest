import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-img',
  templateUrl: './img.component.html',
  styleUrls: ['./img.component.scss'],
})
export class ImgComponent implements OnInit {
  @Input()
  image: string;

  @Input()
  responsive: any = true;

  @Input()
  isMockup: boolean;

  @Input()
  imgClass: string;

  constructor() {}

  ngOnInit() {}
}
