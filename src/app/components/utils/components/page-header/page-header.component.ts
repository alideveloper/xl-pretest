import {
  Component,
  OnInit,
  AfterViewInit,
  Input,
  ViewChild,
  ElementRef,
  ChangeDetectorRef,
  Output,
  EventEmitter,
} from '@angular/core';

import { GlobalService } from '@app/shared/services/global.service';

import { Label } from '@app/shared/types/label';
import { State } from '@app/shared/types/state';

@Component({
  selector: 'app-page-header',
  templateUrl: './page-header.component.html',
  styleUrls: ['./page-header.component.scss'],
})
export class PageHeaderComponent implements OnInit, AfterViewInit {
  @Input() schema: any;
  @Input() value: any;

  @Input() active: boolean;

  @Input() icon: any;
  @Input() title: Label;

  @Input() pre: Label;

  @Input() sub: Label;

  @Input() hasNavigation: boolean;

  @Input() mbSpace = true;
  @Input() noBorder = false;
  @Input() style: any;
  @Input() styleClass: string;
  @Input() state: State;

  @ViewChild('toolsView', { static: false })
  toolsView: ElementRef;

  toolsHidden: boolean;

  @Output() callback: any = new EventEmitter();
  @Output() callbackAction: any = new EventEmitter();

  constructor(
    private cdRef: ChangeDetectorRef,
    public globalService: GlobalService
  ) {}

  ngOnInit() {
    this.initData();
  }

  initData(): any {
    if (this.schema) {
      if (this.schema.hasOwnProperty('title')) this.title = this.schema.title;
      if (this.schema.hasOwnProperty('mbSpace')) {
        this.mbSpace = this.schema.mbSpace;
      }
      if (this.schema.hasOwnProperty('pre')) this.pre = this.schema.pre;
      if (this.schema.hasOwnProperty('noBorder')) {
        this.noBorder = this.schema.noBorder;
      }
      if (this.schema.hasOwnProperty('sub')) this.sub = this.schema.sub;
      if (this.schema.hasOwnProperty('hasNavigation')) {
        this.hasNavigation = this.schema.hasNavigation;
      }
      if (this.schema.hasOwnProperty('icon')) this.icon = this.schema.icon;

      if (this.schema.hasOwnProperty('style')) this.style = this.schema.style;
      if (this.schema.hasOwnProperty('styleClass')) {
        this.styleClass = this.schema.styleClass;
      }
      if (this.schema.hasOwnProperty('state')) this.state = this.schema.state;
    }
  }

  ngAfterViewInit() {
    this.toolsHidden = this.toolsView.nativeElement.children.length === 0;
    this.cdRef.detectChanges();
  }

  onBack() {
    const callback = {
      schema: {
        type: 'back',
        page: this.schema.icon.page,
      },
    };
    this.callbackAction.emit(callback);
  }
}
