import {
  Component,
  OnInit,
  Input,
  OnChanges,
  SimpleChanges,
  ChangeDetectorRef,
} from '@angular/core';
import { BaseComponent } from '@app/core';

@Component({
  selector: 'app-fade-content',
  templateUrl: './fade-content.component.html',
  styleUrls: ['./fade-content.component.scss'],
})
export class FadeContentComponent extends BaseComponent
  implements OnInit, OnChanges {
  @Input()
  contentClass: string;

  @Input()
  height: number;

  constructor(private cdRef: ChangeDetectorRef) {
    super();
  }

  ngOnInit() {}

  ngOnChanges(changes: SimpleChanges): void {
    // logger.debug('ngOnChanges', changes);
    if (changes.height && changes.height.currentValue) {
      this.height = changes.height.currentValue;
    }

    if (changes.contentClass && changes.contentClass.currentValue) {
      this.contentClass = changes.contentClass.currentValue;
    }
    this.cdRef.detectChanges();
  }
}
