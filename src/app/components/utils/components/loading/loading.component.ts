import {
  Component,
  OnChanges,
  SimpleChanges,
  Input,
  ViewChild,
  OnInit,
  ViewEncapsulation,
} from '@angular/core';

import { NgxLoadingComponent } from 'ngx-loading';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class LoadingComponent implements OnInit, OnChanges {
  @Input() isLoading = false;
  @Input() text: string;
  @Input() image: string;
  @Input() primaryColour: string;
  @Input() secondaryColour: string;
  @Input() animation: string;
  @Input() backdropBorderRadius = '1px';

  @ViewChild('ngxLoading', { static: true })
  ngxLoadingComponent: NgxLoadingComponent;

  constructor() {}

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges): void {}
}
