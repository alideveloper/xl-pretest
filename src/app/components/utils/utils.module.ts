import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { IconsModule } from '@components/icons/icons.module';
import { CardsModule } from '@components/cards/cards.module';
import { NavigationsModule } from '@components/navigations/navigations.module';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { NgxLoadingModule } from 'ngx-loading';
import { TranslateModule } from '@ngx-translate/core';

import { ThemeColorPickerComponent } from '@utils/components/theme-color-picker/theme-color-picker.component';
import { LoaderComponent } from '@utils/components/loader/loader.component';
import { LoadingComponent } from '@utils/components/loading/loading.component';
import { PageHeaderComponent } from '@utils/components/page-header/page-header.component';
import { PageOverlayComponent } from '@utils/components/page-overlay/page-overlay.component';
import { VerticalTogglerComponent } from '@utils/components/vertical-toggler/vertical-toggler.component';
import { FadeContentComponent } from '@utils/components/fade-content/fade-content.component';
import { LabelInfoComponent } from '@utils/components/label-info/label-info.component';
import { AddressComponent } from '@utils/components/address/address.component';
import { HtmlCleanPipe } from '@utils/directives/htmlclean.pipe';
import { HtmlPipe } from '@utils/directives/html.pipe';
import { ToggleOpenDirective } from '@utils/directives/toggle-open.directive';
import { OpenParentDirective } from '@utils/directives/open-parent.directive';
import { StickyDirective } from '@utils/directives/sticky.directive';
import { FlagComponent } from '@utils/components/flag/flag.component';
import { ImgComponent } from '@utils/components/img/img.component';

const exports = [
  ThemeColorPickerComponent,
  LoaderComponent,
  LoadingComponent,
  PageHeaderComponent,
  PageOverlayComponent,
  VerticalTogglerComponent,
  FadeContentComponent,
  LabelInfoComponent,
  AddressComponent,
  FlagComponent,
  ImgComponent,

  HtmlCleanPipe,
  HtmlPipe,
  ToggleOpenDirective,
  OpenParentDirective,
  StickyDirective,
];

@NgModule({
  declarations: [...exports, ImgComponent],
  imports: [
    CommonModule,
    RouterModule,
    BsDropdownModule,
    PerfectScrollbarModule,
    NgxLoadingModule.forRoot({}),
    TranslateModule,
    IconsModule,
    CardsModule,
    NavigationsModule,
  ],
  exports,
})
export class UtilsModule {}
