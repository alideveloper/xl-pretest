import {
  Component,
  OnInit,
  Input,
  ViewEncapsulation,
  HostBinding,
  DoCheck,
  ChangeDetectorRef,
} from '@angular/core';

import { State } from '@app/shared/types/state';
import { Container } from '@app/shared/types/container';

import { GlobalService } from '@app/shared/services/global.service';

@Component({
  selector: 'component-container',
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ContainerComponent implements OnInit, DoCheck {
  @HostBinding('class') styleClassHost: string;

  @Input() style: any;
  @Input() styleClass: string;

  @Input() schema: Container;
  @Input() value: any;

  constructor(public global: GlobalService, public cdRef: ChangeDetectorRef) {
    this.styleClassHost = 'width-full-p ';
  }

  ngOnInit(): void {
    this.initData();
  }

  ngDoCheck(): void {
    if (this.schema.state) {
      if (this.schema.state.disabled) {
        this.schema.state.disabled = this.global.disabledConditions(
          this.schema,
          this.value
        );
        this.cdRef.detectChanges();
      }

      this.schema.state.hide = this.global.showHideConditions(
        this.schema,
        this.value
      );
      this.cdRef.detectChanges();
    }
  }

  initData(): void {
    if (this.schema) {
      this.style = this.schema.style;
      this.styleClass = this.schema.styleClass;
      if (this.schema.styleClassHost) {
        this.styleClassHost = this.schema.styleClassHost;
      }
    }
  }
}
