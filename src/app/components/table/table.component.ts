import {
  Component,
  OnInit,
  ViewChild,
  Input,
  Output,
  ChangeDetectorRef,
  OnChanges,
  SimpleChanges,
  SimpleChange,
  EventEmitter,
} from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { BaseDataTable } from './base/base-data-table';
import { ColorScheme } from '@app/core';

import { SelectionType } from '@swimlane/ngx-datatable';

import { Logger } from '@app/core';
const logger = new Logger('NgxDtGroupingComponent');

import { Label } from '@app/shared/types/label';
import { State } from '@app/shared/types/state';

import { GlobalService } from '@app/shared/services/global.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
})
export class TableComponent extends BaseDataTable implements OnInit, OnChanges {
  @ViewChild(DatatableComponent, { static: false })
  table: DatatableComponent;

  ColorScheme = ColorScheme;

  @Input() schema: any;
  @Input() value: any;

  @Input() title: Label;

  @Input() buttonHeader: any = [];

  @Input() messages: any;

  @Input() isCard = true;

  @Input() currentStyles: string[] = [];
  @Input() isTableView = true;
  @Input() isListCollapsed = true;

  @Input() columns: any = [];

  @Input() headerHeight: any = 'auto';
  @Input() rowHeight: any = 'auto';
  @Input() footerHeight: any = 'auto';

  @Input() filter: any;
  @Input() pagination: any;

  @Input() scrollbarV = false;

  @Input() selectedActive = false;
  @Input() selected: any[] = [];
  @Input() SelectionType = SelectionType;
  @Input() selectAllRowsOnPage = false;

  @Input() responsive = true;
  @Input() responsiveExpandable = false;

  @Input() limit = 10;

  @Input() style: any;
  @Input() styleClass: string;
  @Input() state: State;

  @Output() callback: any = new EventEmitter();
  @Output() callbackAction: any = new EventEmitter();

  selectedMessage: any;
  groupingActive: any;

  timeout: any;

  data: any;
  dataStorage: any;
  filterStorage: any;
  countFilter: any = [];
  filterAllActive: any = {
    input: {
      status: false,
      text: '',
    },
    select: {
      status: false,
      text: '',
    },
  };

  styles = [
    {
      name: 'Hovered',
      style: 'table-hover',
    },
    {
      name: 'Striped',
      style: 'table-striped',
    },
    {
      name: 'Bordered',
      style: 'table-bordered',
    },
  ];

  get tableStyles(): string {
    return (this.isTableView ? 'table' : 'listview') + ' ' + this.currentStyles;
  }

  constructor(
    private cdRef: ChangeDetectorRef,
    public globalService: GlobalService
  ) {
    super();
  }

  ngOnInit() {
    this.initData();
    this.getBindingData();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.getBindingData(changes);
  }

  getBindingData(changes?: any) {
    const currentData: SimpleChange = changes
      ? changes.value
      : { currentValue: this.value };
    const value = changes ? changes.value.currentValue : this.value;

    if (currentData && currentData.currentValue) {
      if (this.schema.bindingObject && value[this.schema.bindingObject]) {
        this.data = [...value[this.schema.bindingObject]];
        this.dataStorage = Object.assign([], this.data);
      }
    }
  }

  initData(): any {
    if (this.schema) {
      if (this.schema.hasOwnProperty('title')) this.title = this.schema.title;
      if (this.schema.hasOwnProperty('buttonHeader')) {
        this.buttonHeader = this.schema.buttonHeader;
      }

      if (this.schema.hasOwnProperty('messages')) {
        this.messages = this.schema.messages;
      }

      if (this.schema.hasOwnProperty('isCard')) {
        this.isCard = this.schema.isCard;
      }
      if (this.schema.hasOwnProperty('filter')) {
        this.filter = this.schema.filter;
      }

      if (this.schema.hasOwnProperty('currentStyles')) {
        this.currentStyles = this.schema.currentStyles;
      }
      if (this.schema.hasOwnProperty('isTableView')) {
        this.isTableView = this.schema.isTableView;
      }
      if (this.schema.hasOwnProperty('isListCollapsed')) {
        this.isListCollapsed = this.schema.isListCollapsed;
      }

      if (this.schema.hasOwnProperty('columns')) {
        this.columns = this.schema.columns;
      }

      if (this.schema.hasOwnProperty('styleClass')) {
        this.styleClass = this.schema.styleClass;
      }
      if (this.schema.hasOwnProperty('state')) this.state = this.schema.state;

      if (this.schema.hasOwnProperty('headerHeight')) {
        this.headerHeight = this.schema.headerHeight;
      }
      if (this.schema.hasOwnProperty('rowHeight')) {
        this.rowHeight = this.schema.rowHeight;
      }
      if (this.schema.hasOwnProperty('footerHeight')) {
        this.footerHeight = this.schema.footerHeight;
      }

      if (this.schema.hasOwnProperty('scrollbarV')) {
        this.scrollbarV = this.schema.scrollbarV;
      }

      if (this.schema.hasOwnProperty('selectedActive')) {
        this.selectedActive = this.schema.selectedActive;
      }
      if (this.schema.hasOwnProperty('selected')) {
        this.selected = this.schema.selected;
      }
      if (this.schema.hasOwnProperty('SelectionType')) {
        this.SelectionType = this.schema.SelectionType;
      }
      if (this.schema.hasOwnProperty('selectAllRowsOnPage')) {
        this.selectAllRowsOnPage = this.schema.selectAllRowsOnPage;
      }

      if (this.schema.hasOwnProperty('responsive')) {
        this.responsive = this.schema.responsive;
      }
      if (this.schema.hasOwnProperty('responsiveExpandable')) {
        this.responsiveExpandable = this.schema.responsiveExpandable;
      }

      if (this.schema.hasOwnProperty('pagination')) {
        this.pagination = this.schema.pagination;
      }

      if (this.schema.hasOwnProperty('limit')) this.limit = this.schema.limit;
    }
  }

  onSelect({ selected }: any) {
    this.selected = [...selected];
  }

  onDetailToggle({ value }: any) {
    logger.debug('Detail Toggled', value);
  }

  toggleExpandGroup(group: any) {
    logger.debug('Toggled Expand Group!', group);
    this.table.groupHeader.toggleExpandGroup(group);
  }

  updateFilter(value: any, type: any) {
    logger.debug(value);

    // filter our data
    let filtered;

    if (
      (!value && type === 'Input' && !this.filterAllActive.select.status) ||
      (!value && type === 'Select' && !this.filterAllActive.input.status)
    ) {
      filtered = this.dataStorage;
    }

    if (
      this.filter &&
      this.filter.filterAll &&
      this.filter.type === 'Input' &&
      value !== undefined &&
      value !== null
    ) {
      filtered = this.globalService.filterArrayByString(
        this.dataStorage,
        value
      );
    } else if (
      (this.filter && !this.filter.filterAll && this.filter.type === 'Input') ||
      (this.filter && !this.filter.filterAll && this.filter.type === 'Select')
    ) {
      if (value) {
        filtered = this.dataStorage.filter((data: any) => {
          return (
            data[this.filter.filterKey]
              .toLowerCase()
              .indexOf(value[this.filter.filterKey].toLowerCase()) !== -1
          );
        });
      }
    } else if (
      this.filter &&
      this.filter.filterAll &&
      this.filter.type === 'InputSelect'
    ) {
      if (
        (type === 'Input' && this.filter.filterAll) ||
        this.filterAllActive.input.status
      ) {
        filtered = this.globalService.filterArrayByString(
          this.dataStorage,
          this.filterAllActive.input.status
            ? this.filterAllActive.input.text
            : value
        );
      }

      if (
        type === 'Select' ||
        (type === 'Input' && !this.filter.filterAll) ||
        this.filterAllActive.select.status
      ) {
        const dataFilter = this.filterAllActive.input.status
          ? filtered
          : this.dataStorage;
        if ((type === 'Select' && value) || type === 'Input') {
          filtered = dataFilter.filter((data: any) => {
            const valueFilter = this.filterAllActive.select.status
              ? this.filterAllActive.select.text
              : value;
            return (
              data[this.filter.filterKey]
                .toLowerCase()
                .indexOf(valueFilter.toLowerCase()) !== -1
            );
          });
        }
      }
    }

    this.filterStorage = filtered;

    // update the rows
    this.data = filtered;

    // Whenever the filter changes, always go back to the first page
    try {
      this.table.offset = 0;
    } catch (error) {}
  }

  onPage(event: Event) {
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      logger.debug('paged!', event);
    }, 100);
  }

  onCallback(event: any) {
    this.callback.emit(event);
  }

  onCallbackSearch(event: any, type: any) {
    if (this.filter && this.filter.filterAll && type === 'Select') {
      if (event === null) {
        event = '';
      }
    }

    if (event && typeof event !== 'object') {
      if (this.filter && this.filter.filterAll && type === 'Input') {
        this.filterAllActive.input.status = true;
        this.filterAllActive.input.text = event;
      }
      if (this.filter && this.filter.filterAll && type === 'Select') {
        this.filterAllActive.select.status = true;
        this.filterAllActive.select.text = event;
      }
    } else {
      if (this.filter && this.filter.filterAll && type === 'Input') {
        this.filterAllActive.input.status = false;
        this.filterAllActive.select.text = '';
      }
      if (this.filter && this.filter.filterAll && type === 'Select') {
        this.filterAllActive.select.status = false;
        this.filterAllActive.select.text = '';
      }
    }

    if (typeof event !== 'object' || event === '') {
      this.updateFilter(event, type);
    }
  }

  onCallbackAction(event: any) {
    this.callbackAction.emit(event);
  }
}
