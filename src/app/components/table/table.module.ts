import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@app/shared';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { UtilsModule } from '@app/components/utils/utils.module';
import { NavigationsModule } from '../navigations/navigations.module';

import { TableComponent } from './table.component';

import { ButtonModule } from '@components/button/button.module';
import { InputModule } from '@components/input/input.module';
import { SelectsModule } from '@components/select/select.module';
import { SVGModule } from '@components/svg/svg.module';
import { IconsModule } from '@components/icons/icons.module';

const exports = [TableComponent];

@NgModule({
  declarations: [...exports],
  imports: [
    CommonModule,
    SharedModule,
    NgxDatatableModule,
    UtilsModule,
    ButtonModule,
    InputModule,
    SVGModule,
    IconsModule,
    NavigationsModule,
    SelectsModule,
  ],
  exports,
})
export class DataTablesModule {}
