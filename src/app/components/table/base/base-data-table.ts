import { OnInit, Directive } from '@angular/core';
import { BaseComponent, ColorScheme } from '@app/core';
import { ColumnMode } from '@swimlane/ngx-datatable';

@Directive({})
// tslint:disable-next-line: directive-class-suffix
export class BaseDataTable extends BaseComponent implements OnInit {
  ColumnMode = ColumnMode;
  ColorScheme = ColorScheme;

  constructor() {
    super();
  }

  ngOnInit() {}
}
