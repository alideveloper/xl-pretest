import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { RouterlinkComponent } from './routerlink.component';

@NgModule({
  declarations: [RouterlinkComponent],
  imports: [CommonModule, RouterModule],
  exports: [RouterlinkComponent],
  providers: [],
})
export class RouterlinkModule {}
