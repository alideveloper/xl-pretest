import {
  Component,
  OnInit,
  Input,
  ViewEncapsulation,
  HostBinding,
} from '@angular/core';

import { Routerlink } from '@app/shared/types/routerlink';

import { GlobalService } from '@app/shared/services/global.service';

@Component({
  selector: 'component-routerlink',
  templateUrl: './routerlink.component.html',
  styleUrls: ['./routerlink.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class RouterlinkComponent implements OnInit {
  @HostBinding('class') styleClassHost: string;

  @Input() url: any;
  @Input() style: any;
  @Input() styleClass: string;

  @Input() schema: Routerlink;
  @Input() value: any;

  constructor(public global: GlobalService) {
    this.styleClassHost = 'width-full-p';
  }

  ngOnInit(): void {
    this.initData();
  }

  initData(): void {
    if (this.schema) {
      this.url = this.schema.url;
      this.style = this.schema.style;
      this.styleClass = this.schema.styleClass;
    }
  }
}
