import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TextareaComponent } from './textarea.component';

import { SharedModule } from '@shared/shared.module';
import { QuillModule } from 'ngx-quill';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
  declarations: [TextareaComponent],
  imports: [CommonModule, SharedModule, FontAwesomeModule, QuillModule],
  exports: [TextareaComponent],
  providers: [],
})
export class TextareaModule {}
