import {
  Component,
  OnInit,
  Input,
  DoCheck,
  Output,
  EventEmitter,
} from '@angular/core';

import { Textarea, Append, FormText } from '@app/shared/types/textarea';
import { Label } from '@app/shared/types/label';
import { State } from '@app/shared/types/state';
import { Validation } from '@app/shared/types/validation';

import { GlobalService } from '@app/shared/services/global.service';
import { GlobalValidation } from '@app/shared/services/global.validation';

@Component({
  selector: 'component-textarea',
  templateUrl: './textarea.component.html',
  styleUrls: ['./textarea.component.scss'],
})
export class TextareaComponent implements OnInit, DoCheck {
  @Input() label: Label;
  @Input() labelSubtitle: Label;
  @Input() style: any;
  @Input() styleClass: string;
  @Input() state: State;
  @Input() json = false;
  @Input() readonly = false;
  @Input() required = false;
  @Input() minLength = 0;
  @Input() maxLength = 0;
  @Input() rows = 0;
  @Input() roundedInput = false;
  @Input() placeholder: string;
  @Input() onKeyUpActive = false;
  @Input() onBlurActive = false;
  @Input() append: Append;
  @Input() formText: FormText;
  @Input() validation: Validation;
  @Input() bindingObject: any;
  @Input() parentBinding: any;
  @Input() defaultValue: any;
  @Input() textEditor: boolean;
  @Input() search: boolean;

  @Input() schema: Textarea;
  @Input() value: any;

  validationInput = false;

  @Output() callback: any = new EventEmitter();
  @Output() callbackAction: any = new EventEmitter();

  constructor(
    public global: GlobalService,
    public globalValidation: GlobalValidation
  ) {}

  ngOnInit(): void {
    this.initData();
  }

  ngDoCheck(): void {
    if (
      this.schema &&
      this.schema.state &&
      this.schema.state.hasOwnProperty('hide') &&
      this.schema.state.hasOwnProperty('hideCondition') &&
      this.schema.state.hideCondition.length > 0
    ) {
      // this.schema.state.hide = this.global.showHideCondition(
      //   this.schema?.state?.hideCondition
      // );
    }

    if (this.required || this.minLength || this.maxLength) {
      const validation = {
        minLength: this.minLength,
        maxLength: this.maxLength,
        required: this.required,
        ...this.validation,
      };

      const validationCheck = this.globalValidation.checkValidation(
        validation,
        this.value[this.parentBinding][this.bindingObject]
      );

      this.validationInput = !validationCheck.status;
      this.schema.validationInput = this.validationInput;
      if (this.validationInput) {
        this.formText.invalid.text = validationCheck.text;
      }
    }
  }

  initData(): void {
    if (this.schema) {
      this.label = this.schema.label;
      this.labelSubtitle = this.schema.labelSubtitle;
      this.search = this.schema.search;
      this.style = this.schema.style;
      this.rows = this.schema.rows;
      this.styleClass = this.schema.styleClass;
      this.state = this.schema.state;
      this.json = this.schema.json;
      this.placeholder = this.schema.placeholder;
      this.required = this.schema.required;
      this.readonly = this.schema.readonly;
      this.minLength = this.schema.minLength;
      this.maxLength = this.schema.maxLength;
      this.roundedInput = this.schema.roundedInput;
      this.onKeyUpActive = this.schema.onKeyUpActive;
      this.onBlurActive = this.schema.onBlurActive;
      this.append = this.schema.append;
      this.formText = this.schema.formText;
      this.validation = this.schema.validation;
      this.defaultValue = this.schema.defaultValue;
      this.textEditor = this.schema.textEditor;

      if (this.schema.bindingObject) {
        this.bindingObject = this.schema.bindingObject;
      }
      if (this.schema.parentBinding) {
        this.parentBinding = this.schema.parentBinding;
      }

      if (this.value) {
        if (!this.value[this.parentBinding]) {
          this.value[this.parentBinding] = {};
        }
        this.onCallback({
          [this.parentBinding]: this.value[this.parentBinding],
        });
      }
    }
  }

  onKeyUp(event: any): void {
    if (this.onKeyUpActive) {
    }
  }

  onBlur(event: any): void {
    if (this.onBlurActive) {
    }
  }

  onChange(event: any): void {
    if (this.search) {
      this.onCallback(event);
    }
  }

  onCallback(event: any) {
    this.callback.emit(event);
  }

  onCallbackAction(event: any) {
    this.callbackAction.emit(event);
  }
}
