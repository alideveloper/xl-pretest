import {
  Component,
  OnInit,
  Input,
  DoCheck,
  ViewEncapsulation,
  ChangeDetectorRef,
} from '@angular/core';

import { Text } from '@app/shared/types/text';
import { Label } from '@app/shared/types/label';
import { State } from '@app/shared/types/state';

import { GlobalService } from '@app/shared/services/global.service';

@Component({
  selector: 'component-text',
  templateUrl: './text.component.html',
  styleUrls: ['./text.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class TextComponent implements OnInit, DoCheck {
  @Input() label: Label;
  @Input() style: any;
  @Input() styleClass: string;
  @Input() state: State;

  @Input() schema: Text;
  @Input() value: any;

  constructor(public global: GlobalService, public cdRef: ChangeDetectorRef) {}

  ngOnInit(): void {
    this.initData();
  }

  ngDoCheck(): void {
    if (this.schema.state) {
      if (this.schema.state.disabled) {
        this.schema.state.disabled = this.global.disabledConditions(
          this.schema,
          this.value
        );
        this.cdRef.detectChanges();
      }

      this.schema.state.hide = this.global.showHideConditions(
        this.schema,
        this.value
      );
      this.cdRef.detectChanges();
    }
  }

  initData(): void {
    if (this.schema) {
      this.label = this.schema.label;
      this.style = this.schema.style;
      this.styleClass = this.schema.styleClass;
      this.state = this.schema.state;
    }
  }
}
