import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TextComponent } from './text.component';

import { SharedModule } from '@shared/shared.module';

@NgModule({
  declarations: [TextComponent],
  imports: [CommonModule, SharedModule],
  exports: [TextComponent],
  providers: [],
})
export class TextModule {}
