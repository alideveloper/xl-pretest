import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContainerLoopComponent } from './container-loop.component';

@NgModule({
  declarations: [ContainerLoopComponent],
  imports: [CommonModule],
  exports: [ContainerLoopComponent],
  providers: [],
})
export class ContainerLoopModule {}
