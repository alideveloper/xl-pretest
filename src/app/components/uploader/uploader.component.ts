import {
  Component,
  OnInit,
  Input,
  DoCheck,
  ViewEncapsulation,
  Output,
  EventEmitter,
} from '@angular/core';

import { DomSanitizer } from '@angular/platform-browser';

import { Uploader, FormText } from '@app/shared/types/uploader';
import { Label } from '@app/shared/types/label';
import { State } from '@app/shared/types/state';
import { Validation } from '@app/shared/types/validation';

import { GlobalService } from '@app/shared/services/global.service';
import { GlobalValidation } from '@app/shared/services/global.validation';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'component-uploader',
  templateUrl: './uploader.component.html',
  styleUrls: ['./uploader.component.scss'],
})
export class UploaderComponent implements OnInit, DoCheck {
  @Input() label: Label;
  @Input() labelSubtitle: Label;
  @Input() style: any;
  @Input() styleClass: string;
  @Input() state: State;

  @Input() parentBinding: any;
  @Input() bindingObject: any;
  @Input() bindingObjectBlob: any;
  @Input() isDisabled: boolean;
  @Input() minsize: number;
  @Input() maxsize: number;
  @Input() multiple: boolean;
  @Input() animation: boolean;
  @Input() accept: any;
  @Input() formText: FormText;
  @Input() validation: Validation;
  @Input() required: boolean;
  @Input() valid = false;
  @Input() isImage = false;

  @Input() schema: Uploader;
  @Input() value: any;

  @Output() callback: any = new EventEmitter();
  @Output() callbackAction: any = new EventEmitter();

  public uploadedFiles: Array<File> = [];

  validationInput = false;
  resultBase64: any;

  @Output() callbackActionStatic: any = new EventEmitter();

  constructor(
    public global: GlobalService,
    public globalValidation: GlobalValidation,
    public translateService: TranslateService,
    private domSanitizer: DomSanitizer
  ) {}

  ngOnInit(): void {
    this.initData();
  }

  ngDoCheck(): void {
    if (
      this.schema &&
      this.schema.state &&
      this.schema.state.hasOwnProperty('hide') &&
      this.schema.state.hasOwnProperty('hideCondition') &&
      this.schema.state.hideCondition.length > 0
    ) {
      // this.schema.state.hide = this.global.showHideCondition(
      //   this.schema?.state?.hideCondition
      // );
    }

    if (this.validation) {
      const validation = {
        required: this.required,
        ...this.validation,
      };

      const validationCheck = this.globalValidation.checkValidation(
        validation,
        this.value[this.parentBinding][this.bindingObject]
      );

      this.validationInput = !validationCheck.status;
      this.schema.validationInput = this.validationInput;
      if (this.validationInput) {
        this.formText.invalid.text = validationCheck.text;
      }
    }

    if (this.value) {
      this.onValidationUpload(
        this.value[this.parentBinding][this.bindingObject]
      );
    }
  }

  initData(): void {
    if (this.schema) {
      this.label = this.schema.label;
      this.labelSubtitle = this.schema.labelSubtitle;
      this.style = this.schema.style;
      this.styleClass = this.schema.styleClass;
      this.state = this.schema.state;

      this.parentBinding = this.schema.parentBinding;
      this.bindingObject = this.schema.bindingObject;
      this.bindingObjectBlob = this.schema.bindingObjectBlob;
      this.isDisabled = this.schema.isDisabled;
      this.minsize = this.schema.minsize;
      this.maxsize = this.schema.maxsize;
      this.multiple = this.schema.multiple;
      this.animation = this.schema.animation;
      this.accept = this.schema.accept;
      this.required = this.schema.required;
      this.formText = this.schema.formText;
      this.isImage = this.schema.isImage;
      this.validation = this.schema.validation;
    }

    if (this.schema.bindingObject) {
      this.bindingObject = this.schema.bindingObject;
    }
    if (this.schema.parentBinding) {
      this.parentBinding = this.schema.parentBinding;
    }

    if (this.value) {
      if (!this.value[this.parentBinding]) this.value[this.parentBinding] = {};
      this.onCallback({
        [this.parentBinding]: this.value[this.parentBinding],
      });
    }
  }

  onChange(event: any): void {
    if (event && event[0]) {
      this.value[this.parentBinding][this.bindingObjectBlob] = Object.assign(
        {},
        event[0]
      );
      console.log(this.value[this.parentBinding][this.bindingObject]);
      console.log(event[0]);

      const reader = new FileReader();
      reader.onload = this.handleReaderLoaded.bind(this);
      reader.readAsBinaryString(event[0]);

      try {
        event.forEach((item: any) => {
          if (!this.multiple) {
            const final = {
              extension: /[.]/.exec(item.name)
                ? /[^.]+$/.exec(item.name)[0]
                : undefined,
              originalName: item.name,
              fileName: item.name,
              size: item.size,
              fullPath: '',
              downloadURL: '',
              fileType: item.type,
            };

            this.value[this.parentBinding][this.bindingObject] = final;

            const itemCallbackStatic = {
              [this.bindingObject]: final,
              [this.bindingObjectBlob]: event[0],
            };
            this.callbackActionStatic.emit(itemCallbackStatic);
          }
        });
      } catch (error) {}
    } else {
      this.value[this.parentBinding][this.bindingObject] = null;
      this.callbackActionStatic.emit(null);
    }
  }

  handleReaderLoaded(readerEvt: any): void {
    const binaryString = readerEvt.target.result;
    const base64 = btoa(binaryString);
    const base64Upload =
      'data:' +
      this.value[this.parentBinding][this.bindingObject].fileType +
      ';base64,' +
      base64;
    this.resultBase64 = base64Upload;
  }

  onValidationUpload(event: any) {
    if (!event) {
      if (this.formText.invalid?.active) {
        if (this.required) {
          if (this.isImage) {
            this.formText.invalid.text = this.translateService.instant(
              'Upload your Image!'
            );
          } else {
            this.formText.invalid.text = this.translateService.instant(
              'Upload your File/Image!'
            );
          }
        }
      }
      this.validationInput = true;
      this.schema.validationInput = true;
      return;
    }

    if (event && event.size / Math.pow(1024, 2) > this.maxsize) {
      this.validationInput = true;
      this.schema.validationInput = true;
      if (this.formText.invalid?.active) {
        this.formText.invalid.text =
          'More Than ' + (this.maxsize / 1).toFixed(0) + ' MB!';
      }
      return;
    }
    if (event && event.size / Math.pow(1024, 2) < this.minsize) {
      this.validationInput = true;
      this.schema.validationInput = true;
      if (this.formText.invalid?.active) {
        this.formText.invalid.text =
          'Min Than ' + (this.minsize / 1).toFixed(0) + ' MB!';
      }
      return;
    }
    this.validationInput = false;
    this.schema.validationInput = false;
  }

  onCallback(event: any) {
    this.callback.emit(event);
  }

  onCallbackAction(event: any) {
    this.callbackAction.emit(event);
  }
}
