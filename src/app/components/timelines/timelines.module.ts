import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormControlsModule } from '@components/form-controls/form-controls.module';

import { TranslateModule } from '@ngx-translate/core';

import { VerticalTimelineBasicComponent } from './vertical-timeline-basic/vertical-timeline-basic.component';

const exports = [VerticalTimelineBasicComponent];

@NgModule({
  declarations: [...exports],
  imports: [CommonModule, FormControlsModule, TranslateModule.forChild()],
  exports,
})
export class TimelinesModule {}
