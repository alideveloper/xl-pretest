import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  DoCheck,
  AfterViewInit,
} from '@angular/core';

import { Tabs, Tab } from '@app/shared/types/tabs';
import { Label } from '@app/shared/types/label';
import { State } from '@app/shared/types/state';
import { Action } from '@app/shared/types/action';

import { GlobalService } from '@app/shared/services/global.service';
import { GlobalValidation } from '@app/shared/services/global.validation';

@Component({
  selector: 'component-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss'],
})
export class TabsComponent implements OnInit, DoCheck, AfterViewInit {
  @Input() isCard = false;
  @Input() isContainer = true;
  @Input() title: Label;
  @Input() style: any;
  @Input() styleClass: string;
  @Input() positionTabs = 'start';
  @Input() tabsBordered: boolean;
  @Input() tabsClean: boolean;
  @Input() tabs: Tab[];
  @Input() pills: boolean;
  @Input() slide: boolean;

  @Input() schemas: any;
  @Input() schema: Tabs;
  @Input() value: any;
  @Input() values: any;

  @Output() callback: any = new EventEmitter();
  @Output() callbackAction: any = new EventEmitter();

  constructor(
    public global: GlobalService,
    public globalValidation: GlobalValidation
  ) {}

  ngOnInit(): void {
    this.initData();
  }

  ngDoCheck(): void {}

  initData(): void {
    if (this.schema) {
      this.isCard = this.schema.isCard;
      this.title = this.schema.title;
      if (this.schema.hasOwnProperty('isContainer')) {
        this.isContainer = this.schema.isContainer;
      }
      this.style = this.schema.style;
      this.tabsClean = this.schema.tabsClean;
      this.tabsBordered = this.schema.tabsBordered;
      this.styleClass = this.schema.styleClass;
      this.positionTabs = this.schema.positionTabs;
      this.tabs = this.schema.tabs;
      this.pills = this.schema.pills;
      this.slide = this.schema.slide;
    }
  }

  ngAfterViewInit(): void {
    if (this.tabs && this.tabs.length > 0 && this.schema && this.value) {
      const active = this.tabs.filter((itemTab) => itemTab.selected);
      if (active && active.length > 0) {
        this.onCallbackAction({
          type: 'loading',
          status: true,
        });

        setTimeout(() => {
          this.onCallbackAction({
            type: 'loading',
            status: false,
          });

          this.onCallbackAction(active[0].path);
        }, 2000);
      }
    }
  }

  checkDisabledTabs(tab?: any): boolean {
    return false;
  }

  onCallback(event: any) {
    this.callback.emit(event);
  }

  onCallbackAction(event: any) {
    let callback;
    if (event && event.type === 'loading') {
      callback = {
        schema: event,
        value: this.value,
        values: this.values,
      };
      this.callbackAction.emit(callback);
      return;
    }

    let data;
    if (event.constructor.name === 'Array') {
      data = Object.assign([], event);
    } else if (event.constructor.name === 'Object') {
      data = Object.assign({}, event);
    }

    const schema = {
      type: 'changeContent',
      path: event,
      sub: true,
    };

    callback = {
      schema,
      value: data,
      values: this.values,
    };
    this.callbackAction.emit(callback);
  }
}
