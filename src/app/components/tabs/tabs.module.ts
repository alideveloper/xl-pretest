import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TabsComponent } from './tabs.component';

import { SharedModule } from '@shared/shared.module';
import { UtilsModule } from '@app/components/utils/utils.module';

@NgModule({
  declarations: [TabsComponent],
  imports: [CommonModule, SharedModule, UtilsModule],
  exports: [TabsComponent],
  providers: [],
})
export class TabsModule {}
