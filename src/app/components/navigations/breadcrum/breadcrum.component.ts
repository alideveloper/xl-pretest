import {
  Component,
  OnInit,
  Input,
  ElementRef,
  ViewChild,
  ChangeDetectorRef,
  AfterViewInit,
  Output,
  EventEmitter,
  DoCheck,
} from '@angular/core';
import { IconDefinition } from '@fortawesome/free-solid-svg-icons';

import { Breadcrumb } from '@app/shared/types/breadcrumb';

import { GlobalService } from '@app/shared/services/global.service';
import { GlobalValidation } from '@app/shared/services/global.validation';
import { Router } from '@angular/router';

import { environment } from '@environments/environment';

export interface BreadcrumType {
  label: string;
  url?: string | string[];
  path?: string;
}

@Component({
  selector: 'app-breadcrum',
  templateUrl: './breadcrum.component.html',
  styleUrls: ['./breadcrum.component.scss'],
})
export class BreadcrumComponent implements OnInit, DoCheck {
  @Input() items: BreadcrumType[];
  @Input() separator: IconDefinition | string;
  @Input() isMargin: boolean;

  @Input() schemas: any;
  @Input() schema: Breadcrumb;
  @Input() value: any;
  @Input() values: any;

  @Output() callback: any = new EventEmitter();
  @Output() callbackAction: any = new EventEmitter();

  homeURL: any = environment.indexURL.root + environment.indexURL.home;
  constructor(
    public global: GlobalService,
    public globalValidation: GlobalValidation,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.initData();
  }

  ngDoCheck(): void {}

  initData(): void {
    if (this.schema) {
      this.items = this.schema.items;
      this.separator = this.schema.separator;
    }
  }

  onRedirect(path: any) {
    localStorage.setItem(environment.domain + '-path', path);
    this.router.navigateByUrl(path);
  }
}
