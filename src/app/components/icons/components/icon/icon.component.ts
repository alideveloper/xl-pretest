import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BaseComponent } from '@app/core';
import { SizeProp } from '@fortawesome/fontawesome-svg-core';

import {
  faTrashRestore,
  faPlus,
  faPencilAlt,
  faEye,
  faSave,
  faInfo,
  faInfoCircle,
  faGlobe,
  faDesktop,
  faCopy,
  faSquare,
  faSquareFull,
  faList,
  faLayerGroup,
  faUser,
  faUsers,
  faImage,
  faKey,
  faHome,
  faBackward,
  faArrowLeft,
  faArrowCircleLeft,
  faAngleLeft,
  faLaptop,
  faAddressCard,
  faTools,
  faImages,
  faFile,
  faVirus,
  faInbox,
  faPhone,
  faMapSigns,
  faMapMarker,
  faMapMarkedAlt,
  faQuestion,
  faBook,
  faCalendar,
  faNewspaper,
  faStar,
  faVideo,
  faMoneyBill,
  faPiggyBank,
  faBox,
  faBoxOpen,
  faChartLine,
  faChartArea,
  faChartPie,
  faChartBar,
  faPencilRuler,
  faMale,
  faFemale,
  faBuilding,
  faSignOutAlt,
  faArrowRight,
  faClock,
  faFolderOpen,
  faPrint,
  faFilePdf,
  faFileExcel,
  faDownload,
  faWallet,
  faMoneyCheck,
  faBoxes,
  faSearch,
  faQrcode,
  faBarcode,
  faCamera,
} from '@fortawesome/free-solid-svg-icons';
import {
  faComment,
  faThumbsUp,
  faTrashAlt,
  faClosedCaptioning,
} from '@fortawesome/free-regular-svg-icons';
import {
  faFacebook,
  faGoogle,
  faTwitter,
  faLinkedin,
  faPinterest,
  faGit,
  faTumblr,
  faVimeo,
  faYoutube,
  faFlickr,
  faReddit,
  faDribbble,
  faSkype,
  faInstagram,
  faLastfm,
  faSoundcloud,
  faBehance,
  faMedium,
  faSpotify,
  faQuora,
  faXing,
  faSnapchat,
  faTelegram,
  faWhatsapp,
} from '@fortawesome/free-brands-svg-icons';

import { GlobalService } from '@shared/services/global.service';

@Component({
  selector: 'app-icon',
  templateUrl: './icon.component.html',
  styleUrls: ['./icon.component.scss'],
})
export class IconComponent extends BaseComponent implements OnInit {
  @Input()
  icon: any;

  @Input()
  size: SizeProp | 'md';

  @Input() pulse = false;
  @Input() spin = false;

  @Input() schema: any;
  @Input() value: any;
  @Input() values: any;

  iconShow: any;

  listIcon = {
    faLaptop,
    faDesktop,
    faFacebook,
    faPlus,
    faPencilAlt,
    faTrashAlt,
    faEye,
    faClosedCaptioning,
    faSave,
    faInfo,
    faInfoCircle,
    faGlobe,
    faCopy,
    faSquare,
    faSquareFull,
    faList,
    faLayerGroup,
    faUser,
    faKey,
    faImage,
    faHome,
    faBackward,
    faArrowLeft,
    faArrowCircleLeft,
    faAngleLeft,
    faAddressCard,
    faTools,
    faImages,
    faUsers,
    faFile,
    faVirus,
    faInbox,
    faInstagram,
    faPhone,
    faMapSigns,
    faMapMarker,
    faMapMarkedAlt,
    faYoutube,
    faQuestion,
    faBook,
    faCalendar,
    faNewspaper,
    faStar,
    faVideo,
    faMoneyBill,
    faPiggyBank,
    faBox,
    faBoxOpen,
    faChartLine,
    faChartArea,
    faChartPie,
    faChartBar,
    faPencilRuler,
    faMale,
    faFemale,
    faBuilding,
    faSignOutAlt,
    faArrowRight,
    faClock,
    faFolderOpen,
    faPrint,
    faFilePdf,
    faFileExcel,
    faDownload,
    faWallet,
    faMoneyCheck,
    faBoxes,
    faSearch,
    faQrcode,
    faBarcode,
    faCamera,
  };

  @Output() callback: any = new EventEmitter();
  @Output() callbackAction: any = new EventEmitter();

  constructor(public globalService: GlobalService) {
    super();
  }

  ngOnInit() {
    this.initData();
  }

  initData(): void {
    if (this.schema) {
      if (this.schema.binding) {
        this.iconShow = this.listIcon[
          this.globalService.recursiveData(
            this.schema.icon,
            this.value,
            'text',
            true
          )
        ];
      } else {
        this.iconShow = this.listIcon[this.schema.icon];
      }
      this.size = this.schema.size;
      this.icon = this.schema.icon;
      this.pulse = this.schema.pulse;
      this.spin = this.schema.spin;
    } else {
      this.iconShow = this.listIcon[this.icon];
    }
  }
}
