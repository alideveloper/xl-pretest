import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthShellComponent } from './components/auth-shell/auth-shell.component';
import { AuthLoginComponent } from './components/auth-login/auth-login.component';

import { ForgotPasswordComponent } from './components/auth-login/forgot-password/forgot-password.component';
import { AuthRegisterComponent } from './components/auth-register/auth-register.component';

import { extract } from '@app/core';

import { AuthNonGuard } from '@app/shared/services/auth/guard/auth-non.guard';

import { environment } from '@environments/environment';
const routes: Routes = [
  {
    path: '',
    redirectTo: environment.indexURL.rootRoute + environment.indexURL.login,
    pathMatch: 'full',
  },
  {
    path: environment.indexURL.rootRoute,
    redirectTo: environment.indexURL.rootRoute + environment.indexURL.login,
    pathMatch: 'full',
  },
  {
    path: environment.indexURL.rootRoute,
    component: AuthShellComponent,
    canActivate: [AuthNonGuard],
    children: [
      {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full',
      },
      {
        path: 'login',
        component: AuthLoginComponent,
        data: { title: extract('Login') },
      },
      {
        path: 'register',
        component: AuthRegisterComponent,
        data: { title: extract('Register') },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthRoutingModule {
  static declarations = [
    AuthLoginComponent,
    AuthRegisterComponent,
    AuthShellComponent,
    ForgotPasswordComponent,
  ];
}
