import { Component, OnInit } from '@angular/core';

import { FirebaseService } from '@firebase/firebase.service';

import { BsModalRef } from 'ngx-bootstrap/modal';

import { TranslateService } from '@ngx-translate/core';

import { FormGroup, FormControl, Validators } from '@angular/forms';

import { GlobalAction } from '@app/shared/services/global.action';
import { LoadingService } from '@app/shared/services/loading.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss'],
})
export class ForgotPasswordComponent implements OnInit {
  form: any;

  constructor(
    private _bsForgotPasswordRef: BsModalRef,
    private _translateService?: TranslateService,
    private _firebaseService?: FirebaseService,
    private _globalAction?: GlobalAction,
    private _loadingService?: LoadingService
  ) {}

  async ngOnInit(): Promise<any> {
    this.initForm();
  }

  initForm() {
    this.form = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
    });
  }

  async resetPassword() {
    this._loadingService.loading.isLoading = true;

    this._firebaseService.resetPassword(this.form.get('email').value).then(
      async (result) => {
        console.log(result);
        this._globalAction.showToast({
          title: 'Info',
          type: 'success',
          message: await this._translateService.instant(
            'The Reset Password link is sent via your Email'
          ),
        });

        this._loadingService.loading.isLoading = false;
        this.close();
      },
      async (error) => {
        console.log(error);
        let message = error.message;
        if (error.code === 'auth/user-not-found') {
          message = await this._translateService.instant(
            'Email not registered!'
          );
        }
        this._globalAction.showToast({
          title: 'Info',
          type: 'error',
          message,
        });

        this._loadingService.loading.isLoading = false;
      }
    );
  }

  async close(): Promise<any> {
    this._bsForgotPasswordRef.hide();
  }
}
