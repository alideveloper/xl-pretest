import { Component, OnInit } from '@angular/core';
import { Router, Params } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { BaseFormComponent } from '@app/core';
import { RedirectService } from '@app/core/services/redirect.service';

import { GlobalAction } from '@app/shared/services/global.action';
import { LoadingService } from '@app/shared/services/loading.service';
import { CredentialsService } from '@app/shared/services/auth/guard/credentials.service';

import { FirebaseService } from '@app/@firebase/firebase.service';

import { TranslateService } from '@ngx-translate/core';

import { environment } from '@environments/environment';

import { AppService } from '@app/app.service';
import { FirebaseConfigModel } from '@app/shared/model/other/firebase-config.model';
import { Site } from '@app/shared/model/panel/site.model';
import { User } from '@app/shared/model/user.model';

const moment = require('moment');
moment();

@Component({
  selector: 'app-auth-login',
  templateUrl: './auth-login.component.html',
  styleUrls: ['./auth-login.component.scss'],
})
export class AuthLoginComponent extends BaseFormComponent implements OnInit {
  site: Site;

  value: any = {
    form: {
      rememberMe: false,
    },
  };

  rememberMe: any = false;

  messageError: any;

  loginRole: any;

  user: User;
  role: any;

  projectName: any = environment.projectId;

  form: FormGroup;

  isCheckEmail: boolean;

  constructor(
    private _router: Router,
    private _redirect: RedirectService,
    private _globalAction?: GlobalAction,
    private _firebaseService?: FirebaseService,
    private _translateService?: TranslateService,
    private _credentialsService?: CredentialsService,
    private _loadingService?: LoadingService,
    private _appService?: AppService
  ) {
    super();
    this.isLoading = false;
  }

  ngOnInit() {
    this.initData();
    this.initForm();
  }

  initData() {
    this.site = JSON.parse(localStorage.getItem(environment.domain + '-site'));
  }

  initForm() {
    this.form = new FormGroup({
      email: new FormControl('', [
        Validators.required,
        Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$'),
      ]),
      password: new FormControl('', [Validators.required]),
      rememberMe: new FormControl(false),
    });

    let rememberMePayload: any = localStorage.getItem(
      environment.domain + '-LoginRememberMe'
    );

    if (rememberMePayload) {
      rememberMePayload = JSON.parse(rememberMePayload);
      this.form.get('email').setValue(rememberMePayload.email);
      this.form.get('password').setValue(rememberMePayload.password);
      this.form.get('rememberMe').setValue(true);
    }
  }

  redirect(params: Params) {
    if (params.redirect) {
      this._redirect.to(params.redirect, { replaceUrl: true });
    } else {
      this._redirect.toHome();
    }
  }

  async onLogin(): Promise<any> {
    this._loadingService.loading.isLoading = true;

    await this.getUsersByEmail();

    if (this.user) {
      localStorage.setItem(environment.domain + '-LoginRole', this.loginRole);
      localStorage.setItem(
        environment.domain + '-Login',
        JSON.stringify(this.user)
      );
      sessionStorage.setItem(
        environment.domain + '-Login',
        JSON.stringify(this.user)
      );

      console.log('getCompanies');
      await this._appService.getCompanies();
      this.messageError = null;

      this._loadingService.loading.isLoading = false;

      this._credentialsService.setCredentials();

      if (this.form.get('rememberMe').value) {
        const payloadRememberMe = {
          email: this.form.get('email').value,
          password: this.form.get('password').value,
        };
        localStorage.setItem(
          environment.domain + '-LoginRememberMe',
          JSON.stringify(payloadRememberMe)
        );
      } else {
        localStorage.removeItem(environment.domain + '-LoginRememberMe');
      }

      this._router.navigate(['/home'], { replaceUrl: true });
      this._globalAction.showToast({
        title: 'Info',
        type: 'success',
        message: this._translateService.instant('LOGIN_SUCCESS'),
      });
      return;
    }

    this.messageError = this._translateService.instant(
      'The email or password entered is not incorrect!' + '!'
    );
    this._loadingService.loading.isLoading = false;
  }

  async getUsersByEmail(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        const configFirebase: FirebaseConfigModel = {
          collection: {
            status: true,
            name: 'globals',
            document: {
              status: true,
              name: 'Users',
              collection: {
                status: true,
                name: 'users',
                document: {
                  status: false,
                  name: null,
                  document: null,
                },
                orderBy: {
                  object: 'created',
                  order: 'desc',
                },
                query: [
                  {
                    object: 'email',
                    condition: '==',
                    value: this.form.get('email').value,
                  },
                ],
              },
            },
          },
        };

        const data: any = await this._firebaseService.handleCollection(
          null,
          configFirebase
        );
        data.subscribe(async (response: User[]) => {
          if (response && response.length > 0) {
            await this._firebaseService
              .loginEmail(
                this.form.get('email').value,
                this.form.get('password').value
              )
              .then(
                (resultLogin) => {
                  this.loginRole = 'User';
                  this.user = response[0];

                  resolve(response[0]);
                  return;
                },
                (errorLogin) => {
                  console.log(errorLogin);
                  resolve(false);
                }
              );
          } else {
            resolve(false);
          }
          resolve(false);
        });
      } catch (error) {
        resolve(false);
      }
    });
  }

  async updateUser(emailVerified?: boolean): Promise<any> {
    try {
      this.isCheckEmail = true;

      const firebaseConfig: FirebaseConfigModel = {
        method: 'PUT',
        collection: {
          status: true,
          name: 'globals',
          document: {
            status: true,
            name: 'Users',
            collection: {
              status: true,
              name: 'users',
              document: {
                status: false,
                name: null,
                document: null,
              },
              orderBy: {
                object: 'created',
                order: 'desc',
              },
              query: [],
            },
          },
        },
      };

      const payload: User = {};
      payload.key = this.user.key;
      payload.lastLogin = this._firebaseService.timestampPost;
      if (emailVerified && !this.user.emailVerified) {
        payload.emailVerified = true;
      }

      this._loadingService.loading.isLoading = true;
      this._firebaseService
        .updateCollection(null, firebaseConfig, payload)
        .then(
          async (result: any) => {
            this._loadingService.loading.isLoading = false;
            await this.getUsersByEmailByKey();
            localStorage.setItem(
              environment.domain + '-Login',
              JSON.stringify(this.user)
            );
          },
          (error: any) => {
            console.log(error);
          }
        );
    } catch (error) {
      return true;
    }
  }

  async getUsersByEmailByKey(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        const configFirebase: FirebaseConfigModel = {
          collection: {
            status: true,
            name: 'globals',
            document: {
              status: true,
              name: 'Users',
              collection: {
                status: true,
                name: 'users',
                document: {
                  status: false,
                  name: null,
                  document: null,
                },
                orderBy: {
                  object: 'created',
                  order: 'desc',
                },
                query: [
                  {
                    object: 'email',
                    condition: '==',
                    value: this.user.email,
                  },
                ],
              },
            },
          },
        };

        const data: any = await this._firebaseService.handleCollection(
          null,
          configFirebase
        );
        data.subscribe(async (response: User[]) => {
          if (response && response.length > 0) {
            this.user = response[0];
            resolve(response[0]);
            return;
          }
          resolve(false);
        });
      } catch (error) {
        resolve(false);
      }
    });
  }

  forgotPassword(): void {
    this._globalAction.openModalForgotPasswordStatic(
      'modal-md modal-dialog-centered',
      ''
    );
  }

  signUp() {
    this._router.navigateByUrl('/register');
  }
}
