import { Component, OnInit } from '@angular/core';

import { GlobalConstants } from '@app/shared/constant/global.constant';

import { UnsanitizePipe } from '@app/shared/pipes/unsanitize.pipe';

import { environment } from '@environments/environment';
import { Site } from '@app/shared/model/panel/site.model';
import { Loading } from '@app/shared/model/other/loading.model';

@Component({
  selector: 'app-auth-shell',
  templateUrl: './auth-shell.component.html',
  styleUrls: ['./auth-shell.component.scss'],
})
export class AuthShellComponent implements OnInit {
  site: Site;

  loading: Loading;

  constructor(public unsanitizePipe: UnsanitizePipe) {}

  ngOnInit() {
    this.getLoading();
    this.initData();
  }

  initData() {
    this.site = JSON.parse(localStorage.getItem(environment.domain + '-site'));
  }

  getLoading() {
    this.loading = GlobalConstants.loading;
  }
}
