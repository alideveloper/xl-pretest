import { Component, OnInit } from '@angular/core';
import { BaseFormComponent } from '@app/core';
import { Router, Params } from '@angular/router';

import { GlobalAction } from '@app/shared/services/global.action';

import { FirebaseService } from '@app/@firebase/firebase.service';

import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';

import { RedirectService } from '@app/core/services/redirect.service';

import { environment } from '@environments/environment';

import { CredentialsService } from '@app/shared/services/auth/guard/credentials.service';

import { FormGroup, FormControl, Validators } from '@angular/forms';

const moment = require('moment');
moment();
import { LoadingService } from '@app/shared/services/loading.service';
import { FirebaseConfigModel } from '@app/shared/model/other/firebase-config.model';
import { User } from '@app/shared/model/user.model';
import { Site } from '@app/shared/model/panel/site.model';
import { Business } from '@app/shared/model/business.model';
import { AppService } from '@app/app.service';

@Component({
  selector: 'app-auth-register',
  templateUrl: './auth-register.component.html',
  styleUrls: ['./auth-register.component.scss'],
})
export class AuthRegisterComponent extends BaseFormComponent implements OnInit {
  site: Site;

  value: any = {
    form: {},
  };

  rememberMe: any = false;
  messageError: any;

  registerRole: any;

  user: User;

  projectName: any = environment.projectId;

  form: FormGroup;
  formAddressed: FormGroup;

  numbersOnly = /^[0-9]*$/;

  step: any = 1;

  schemaMaps: any = {
    component: 'maps',
    position: 'body',
    style: {},
    styleClass: 'maps-md',
    label: {
      active: true,
      binding: false,
      style: {},
      styleClass: '',
      text: 'Select Maps',
      type: 'text',
      state: {
        hide: false,
        disabled: false,
        hideCondition: [],
        disabledCondition: [],
      },
    },
    labelSubtitle: {
      active: false,
    },
    search: {
      active: true,
      placeholder: 'Search Address',
    },
    address: {
      active: true,
      bindingObject: 'address',
    },
    zoom: 13,
    minZoom: 5,
    maxZoom: 20,
    parentBinding: 'form',
    latitude: 'latitude',
    longitude: 'longitude',
    callbackMaps: {
      latitude: 'latitude',
      longitude: 'longitude',
      accuracy: 'accuracy',
      altitude: 'altitude',
      altitudeAccuracy: 'altitudeAccuracy',
      heading: 'heading',
      speed: 'speed',
    },
  };

  business: any;

  constructor(
    private _router: Router,
    private _redirect: RedirectService,
    private _globalAction?: GlobalAction,
    private _firebaseService?: FirebaseService,
    private _translateService?: TranslateService,
    private _toastr?: ToastrService,
    private _credentialsService?: CredentialsService,
    private _loadingService?: LoadingService,
    private _appService?: AppService
  ) {
    super();
    this.isLoading = false;
  }

  ngOnInit() {
    this.initData();
    this.initForm();
  }

  initData() {
    this.site = JSON.parse(localStorage.getItem(environment.domain + '-site'));
  }

  initForm() {
    this.form = new FormGroup({
      email: new FormControl('', [
        Validators.required,
        Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$'),
      ]),
      password: new FormControl('', [Validators.required]),
      passwordRepeat: new FormControl('', [Validators.required]),
      displayName: new FormControl('', [
        Validators.required,
        Validators.minLength(3),
      ]),
      phoneNumber: new FormControl('', [
        Validators.required,
        Validators.minLength(11),
      ]),
    });

    this.formAddressed = new FormGroup({
      address: new FormControl('', [
        Validators.required,
        Validators.minLength(3),
      ]),
      addressDetail: new FormControl('', [Validators.required]),
      addressed: new FormControl('', [Validators.required]),
      country: new FormControl('', [Validators.required]),
    });
  }

  redirect(params: Params) {
    if (params.redirect) {
      this._redirect.to(params.redirect, { replaceUrl: true });
    } else {
      this._redirect.toHome();
    }
  }

  onChangeToken(event: any) {}

  async registerToken(): Promise<any> {
    this._loadingService.loading.isLoading = true;

    if (this.user) {
      localStorage.setItem(
        environment.domain + '-LoginRole',
        this.registerRole
      );
      localStorage.setItem(
        environment.domain + '-Login',
        JSON.stringify(this.user)
      );
      sessionStorage.setItem(
        environment.domain + '-Login',
        JSON.stringify(this.user)
      );
      this.messageError = null;
      this._loadingService.loading.isLoading = false;

      this._credentialsService.setCredentials();

      this._router.navigate(['/home'], { replaceUrl: true });
      this._globalAction.showToast({
        title: 'Info',
        type: 'success',
        message: this._translateService.instant('LOGIN_SUCCESS'),
      });
      return;
    }

    this._loadingService.loading.isLoading = false;
  }

  forgotToken(): void {
    this._globalAction.openModalForgotPasswordStatic(
      'modal-md modal-dialog-centered',
      ''
    );
  }

  signIn() {
    this._router.navigateByUrl('/login');
  }

  async getUserByEmail(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const configFirebase: FirebaseConfigModel = {
        collection: {
          status: true,
          name: 'globals',
          document: {
            status: true,
            name: 'Users',
            collection: {
              status: true,
              name: 'users',
              document: {
                status: false,
                name: null,
                document: null,
              },
              orderBy: {
                object: 'created',
                order: 'desc',
              },
              query: [
                {
                  object: 'email',
                  condition: '==',
                  value: this.form.get('email').value.toLowerCase(),
                },
              ],
            },
          },
        },
      };

      const data: any = await this._firebaseService.handleCollection(
        null,
        configFirebase
      );
      data.subscribe(
        async (response: User[]) => {
          if (response && response.length > 0) {
            this.user = response[0];
            resolve(response[0]);
            return response[0];
          }
          resolve(false);
        },
        (error: any) => {
          console.log(error);
        }
      );
    });
  }

  callbackMaps(event: any) {
    console.log(event);
    this.formAddressed.get('address').setValue(event.address);
    this.formAddressed.get('addressed').setValue(event);
  }

  callbackCountry(event: any) {
    console.log('callbackCountry', event);
    this.formAddressed.get('country').setValue(event);
  }

  async registerNextStep() {
    this.step++;
  }

  registerBackStep() {
    this.step--;
  }

  async addBusiness(userKey: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const firebaseConfig: FirebaseConfigModel = {
        method: 'POST',
        collection: {
          status: true,
          name: 'apps',
          document: {
            status: true,
            name: 'Database',
            collection: {
              status: true,
              name: 'business',
              document: {
                status: false,
                name: null,
                document: null,
              },
              orderBy: {
                object: 'created',
                order: 'desc',
              },
              query: [],
            },
          },
        },
      };

      const payload: Business = {
        businessName: 'Personal',
        businessDescription: 'Personal',
        logo: null,
        address: null,
        addressDetail: null,
        accuracy: null,
        altitude: null,
        altitudeAccuracy: null,
        latitude: null,
        longitude: null,
        speed: null,
        userKey,
        currency: {
          align: 'right',
          allowNegative: false,
          decimal: ',',
          precision: 2,
          prefix: 'Rp',
          suffix: '',
          thousands: '.',
          nullable: false,
        },
      };

      this._loadingService.loading.isLoading = true;
      this._firebaseService.addCollection(null, firebaseConfig, payload).then(
        (result: any) => {
          this._loadingService.loading.isLoading = false;
          this._globalAction.showToast({
            title: this._translateService.instant('Info!'),
            message: this._translateService.instant(
              'Successfully added Business!'
            ),
            type: 'success',
          });
          this.business = true;
          resolve(true);
        },
        (error: any) => {
          console.log(error);
          this._loadingService.loading.isLoading = false;
          this._globalAction.showToast({
            title: this._translateService.instant('Info!'),
            message: error,
            type: 'error',
          });
          this.business = false;
          resolve(false);
        }
      );
    });
  }

  async finish() {
    const payload: User = {
      email: this.form.get('email').value.toLowerCase(),
      emailVerified: false,
      displayName: this.form.get('displayName').value,
      phoneNumber: this.form.get('phoneNumber').value,
      currency: {
        align: 'right',
        allowNegative: false,
        decimal: ',',
        precision: 2,
        prefix: 'Rp',
        suffix: '',
        thousands: '.',
        nullable: false,
      },
      address: this.formAddressed.get('address').value,
      addressDetail: this.formAddressed.get('addressDetail').value,
      accuracy: this.formAddressed.get('addressed').value.accuracy,
      altitude: this.formAddressed.get('addressed').value.altitude,
      altitudeAccuracy: this.formAddressed.get('addressed').value
        .altitudeAccuracy,
      latitude: this.formAddressed.get('addressed').value.latitude,
      longitude: this.formAddressed.get('addressed').value.longitude,
      speed: this.formAddressed.get('addressed').value.speed,
      country: this.formAddressed.get('country').value,
      key: this._firebaseService.customKey,
      created: this._firebaseService.timestampPost,
      updated: this._firebaseService.timestampPost,
      lastLogin: this._firebaseService.timestampPost,
      lastLogout: this._firebaseService.timestampPost,
      roleId: 'User',
    };

    await this.addBusiness(payload.key);

    if (this.business) {
      const firebaseConfig: FirebaseConfigModel = {
        method: 'POST',
        customKey: 'key',
        collection: {
          status: true,
          name: 'globals',
          document: {
            status: true,
            name: 'Users',
            collection: {
              status: true,
              name: 'users',
              document: {
                status: false,
                name: null,
                document: null,
              },
              orderBy: {
                object: 'created',
                order: 'desc',
              },
              query: [],
            },
          },
        },
      };

      console.log('payload', payload);

      this._loadingService.loading.isLoading = true;
      this._firebaseService
        .addCollection(null, firebaseConfig, payload, payload.key)
        .then(
          async (result: any) => {
            this._firebaseService
              .signUpEmail(payload.email, this.form.get('password').value)
              .then(
                async (resultSignUp) => {
                  await this._firebaseService.emailVerified();
                  await this.getUserByEmail();
                  this._loadingService.loading.isLoading = false;
                  this._globalAction.showToast({
                    title: this._translateService.instant('Info!'),
                    message: this._translateService.instant(
                      'Successfully Register!'
                    ),
                    type: 'success',
                  });

                  this.registerRole = 'User';
                  localStorage.setItem(
                    environment.domain + '-LoginRole',
                    this.registerRole
                  );
                  localStorage.setItem(
                    environment.domain + '-Login',
                    JSON.stringify(this.user)
                  );
                  sessionStorage.setItem(
                    environment.domain + '-Login',
                    JSON.stringify(this.user)
                  );
                  this.messageError = null;

                  await this._appService.getCompanies();

                  this._credentialsService.setCredentials();

                  this._router.navigate(['/home'], { replaceUrl: true });
                },
                (error) => {
                  console.log(error);
                  this._loadingService.loading.isLoading = false;
                  this._globalAction.showToast({
                    title: this._translateService.instant('Info!'),
                    message: error,
                    type: 'error',
                  });
                }
              );
          },
          (error: any) => {
            console.log(error);
            this._loadingService.loading.isLoading = false;
            this._globalAction.showToast({
              title: this._translateService.instant('Info!'),
              message: error,
              type: 'error',
            });
          }
        );
    }
  }
}
