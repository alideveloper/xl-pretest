import { NgModule } from '@angular/core';
import { SharedModule } from '@app/shared';
import { UnsanitizePipe } from '@app/shared/pipes/unsanitize.pipe';

import { AuthRoutingModule } from './auth-routing.module';

import { TranslateModule } from '@ngx-translate/core';

import { UtilsModule } from '@app/components/utils/utils.module';
import { MapsModule } from '@app/components/maps/maps.module';
import { UploaderModule } from '@app/components/uploader/uploader.module';
import { AlertsModule } from '@app/components/alerts/alerts.module';
import { SelectCountryModule } from '@app/components/select-country/select-country.component.module';
@NgModule({
  declarations: [AuthRoutingModule.declarations],
  imports: [
    SharedModule,
    UtilsModule,
    TranslateModule.forChild(),
    AuthRoutingModule,
    MapsModule,
    UploaderModule,
    AlertsModule,
    SelectCountryModule,
  ],
  providers: [UnsanitizePipe],
})
export class AuthModule {}
