import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutModule } from '@app/layout/layout.module';
import { UtilsModule } from '@app/components/utils/utils.module';

import { ShellComponent } from '@shell/components/shell/shell.component';

@NgModule({
  imports: [CommonModule, UtilsModule, LayoutModule],
  declarations: [ShellComponent],
})
export class ShellModule {}
