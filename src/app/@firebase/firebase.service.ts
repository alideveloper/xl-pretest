import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';

import { map } from 'rxjs/operators';
import * as firebase from 'firebase/app';

import { environment } from '@environments/environment';

import 'firebase/storage';
import { FirebaseConfigModel } from '@app/shared/model/other/firebase-config.model';
import { User } from '@app/shared/model/user.model';
import { GlobalAction } from '@app/shared/services/global.action';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root',
})
export class FirebaseService {
  project = environment.projectId;
  user: User;

  constructor(
    private _angularFirestore: AngularFirestore,
    private _AngularFireAuth: AngularFireAuth,
    private router: Router,
    private _globalAction: GlobalAction,
    private _translateService: TranslateService
  ) {}

  get timestampPost(): any {
    return firebase.firestore.FieldValue.serverTimestamp();
  }

  get timestamp(): any {
    return firebase.firestore.Timestamp;
  }

  get customKey(): any {
    return this._angularFirestore.createId();
  }

  randomName(type: any, length: any): string {
    let charset = '';
    let randomName = '';

    const numeric = '0123456789';
    const alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLNMOPQRSTUPWXYZ';
    const alphaNumberic = numeric + alphabet;

    if (type === 'numeric') {
      charset = numeric;
    } else if (type === 'alphabet') {
      charset = alphabet;
    } else if (type === 'alphaNumeric') {
      charset = alphaNumberic;
    }

    for (let i = 0; i < length; i++) {
      randomName += charset.charAt(Math.floor(Math.random() * charset.length));
    }
    return randomName;
  }

  getDownloadURL(iRef: any): any {
    return new Promise((resolve, reject) => {
      iRef.getDownloadURL().then((url: any) => {
        resolve(url);
      });
    });
  }

  deleteStorageDirectoryFirebase(fullPath: any): any {
    return new Promise(async (resolve, reject) => {
      console.log('deleteStorageDirectoryFirebase fullPath', fullPath);
      const deleteStorage = await firebase
        .storage()
        .ref(fullPath)
        .listAll()
        .then((dir: any) => {
          console.log('dir', dir);
          dir.items.forEach((fileRef) => {
            console.log('fileRef', fileRef);
            this.deleteStorageFirebase(fileRef.fullPath);
          });
          dir.prefixes.forEach((folderRef) => {
            console.log('folderRef', folderRef);
            this.deleteStorageDirectoryFirebase(folderRef.fullPath);
          });
        });
    });
  }

  deleteStorageFirebase(fullPath: any): any {
    return new Promise(async (resolve, reject) => {
      console.log('fullPath', fullPath);
      const deleteStorage = await firebase
        .storage()
        .ref(fullPath)
        .delete()
        .then((result: any) => {
          resolve({ status: false });
          return { status: true };
        })
        .catch((error: any) => {
          console.log('error', error);
          resolve({ status: false });
          return { status: false };
        });
    });
  }

  uploadStorageFirebase(
    path: any,
    file: any,
    storageName: any,
    deletePath?: any
  ): any {
    return new Promise((resolve, reject) => {
      const storageRef = firebase.storage().ref();
      const fullPath = `${path}/${storageName}`;
      const iRef = storageRef.child(fullPath);
      const uploadTask = iRef.put(file);

      uploadTask.on(
        'state_changed',
        (snapshot) => {
          switch (snapshot.state) {
            case firebase.storage.TaskState.PAUSED:
              break;
            case firebase.storage.TaskState.RUNNING:
              break;
          }
        },
        (error) => {
          const response = {
            status: false,
            message: error,
          };
          resolve(response);
        },
        () => {
          this.getDownloadURL(iRef).then((url: any) => {
            const response = {
              status: true,
              fullPath: iRef.fullPath,
              downloadURL: url,
            };

            if (deletePath) {
              this.deleteStorageFirebase(deletePath);
            }
            resolve(response);
          });
        }
      );
    });
  }
  async handleCollection(result: any, item: any, value?: any): Promise<any> {
    if (item.collection.status && !item.collection.document.status) {
      result = await this.getCollection(item.collection.name, (ref: any) =>
        this.handleRefRecursive(ref, item.collection.orderBy, item.query, value)
      );
    } else if (item.collection.status && item.collection.document.status) {
      if (!result) {
        result = await this.getCollections(item.collection.name);
      }
      result = await this.handleDocumentRecursive(
        result,
        item.collection.document
      );
    }
    return result;
  }

  async deleteBatchCollection(
    result: any,
    item: any,
    value?: any,
    batch?: any,
    single?: boolean
  ): Promise<any> {
    try {
      if (item.storageDelete && item.storageDelete.active) {
        if (item.storageDelete.keyPath.constructor.name === 'Array') {
          for (let i = 0; i < item.storageDelete.keyPath.length; i++) {
            const resultStorage = await this.deleteStorageFirebase(
              value[item.storageDelete.keyPath[i]][
                item.storageDelete.bindingPath
              ]
            );
          }
        } else {
          const resultStorage = await this.deleteStorageFirebase(
            value[item.storageDelete.keyPath][item.storageDelete.bindingPath]
          );
        }
      }

      if (item.storageDeleteFullPath) {
        try {
          const resultStorageAll = this.deleteStorageDirectoryFirebase(
            item.storageDeleteFullPath
          );
          console.log('resultStorageAll', resultStorageAll);
        } catch (error) {
          console.log('error', error);
        }
      }
    } catch (error) {
      console.log('error', error);
    }

    if (item.collection.status && !item.collection.document.status) {
      result = await this.getCollection(item.collection.name, (ref: any) =>
        this.handleRefRecursive(ref, item.collection.orderBy, item.query, value)
      );
    } else if (item.collection.status && item.collection.document.status) {
      if (!result) {
        result = await this.getCollections(item.collection.name);
      }
      result = await this.handleDocumentBatchRecursive(
        result,
        item.collection.document,
        value,
        batch,
        single
      );
    }
    return result;
  }

  handleRefRecursive(ref: any, orderBy: any, query: any, value?: any): any {
    if (orderBy && orderBy.object && orderBy.order) {
      ref = ref.orderBy(orderBy.object, orderBy.order);
    }
    ref = this.handleWhereRecursive(ref, query, value);
    return ref;
  }

  handleWhereRecursive(ref: any, query: any, value?: any): any {
    if (query && query.length > 0) {
      query.forEach((queryItem: any) => {
        if (queryItem.bindingValue) {
          if (queryItem.valueParent) {
            ref = ref.where(
              queryItem.object,
              queryItem.condition,
              value[queryItem.valueParent][queryItem.value]
            );
            console.log(value);
            console.log(
              queryItem.object,
              queryItem.condition,
              value[queryItem.valueParent][queryItem.value]
            );
          } else {
            ref = ref.where(
              queryItem.object,
              queryItem.condition,
              value[queryItem.value]
            );
          }
        } else {
          ref = ref.where(
            queryItem.object,
            queryItem.condition,
            queryItem.value
          );
          console.log('queryItem', queryItem);
        }
      });
    }
    return ref;
  }

  async handleCollectionBatchRecursive(
    result: any,
    item: any,
    data?: any,
    batch?: any,
    single?: boolean
  ): Promise<any> {
    console.log('collection recursive', item);
    if (item.document && item.document.status) {
      result = await this.getCollectionRecursive(result, item.name);
      result = await this.handleDocumentBatchRecursive(
        result,
        item.document,
        data,
        batch,
        single
      );
    } else {
      result = await this.getCollectionFinalBatchRecursive(
        result,
        item.name,
        (ref: any) => this.handleRefRecursive(ref, item.orderBy, item.query),
        data,
        batch,
        single
      );
    }
    return result;
  }

  async handleCollectionRecursive(result: any, item: any): Promise<any> {
    console.log('collection recursive', item);
    if (item.document && item.document.status) {
      result = await this.getCollectionRecursive(result, item.name);
      result = await this.handleDocumentRecursive(result, item.document);
    } else {
      result = await this.getCollectionFinalRecursive(
        result,
        item.name,
        (ref: any) => this.handleRefRecursive(ref, item.orderBy, item.query)
      );
    }
    return result;
  }

  async handleDocumentBatchRecursive(
    result: any,
    item: any,
    data?: any,
    batch?: any,
    single?: boolean
  ): Promise<any> {
    console.log('document recursive', item);
    result = await this.getDocuments(
      result,
      item.name === 'PBproject' ? this.project : item.name
    );
    if (item.collection.status) {
      result = await this.handleCollectionBatchRecursive(
        result,
        item.collection,
        data,
        batch,
        single
      );
    }
    return result;
  }

  async handleDocumentRecursive(result: any, item: any): Promise<any> {
    console.log('document recursive', item);
    result = await this.getDocuments(
      result,
      item.name === 'PBproject' ? this.project : item.name
    );
    if (item.collection.status) {
      result = await this.handleCollectionRecursive(result, item.collection);
    }
    return result;
  }

  // Collection
  getCollection<T>(collection: any, queryFn?: any): any {
    return this._angularFirestore
      .collection(collection, queryFn)
      .snapshotChanges()
      .pipe(
        map((actions: any) => {
          return actions.map((a: any) => {
            const data: any = a.payload.doc.data() as T;
            return data;
          });
        })
      );
  }

  // Collection
  getCollectionFinalRecursive<T>(
    result: any,
    collection: any,
    queryFn?: any
  ): any {
    return result
      .collection(collection, queryFn)
      .snapshotChanges()
      .pipe(
        map((actions: any) => {
          return actions.map((a: any) => {
            const data: any = a.payload.doc.data() as T;
            return data;
          });
        })
      );
  }

  getCollectionFinalBatchRecursive<T>(
    result: any,
    collection: any,
    queryFn?: any,
    data?: any,
    batch?: any,
    single?: boolean
  ): any {
    return new Promise(async (resolve, reject) => {
      console.log('SINGLE', single);
      console.log('getCollectionFinalBatchRecursive', result);
      console.log('getCollectionFinalBatchRecursive', collection);
      console.log('getCollectionFinalBatchRecursive', queryFn);
      console.log('getCollectionFinalBatchRecursive', data);
      if (single) {
        console.log('SINGLEMASUK');
        if (queryFn) {
          result = result.collection(collection, queryFn);
        } else {
          result = result.collection(collection);
        }
        batch.delete(result.doc(data.key).ref);
        resolve(batch);
      } else {
        console.log('masuk collection', collection, queryFn);
        result
          .collection(collection, queryFn)
          .get()
          .subscribe(
            (querySnapshot: any) => {
              console.log('querySnapshot', querySnapshot);

              querySnapshot.forEach((doc: any) => {
                console.log('doc', doc);
                batch.delete(doc.ref);
              });
              resolve(batch);
            },
            (error: any) => {
              console.log(error);
              reject(false);
            }
          );
      }
    });
  }

  getCollections(collection: any): any {
    return this._angularFirestore.collection(collection);
  }

  getCollectionBatchRecursive(result: any, collection: any, where?: any): any {
    console.log('collection', collection);
    console.log('where', where);
    if (where) {
      console.log('where masuk', where);
      return result.collection(collection, (ref: any) =>
        this.handleRefRecursive(ref, {}, where)
      );
    } else {
      console.log('tidak where masuk', where);
      return result.collection(collection);
    }
  }

  getCollectionRecursive(result: any, collection: any): any {
    console.log('collection', collection);
    return result.collection(collection);
  }

  getDocuments(result: any, document: any): any {
    console.log('document', document);
    return result.doc(document);
  }

  async addBatchFirebase(
    result: any,
    data: any,
    customKey?: any,
    batch?: any
  ): Promise<void> {
    return new Promise(async (resolve, reject) => {
      const timestamp = this.timestampPost;
      data.created = timestamp;
      data.updated = timestamp;
      data.key = this.customKey;

      if (customKey) {
        data.key = customKey;
      }

      console.log('addBatchFirebase');
      console.log(result.doc(data.key));
      batch.set(result.doc(data.key).ref, data);
      resolve(batch);
    });
  }

  async addFirebase(result: any, data: any, customKey?: any): Promise<void> {
    const timestamp = this.timestampPost;
    data.created = timestamp;
    data.updated = timestamp;
    data.key = this.customKey;

    if (customKey) {
      data.key = customKey;
    }

    result = result.doc(data.key).set(data);
    return result;
  }

  async updateFirebase(result: any, data: any): Promise<void> {
    const timestamp = this.timestampPost;
    data.updated = timestamp;

    result = result.doc(data.key).update(data);
    return result;
  }

  async updateBatchFirebase(result: any, data: any, batch: any): Promise<void> {
    return new Promise(async (resolve, reject) => {
      const timestamp = this.timestampPost;
      data.updated = timestamp;

      result.get().subscribe(
        (querySnapshot: any) => {
          console.log('querySnapshot', querySnapshot);

          querySnapshot.forEach((doc: any) => {
            console.log('doc.ref', doc.ref);
            console.log('data', data);
            batch.update(doc.ref, data);
          });
          resolve(batch);
        },
        (error: any) => {
          console.log('error', error);
          reject(false);
        }
      );
    });
  }

  async deleteFirebase(result: any, data: any): Promise<void> {
    result = result.doc(data.key).delete();
    return result;
  }

  async itemRecursiveDocument(item?: any): Promise<void> {
    if (item.document && item.document.status) {
      return item.document;
    }
  }

  async itemRecursiveCollection(item?: any): Promise<void> {
    if (item.collection && item.collection.status) {
      return item.collection;
    }
  }

  async addAndUpdateCollectionBatchRecursive(
    result: any,
    item: any,
    data: any,
    customKey?: any,
    where?: any
  ): Promise<any> {
    console.log('item', item);
    if (
      item.collection &&
      item.collection.status &&
      result &&
      !item.collection.document.status
    ) {
      console.log('addAndUpdateCollectionRecursive 1', item);
      result = await this.getDocuments(
        result,
        item.name === 'PBproject' ? this.project : item.name
      );
      result = await this.getCollectionBatchRecursive(
        result,
        item.collection.name,
        where
      );
    }

    let recursive = false;
    if (
      item.collection &&
      item.collection.status &&
      item.collection.document.status
    ) {
      console.log('addAndUpdateCollectionRecursive 2', item);
      result = await this.getDocuments(
        result,
        item.collection.document.name === 'PBproject'
          ? this.project
          : item.collection.document.name
      );
      item = await this.itemRecursiveDocument(item.collection);
      if (item && item.status && result && item.collection.status) {
        console.log('addAndUpdateCollectionRecursive 3', item);
        result = await this.getCollectionBatchRecursive(
          result,
          item.collection.name
        );
        item = await this.itemRecursiveCollection(item);
      }
      recursive = true;
    }

    console.log('item 2', item);
    if (item.document && item.document.status && recursive) {
      console.log('addAndUpdateCollectionRecursive 4', item);
      result = await this.addAndUpdateCollectionBatchRecursive(
        result,
        item.document,
        data,
        customKey,
        where
      );
    }
    return result;
  }

  async addAndUpdateCollectionRecursive(
    result: any,
    item: any,
    data: any,
    customKey?: any
  ): Promise<any> {
    console.log('item', item);
    if (
      item.collection &&
      item.collection.status &&
      result &&
      !item.collection.document.status
    ) {
      console.log('addAndUpdateCollectionRecursive 1', item);
      result = await this.getDocuments(
        result,
        item.name === 'PBproject' ? this.project : item.name
      );
      result = await this.getCollectionRecursive(result, item.collection.name);
    }

    let recursive = false;
    if (
      item.collection &&
      item.collection.status &&
      item.collection.document.status
    ) {
      console.log('addAndUpdateCollectionRecursive 2', item);
      result = await this.getDocuments(
        result,
        item.collection.document.name === 'PBproject'
          ? this.project
          : item.collection.document.name
      );
      item = await this.itemRecursiveDocument(item.collection);
      if (item && item.status && result && item.collection.status) {
        console.log('addAndUpdateCollectionRecursive 3', item);
        result = await this.getCollectionRecursive(
          result,
          item.collection.name
        );
        item = await this.itemRecursiveCollection(item);
      }
      recursive = true;
    }

    console.log('item 2', item);
    if (item.document && item.document.status && recursive) {
      console.log('addAndUpdateCollectionRecursive 4', item);
      result = await this.addAndUpdateCollectionRecursive(
        result,
        item.document,
        data,
        customKey
      );
    }
    return result;
  }

  async addCollection(
    result: any,
    item: any,
    data: any,
    customKey?: any,
    allData?: any
  ): Promise<any> {
    const resultAddAndUpdate = await this.addAndUpdateCollection(
      result,
      item,
      data,
      customKey,
      allData
    );

    result = resultAddAndUpdate.result;

    if (result.hasOwnProperty('status') && !result.status) {
      return result;
    }

    customKey = resultAddAndUpdate.customKey;

    const resultFinal = await this.addFirebase(result, data, customKey);
    return {
      status: true,
      result: resultFinal,
    };
  }

  async deleteBatchSubCollection(
    result: any,
    item: any,
    data?: any,
    batch?: any,
    single?: boolean
  ): Promise<any> {
    console.log(
      'deleteBatchSubCollection',
      item.collection.document.collection.document.collection.document
        .collection.name
    );
    result = await this._angularFirestore.collection(item.collection.name);
    item = await this.itemRecursiveDocument(item.collection);
    console.log('name', item.name);
    result = await result.doc(item.name);
    item = await this.itemRecursiveCollection(item);
    console.log('name', item.name);
    result = await result.collection(item.name);
    item = await this.itemRecursiveDocument(item);
    console.log('name', item.name);
    result = await result.doc(item.name);
    item = await this.itemRecursiveCollection(item);
    console.log('name', item.name);
    result = await result.collection(item.name);
    item = await this.itemRecursiveDocument(item);
    console.log('name', item.name);
    result = await result.doc(item.name);
    item = await this.itemRecursiveCollection(item);

    console.log(item);
    console.log('item.name', item.name);
    const resultFinal = await this.getCollectionFinalBatchRecursive(
      result,
      item.name,
      (ref: any) => this.handleRefRecursive(ref, item.orderBy, item.query),
      data,
      batch,
      single
    );
    return resultFinal;
  }

  async addBatchSubCollection(
    result: any,
    item: any,
    data: any,
    customKey?: any,
    allData?: any,
    batch?: any
  ): Promise<any> {
    console.log(
      'addBatchSubCollection',
      item.collection.document.collection.document.collection.document
        .collection.name
    );
    result = await this._angularFirestore.collection(item.collection.name);
    item = await this.itemRecursiveDocument(item.collection);
    console.log('name', item.name);
    result = await result.doc(item.name);
    item = await this.itemRecursiveCollection(item);
    console.log('name', item.name);
    result = await result.collection(item.name);
    item = await this.itemRecursiveDocument(item);
    console.log('name', item.name);
    result = await result.doc(item.name);
    item = await this.itemRecursiveCollection(item);
    console.log('name', item.name);
    result = await result.collection(item.name);
    item = await this.itemRecursiveDocument(item);
    console.log('name', item.name);
    result = await result.doc(item.name);
    item = await this.itemRecursiveCollection(item);
    console.log('name', item.name);
    result = await result.collection(item.name);

    console.log(item);
    const resultFinal = await this.addBatchFirebase(
      result,
      data,
      customKey,
      batch
    );
    return resultFinal;
  }

  async updatedBatchSubCollection(
    result: any,
    item: any,
    data: any,
    customKey?: any,
    allData?: any,
    dataStorage?: any,
    batch?: any,
    where?: any
  ): Promise<any> {
    console.log(
      'updatedBatchSubCollection',
      item.collection.document.collection.document.collection.document
        .collection.name
    );
    result = await this._angularFirestore.collection(item.collection.name);
    item = await this.itemRecursiveDocument(item.collection);
    console.log('name', item.name);
    result = await result.doc(item.name);
    item = await this.itemRecursiveCollection(item);
    console.log('name', item.name);
    result = await result.collection(item.name);
    item = await this.itemRecursiveDocument(item);
    console.log('name', item.name);
    result = await result.doc(item.name);
    item = await this.itemRecursiveCollection(item);
    console.log('name', item.name);
    result = await result.collection(item.name);
    item = await this.itemRecursiveDocument(item);
    console.log('name', item.name);
    result = await result.doc(item.name);
    item = await this.itemRecursiveCollection(item);
    console.log('namefinal', item);
    result = await result.collection(item.name, (ref: any) =>
      this.handleRefRecursive(ref, item.orderBy, item.query)
    );

    console.log(result);
    console.log(data);
    console.log(batch);
    const resultFinal = await this.updateBatchFirebase(result, data, batch);
    return resultFinal;
  }

  async addBatchCollection(
    result: any,
    item: any,
    data: any,
    customKey?: any,
    allData?: any,
    batch?: any
  ): Promise<any> {
    const resultAddAndUpdate = await this.addAndUpdateBatchCollection(
      result,
      item,
      data,
      customKey,
      allData
    );

    result = resultAddAndUpdate.result;

    if (result.hasOwnProperty('status') && !result.status) {
      return result;
    }

    customKey = resultAddAndUpdate.customKey;

    const resultFinal = await this.addBatchFirebase(
      result,
      data,
      customKey,
      batch
    );
    return resultFinal;
  }

  async updateCollection(
    result: any,
    item: any,
    data: any,
    customKey?: any,
    allData?: any,
    dataStorage?: any
  ): Promise<any> {
    const resultAddAndUpdate = await this.addAndUpdateCollection(
      result,
      item,
      data,
      customKey,
      allData,
      dataStorage
    );

    result = resultAddAndUpdate.result;

    if (result.hasOwnProperty('status') && !result.status) {
      return result;
    }

    const resultFinal = await this.updateFirebase(result, data);
    return {
      status: true,
      result: resultFinal,
    };
  }

  async updateBatchCollection(
    result: any,
    item: any,
    data: any,
    customKey?: any,
    allData?: any,
    dataStorage?: any,
    batch?: any,
    where?: any
  ): Promise<any> {
    const resultAddAndUpdate = await this.addAndUpdateBatchCollection(
      result,
      item,
      data,
      customKey,
      allData,
      dataStorage,
      where
    );

    result = resultAddAndUpdate.result;

    if (result.hasOwnProperty('status') && !result.status) {
      return result;
    }

    const resultFinal = await this.updateBatchFirebase(result, data, batch);
    return resultFinal;
  }

  async deleteCollection(
    result: any,
    item: any,
    data: any,
    customKey?: any,
    allData?: any
  ): Promise<any> {
    if (item.storageDelete && item.storageDelete.active) {
      if (item.storageDelete.keyPath.constructor.name === 'Array') {
        for (let i = 0; i < item.storageDelete.keyPath.length; i++) {
          const resultStorage = await this.deleteStorageFirebase(
            data[item.storageDelete.keyPath[i]][item.storageDelete.bindingPath]
          );
        }
      } else {
        const resultStorage = await this.deleteStorageFirebase(
          data[item.storageDelete.keyPath][item.storageDelete.bindingPath]
        );
      }
    }

    if (item.storageDeleteFullPath) {
      try {
        const resultStorageAll = this.deleteStorageDirectoryFirebase(
          item.storageDeleteFullPath
        );
        console.log('resultStorageAll', resultStorageAll);
      } catch (error) {
        console.log('error', error);
      }
    }

    const resultAddAndUpdate = await this.addAndUpdateCollection(
      result,
      item,
      data,
      customKey,
      allData
    );

    result = resultAddAndUpdate.result;

    if (result.hasOwnProperty('status') && !result.status) {
      return result;
    }

    const resultFinal = await this.deleteFirebase(result, data);
    return {
      status: true,
      result: resultFinal,
    };
  }

  async addAndUpdateBatchCollection(
    result: any,
    item: any,
    data: any,
    customKey?: any,
    allData?: any,
    dataStorage?: any,
    where?: any
  ): Promise<any> {
    if (data[customKey]) {
      customKey = data[customKey];
    }

    if (allData && allData.length > 0) {
      const findExistingData = allData.filter((itemData: any) => {
        if (item.allDataKeyObject) {
          if (itemData[item.allDataKeyObject] === data[item.allDataKeyObject]) {
            return itemData;
          }
        } else {
          if (itemData.key === customKey) {
            return itemData;
          }
        }
      });

      if (findExistingData.length > 0) {
        return {
          result: {
            status: false,
            title: 'Info',
            message: item.allDataText ? item.allDataText : 'Existing Data...',
            type: 'info',
          },
        };
      }
    }

    if (item.storageDelete && item.storageDelete.active && dataStorage) {
      const resultStorage = await this.deleteStorageFirebase(
        dataStorage[item.storageDelete.keyPath][item.storageDelete.bindingPath]
      );
    }

    if (item.storage && item.storage.active) {
      if (item.storage.keyUpload.constructor.name === 'Array') {
        for (let i = 0; i < item.storage.keyUpload.length; i++) {
          const randomName =
            this.randomName('alphaNumeric', 8) +
            '.' +
            data[item.storage.keyResult[i]].extension;
          const resultUpload = await this.uploadStorageFirebase(
            item.storage.path,
            data[item.storage.keyUpload[i]],
            randomName,
            item.storage.deletePath
          );

          if (resultUpload.status) {
            data[item.storage.keyResult[i]].downloadURL =
              resultUpload.downloadURL;
            data[item.storage.keyResult[i]].fullPath = resultUpload.fullPath;
            data[item.storage.keyResult[i]].fileName = randomName;
            delete data[item.storage.keyUpload[i]];
          } else {
            return {
              result: {
                status: false,
                title: 'Upload',
                message: 'Failed to Upload...',
                type: 'danger',
              },
            };
          }
        }
      } else {
        const randomName =
          this.randomName('alphaNumeric', 8) +
          '.' +
          data[item.storage.keyResult].extension;
        const resultUpload = await this.uploadStorageFirebase(
          item.storage.path,
          data[item.storage.keyUpload],
          randomName,
          item.storage.deletePath
        );

        if (resultUpload.status) {
          data[item.storage.keyResult].downloadURL = resultUpload.downloadURL;
          data[item.storage.keyResult].fullPath = resultUpload.fullPath;
          data[item.storage.keyResult].fileName = randomName;
          delete data[item.storage.keyUpload];
        } else {
          return {
            result: {
              status: false,
              title: 'Upload',
              message: 'Failed to Upload...',
              type: 'danger',
            },
          };
        }
      }
    }

    if (!result) result = await this.getCollections(item.collection.name);

    if (item.collection.document.status) {
      result = await this.addAndUpdateCollectionBatchRecursive(
        result,
        item,
        data,
        customKey,
        where
      );
    }

    return {
      result,
      customKey,
    };
  }

  async addAndUpdateCollection(
    result: any,
    item: any,
    data: any,
    customKey?: any,
    allData?: any,
    dataStorage?: any
  ): Promise<any> {
    if (data[customKey]) {
      customKey = data[customKey];
    }

    if (allData && allData.length > 0) {
      const findExistingData = allData.filter((itemData: any) => {
        if (item.allDataKeyObject) {
          if (itemData[item.allDataKeyObject] === data[item.allDataKeyObject]) {
            return itemData;
          }
        } else {
          if (itemData.key === customKey) {
            return itemData;
          }
        }
      });

      if (findExistingData.length > 0) {
        return {
          result: {
            status: false,
            title: 'Info',
            message: item.allDataText ? item.allDataText : 'Existing Data...',
            type: 'info',
          },
        };
      }
    }

    if (item.storageDelete && item.storageDelete.active && dataStorage) {
      const resultStorage = await this.deleteStorageFirebase(
        dataStorage[item.storageDelete.keyPath][item.storageDelete.bindingPath]
      );
    }

    if (item.storage && item.storage.active) {
      if (item.storage.keyUpload.constructor.name === 'Array') {
        for (let i = 0; i < item.storage.keyUpload.length; i++) {
          const randomName =
            this.randomName('alphaNumeric', 8) +
            '.' +
            data[item.storage.keyResult[i]].extension;
          const resultUpload = await this.uploadStorageFirebase(
            item.storage.path,
            data[item.storage.keyUpload[i]],
            randomName,
            item.storage.deletePath
          );

          if (resultUpload.status) {
            data[item.storage.keyResult[i]].downloadURL =
              resultUpload.downloadURL;
            data[item.storage.keyResult[i]].fullPath = resultUpload.fullPath;
            data[item.storage.keyResult[i]].fileName = randomName;
            delete data[item.storage.keyUpload[i]];
          } else {
            return {
              result: {
                status: false,
                title: 'Upload',
                message: 'Failed to Upload...',
                type: 'danger',
              },
            };
          }
        }
      } else {
        const randomName =
          this.randomName('alphaNumeric', 8) +
          '.' +
          data[item.storage.keyResult].extension;
        const resultUpload = await this.uploadStorageFirebase(
          item.storage.path,
          data[item.storage.keyUpload],
          randomName,
          item.storage.deletePath
        );

        if (resultUpload.status) {
          data[item.storage.keyResult].downloadURL = resultUpload.downloadURL;
          data[item.storage.keyResult].fullPath = resultUpload.fullPath;
          data[item.storage.keyResult].fileName = randomName;
          delete data[item.storage.keyUpload];
        } else {
          return {
            result: {
              status: false,
              title: 'Upload',
              message: 'Failed to Upload...',
              type: 'danger',
            },
          };
        }
      }
    }

    if (!result) result = await this.getCollections(item.collection.name);

    if (item.collection.document.status) {
      result = await this.addAndUpdateCollectionRecursive(
        result,
        item,
        data,
        customKey
      );
    }

    return {
      result,
      customKey,
    };
  }

  getRoleLogin() {
    const role = localStorage.getItem(environment.domain + '-LoginRole');
    return role;
  }

  async logout(): Promise<any> {
    const role = this.getRoleLogin();

    console.log(role);
    if (role === 'SuperAdmin') {
      await this.logoutEmail();
    }

    return true;
  }

  async loginEmail(email: string, password: string): Promise<any> {
    return await this._AngularFireAuth.signInWithEmailAndPassword(
      email,
      password
    );
  }

  getUserLogin() {
    const user: any = localStorage.getItem(environment.domain + '-Login');
    return JSON.parse(user);
  }

  async logoutEmail(): Promise<any> {
    console.log('LOGOUT EMAIL');
    return await this._AngularFireAuth.signOut();
  }

  async signUpEmail(email: string, password: string): Promise<any> {
    return await this._AngularFireAuth.createUserWithEmailAndPassword(
      email,
      password
    );
  }

  async resetPassword(email: string): Promise<any> {
    return await this._AngularFireAuth.sendPasswordResetEmail(email);
  }

  async emailVerified(): Promise<any> {
    const user = await this._AngularFireAuth.currentUser;
    user.sendEmailVerification();
  }

  async currentUser(): Promise<any> {
    const user = await this._AngularFireAuth.currentUser;
    console.log('current user', user);
    return user;
  }
}
