export const firebaseConfigStaging = {
  apiKey: 'AIzaSyAZntjTI6-oTG0YBFmYZmvr7ciP5bSEi0k',
  authDomain: 'xl-pretest.firebaseapp.com',
  databaseURL: 'https://xl-pretest.firebaseio.com',
  projectId: 'xl-pretest',
  storageBucket: 'xl-pretest.appspot.com',
  messagingSenderId: '982420636195',
  appId: '1:982420636195:web:8ba5a0114ff1d9575a2796',
  measurementId: 'G-9S88ZG13TY',
};

export const firebaseConfigProd = {
  apiKey: 'AIzaSyAZntjTI6-oTG0YBFmYZmvr7ciP5bSEi0k',
  authDomain: 'xl-pretest.firebaseapp.com',
  databaseURL: 'https://xl-pretest.firebaseio.com',
  projectId: 'xl-pretest',
  storageBucket: 'xl-pretest.appspot.com',
  messagingSenderId: '982420636195',
  appId: '1:982420636195:web:8ba5a0114ff1d9575a2796',
  measurementId: 'G-9S88ZG13TY',
};
