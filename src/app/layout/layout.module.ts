import { NgModule } from '@angular/core';
import { SharedModule } from '@app/shared';
import { TimelinesModule } from '@app/components/timelines/timelines.module';
import { AvatarsModule } from '@app/components/avatars/avatars.module';
import { UtilsModule } from '@app/components/utils';
import { ProgressModule } from '@app/components/progress/progress.module';
import { NavigationsModule } from '@app/components/navigations/navigations.module';
import { SelectBusinessModule } from '@app/components/select-business/select-business.module';

// Layout specific components
import { MainBodyComponent } from '@layout/components/main-body/main-body.component';
import { MenuTogglerComponent } from '@layout/components/menu-toggler/menu-toggler.component';
import { HeaderComponent } from '@layout/components/header-components/header/header.component';
import { HeaderSearchComponent } from '@layout/components/header-components/header-search/header-search.component';
import { HeaderMenuAppsComponent } from '@layout/components/header-components/header-menu-apps/header-menu-apps.component';
import { HeaderMenuMessagesComponent } from '@layout/components/header-components/header-menu-messages/header-menu-messages.component';
import { HeaderMenuNotificationsComponent } from '@layout/components/header-components/header-menu-notifications/header-menu-notifications.component';
import { HeaderUserAccountComponent } from '@layout/components/header-components/header-user-account/header-user-account.component';
import { HeaderLanguageComponent } from '@layout/components/header-components/header-language/header-language.component';
import { QuickSidenavComponent } from '@layout/components/quick-sidenav-components/quick-sidenav/quick-sidenav.component';
import { SidenavComponent } from '@layout/components/sidenav-components/sidenav/sidenav.component';
import { FooterComponent } from '@layout/components/footer-components/footer/footer.component';
import { QuickSidenavTasksComponent } from '@layout/components/quick-sidenav-components/quick-sidenav-tasks/quick-sidenav-tasks.component';
import { QuickSidenavContactsComponent } from '@layout/components/quick-sidenav-components/quick-sidenav-contacts/quick-sidenav-contacts.component';
import { QuickSidenavSettingsComponent } from '@layout/components/quick-sidenav-components/quick-sidenav-settings/quick-sidenav-settings.component';
import { SidenavItemComponent } from '@layout/components/sidenav-components/sidenav-item/sidenav-item.component';
import { SidenavLinkComponent } from '@layout/components/sidenav-components/sidenav-link/sidenav-link.component';
import { HeaderToolbarComponent } from '@layout/components/header-components/header-toolbar/header-toolbar.component';
import { HeaderNavbarComponent } from '@layout/components/header-components/header-navbar/header-navbar.component';
import { HeaderNavbarItemComponent } from '@layout/components/header-components/header-navbar-item/header-navbar-item.component';

// Main Layout Components, will be exported to be used in the Shell
import { VerticalLayoutDefaultComponent } from '@layout/vertical/vertical-layout-default/vertical-layout-default.component';
import { HorizontalLayoutDefaultComponent } from '@layout/horizontal/horizontal-layout-default/horizontal-layout-default.component';

const exports = [
  VerticalLayoutDefaultComponent,
  HorizontalLayoutDefaultComponent,
];

@NgModule({
  declarations: [
    ...exports,
    HeaderComponent,
    HeaderSearchComponent,
    HeaderMenuAppsComponent,
    HeaderMenuMessagesComponent,
    HeaderMenuNotificationsComponent,
    HeaderUserAccountComponent,
    HeaderLanguageComponent,
    QuickSidenavComponent,
    SidenavComponent,
    MenuTogglerComponent,
    MainBodyComponent,
    FooterComponent,
    QuickSidenavTasksComponent,
    QuickSidenavContactsComponent,
    QuickSidenavSettingsComponent,
    SidenavItemComponent,
    SidenavLinkComponent,
    HeaderToolbarComponent,
    HeaderNavbarComponent,
    HeaderNavbarItemComponent,
  ],
  imports: [
    SharedModule,
    UtilsModule,
    ProgressModule,
    TimelinesModule,
    AvatarsModule,
    NavigationsModule,
    SelectBusinessModule,
  ],
  exports,
})
export class LayoutModule {}
