import { Component, OnInit } from '@angular/core';
import { BaseLayout } from '@layout/base/base-layout';

import { environment } from '@environments/environment';

import { GlobalAction } from '@app/shared/services/global.action';
import { Loading } from '@app/shared/model/other/loading.model';
import { GlobalConstants } from '@app/shared/constant/global.constant';
import { User } from '@app/shared/model/user.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-horizontal-layout-default',
  templateUrl: './horizontal-layout-default.component.html',
  styleUrls: ['./horizontal-layout-default.component.scss'],
})
export class HorizontalLayoutDefaultComponent extends BaseLayout
  implements OnInit {
  isSidenavCollapsed: boolean;

  loading: Loading;

  layout: any = environment.layout;

  user: User;
  dayLeft: any;
  isTrialAccount: boolean;

  constructor(public globalAction: GlobalAction, private _router: Router) {
    super();
  }

  ngOnInit() {
    console.log('HorizontalLayoutDefaultComponent');
    this.initData();
    this.getLoading();
  }

  getLoading() {
    this.loading = GlobalConstants.loading;
  }

  async initData() {
    this.user = this.globalAction.getUserLogin();
  }

  onSidenavToggled(collapsed: boolean) {
    this.isSidenavCollapsed = collapsed;
  }
}
