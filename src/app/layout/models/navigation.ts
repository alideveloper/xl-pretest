export interface NavigationOptions {
  open?: any;
  title: string;
  link?: string;
  icon?: NavigationIcon;
  level?: number;
  items?: NavigationOptions[];
  target?: string;
}

export interface NavigationIcon {
  name?: any;
  letter?: string;
  size?: string;
}
