import { Injectable } from '@angular/core';

import { environment } from '@environments/environment';

@Injectable({
  providedIn: 'root',
})
export class NavigationService {
  constructor() {}

  async getNavigations(): Promise<any> {
    const menus = JSON.parse(
      localStorage.getItem(environment.domain + '-menus')
    );

    return menus ? menus : [];
  }

  async getNavigationItems(): Promise<any> {
    localStorage.setItem(
      environment.domain + '-menus',
      JSON.stringify(this.defaultNavigation)
    );
  }

  get defaultNavigation() {
    return [
      {
        page: 'IboTN0BIvQmG2RMwUgxg',
        order: 1,
        key: 'xUxa04XjxFXN6YtOYfBx',
        category: 'Dashboard',
        created: {
          seconds: 1599961546,
          nanoseconds: 333000000,
        },
        domain: 'www.xl.ngodings.com',
        icon: 'faHome',
        updated: {
          seconds: 1599961546,
          nanoseconds: 333000000,
        },
        title: 'Dashboard',
        link: '/dashboard',
        pages: {
          name: 'Dashboard',
          type: 'Page Static',
          path: '/dashboard',
          updated: {
            seconds: 1599959779,
            nanoseconds: 198000000,
          },
          created: {
            seconds: 1599959779,
            nanoseconds: 198000000,
          },
          module: 'xl',
          key: 'IboTN0BIvQmG2RMwUgxg',
          domain: 'www.xl.ngodings.com',
        },
        type: 'Page Static',
      },
      {
        created: {
          seconds: 1599961591,
          nanoseconds: 409000000,
        },
        domain: 'www.xl.ngodings.com',
        updated: {
          seconds: 1599961591,
          nanoseconds: 409000000,
        },
        category: 'Master',
        icon: 'faSquareFull',
        order: 2,
        key: 'vs2ooF6eqGETn5k43ftY',
        title: 'Master',
        items: [
          {
            key: 'imJxgNV1bwAhFC6ddS2a',
            name: 'Warehouse',
            page: 'c95Y7NrpnjYUPW1mdcPE',
            updated: {
              seconds: 1599962634,
              nanoseconds: 254000000,
            },
            created: {
              seconds: 1599962087,
              nanoseconds: 41000000,
            },
            category: 'vs2ooF6eqGETn5k43ftY',
            domain: 'www.xl.ngodings.com',
            module: 'xl',
            menusCategory: {
              order: 2,
              domain: 'www.xl.ngodings.com',
              key: 'vs2ooF6eqGETn5k43ftY',
              category: 'Master',
              icon: 'faSquareFull',
              created: {
                seconds: 1599961591,
                nanoseconds: 409000000,
              },
              updated: {
                seconds: 1599961591,
                nanoseconds: 409000000,
              },
            },
            order: 1,
            pages: {
              domain: 'www.xl.ngodings.com',
              module: 'xl',
              path: '/master/warehouse',
              created: {
                seconds: 1599959817,
                nanoseconds: 638000000,
              },
              key: 'c95Y7NrpnjYUPW1mdcPE',
              name: 'Warehouse',
              updated: {
                seconds: 1599959817,
                nanoseconds: 638000000,
              },
              type: 'Page Static',
            },
            active: true,
            title: 'Warehouse',
            project: 'xl',
            collection: 'apps',
            link: '/master/warehouse',
            type: 'Page Static',
          },
          {
            active: true,
            domain: 'www.xl.ngodings.com',
            key: 'aLiiHdMP5vNuPL21A8u7',
            page: 'Z6j782EoRIGT124BKMxK',
            updated: {
              seconds: 1601084017,
              nanoseconds: 457000000,
            },
            order: 2,
            category: 'vs2ooF6eqGETn5k43ftY',
            module: 'xl',
            name: 'Goods Category',
            created: {
              seconds: 1599962107,
              nanoseconds: 803000000,
            },
            pages: {
              path: '/master/category',
              name: 'Goods Category',
              module: 'xl',
              key: 'Z6j782EoRIGT124BKMxK',
              type: 'Page Static',
              updated: {
                seconds: 1601083858,
                nanoseconds: 299000000,
              },
              created: {
                seconds: 1599960580,
                nanoseconds: 889000000,
              },
              domain: 'www.xl.ngodings.com',
            },
            menusCategory: {
              order: 2,
              category: 'Master',
              updated: {
                seconds: 1599961591,
                nanoseconds: 409000000,
              },
              key: 'vs2ooF6eqGETn5k43ftY',
              domain: 'www.xl.ngodings.com',
              icon: 'faSquareFull',
              created: {
                seconds: 1599961591,
                nanoseconds: 409000000,
              },
            },
            title: 'Goods Category',
            project: 'xl',
            collection: 'apps',
            link: '/master/category',
            type: 'Page Static',
          },
          {
            pages: {
              key: '6bk8eQFgYSt5hbSnE3V0',
              type: 'Page Static',
              created: {
                seconds: 1599960597,
                nanoseconds: 509000000,
              },
              updated: {
                seconds: 1601083843,
                nanoseconds: 917000000,
              },
              path: '/master/goods',
              name: 'Goods',
              module: 'xl',
              domain: 'www.xl.ngodings.com',
            },
            updated: {
              seconds: 1601084026,
              nanoseconds: 205000000,
            },
            menusCategory: {
              updated: {
                seconds: 1599961591,
                nanoseconds: 409000000,
              },
              category: 'Master',
              created: {
                seconds: 1599961591,
                nanoseconds: 409000000,
              },
              icon: 'faSquareFull',
              order: 2,
              domain: 'www.xl.ngodings.com',
              key: 'vs2ooF6eqGETn5k43ftY',
            },
            domain: 'www.xl.ngodings.com',
            active: true,
            order: 3,
            key: 'rliose4cOzO1kd15LvpG',
            created: {
              seconds: 1599962124,
              nanoseconds: 261000000,
            },
            name: 'Goods',
            page: '6bk8eQFgYSt5hbSnE3V0',
            module: 'xl',
            category: 'vs2ooF6eqGETn5k43ftY',
            title: 'Goods',
            project: 'xl',
            collection: 'apps',
            link: '/master/goods',
            type: 'Page Static',
          },
          {
            module: 'xl',
            updated: {
              seconds: 1599962293,
              nanoseconds: 977000000,
            },
            domain: 'www.xl.ngodings.com',
            active: true,
            menusCategory: {
              domain: 'www.xl.ngodings.com',
              category: 'xl',
              created: {
                seconds: 1599962036,
                nanoseconds: 615000000,
              },
              key: 'geOfFoTMt5NpGgPd78bv',
              updated: {
                seconds: 1599962036,
                nanoseconds: 615000000,
              },
              icon: 'faBoxes',
              order: 7,
            },
            created: {
              seconds: 1599962163,
              nanoseconds: 18000000,
            },
            order: 4,
            pages: {
              module: 'xl',
              updated: {
                seconds: 1601083822,
                nanoseconds: 996000000,
              },
              path: '/master/warehouse-goods',
              type: 'Page Static',
              name: 'Goods in Warehouse',
              key: '87laVztPDMK0yIbzyiBn',
              domain: 'www.xl.ngodings.com',
              created: {
                seconds: 1599960674,
                nanoseconds: 79000000,
              },
            },
            page: '87laVztPDMK0yIbzyiBn',
            name: 'Goods in Warehouse',
            category: 'vs2ooF6eqGETn5k43ftY',
            key: 'juwIyjnS1aZSKWXvBGQ5',
            title: 'Goods in Warehouse',
            project: 'xl',
            collection: 'apps',
            link: '/master/warehouse-goods',
            type: 'Page Static',
          },
          {
            key: 'XNubyqCADxn5XX12NoSN',
            menusCategory: {
              order: 7,
              category: 'xl',
              icon: 'faBoxes',
              key: 'geOfFoTMt5NpGgPd78bv',
              domain: 'www.xl.ngodings.com',
              updated: {
                seconds: 1599962036,
                nanoseconds: 615000000,
              },
              created: {
                seconds: 1599962036,
                nanoseconds: 615000000,
              },
            },
            active: true,
            category: 'vs2ooF6eqGETn5k43ftY',
            pages: {
              module: 'xl',
              domain: 'www.xl.ngodings.com',
              updated: {
                seconds: 1599960757,
                nanoseconds: 651000000,
              },
              created: {
                seconds: 1599960757,
                nanoseconds: 651000000,
              },
              type: 'Page Static',
              key: 'QD20JSOWuJBxE6UagQca',
              name: 'Supplier',
              path: '/master/supplier',
            },
            order: 5,
            updated: {
              seconds: 1599962300,
              nanoseconds: 825000000,
            },
            name: 'Supplier',
            domain: 'www.xl.ngodings.com',
            module: 'xl',
            page: 'QD20JSOWuJBxE6UagQca',
            created: {
              seconds: 1599962193,
              nanoseconds: 908000000,
            },
            title: 'Supplier',
            project: 'xl',
            collection: 'apps',
            link: '/master/supplier',
            type: 'Page Static',
          },
          {
            name: 'Purchaser',
            menusCategory: {
              icon: 'faSquareFull',
              order: 2,
              domain: 'www.xl.ngodings.com',
              key: 'vs2ooF6eqGETn5k43ftY',
              created: {
                seconds: 1599961591,
                nanoseconds: 409000000,
              },
              category: 'Master',
              updated: {
                seconds: 1599961591,
                nanoseconds: 409000000,
              },
            },
            domain: 'www.xl.ngodings.com',
            category: 'vs2ooF6eqGETn5k43ftY',
            page: 'njtvfHU6cmE3rXsbVAsz',
            key: 'dfdxPODBC5vdIUZF87pa',
            order: 6,
            active: true,
            created: {
              seconds: 1600178931,
              nanoseconds: 282000000,
            },
            pages: {
              module: 'xl',
              key: 'njtvfHU6cmE3rXsbVAsz',
              name: 'Purchaser',
              domain: 'www.xl.ngodings.com',
              created: {
                seconds: 1600178913,
                nanoseconds: 543000000,
              },
              type: 'Page Static',
              updated: {
                seconds: 1600178913,
                nanoseconds: 543000000,
              },
              path: '/master/purchaser',
            },
            module: 'xl',
            updated: {
              seconds: 1600179285,
              nanoseconds: 749000000,
            },
            title: 'Purchaser',
            project: 'xl',
            collection: 'apps',
            link: '/master/purchaser',
            type: 'Page Static',
          },
          {
            name: 'Goods Unit',
            created: {
              seconds: 1600022740,
              nanoseconds: 537000000,
            },
            key: 'L3uBiaoLJU5WcRIAyXhC',
            menusCategory: {
              updated: {
                seconds: 1599961591,
                nanoseconds: 409000000,
              },
              created: {
                seconds: 1599961591,
                nanoseconds: 409000000,
              },
              order: 2,
              category: 'Master',
              icon: 'faSquareFull',
              key: 'vs2ooF6eqGETn5k43ftY',
              domain: 'www.xl.ngodings.com',
            },
            updated: {
              seconds: 1601083992,
              nanoseconds: 134000000,
            },
            domain: 'www.xl.ngodings.com',
            pages: {
              name: 'Goods Unit',
              type: 'Page Static',
              module: 'xl',
              path: '/master/unit',
              key: 'DZlzhZQ3dYpokdYwzz5S',
              domain: 'www.xl.ngodings.com',
              updated: {
                seconds: 1601083760,
                nanoseconds: 433000000,
              },
              created: {
                seconds: 1600022717,
                nanoseconds: 228000000,
              },
            },
            order: 7,
            module: 'xl',
            page: 'DZlzhZQ3dYpokdYwzz5S',
            active: true,
            category: 'vs2ooF6eqGETn5k43ftY',
            title: 'Goods Unit',
            project: 'xl',
            collection: 'apps',
            link: '/master/unit',
            type: 'Page Static',
          },
        ],
      },
      {
        domain: 'www.xl.ngodings.com',
        category: 'Manage Stock',
        icon: 'faBoxes',
        key: 'l91Qbf11DgofnlGPVF4X',
        order: 3,
        created: {
          seconds: 1599961836,
          nanoseconds: 382000000,
        },
        updated: {
          seconds: 1599961867,
          nanoseconds: 460000000,
        },
        title: 'Manage Stock',
        items: [
          {
            domain: 'www.xl.ngodings.com',
            updated: {
              seconds: 1599962528,
              nanoseconds: 819000000,
            },
            module: 'xl',
            key: 'hB5OIbWs8qHJsKXwYSR2',
            page: 'FUew2Hb9kOBZgq6kciFn',
            active: true,
            category: 'l91Qbf11DgofnlGPVF4X',
            name: 'In Stock',
            pages: {
              module: 'xls',
              created: {
                seconds: 1599960817,
                nanoseconds: 387000000,
              },
              updated: {
                seconds: 1601002123,
                nanoseconds: 626000000,
              },
              name: 'In Stock',
              domain: 'www.xl.ngodings.com',
              key: 'FUew2Hb9kOBZgq6kciFn',
              type: 'Page Static',
              path: '/manage/in-stock',
            },
            order: 1,
            created: {
              seconds: 1599962231,
              nanoseconds: 644000000,
            },
            menusCategory: {
              created: {
                seconds: 1599961836,
                nanoseconds: 382000000,
              },
              order: 3,
              key: 'l91Qbf11DgofnlGPVF4X',
              updated: {
                seconds: 1599961867,
                nanoseconds: 460000000,
              },
              category: 'Manage Stock',
              domain: 'www.xl.ngodings.com',
              icon: 'faBoxes',
            },
            title: 'In Stock',
            project: 'xl',
            collection: 'apps',
            link: '/manage/in-stock',
            type: 'Page Static',
          },
          {
            created: {
              seconds: 1599962318,
              nanoseconds: 610000000,
            },
            domain: 'www.xl.ngodings.com',
            menusCategory: {
              key: 'l91Qbf11DgofnlGPVF4X',
              icon: 'faBoxes',
              category: 'Manage Stock',
              domain: 'www.xl.ngodings.com',
              created: {
                seconds: 1599961836,
                nanoseconds: 382000000,
              },
              order: 3,
              updated: {
                seconds: 1599961867,
                nanoseconds: 460000000,
              },
            },
            order: 2,
            name: 'Out Stock',
            category: 'l91Qbf11DgofnlGPVF4X',
            page: 'hj8oMRl5B7K1qsQZ676b',
            pages: {
              name: 'Out Stock',
              type: 'Page Static',
              updated: {
                seconds: 1601002115,
                nanoseconds: 440000000,
              },
              created: {
                seconds: 1599960848,
                nanoseconds: 750000000,
              },
              key: 'hj8oMRl5B7K1qsQZ676b',
              domain: 'www.xl.ngodings.com',
              path: '/manage/out-stock',
              module: 'xls',
            },
            updated: {
              seconds: 1599962596,
              nanoseconds: 149000000,
            },
            module: 'xl',
            active: true,
            key: 'luRC8gb6OrzDnqfSMxIe',
            title: 'Out Stock',
            project: 'xl',
            collection: 'apps',
            link: '/manage/out-stock',
            type: 'Page Static',
          },
        ],
      },
      {
        key: 'PjRcpojGkEFJMh6jq2ik',
        updated: {
          seconds: 1599961898,
          nanoseconds: 398000000,
        },
        category: 'Reporting',
        domain: 'www.xl.ngodings.com',
        order: 4,
        icon: 'faFile',
        created: {
          seconds: 1599961898,
          nanoseconds: 398000000,
        },
        title: 'Reporting',
        items: [
          {
            created: {
              seconds: 1599962756,
              nanoseconds: 60000000,
            },
            menusCategory: {
              order: 4,
              category: 'Reporting',
              created: {
                seconds: 1599961898,
                nanoseconds: 398000000,
              },
              key: 'PjRcpojGkEFJMh6jq2ik',
              domain: 'www.xl.ngodings.com',
              icon: 'faFile',
              updated: {
                seconds: 1599961898,
                nanoseconds: 398000000,
              },
            },
            page: 'RuF460E5ngM1goCKBFPh',
            name: 'Goods in Warehouse',
            module: 'xl',
            category: 'PjRcpojGkEFJMh6jq2ik',
            pages: {
              updated: {
                seconds: 1601083774,
                nanoseconds: 857000000,
              },
              module: 'xl',
              name: 'Goods in Warehouse',
              domain: 'www.xl.ngodings.com',
              created: {
                seconds: 1599960984,
                nanoseconds: 853000000,
              },
              path: '/reporting/warehouse-goods',
              type: 'Page Static',
              key: 'RuF460E5ngM1goCKBFPh',
            },
            domain: 'www.xl.ngodings.com',
            key: 'prFvFOq6qiyL7FY2t7B0',
            active: true,
            updated: {
              seconds: 1601083744,
              nanoseconds: 350000000,
            },
            order: 4,
            title: 'Goods in Warehouse',
            project: 'xl',
            collection: 'apps',
            link: '/reporting/warehouse-goods',
            type: 'Page Static',
          },
          {
            page: 'xvzmktw6fPDUIweJDJpi',
            name: 'In Stock',
            order: 7,
            key: 'AKrwVgAU4xYcQRx67ete',
            menusCategory: {
              domain: 'www.xl.ngodings.com',
              key: 'PjRcpojGkEFJMh6jq2ik',
              created: {
                seconds: 1599961898,
                nanoseconds: 398000000,
              },
              order: 4,
              category: 'Reporting',
              icon: 'faFile',
              updated: {
                seconds: 1599961898,
                nanoseconds: 398000000,
              },
            },
            created: {
              seconds: 1599962832,
              nanoseconds: 129000000,
            },
            active: true,
            category: 'PjRcpojGkEFJMh6jq2ik',
            module: 'xl',
            pages: {
              updated: {
                seconds: 1601002091,
                nanoseconds: 865000000,
              },
              domain: 'www.xl.ngodings.com',
              name: 'In Stock',
              module: 'xl',
              created: {
                seconds: 1599961051,
                nanoseconds: 163000000,
              },
              path: '/reporting/in-stock',
              key: 'xvzmktw6fPDUIweJDJpi',
              type: 'Page Static',
            },
            domain: 'www.xl.ngodings.com',
            updated: {
              seconds: 1600178967,
              nanoseconds: 330000000,
            },
            title: 'In Stock',
            project: 'xl',
            collection: 'apps',
            link: '/reporting/in-stock',
            type: 'Page Static',
          },
          {
            key: 'tHPZhjWOnQQOa05vtpVs',
            domain: 'www.xl.ngodings.com',
            created: {
              seconds: 1599962854,
              nanoseconds: 244000000,
            },
            menusCategory: {
              icon: 'faFile',
              category: 'Reporting',
              updated: {
                seconds: 1599961898,
                nanoseconds: 398000000,
              },
              created: {
                seconds: 1599961898,
                nanoseconds: 398000000,
              },
              domain: 'www.xl.ngodings.com',
              order: 4,
              key: 'PjRcpojGkEFJMh6jq2ik',
            },
            module: 'xl',
            category: 'PjRcpojGkEFJMh6jq2ik',
            page: 'P83M4JhQZGA5zkeQXT0u',
            active: true,
            order: 8,
            updated: {
              seconds: 1600178975,
              nanoseconds: 663000000,
            },
            pages: {
              updated: {
                seconds: 1601002109,
                nanoseconds: 177000000,
              },
              name: 'Out Stock',
              path: '/reporting/out-stock',
              module: 'xl',
              domain: 'www.xl.ngodings.com',
              type: 'Page Static',
              key: 'P83M4JhQZGA5zkeQXT0u',
              created: {
                seconds: 1599961083,
                nanoseconds: 693000000,
              },
            },
            name: 'Out Stock',
            title: 'Out Stock',
            project: 'xl',
            collection: 'apps',
            link: '/reporting/out-stock',
            type: 'Page Static',
          },
        ],
      },
      {
        updated: {
          seconds: 1601643885,
          nanoseconds: 385000000,
        },
        domain: 'www.xl.ngodings.com',
        created: {
          seconds: 1599962010,
          nanoseconds: 300000000,
        },
        key: '17W30IhUqX3Xy9S2QRzn',
        category: 'Settings',
        icon: 'faTools',
        order: 7,
        title: 'Settings',
        items: [
          {
            name: 'Profile',
            category: '17W30IhUqX3Xy9S2QRzn',
            page: '2jNRm8bhPzmX5sGmhEt9',
            active: true,
            domain: 'www.xl.ngodings.com',
            key: 'qWt1nUGHfszLs3OP8eyU',
            module: 'xl',
            order: 1,
            updated: {
              seconds: 1599962369,
              nanoseconds: 105000000,
            },
            created: {
              seconds: 1599962369,
              nanoseconds: 105000000,
            },
            title: 'Profile',
            project: 'xl',
            collection: 'apps',
            link: '/settings/profile',
            pages: {
              created: {
                seconds: 1599961195,
                nanoseconds: 218000000,
              },
              type: 'Page Static',
              domain: 'www.xl.ngodings.com',
              updated: {
                seconds: 1599961195,
                nanoseconds: 218000000,
              },
              module: 'xl',
              key: '2jNRm8bhPzmX5sGmhEt9',
              name: 'Profile',
              path: '/settings/profile',
            },
            type: 'Page Static',
          },
        ],
      },
    ];
  }
}
