import { Component, OnInit, Input } from '@angular/core';
import { I18nService } from '@app/core';
import { AuthenticationService, BaseComponent } from '@app/core';
import { Router } from '@angular/router';

import { AuthService } from '@app/shared/services/auth/services/auth.service';

import { environment } from '@environments/environment';

@Component({
  selector: 'app-header-language',
  templateUrl: './header-language.component.html',
  styleUrls: ['./header-language.component.scss'],
})
export class HeaderLanguageComponent extends BaseComponent implements OnInit {
  user: any;

  @Input()
  dark: boolean;

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router,
    private authService: AuthService,
    private i18nService: I18nService
  ) {
    super();
  }

  ngOnInit() {
    this.initData();
  }

  initData() {
    this.user = JSON.parse(localStorage.getItem(environment.domain + '-user'));
    if (this.user) {
      this.user.status = 'Online';
    }
  }

  async logout() {
    await this.authService.logout();
  }

  redirect() {
    this.router.navigate([environment.indexURL.root + '/login'], {
      replaceUrl: true,
    });
  }
}
