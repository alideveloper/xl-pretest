import {
  Component,
  OnInit,
  Input,
  HostBinding,
  Output,
  EventEmitter,
} from '@angular/core';
import { BaseComponent } from '@app/core';
import { Router } from '@angular/router';

import { NavigationService } from '@app/layout/services/navigation.service';
import { NavigationOptions } from '@app/layout/models/navigation';

import { environment } from '@environments/environment';

import { CredentialsService } from '@app/shared/services/auth/guard/credentials.service';

import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { FirebaseService } from '@app/@firebase/firebase.service';
import { LoadingService } from '@app/shared/services/loading.service';
import { Site } from '@app/shared/model/panel/site.model';
import { AppService } from '@app/app.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { AppData } from '@app/app.data';
import { User } from '@shared/model/user.model';
import { FirebaseConfigModel } from '@app/shared/model/other/firebase-config.model';
import { GlobalAction } from '@app/shared/services/global.action';
import { GlobalService } from '@app/shared/services/global.service';
import { Staff } from '@app/shared/model/staff.model';

@Component({
  selector: 'app-header-navbar',
  templateUrl: './header-navbar.component.html',
  styleUrls: ['./header-navbar.component.scss'],
})
export class HeaderNavbarComponent extends BaseComponent implements OnInit {
  navGroups: NavigationOptions[];

  @HostBinding('class.collapsed')
  @Input()
  sidenavCollapsed: boolean;

  @Input()
  dark: boolean;

  @Output()
  sideNavToggled: EventEmitter<boolean> = new EventEmitter<boolean>();

  site: Site;

  user: User;
  staff: Staff;

  stopRouting: boolean;

  constructor(
    private navigation: NavigationService,
    private router: Router,
    public toastr?: ToastrService,
    public translateService?: TranslateService,
    private credentialsService?: CredentialsService,
    private firebaseService?: FirebaseService,
    private loadingService?: LoadingService,
    private _firebaseService?: FirebaseService,
    private _globalService?: GlobalService,
    private _globalAction?: GlobalAction
  ) {
    super('header-navbar');
  }

  async ngOnInit() {
    if (environment.layout.type === 'horizontal') {
      await this.navigation.getNavigationItems();
    }

    this.user = JSON.parse(localStorage.getItem(environment.domain + '-Login'));
    if (this.user) {
    }

    this.navGroups = await this.navigation.getNavigations();
    console.log('HEADERNAVBAR', this.navGroups);
    this.initData();
  }

  toggleSidenav() {
    this.sidenavCollapsed = !this.sidenavCollapsed;
    this.sideNavToggled.emit(this.sidenavCollapsed);
  }

  initData() {
    this.site = JSON.parse(localStorage.getItem(environment.domain + '-site'));
  }

  async logout(noAccess?: boolean) {
    this.loadingService.loading.isLoading = true;
    console.log('next');

    localStorage.removeItem(environment.domain + '-Login');
    localStorage.removeItem(environment.domain + '-LoginStaff');
    localStorage.removeItem(environment.domain + '-LoginRole');
    localStorage.removeItem(environment.domain + '-Role');
    localStorage.removeItem(environment.domain + '-menus');
    localStorage.removeItem(environment.domain + '-business');
    localStorage.removeItem(environment.domain + '-selectedBusiness');
    localStorage.removeItem(environment.domain + '-selectedBusinessValue');
    sessionStorage.removeItem(environment.domain + '-Login');
    sessionStorage.removeItem(environment.domain + '-LoginStaff');
    this.credentialsService.setCredentials();

    this.showToast({
      title: 'Info',
      type: noAccess ? 'error' : 'success',
      message: this.translateService.instant(
        noAccess
          ? 'Your Access Permission is Empty, Please Contact Your Supervisor'
          : 'LOGOUT_SUCCESS'
      ),
    });

    this.loadingService.loading.isLoading = false;
    this.router.navigate(
      [environment.indexURL.rootRoute + environment.indexURL.login],
      {
        replaceUrl: true,
      }
    );
  }

  showToast(event: any): void {
    this.toastr[event.type](event.message, event.title, {
      closeButton: true,
      timeOut: 5000,
      extendedTimeOut: 1000,
      easing: 'easi-in',
      easiTime: 300,
      progressBar: true,
      progressAnimation: 'increasing',
      positionClass: 'toast-top-center',
      titleClass: 'toast-title',
      messageClass: 'toast-message',
      tapToDismiss: true,
      onActivateTick: false,
    });
  }

  async getStaffByLogin(key: string): Promise<any> {
    console.log('getStaffByLogin');
    return new Promise(async (resolve, reject) => {
      const configFirebase: FirebaseConfigModel = {
        collection: {
          status: true,
          name: 'apps',
          document: {
            status: true,
            name: environment.projectId,
            collection: {
              status: true,
              name: 'staffs',
              document: {
                status: false,
                name: null,
                document: null,
              },
              orderBy: {
                object: 'created',
                order: 'desc',
              },
              query: [
                {
                  object: 'key',
                  condition: '==',
                  value: key,
                },
              ],
            },
          },
        },
      };

      const data: any = await this._firebaseService.handleCollection(
        null,
        configFirebase
      );
      data.subscribe(async (response: Staff[]) => {
        console.log('getStaffByLogin', response);
        if (response && response.length > 0) {
          const staff: Staff = response[0];

          if (staff.status === 'Blocked') {
            this.logout();
          }
        }
      });
    });
  }
}
