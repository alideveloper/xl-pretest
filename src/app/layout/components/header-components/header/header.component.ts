import { Component, OnInit, Input, HostBinding } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  // tslint:disable-next-line: no-host-metadata-property
  host: { class: 'main-header' },
})
export class HeaderComponent implements OnInit {
  @HostBinding('class.fixed')
  @Input()
  fixed: boolean;

  @Input()
  dark: boolean;

  constructor() {}

  ngOnInit() {}
}
