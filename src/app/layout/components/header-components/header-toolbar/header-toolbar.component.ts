import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { faBars, faAlignRight } from '@fortawesome/free-solid-svg-icons';
import { BaseComponent } from '@app/core';

import { environment } from '@environments/environment';
import { FirebaseService } from '@app/@firebase/firebase.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { GlobalAction } from '@app/shared/services/global.action';
import { Site } from '@app/shared/model/panel/site.model';
import { User } from '@app/shared/model/user.model';
import { Storage } from '@app/shared/model/other/firebase-config.model';
import { Business } from '@app/shared/model/business.model';

@Component({
  selector: 'app-header-toolbar',
  templateUrl: './header-toolbar.component.html',
  styleUrls: ['./header-toolbar.component.scss'],
})
export class HeaderToolbarComponent extends BaseComponent implements OnInit {
  icons = {
    faBars,
    faAlignRight,
  };

  @Input()
  dark: boolean;

  @Input()
  quickSidenavCollapsed: boolean;

  @Input()
  sidenavCollapsed: boolean;

  @Output()
  quickSidenavToggled: EventEmitter<boolean> = new EventEmitter<boolean>();

  @Output()
  sidenavToggled: EventEmitter<boolean> = new EventEmitter<boolean>();

  @Input()
  branded: boolean;

  menuHidden = true;

  site: Site;

  logo: Storage;

  configLayout: any = environment.layout;
  configHeader: any = environment.layout.header;

  role: any = localStorage.getItem(environment.domain + '-LoginRole');
  user: User = JSON.parse(localStorage.getItem(environment.domain + '-Login'));

  constructor(
    private firebaseService: FirebaseService,
    private globalAction?: GlobalAction,
    private translateService?: TranslateService
  ) {
    super('header-toolbar');
    console.log('role', this.role);
    console.log('user', this.user);
  }

  handleLogo() {
    if (environment.logo === 'logoWhite') {
      this.logo = this.site.logoWhite.downloadURL;
    } else {
      this.logo = this.site.logo.downloadURL;
    }
  }

  toggleQuickSidenav() {
    this.quickSidenavCollapsed = !this.quickSidenavCollapsed;
    this.quickSidenavToggled.emit(this.quickSidenavCollapsed);
  }

  toggleSidenav() {
    this.sidenavCollapsed = !this.sidenavCollapsed;
    this.sidenavToggled.emit(this.sidenavCollapsed);
  }

  ngOnInit() {
    this.initData();
  }

  toggleMenu() {
    this.menuHidden = !this.menuHidden;
  }

  initData() {
    this.site = JSON.parse(localStorage.getItem(environment.domain + '-site'));

    this.handleLogo();

    this.user = JSON.parse(localStorage.getItem(environment.domain + '-user'));
  }

  async verifyEmail() {
    this.firebaseService.emailVerified().then(
      (result) => {
        console.log(result);
        this.globalAction.showToast({
          title: 'Info',
          message: this.translateService.instant(
            'A verification email has been sent to your email!'
          ),
          type: 'success',
        });
      },
      (error) => {
        console.log(error);
        this.globalAction.showToast({
          title: 'Info',
          message: this.translateService.instant(
            'Too many requests, wait a few minutes!'
          ),
          type: 'error',
        });
      }
    );
  }
}
