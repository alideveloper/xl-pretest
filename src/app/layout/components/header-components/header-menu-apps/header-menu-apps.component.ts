import { Component, OnInit, Input } from '@angular/core';
import { BaseComponent } from '@app/core';

import { environment } from '@environments/environment';

import { FirebaseService } from '@app/@firebase/firebase.service';
import { FirebaseConfigModel } from '@app/shared/model/other/firebase-config.model';
import { Content } from '@app/shared/model/panel/content.model';

@Component({
  selector: 'app-header-menu-apps',
  templateUrl: './header-menu-apps.component.html',
  styleUrls: ['./header-menu-apps.component.scss'],
})
export class HeaderMenuAppsComponent extends BaseComponent implements OnInit {
  apps: any[];
  categoryMyApps: any;

  @Input()
  dark: boolean;

  constructor(private _firebaseService: FirebaseService) {
    super();
  }

  ngOnInit() {
    this.initData();
  }

  async initData() {
    this.categoryMyApps = await this.getCategoryMyApps();
    this.apps = await this.getMyApps(this.categoryMyApps.key);
  }

  async getCategoryMyApps(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const configFirebase: FirebaseConfigModel = {
        collection: {
          status: true,
          name: 'apps',
          document: {
            status: true,
            name: environment.projectId,
            collection: {
              status: true,
              name: 'contentsCategory',
              document: {
                status: false,
                name: null,
                document: null,
              },
              orderBy: {
                object: 'created',
                order: 'asc',
              },
              query: [
                {
                  object: 'category',
                  condition: '==',
                  value: 'Link Menu Apps',
                },
              ],
            },
          },
        },
      };

      const result = await this._firebaseService.handleCollection(
        null,
        configFirebase
      );
      result.subscribe((response: any) => {
        if (response && response.length > 0) {
          resolve(response[0]);
        }
        resolve(true);
      });
    });
  }

  async getMyApps(category: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const configFirebase: FirebaseConfigModel = {
        collection: {
          status: true,
          name: 'apps',
          document: {
            status: true,
            name: environment.projectId,
            collection: {
              status: true,
              name: 'contents',
              document: {
                status: false,
                name: null,
                document: null,
              },
              orderBy: {
                object: 'created',
                order: 'asc',
              },
              query: [
                {
                  object: 'category',
                  condition: '==',
                  value: category,
                },
              ],
            },
          },
        },
      };

      const result = await this._firebaseService.handleCollection(
        null,
        configFirebase
      );
      result.subscribe((response: Content[]) => {
        if (response && response.length > 0) {
          resolve(response);
        }
        resolve(true);
      });
    });
  }

  onOpenChange(status: boolean) {
    // Drop down is open => status
  }
}
