import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@app/core';
import { Router } from '@angular/router';

import { AuthService } from '@app/shared/services/auth/services/auth.service';

import { environment } from '@environments/environment';

@Component({
  selector: 'app-header-user-account',
  templateUrl: './header-user-account.component.html',
  styleUrls: ['./header-user-account.component.scss'],
})
export class HeaderUserAccountComponent extends BaseComponent
  implements OnInit {
  user: any;

  constructor(private router: Router, private authService: AuthService) {
    super();
  }

  ngOnInit() {
    this.initData();
  }

  initData() {
    this.user = JSON.parse(localStorage.getItem(environment.domain + '-user'));
    this.user.status = 'Online';
  }

  async logout() {
    await this.authService.logout();
  }

  redirect() {
    this.router.navigate([environment.indexURL.root + '/login'], {
      replaceUrl: true,
    });
  }

  redirectURL(path: any) {
    localStorage.setItem(environment.domain + '-path', path);
    this.router.navigateByUrl(path);
  }
}
