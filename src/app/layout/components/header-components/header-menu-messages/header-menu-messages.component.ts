import { Component, OnInit, Input } from '@angular/core';
import { MessagesService } from '@layout/services/messages.service';
import { Message } from '@layout/models/message';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

import { Logger, BaseComponent } from '@app/core';

const logger = new Logger('HeaderMenuMessagesComponent');
@Component({
  selector: 'app-header-menu-messages',
  templateUrl: './header-menu-messages.component.html',
  styleUrls: ['./header-menu-messages.component.scss'],
})
export class HeaderMenuMessagesComponent extends BaseComponent
  implements OnInit {
  protected loaded: boolean;

  iconClose = faTimes;
  messages: any[] = [];

  @Input()
  dark: boolean;

  constructor(private messagesService: MessagesService) {
    super();
    this.isLoading = true;
  }

  ngOnInit() {}

  onOpenChange(status: boolean) {
    if (status && !this.loaded) {
      this.loaded = !this.loaded;

      this.messagesService
        .getLatestMessages()
        .subscribe((messages: Message[]) => {
          logger.debug('Messages', messages);
          this.isLoading = false;
          this.messages = [...messages, ...messages];
        });
    }
  }
}
