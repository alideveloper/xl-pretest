import { Component, OnInit, Input } from '@angular/core';
import { Notification } from '@layout/models/notification';
import { NotificationsService } from '@layout/services/notifications.service';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

import { Logger, BaseComponent } from '@app/core';

const logger = new Logger('HeaderMenuNotificationsComponent');

@Component({
  selector: 'app-header-menu-notifications',
  templateUrl: './header-menu-notifications.component.html',
  styleUrls: ['./header-menu-notifications.component.scss'],
})
export class HeaderMenuNotificationsComponent extends BaseComponent
  implements OnInit {
  protected loaded: boolean;

  iconClose = faTimes;
  notifications: Notification[];

  @Input()
  dark: boolean;

  constructor(private notificationService: NotificationsService) {
    super();

    this.isLoading = true;
  }

  ngOnInit() {}

  onOpenChange(status: boolean) {
    if (status && !this.loaded) {
      this.loaded = !this.loaded;

      this.notificationService
        //
        .getLatestNotifications()
        .subscribe((notifications) => {
          logger.debug('Notifications', notifications);

          this.isLoading = false;
          this.notifications = notifications;
        });
    }
  }
}
