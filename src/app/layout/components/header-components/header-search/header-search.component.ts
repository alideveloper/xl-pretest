import { Component, OnInit, Input } from '@angular/core';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { BaseComponent } from '@app/core';

@Component({
  selector: 'app-header-search',
  templateUrl: './header-search.component.html',
  styleUrls: ['./header-search.component.scss'],
})
export class HeaderSearchComponent extends BaseComponent implements OnInit {
  search = faSearch;

  @Input()
  dark: boolean;

  constructor() {
    super('search-form');
  }

  ngOnInit() {}
}
