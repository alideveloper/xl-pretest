import {
  Component,
  OnInit,
  HostBinding,
  Input,
  Output,
  EventEmitter,
  HostListener,
} from '@angular/core';
import { AuthenticationService, Logger } from '@app/core';
import { Router } from '@angular/router';
import {
  faInbox,
  faLock,
  faSignOutAlt,
  faUser,
} from '@fortawesome/free-solid-svg-icons';
import { NavigationService } from '@app/layout/services/navigation.service';
import { NavigationOptions } from '@app/layout/models/navigation';

import { AuthService } from '@app/shared/services/auth/services/auth.service';

import { environment } from '@environments/environment';
import { Site } from '@app/shared/model/panel/site.model';
import { Storage } from '@app/shared/model/other/firebase-config.model';

const logger = new Logger('SidenavComponent');

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss'],
  // tslint:disable-next-line: no-host-metadata-property
  host: { class: 'sidenav' },
})
export class SidenavComponent implements OnInit {
  user: any = {
    photo: {
      downloadURL: 'https://randomuser.me/api/portraits/med/women/96.jpg',
    },
    displayName: 'pauline myers',
    email: 'admin@pbconsole.id',
  };

  icons = {
    faInbox,
    faLock,
    faSignOutAlt,
    faUser,
  };

  navGroups: NavigationOptions[];

  site: Site;

  logo: Storage;

  @Output()
  sideNavToggled: EventEmitter<boolean> = new EventEmitter<boolean>();

  @HostBinding('class.collapsed')
  @Input()
  collapsed: boolean;

  @HostBinding('class.fixed')
  @Input()
  fixed: boolean;

  @HostBinding('class.hover')
  hover: boolean;

  @HostListener('mouseenter')
  onMouseOver() {
    this.hover = true;
  }

  @HostListener('mouseleave')
  onMouseLeave() {
    this.hover = false;
  }

  constructor(
    private authService: AuthService,
    private router: Router,
    private navigation: NavigationService,
    private authenticationService: AuthenticationService
  ) {}

  async ngOnInit() {
    console.log('SIDENAV');
    this.navGroups = await this.navigation.getNavigations();
    this.initData();
  }

  handleLogo() {
    if (environment.logo === 'logoWhite') {
      this.logo = this.site.logoWhite.downloadURL;
    } else {
      this.logo = this.site.logo.downloadURL;
    }
  }

  toggleSidenav() {
    this.collapsed = !this.collapsed;
    this.sideNavToggled.emit(this.collapsed);
  }

  async logout() {
    await this.authService.logout();
  }

  onNavLinkToggle(isOpen: boolean) {
    logger.debug(`Nav link toggled ${isOpen}`);
  }

  initData() {
    this.site = JSON.parse(localStorage.getItem(environment.domain + '-site'));
    this.user = JSON.parse(localStorage.getItem(environment.domain + '-user'));
    this.user.status = 'Online';
    this.handleLogo();
  }

  redirectURL(path: any) {
    localStorage.setItem(environment.domain + '-path', path);
    this.router.navigateByUrl(path);
  }
}
