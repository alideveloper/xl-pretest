import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { NavigationOptions } from '@app/layout/models/navigation';
import { BaseComponent } from '@app/core';
import { Router } from '@angular/router';

import { environment } from '@environments/environment';

@Component({
  selector: 'app-sidenav-item',
  templateUrl: './sidenav-item.component.html',
  styleUrls: ['./sidenav-item.component.scss'],
})
export class SidenavItemComponent extends BaseComponent implements OnInit {
  @Input()
  option: NavigationOptions;

  @HostBinding('class.collapsed')
  @Input()
  collapsed: boolean;

  @HostBinding('class.hover')
  @Input()
  hover: boolean;

  @Input() showTitle: any = true;

  @Input() showToggler: any = true;

  @HostBinding('class.nav-dropdown')
  get hasDropDown(): boolean {
    return !!this.option.items;
  }

  @HostBinding('class.level-2')
  get paddingFromLevel(): boolean {
    return this.option.level === 2;
  }

  constructor(private router: Router) {
    super('nav-item');
  }

  ngOnInit() {}

  goToPage(item: any): void {
    localStorage.setItem(environment.domain + '-path', item.link);
  }
}
