import { Component, OnInit } from '@angular/core';

import { environment } from '@environments/environment';
import { Site } from '@app/shared/model/panel/site.model';
import { Domain } from '@app/shared/model/panel/domain.model';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {
  site: Site;
  domain: Domain;

  constructor() {}

  ngOnInit() {
    this.initData();
  }

  initData() {
    this.site = JSON.parse(localStorage.getItem(environment.domain + '-site'));
    this.domain = JSON.parse(
      localStorage.getItem(environment.domain + '-domain')
    );
  }
}
