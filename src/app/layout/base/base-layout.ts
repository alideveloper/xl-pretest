import { Input, HostBinding, Directive } from '@angular/core';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

@Directive({})
// tslint:disable-next-line: directive-class-suffix
export class BaseLayout {
  @HostBinding('class.fixed-header')
  @Input()
  fixedHeader: boolean;

  isQuickSidenavCollapsed: any = true;

  closeIcon = faTimes;

  /*
   * Handles the quick side navigation toggled state.
   * This takes the result from the header trigger button and passes it to the quick-side-nav component
   */
  onQuickSidenavToggled(collapsed: boolean) {
    this.isQuickSidenavCollapsed = collapsed;
  }
}
