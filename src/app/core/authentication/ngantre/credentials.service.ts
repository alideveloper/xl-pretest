import { Injectable } from '@angular/core';
import { environment } from '@environments/environment';

const credentialsKey = 'PBLogin';

@Injectable({
  providedIn: 'root',
})
export class NgUangCredentialsService {
  private _credentials: any;

  constructor() {
    const savedCredentials =
      sessionStorage.getItem(environment.domain + '-' + credentialsKey) ||
      localStorage.getItem(environment.domain + '-' + credentialsKey);
    if (savedCredentials) {
      this._credentials = JSON.parse(savedCredentials);
    }
  }

  isAuthenticated(): boolean {
    return !!this.credentials;
  }

  get credentials(): any {
    return this._credentials;
  }
}
