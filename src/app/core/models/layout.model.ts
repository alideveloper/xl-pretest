export enum Layouts {
  VerticalDefault = 'vertical-default',
  HorizontalDefault = 'horizontal-default',
}

export interface LayoutModel {
  style: any;
  name: string;
  vertical?: LayoutVerticalConfigurationModel;
  horizontal?: LayoutHorizontalConfigurationModel;
}

export interface LayoutConfigurationModel {
  fixedHeader: boolean;
}

export interface LayoutVerticalConfigurationModel
  extends LayoutConfigurationModel {
  fixedSideNav: true;
  sidenavCollapsed: false;
}

// tslint:disable-next-line: no-empty-interface
export interface LayoutHorizontalConfigurationModel
  extends LayoutConfigurationModel {}
