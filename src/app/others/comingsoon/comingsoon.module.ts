import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CountdownModule } from 'ngx-countdown';

import { ComingsoonComponent } from '@app/others/comingsoon/comingsoon.component';

import { SharedModule } from '@app/shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';

const routes = [
  {
    path: 'comingsoon',
    component: ComingsoonComponent,
  },
];

@NgModule({
  declarations: [ComingsoonComponent],
  imports: [
    RouterModule.forChild(routes),

    CountdownModule,

    TranslateModule,
    SharedModule,
  ],
  exports: [ComingsoonComponent],
})
export class ComingsoonModule {}
