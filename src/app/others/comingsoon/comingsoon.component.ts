import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { environment } from '@environments/environment';
import { Site } from '@app/shared/model/panel/site.model';

@Component({
  selector: 'app-comingsoon',
  templateUrl: './comingsoon.component.html',
  styleUrls: ['./comingsoon.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ComingsoonComponent implements OnInit {
  site: Site;
  countDown: any;
  constructor() {}

  ngOnInit(): void {
    this.site = JSON.parse(localStorage.getItem(environment.domain + '-site'));
    console.log(this.site);

    const currentTime = new Date(
      this.site.comingSoonDate?.seconds * 1000
    ).getTime();

    setInterval(() => {
      const now = new Date().getTime();

      console.log(currentTime);
      console.log(now);
      console.log(this.site.comingSoonDate?.seconds);
      const distance = currentTime - now;

      this.countDown = {
        days: Math.floor(distance / (1000 * 60 * 60 * 24)),
        hours: Math.floor(
          (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
        ),
        minutes: Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60)),
        seconds: Math.floor((distance % (1000 * 60)) / 1000),
      };
      console.log(this.countDown);
    }, 1000);
  }
}
