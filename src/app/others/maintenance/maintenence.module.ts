import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MaintenanceComponent } from '@app/others/maintenance/maintenance.component';

import { SharedModule } from '@app/shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';

const routes = [
  {
    path: 'maintenance',
    component: MaintenanceComponent,
  },
];

@NgModule({
  declarations: [MaintenanceComponent],
  imports: [RouterModule.forChild(routes), TranslateModule, SharedModule],
  exports: [MaintenanceComponent],
})
export class MaintenanceModule {}
