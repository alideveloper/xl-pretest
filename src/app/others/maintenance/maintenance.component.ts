import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { environment } from '@environments/environment';
import { Site } from '@app/shared/model/panel/site.model';

@Component({
  selector: 'app-maintenance',
  templateUrl: './maintenance.component.html',
  styleUrls: ['./maintenance.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class MaintenanceComponent implements OnInit {
  site: Site;
  constructor() {}

  ngOnInit(): void {
    this.site = JSON.parse(localStorage.getItem(environment.domain + '-site'));
    console.log(this.site);
  }
}
