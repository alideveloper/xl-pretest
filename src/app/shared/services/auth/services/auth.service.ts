import { Injectable } from '@angular/core';

import { BehaviorSubject, Observable, config } from 'rxjs';

import { FirebaseService } from '@app/@firebase/firebase.service';
import { LoadingService } from '@app/shared/services/loading.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { NavigationService } from '@app/layout/services/navigation.service';

import { Router } from '@angular/router';

import * as _ from 'lodash';
import { environment } from '@environments/environment';

import { GlobalConstants } from '@app/shared/constant/global.constant';
import { Loading } from '@app/shared/model/other/loading.model';
import { User } from '@app/shared/model/user.model';

@Injectable()
export class AuthService {
  env: string;

  onAuthChanged: BehaviorSubject<any>;

  loading: Loading;

  constructor(
    private _firebaseService: FirebaseService,
    private _angularFireAuth: AngularFireAuth,
    private _navigationService: NavigationService,
    private _router: Router,
    private _loadingService: LoadingService
  ) {
    this.onAuthChanged = new BehaviorSubject({});
    this.loading = GlobalConstants.loading;
  }

  async authState(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      await this._angularFireAuth.authState.subscribe(async (user) => {
        if (user) {
          resolve(true);
        } else {
          resolve(false);
        }
      });
    });
  }

  async getUsers(email: any, password: any): Promise<any> {
    const whereEmail = {
      object: 'email',
      condition: '==',
      value: email,
    };
    const whereProvider = {
      object: 'provider',
      condition: '==',
      value: 'email',
    };
    return new Promise(async (resolve, reject) => {
      const result: any = {
        status: null,
        message: null,
      };

      const userFirebaseConfig: any = {
        bindingKey: 'users',
        collection: {
          status: true,
          name: 'globals',
          document: {
            status: true,
            name: 'Users',
            collection: {
              status: true,
              name: 'users',
              document: {
                status: false,
                name: null,
                document: null,
              },
              orderBy: {
                object: 'created',
                order: 'desc',
              },
              query: [whereEmail, whereProvider],
            },
          },
        },
      };

      const getUser = await this._firebaseService.handleCollection(
        null,
        userFirebaseConfig
      );
      getUser.subscribe(async (response: any) => {
        if (response.length > 0) {
          result.status = 'success';
          const data = response[0];
          if (data.status === 'blocked') {
            this._loadingService.loading.isLoading = false;
            result.status = 'error';
            result.message = 'EMAIL_BLOCKED';
            resolve(result);
            return;
          }

          localStorage.setItem(
            environment.domain + '-user',
            JSON.stringify(data)
          );
          if (data.role) {
            localStorage.setItem(environment.domain + '-role', data.role);
          }
          resolve(result);
        } else {
          this._loadingService.loading.isLoading = false;
          result.status = 'error';
          result.message = 'EMAIL_NOT_FOUND';
          resolve(result);
        }
      });
    });
  }

  async loginEmail(payload: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      this._loadingService.loading.isLoading = true;

      const result: any = {
        status: null,
        message: null,
      };

      const resultUsers = await this.getUsers(payload.email, payload.password);
      const user = JSON.parse(
        localStorage.getItem(environment.domain + '-user')
      );

      const userFirebaseConfig: any = {
        method: 'PUT',
        bindingKey: 'form',
        collection: {
          status: true,
          name: 'globals',
          document: {
            status: true,
            name: 'Users',
            collection: {
              status: true,
              name: 'users',
              document: {
                status: false,
                name: null,
                document: null,
              },
              orderBy: {
                object: 'created',
                order: 'desc',
              },
              query: [],
            },
          },
        },
      };

      if (resultUsers.status === 'success') {
        this._firebaseService
          .loginEmail(payload.email, payload.password)
          .then(async (resultLogin) => {
            const dataNew: User = {};
            dataNew.key = user.key;
            if (!dataNew.uid) dataNew.uid = resultLogin.user.uid;
            dataNew.lastLogin = this._firebaseService.timestampPost;
            dataNew.isActiveLogin = true;

            this._firebaseService
              .updateCollection(null, userFirebaseConfig, dataNew)
              .then(async (responseUse: any) => {
                await this._navigationService.getNavigationItems();
                this._loadingService.loading.isLoading = false;
                result.status = 'success';
                result.message = 'LOGIN_SUCCESS';
                resolve(result);
                this._router.navigateByUrl(
                  environment.indexURL.root + environment.indexURL.home
                );
                return;
              });
          })
          .catch((errorLogin) => {
            result.status = 'error';
            if (errorLogin.code === 'auth/user-not-found') {
              if (payload.password !== user.password) {
                this._loadingService.loading.isLoading = false;
                // delete user.password;
                result.status = 'error';
                result.message = 'WRONG_PASSWORD_LOGIN';
                resolve(result);
                return;
              }
              this._firebaseService
                .signUpEmail(payload.email, payload.password)
                .then(async (resultSignUp) => {
                  const dataNew: User = {};
                  dataNew.key = user.key;
                  if (!dataNew.uid) dataNew.uid = resultSignUp.user.uid;
                  dataNew.lastLogin = this._firebaseService.timestampPost;
                  dataNew.isActiveLogin = true;

                  this._firebaseService
                    .updateCollection(null, userFirebaseConfig, dataNew)
                    .then(async (responseUse: any) => {
                      await this._navigationService.getNavigationItems();
                      this._loadingService.loading.isLoading = false;
                      result.status = 'success';
                      result.message = 'SIGNUP_FOR_LOGIN_SUCCESS';
                      resolve(result);
                      this._router.navigateByUrl(
                        environment.indexURL.root + environment.indexURL.home
                      );
                      return;
                    });
                })
                .catch((errorSignUp) => {
                  this._loadingService.loading.isLoading = false;
                  if (errorSignUp.code === 'auth/email-already-in-use') {
                    result.message = 'EMAIL_ALREADY_IN_USE';
                    resolve(result);
                  } else if (errorSignUp.code === 'auth/invalid-email') {
                    result.message = 'INVALID_EMAIL';
                    resolve(result);
                  } else if (
                    errorSignUp.code === 'auth/operation-not-allowed'
                  ) {
                    result.message = 'AUTH_OPERATION_NOT_ALLOWED';
                    resolve(result);
                  } else if (errorSignUp.code === 'auth/auth/weak-password') {
                    result.message = 'WEAK_PASSWORD';
                    resolve(result);
                  }
                });
            } else if (errorLogin.code === 'auth/user-disabled') {
              this._loadingService.loading.isLoading = false;
              result.message = 'USER_DISABLED';
              resolve(result);
            } else if (errorLogin.code === 'auth/wrong-password') {
              this._loadingService.loading.isLoading = false;
              result.message = 'WRONG_PASSWORD_LOGIN';
              resolve(result);
            } else if (errorLogin.code === 'auth/invalid-email') {
              this._loadingService.loading.isLoading = false;
              result.message = 'INVALID_EMAIL';
              resolve(result);
            }
          });
      } else {
        resolve(resultUsers);
      }
    });
  }

  async logout(): Promise<any> {
    const user = JSON.parse(localStorage.getItem(environment.domain + '-user'));
    return new Promise(async (resolve, reject) => {
      const result: any = {
        status: null,
        message: null,
      };
      this._loadingService.loading.isLoading = true;
      await this._firebaseService.logout().then(async (response) => {
        const dataNew: User = {};
        dataNew.key = user.key;
        dataNew.lastLogout = this._firebaseService.timestampPost;

        const userFirebaseConfig: any = {
          bindingKey: 'users',
          collection: {
            status: true,
            name: 'globals',
            document: {
              status: true,
              name: 'Users',
              collection: {
                status: true,
                name: 'users',
                document: {
                  status: false,
                  name: null,
                  document: null,
                },
                orderBy: {
                  object: 'created',
                  order: 'desc',
                },
                query: [],
              },
            },
          },
        };

        await this._firebaseService
          .updateCollection(null, userFirebaseConfig, dataNew)
          .then((responseUse: any) => {
            this._loadingService.loading.isLoading = false;
            localStorage.removeItem(environment.domain + '-menus');
            localStorage.removeItem(environment.domain + '-LoginRole');
            localStorage.removeItem(environment.domain + '-Login');
            localStorage.removeItem(environment.domain + '-business');
            this._router.navigate([
              environment.indexURL.root + environment.indexURL.login,
            ]);
          });
      });
      result.status = 'success';
      result.message = 'LOGOUT_SUCCESS';
      resolve(result);
    });
  }

  async resetPassword(payload: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const result: any = {
        status: null,
        message: null,
      };
      console.log(this._loadingService.loading);
      this._loadingService.loading.isLoading = true;
      await this._firebaseService
        .resetPassword(
          payload.hasOwnProperty('email') ? payload.email : payload
        )
        .then((response) => {
          result.status = 'success';
          result.message = 'RESET_PASSWORD_SUCCESS';
          this._loadingService.loading.isLoading = false;
          resolve(result);
        })
        .catch((error) => {
          this._loadingService.loading.isLoading = false;
          result.status = 'error';
          if (error.code === 'auth/invalid-email') {
            result.message = 'INVALID_EMAIL';
            resolve(result);
          } else if (error.code === 'auth/missing-android-pkg-name') {
            result.message = 'MISSING_ANDROID_PACKAGE_NAME';
            resolve(result);
          } else if (error.code === 'auth/missing-continue-uri') {
            result.message = 'MISSING_CONTINUE_URI';
            resolve(result);
          } else if (error.code === 'auth/missing-ios-bundle-id') {
            result.message = 'MISSING_IOS_BUNDLE_ID';
            resolve(result);
          } else if (error.code === 'auth/auth/invalid-continue-uri') {
            result.message = 'INVALID_CONTINUE_URI';
            resolve(result);
          } else if (error.code === 'auth/unauthorized-continue-uri') {
            result.message = 'UNAUTHORIZED_CONTINUE_URI';
            resolve(result);
          } else if (error.code === 'auth/user-not-found') {
            result.message = 'EMAIL_NOT_REGISTERED';
            resolve(result);
          }
        });
    });
  }
}
