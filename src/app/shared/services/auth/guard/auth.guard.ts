import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router,
} from '@angular/router';
import { AuthService } from '@app/shared/services/auth/services/auth.service';
import { Observable } from 'rxjs';

import { environment } from '@environments/environment';

import { AngularFireAuth } from '@angular/fire/auth';

import { CredentialsService } from './credentials.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(
    public _authService: AuthService,
    public _router: Router,
    public _angularFireAuth: AngularFireAuth,
    public _credentialsService: CredentialsService
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    console.log(
      'this._credentialsService.isAuthenticated()',
      this._credentialsService.isAuthenticated()
    );
    if (this._credentialsService.isAuthenticated()) {
      return true;
    }

    console.log('NAVIGATE GUARD');
    this._router.navigate(
      [environment.indexURL.rootRoute + environment.indexURL.login],
      {
        replaceUrl: true,
      }
    );
    return false;
  }
}
