import { Injectable } from '@angular/core';
import { environment } from '@environments/environment';

const credentialsKey = 'Login';
const credentialsStaffKey = 'LoginStaff';

@Injectable({
  providedIn: 'root',
})
export class CredentialsService {
  private _credentials: any;

  constructor() {
    const savedCredentials =
      sessionStorage.getItem(environment.domain + '-' + credentialsKey) ||
      localStorage.getItem(environment.domain + '-' + credentialsKey);
    if (savedCredentials) {
      this._credentials = JSON.parse(savedCredentials);
    } else {
      this._credentials = null;
    }
  }

  isAuthenticated(): boolean {
    return !!this.credentials;
  }

  get credentials(): any {
    return this._credentials;
  }

  setCredentials(): any {
    const savedCredentials =
      sessionStorage.getItem(environment.domain + '-' + credentialsKey) ||
      localStorage.getItem(environment.domain + '-' + credentialsKey);
    this._credentials = savedCredentials;
  }

  setCredentialStaffs(): any {
    const savedCredentials =
      sessionStorage.getItem(environment.domain + '-' + credentialsStaffKey) ||
      localStorage.getItem(environment.domain + '-' + credentialsStaffKey);
    this._credentials = savedCredentials;
  }
}
