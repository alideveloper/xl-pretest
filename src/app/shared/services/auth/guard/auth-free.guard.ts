import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router,
} from '@angular/router';
import { AuthService } from '@app/shared/services/auth/services/auth.service';
import { Observable } from 'rxjs';

import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root',
})
export class AuthFreeGuard implements CanActivate {
  constructor(
    public _authService: AuthService,
    public _router: Router,
    public _angularFireAuth: AngularFireAuth
  ) {}

  async canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<any> {
    return new Promise(async (resolve, reject) => {
      this._authService.authState().then((response: any) => {
        resolve(true);
      });
    });
  }
}
