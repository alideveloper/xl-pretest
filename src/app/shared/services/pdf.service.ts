import { Injectable } from '@angular/core';

import { GlobalService } from '@shared/services/global.service';

import * as _ from 'lodash';
import { Site } from '@shared/model/panel/site.model';
import { environment } from '@environments/environment';
import { LoadingService } from '@shared/services/loading.service';

declare let pdfMake: any;
declare let vfsFonts: any;

import * as pdfFonts from 'pdfmake/build/vfs_fonts';

@Injectable()
export class PDFService {
  site: Site;

  constructor(
    private _globalService: GlobalService,
    private _loadingService: LoadingService
  ) {}

  A4InitPage(orientation = 'potrait'): any {
    const docDefinition = {
      pageSize: 'A4',
      pageOrientation: orientation,
      pageMargins: [40, 60, 40, 60],
    };

    return docDefinition;
  }

  async setContent(
    table: any,
    data: any,
    rangeDate?: string,
    total?: string,
    custom: boolean = false,
    customHeaderActive: boolean = false
  ): Promise<any> {
    const content = [];

    const fullDate = await this._globalService.rawFullDateName();
    content.push({
      text: fullDate,
      style: 'standardFullDate',
      alignment: 'left',
      margin: [0, 0, 0, 0],
    });

    if (rangeDate) {
      content.push({
        text: '',
        style: 'standardMarginInfo',
      });
      content.push({
        text: rangeDate,
        style: 'standardRangeDate',
        alignment: 'left',
        margin: [0, 0, 0, 0],
      });
    }

    if (total) {
      content.push({
        text: total,
        style: 'standardBalance',
        alignment: 'left',
        margin: [0, 10, 0, 0],
      });
    }

    content.push({
      text: '',
      style: 'standardTable',
    });

    content.push({
      columns: [this.getData(table, data, custom, customHeaderActive)],
    });

    return content;
  }

  getData(
    table: any,
    data: any,
    custom: boolean = false,
    customHeaderActive: boolean = false
  ): any {
    const body: any = [];
    table.header.forEach((result: any) => {
      console.log('result', result);
      console.log('custom', custom);
      if (!custom || (custom && customHeaderActive)) {
        const item = {
          text: result,
          style: 'standardHeader',
          fillColor: '#dddddd',
        };
        body.push(item);
      } else {
        const item = {
          text: '',
          style: 'standardHeader',
        };
        body.push(item);
      }
    });

    return {
      table: {
        widths: table.width,
        body: [
          body,
          ...data.map((item: any) => {
            console.log('item', item);
            const response: any = [];
            let no = 0;
            table.data.forEach((result: any) => {
              console.log('result 2', result);
              let value;
              if (custom) {
                console.log('table.data[no + 1]', table.data[no + 1]);
                console.log(
                  'item[table.data[no + 1]]',
                  item[table.data[no + 1]]
                );
                value = {
                  text: item[result],
                  style: item[result + 'Style'],
                };
              } else {
                value = {
                  text: item[result],
                  style: 'standardBody',
                };
              }
              no++;
              response.push(value);
            });
            return response;
          }),
        ],
      },
    };
  }

  async setHeaderImageCompany(title: any, accounting: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      let image;

      if (accounting) {
        image = await this._globalService.makeRequest(accounting.image);
      } else {
        image = await this._globalService.makeRequest(
          this.site.favicon.downloadURL
        );
      }

      const headerImage = {
        width: '100%',
        columns: [
          { text: title, style: 'standardSubTitle', margin: [0, 35, 0, 0] },
          { image, width: 75, alignment: 'right', opacity: 0.8 },
        ],
      };

      resolve(headerImage);
    });
  }

  async setHeader(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const header = {
        columns: [{ text: this.site.name, style: 'standardTitle' }],
      };
      resolve(header);
    });
  }

  setMetadata(title: any): any {
    return {
      info: {
        title,
        author: this.site.name,
        subject: title,
        keywords: 'Data',
      },
    };
  }

  async setDocument(
    typePage: string = 'A4',
    title: string = '',
    table = {},
    data: any[] = [],
    typeHeader: string = 'company',
    orientation: string,
    accounting: any,
    rangeDate: string,
    total: string,
    custom: boolean = false,
    customHeaderActive: boolean = false
  ): Promise<any> {
    return new Promise(async (resolve, reject) => {
      let page;
      if (typePage === 'A4') {
        page = this.A4InitPage(orientation);
      }

      console.log('site', this.site);
      const docDefinition: any = {
        header(currentPage: number, pageCount: number, pageSize: any) {
          let text;
          if (accounting) {
            text = accounting.name;
          } else {
            const site = JSON.parse(
              localStorage.getItem(environment.domain + '-site')
            );
            text = site.alias;
          }

          return [
            {
              stack: [
                {
                  text,
                  alignment: 'center',
                  style: 'standardTitle',
                },
              ],
              margin: [72, 20, 72, 40],
            },
          ];
        },
        content: [],
        footer(currentPage: number, pageCount: number) {
          return [
            {
              margin: [40, 10, 40, 40],
              stack: [
                {
                  text: currentPage.toString() + ' of ' + pageCount,
                  alignment: 'right',
                  style: 'standardFooter',
                },
              ],
            },
          ];
        },
      };

      let headerImage;
      if (typeHeader === 'company') {
        headerImage = await this.setHeaderImageCompany(
          accounting ? accounting.title : title,
          accounting
        );
        docDefinition.content.push(headerImage);
      }

      const content = await this.setContent(
        table,
        data,
        rangeDate,
        total,
        custom,
        customHeaderActive
      );
      docDefinition.content = _.concat(docDefinition.content, content);

      const styles = this.setStyle();
      const metaData = this.setMetadata(title);

      _.merge(docDefinition, page);
      _.merge(docDefinition, metaData);
      _.merge(docDefinition, styles);

      resolve(docDefinition);
    });
  }

  setStyle(): any {
    const result = {
      styles: {
        standardTitle: {
          fontSize: 16,
          bold: true,
        },
        standardSubTitle: {
          fontSize: 14,
          bold: true,
        },
        standardTable: {
          margin: [0, 50, 0, 0],
        },
        standardHeader: {
          fontSize: 10,
          bold: true,
        },
        standardBody: {
          fontSize: 8,
          bold: false,
        },
        standardFooter: {
          fontSize: 10,
          bold: false,
        },
        standardFullDate: {
          fontSize: 10,
          bold: false,
          color: 'gray',
        },
        standardMarginInfo: {
          margin: [50, 20, 0, 0],
        },
        standardRangeDate: {
          fontSize: 11,
          bold: true,
        },
        standardBalance: {
          margin: [50, 10, 0, 0],
          fontSize: 11,
          bold: false,
          color: 'green',
        },
        standardTitleAccounting: {
          fontSize: 10,
          bold: true,
        },
        standardCenterAccounting: {
          fontSize: 8,
          bold: false,
          alignment: 'center',
        },
        standardLeftAccounting: {
          fontSize: 8,
          bold: false,
          alignment: 'left',
        },
        standardRightAccounting: {
          fontSize: 8,
          bold: false,
          alignment: 'right',
        },
        standardTitleCenterAccounting: {
          fontSize: 10,
          bold: true,
          alignment: 'center',
        },
        standardTitleLeftAccounting: {
          fontSize: 10,
          bold: true,
          alignment: 'left',
        },
        standardTitleRightAccounting: {
          fontSize: 10,
          bold: true,
          alignment: 'right',
        },
        standardSuccessAccounting: {
          fontSize: 10,
          bold: false,
          color: 'green',
        },
        standardSuccessRightAccounting: {
          fontSize: 10,
          bold: false,
          color: 'green',
          alignment: 'right',
        },
        standardDangerAccounting: {
          fontSize: 10,
          bold: false,
          color: 'red',
        },
        standardDangerRightAccounting: {
          fontSize: 10,
          bold: false,
          color: 'red',
          alignment: 'right',
        },
      },
    };

    return result;
  }

  async onLoad(
    action: string,
    title: string,
    table?: any,
    data?: any,
    typePage?: any,
    header?: any,
    orientation = 'potrait',
    accounting?: any,
    rangeDate?: string,
    total?: string,
    custom: boolean = false,
    customHeaderActive: boolean = false
  ): Promise<any> {
    this.site = JSON.parse(localStorage.getItem(environment.domain + '-site'));
    console.log('site', this.site);
    this._loadingService.loading.isLoading = true;
    const document = await this.setDocument(
      typePage,
      title,
      table,
      data,
      header,
      orientation,
      accounting,
      rangeDate,
      total,
      custom,
      customHeaderActive
    );
    const fileName = this._globalService.setFileName(title);
    this._loadingService.loading.isLoading = false;

    if (action === 'open') {
      this.openPdf(document);
    } else if (action === 'download') {
      this.downloadPdf(document, fileName);
    } else if (action === 'print') {
      this.printPdf(document);
    }
  }

  openPdf(docDefinition: any): void {
    pdfMake.createPdf(docDefinition, null, null, pdfFonts.pdfMake.vfs).open();
  }

  printPdf(docDefinition: any): void {
    pdfMake.createPdf(docDefinition, null, null, pdfFonts.pdfMake.vfs).print();
  }

  downloadPdf(docDefinition: any, fileName: string): void {
    pdfMake
      .createPdf(docDefinition, null, null, pdfFonts.pdfMake.vfs)
      .download(fileName + '.pdf');
  }

  downloadPdfWithName(customName: string): void {
    pdfMake.download(customName, null, null, pdfFonts.pdfMake.vfs);
  }
}
