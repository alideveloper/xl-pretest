import { Injectable } from '@angular/core';

import { GlobalConstants } from '@app/shared/constant/global.constant';
import { Loading } from '../model/other/loading.model';

@Injectable({
  providedIn: 'root',
})
export class LoadingService {
  loading: Loading;

  constructor() {
    if (!this.loading) {
      this.dataLoading = GlobalConstants.loading;
    }
  }

  set dataLoading(dataLoading: any) {
    this.loading = dataLoading;
    console.log('LOADINGSERVICE');
    console.log(this.loading);
  }

  get dataLoading() {
    return this.loading;
  }
}
