import { Injectable } from '@angular/core';

import * as FileSaver from 'file-saver';

import { GlobalService } from '@shared/services/global.service';

import * as _ from 'lodash';
import { Site } from '../model/panel/site.model';
import { environment } from '@environments/environment';
import { LoadingService } from './loading.service';
import { range } from 'lodash';

declare const ExcelJS: any;

@Injectable({
  providedIn: 'root',
})
export class ExcelService {
  site: Site;

  constructor(
    private _globalService: GlobalService,
    private _loadingService: LoadingService
  ) {}

  async setDocument(
    title: string = '',
    data: any,
    typeHeader: string = 'company',
    accounting?: any,
    rangeDate?: string,
    total?: string,
    custom: boolean = false,
    customHeaderActive: boolean = false
  ): Promise<any> {
    const workbook = new ExcelJS.Workbook();
    const worksheet = workbook.addWorksheet(title);

    let titleResult = '';

    if (accounting) {
      titleResult = accounting.name;
    } else {
      titleResult = this.site.name;
    }

    const titleRow = worksheet.addRow([titleResult]);
    titleRow.font = { name: 'Comic Sans MS', family: 4, size: 16, bold: true };
    worksheet.addRow([]);
    worksheet.addRow([]);

    const subTitleRow = worksheet.addRow([
      accounting ? accounting.title : title,
    ]);
    subTitleRow.font = { bold: true, size: 14 };

    worksheet.addRow([]);

    if (typeHeader === 'company') {
      let image;
      if (accounting) {
        image = await this._globalService.makeRequest(accounting.image);
      } else {
        image = await this._globalService.makeRequest(
          this.site.favicon.downloadURL
        );
      }

      const logo = workbook.addImage({
        base64: image,
        extension: 'png',
      });
      worksheet.addImage(logo, 'E1:E5');
      worksheet.getColumn(5).width = 25;
    }
    worksheet.mergeCells('A1:D2');
    worksheet.mergeCells('A3:D3');
    worksheet.mergeCells('A4:D4');
    worksheet.mergeCells('A5:D5');

    worksheet.addRow([]);

    if (rangeDate) {
      const rangeDateRow = worksheet.addRow([rangeDate]);
      rangeDateRow.font = { size: 9 };
      rangeDateRow.getCell(1).border = {
        top: { style: 'thin' },
        left: { style: 'thin' },
        bottom: { style: 'thin' },
        right: { style: 'thin' },
      };

      worksheet.mergeCells(`A${rangeDateRow.number}:F${rangeDateRow.number}`);
      worksheet.addRow([]);
    }

    if (total) {
      const totalRow = worksheet.addRow([total]);
      totalRow.font = { size: 10 };
      totalRow.getCell(1).fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: 'FFCCFFE5' },
      };
      totalRow.getCell(1).border = {
        top: { style: 'thin' },
        left: { style: 'thin' },
        bottom: { style: 'thin' },
        right: { style: 'thin' },
      };

      worksheet.mergeCells(`A${totalRow.number}:F${totalRow.number}`);
      worksheet.addRow([]);
    }

    const headerRow = worksheet.addRow(data.header);

    if (!custom || (custom && customHeaderActive)) {
      headerRow.eachCell((cell: any, _number: any) => {
        cell.font = { bold: true, size: 12 };
        cell.fill = {
          type: 'pattern',
          pattern: 'solid',
          fgColor: { argb: 'FFFFFF00' },
          bgColor: { argb: 'FF0000FF' },
        };
        cell.border = {
          top: { style: 'thin' },
          left: { style: 'thin' },
          bottom: { style: 'thin' },
          right: { style: 'thin' },
        };
      });
    }

    let index = 0;
    console.log('data.tableStyle', data.tableStyle);
    data.table.forEach((itemData: any) => {
      const row = worksheet.addRow(itemData);
      console.log('itemData', itemData);
      let indexSub = 0;
      row.eachCell((cell: any, _number: any) => {
        console.log(cell, _number);
        if (custom && customHeaderActive) {
          console.log(
            'data.tableStyle[index][indexSub]',
            data.tableStyle[index][indexSub]
          );
          if (data.tableStyle[index][indexSub] === 'standardBody') {
            cell.font = { bold: false, size: 10 };
          } else if (
            data.tableStyle[index][indexSub] === 'standardTitleAccounting'
          ) {
            cell.font = { bold: true, size: 11 };
          } else if (
            data.tableStyle[index][indexSub] === 'standardSuccessAccounting'
          ) {
            cell.font = { bold: true, size: 11, color: { argb: '00188643' } };
          } else if (
            data.tableStyle[index][indexSub] === 'standardDangerAccounting'
          ) {
            cell.font = { bold: true, size: 11, color: { argb: '00861818' } };
          }
        } else {
          cell.font = { bold: false, size: 10 };
        }
        cell.border = {
          top: { style: 'thin' },
          left: { style: 'thin' },
          bottom: { style: 'thin' },
          right: { style: 'thin' },
        };
        indexSub++;
      });
      index++;
    });

    let indexWidth = 1;
    data.width.forEach((item: any) => {
      if (indexWidth !== 5) worksheet.getColumn(indexWidth).width = item;
      indexWidth++;
    });

    worksheet.addRow([]);

    const fullDate = await this._globalService.rawFullDateNameSpace();
    const date =
      (await this._globalService.getTranslate('Date')) + ': ' + fullDate;

    const footerRow = worksheet.addRow([date]);
    footerRow.font = { size: 9 };
    footerRow.getCell(1).fill = {
      type: 'pattern',
      pattern: 'solid',
      fgColor: { argb: 'FFCCFFE5' },
    };
    footerRow.getCell(1).border = {
      top: { style: 'thin' },
      left: { style: 'thin' },
      bottom: { style: 'thin' },
      right: { style: 'thin' },
    };

    worksheet.mergeCells(`A${footerRow.number}:F${footerRow.number}`);
    worksheet.headerFooter.oddFooter = '&LPage &P of &N';

    return workbook;
  }

  async onLoad(
    action: string,
    title?: string,
    data?: any,
    header?: any,
    accounting?: any,
    rangeDate?: string,
    total?: string,
    custom: boolean = false,
    customHeaderActive: boolean = false
  ): Promise<any> {
    this._loadingService.loading.isLoading = true;

    this.site = JSON.parse(localStorage.getItem(environment.domain + '-site'));
    const workbook = await this.setDocument(
      title,
      data,
      header,
      accounting,
      rangeDate,
      total,
      custom
    );
    const fileName = this._globalService.setFileName(title);

    let type: string;
    if (action === 'xlsx') {
      type =
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
    }
    workbook[action].writeBuffer().then((dataWorkbook: any) => {
      this._loadingService.loading.isLoading = false;
      const blob = new Blob([dataWorkbook], { type });
      FileSaver.saveAs(blob, fileName + '.' + action);
    });
  }
}
