import { Injectable } from '@angular/core';

import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { ToastrService } from 'ngx-toastr';

import * as _ from 'lodash';
const moment = require('moment');
moment();

import { ForgotPasswordComponent } from '@app/auth/components/auth-login/forgot-password/forgot-password.component';
import { environment } from '@environments/environment';
import { Router } from '@angular/router';
import { User } from '@shared/model/user.model';
import { Business } from '../model/business.model';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class GlobalAction {
  modalRef: BsModalRef;

  constructor(
    private modalService: BsModalService,
    public toastr?: ToastrService,
    private router?: Router,
    private _translateService?: TranslateService
  ) {}

  showToast(event: any): void {
    this.toastr[event.type](event.message, event.title, {
      closeButton: true,
      timeOut: 5000,
      extendedTimeOut: 1000,
      easing: 'easi-in',
      easiTime: 300,
      progressBar: true,
      progressAnimation: 'increasing',
      positionClass: 'toast-top-center',
      titleClass: 'toast-title',
      messageClass: 'toast-message',
      tapToDismiss: true,
      onActivateTick: false,
    });
  }

  randomName(type: any, length: any): string {
    let charset = '';
    let randomName = '';

    const numeric = '0123456789';
    const alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLNMOPQRSTUPWXYZ';
    const alphaNumberic = numeric + alphabet;

    if (type === 'numeric') {
      charset = numeric;
    } else if (type === 'alphabet') {
      charset = alphabet;
    } else if (type === 'alphaNumeric') {
      charset = alphaNumberic;
    }

    for (let i = 0; i < length; i++) {
      randomName += charset.charAt(Math.floor(Math.random() * charset.length));
    }
    return randomName;
  }

  openModalForgotPasswordStatic(modalClass?: any, data?: any) {
    const navParam = {
      initialState: {
        navParam: data,
      },
      class: modalClass,
    };

    this.modalRef = this.modalService.show(ForgotPasswordComponent, navParam);
  }

  getRoleLogin() {
    const role = localStorage.getItem(environment.domain + '-LoginRole');
    return role;
  }

  getBusiness() {
    const business: Business[] | any = localStorage.getItem(
      environment.domain + '-business'
    );
    return JSON.parse(business);
  }

  getUserLogin() {
    const user = localStorage.getItem(environment.domain + '-Login');
    return JSON.parse(user);
  }

  isMonkeyAccount() {
    const user: User = this.getUserLogin();

    if (!user.isMonkey) {
      this.showToast({
        title: this._translateService.instant('Info!'),
        message: this._translateService.instant(
          'Do not have access permission!'
        ),
        type: 'error',
      });
      this.router.navigateByUrl('/dashboard');
      return true;
    }

    return false;
  }

  isEmptyBusinessValue() {
    return new Promise((resolve, reject) => {
      const business: Business[] = this.getBusiness();

      if ((business && business.length === 0) || !business) {
        resolve(true);
      }

      resolve(false);
    });
  }

  isEmptyBusiness() {
    return new Promise((resolve, reject) => {
      const business: Business[] = this.getBusiness();

      if ((business && business.length === 0) || !business) {
        this.showToast({
          title: this._translateService.instant('Info!'),
          message: this._translateService.instant('Add Business First!'),
          type: 'error',
        });
        resolve(true);
        this.router.navigateByUrl('/dashboard');
      }

      resolve(false);
    });
  }

  async alertPermissionStaff() {
    this.showToast({
      title: 'Info',
      type: 'error',
      message: await this._translateService.instant(
        'Please Contact Your Supervisor For This!'
      ),
    });
  }

  async getFirstURL() {}

  async menuRouting(link: any) {
    return true;
  }

  async submenuRouting(link: any) {
    return true;
  }
}
