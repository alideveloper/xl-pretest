import { Injectable } from '@angular/core';
import { Coordinates } from '@app/shared/services/maps/maps.model';

import * as _ from 'lodash';

@Injectable()
export class MapsService {
  constructor() {}

  getLocation(): Promise<any> {
    return new Promise((resolve, reject) => {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(
          (position: Position) => {
            if (position) {
              const coords: Coordinates = {
                accuracy: position.coords.accuracy,
                altitude: position.coords.altitude,
                altitudeAccuracy: position.coords.altitudeAccuracy,
                heading: position.coords.heading,
                latitude: position.coords.latitude,
                longitude: position.coords.longitude,
                speed: position.coords.speed,
              };
              resolve(coords);
            }
            resolve(false);
          },
          (error: PositionError) => {
            console.log(error);
            resolve(false);
          }
        );
      } else {
        alert('Geolocation is not supported by this browser.');
        resolve(false);
      }
    });
  }

  mapClick(event: any): any {
    return {
      latitude: event.coords.lat,
      longitude: event.coords.lng,
    };
  }
}
