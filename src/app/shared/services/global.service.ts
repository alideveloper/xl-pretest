import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';

import { CurrencyPipe } from '@angular/common';

import * as _ from 'lodash';
const moment = require('moment');
moment();
import { TranslateService } from '@ngx-translate/core';
declare let JsBarcode: any;
@Injectable()
export class GlobalService {
  constructor(
    private http: HttpClient,
    private cp: CurrencyPipe,
    private _translateService: TranslateService
  ) {}

  public bindingDataGlobal(condition: any): any {
    if (condition) {
      if (condition.type === 'year') {
        return this.bindingDataYear(condition);
      }
    }
  }

  public bindingDataYear(condition: any): any {
    let date;
    if (condition.max === 'yearNow') {
      date = new Date();
    }

    const data = [];
    for (
      let i = date.getFullYear();
      i > date.getFullYear() - condition.min;
      i--
    ) {
      const item = {
        label: i,
        value: i,
      };
      data.push(item);
    }

    return data;
  }

  public conditionRecursive(condition: any, value?: any): boolean {
    let result = false;
    if (condition && condition.length > 0) {
      condition.forEach((itemCondition: any) => {
        if (
          itemCondition.type === 'value' &&
          value !== undefined &&
          value !== null
        ) {
          result = this.conditionRecursiveItem(result, itemCondition, value);
        }
      });
    }
    return result;
  }

  public conditionRecursiveItem(
    result: boolean,
    itemCondition: any,
    value?: any
  ): boolean {
    if (itemCondition.operator === 'notnull') {
      if (
        itemCondition.parentKey
          ? !value[itemCondition.parentKey][itemCondition.key]
          : !value[itemCondition.key]
      ) {
        result = true;
      }
    }
    if (itemCondition.operator === 'equal') {
      if (
        (itemCondition.parentKey
          ? value[itemCondition.parentKey][itemCondition.key]
          : value[itemCondition.key]) === itemCondition.value
      ) {
        result = true;
      }
    }
    if (itemCondition.operator === 'notequal') {
      if (
        (itemCondition.parentKey
          ? value[itemCondition.parentKey][itemCondition.key]
          : value[itemCondition.key]) !== itemCondition.value
      ) {
        result = true;
      }
    }

    if (itemCondition.operator === 'notnullArray') {
      if (value[itemCondition.key]) {
        result = true;
      } else {
      }
    }

    if (itemCondition.operator === 'emptyArray') {
      if (value[itemCondition.key] && value[itemCondition.key].length === 0) {
      } else {
        result = true;
      }
    }

    if (itemCondition.operator === 'emptyObject') {
      console.log(value);
      if (value[itemCondition.key]) {
      } else {
        result = true;
      }
    }
    return result;
  }

  public checkTypeData(text: any, type: any): any {
    if (!type) return false;

    if (type === 'boolean') {
      return true;
    } else if (type === 'text') {
      if (text) {
        return true;
      }
    } else if (type === 'link') {
      if (text) {
        return true;
      }
    } else if (type === 'price') {
      if (text) {
        return true;
      }
    } else if (type === 'image') {
      if (text) {
        return true;
      }
    } else if (type === 'timestamp') {
      if (text) {
        return true;
      }
    } else if (type === 'date') {
      if (text) {
        return true;
      }
    } else if (type === 'number') {
      if (text !== undefined && text !== null) {
        return true;
      }
    }
    return false;
  }

  public returnByTypeData(text: any, type: any, format?: any): any {
    if (type === 'boolean') {
      return text;
    }
    if (type === 'link') {
      if (text) {
        return text;
      }
    }
    if (type === 'text') {
      if (text) {
        return text;
      }
    }
    if (type === 'price') {
      if (text) {
        return this.cp.transform(text, 'Rp', 'symbol', '1.2-2');
      }
    }
    if (type === 'number') {
      if (text !== undefined && text !== null) {
        return text;
      }
    }
    if (type === 'image') {
      if (text) {
        return text;
      }
    } else if (type === 'timestamp') {
      if (text) {
        try {
          return moment(text.seconds * 1000).format(format);
        } catch (error) {
          return 'Invalid Date';
        }
      }
    } else if (type === 'date') {
      if (text) {
        try {
          return moment(text).format(format);
        } catch (error) {
          return 'Invalid Date';
        }
      }
    }
    return null;
  }

  public checkData(schema: any, value?: any): any {
    if (schema.binding) {
      return;
    }
  }

  public recursiveData(
    schema: any,
    value: any,
    type?: any,
    binding?: boolean,
    defaultValue?: any
  ): any {
    if (schema === '' || !schema) {
      return this.recursiveObjectFalse(schema);
    }
    const regex = /^[a-zA-Z0-9]+$/;
    const result = regex.test(schema);

    if (!result) {
      const splitData = schema.split('.');
      if (binding) {
        if (splitData[2]) {
          if (
            value[splitData[0]] &&
            splitData[1] &&
            value[splitData[0]][splitData[1]] &&
            value[splitData[0]][splitData[1]][splitData[2]]
          ) {
            return this.recursiveObjectTrue(
              value[splitData[0]][splitData[1]][splitData[2]],
              type
            );
          }
        }

        if (splitData[1]) {
          if (
            value[splitData[0]] &&
            value[splitData[0]][splitData[1]] !== undefined &&
            value[splitData[0]] &&
            value[splitData[0]][splitData[1]] !== null
          ) {
            return this.recursiveObjectTrue(
              value[splitData[0]][splitData[1]],
              type
            );
          }
        }
      }

      return this.recursiveHardcode(splitData[0], schema);
    }

    if (value[schema]) {
      return value[schema];
    }
    return schema;
  }

  public recursiveObject(schema: any, value: any): any {
    if (schema.text === '' || !schema.text) {
      return this.recursiveObjectFalse(schema);
    }
    const regex = /^[a-zA-Z0-9]+$/;
    const result = regex.test(schema.text);

    if (!result) {
      const splitData = schema.text.split('.');
      if (schema.binding) {
        if (splitData[2]) {
          if (
            value[splitData[0]] &&
            splitData[1] &&
            value[splitData[0]][splitData[1]] &&
            value[splitData[0]][splitData[1]][splitData[2]]
          ) {
            return this.recursiveObjectTrue(
              value[splitData[0]][splitData[1]][splitData[2]],
              schema.type,
              schema.format
            );
          }
        }

        if (splitData[1]) {
          if (
            value[splitData[0]] &&
            value[splitData[0]][splitData[1]] !== undefined &&
            value[splitData[0]] &&
            value[splitData[0]][splitData[1]] !== null
          ) {
            return this.recursiveObjectTrue(
              value[splitData[0]][splitData[1]],
              schema.type,
              schema.format
            );
          }
        }
      }

      return this.recursiveHardcode(splitData[0], schema);
    }

    if (schema.binding) {
      return this.recursiveObjectTrue(
        value[schema.text],
        schema.type,
        schema.format
      );
    } else if (!schema.defaultText) {
      return this.recursiveHardcode(schema.text, schema);
    }

    return this.recursiveObjectFalse(schema);
  }

  recursiveHardcode(text: any, schema: any): any {
    return this.checkTypeData(text, schema.type)
      ? this.returnByTypeData(text, schema.type, schema.format)
      : null;
  }

  recursiveObjectTrue(text: any, type: any, format?: any): any {
    return this.checkTypeData(text, type)
      ? this.returnByTypeData(text, type, format)
      : null;
  }

  recursiveObjectFalse(schema: any): any {
    return this.checkTypeData(schema.defaultText, schema.defaultType)
      ? this.returnByTypeData(schema.defaultText, schema.defaultType)
      : null;
  }

  recursiveDataFalse(defaultValue: any, type: any): any {
    return this.checkTypeData(defaultValue, type)
      ? this.returnByTypeData(defaultValue, type)
      : null;
  }

  public async getJSON(URL: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      this.http.get(URL).subscribe(
        (result) => {
          resolve(result);
        },
        (error) => {
          reject();
        }
      );
    });
  }

  public filterArrayByString(mainArr: any, searchText: any): any {
    if (searchText === '') {
      return mainArr;
    }

    searchText = searchText.toLowerCase();

    return mainArr.filter((itemObj: any) => {
      return this.searchInObj(itemObj, searchText);
    });
  }

  public searchInObj(itemObj: any, searchText: any): boolean {
    for (const prop in itemObj) {
      if (!itemObj.hasOwnProperty(prop)) {
        continue;
      }

      const value = itemObj[prop];

      if (typeof value === 'string') {
        if (this.searchInString(value, searchText)) {
          return true;
        }
      } else if (Array.isArray(value)) {
        if (this.searchInArray(value, searchText)) {
          return true;
        }
      }

      if (typeof value === 'object') {
        if (this.searchInObj(value, searchText)) {
          return true;
        }
      }
    }
  }

  public searchInArray(arr: any, searchText: any): boolean {
    for (const value of arr) {
      if (typeof value === 'string') {
        if (this.searchInString(value, searchText)) {
          return true;
        }
      }

      if (typeof value === 'object') {
        if (this.searchInObj(value, searchText)) {
          return true;
        }
      }
    }
  }

  public searchInString(value: any, searchText: any): any {
    return value.toLowerCase().includes(searchText);
  }

  showHideConditions(schema: any, value: any): boolean {
    if (
      schema &&
      schema.state &&
      schema.state.hasOwnProperty('hide') &&
      schema.state.hasOwnProperty('hideCondition') &&
      schema.state.hideCondition.length > 0
    ) {
      return this.conditionRecursive(schema?.state?.hideCondition, value);
    }
  }

  disabledConditions(schema: any, value: any): boolean {
    if (
      schema &&
      schema.state &&
      schema.state.hasOwnProperty('disabled') &&
      schema.state.hasOwnProperty('disabledCondition') &&
      schema.state.disabledCondition.length > 0
    ) {
      return this.conditionRecursive(schema?.state?.disabledCondition, value);
    }
  }

  get fullDateFileNameNow(): any {
    const date = new Date();
    const yearNow = date.getFullYear();
    const monthNow = ('0' + (date.getMonth() + 1)).slice(-2);
    const dateNow = ('0' + (date.getDate() + 1)).slice(-2);
    return yearNow + '' + monthNow + '' + dateNow;
  }

  get fullDateFileNow(): any {
    const date = new Date();
    const yearNow = date.getFullYear();
    const monthNow = ('0' + (date.getMonth() + 1)).slice(-2);
    const dateNow = ('0' + (date.getDate() + 1)).slice(-2);
    return yearNow + '-' + monthNow + '-' + dateNow;
  }

  get fullDateNow(): any {
    const date = new Date();
    const yearNow = date.getFullYear();
    const monthNow = ('0' + (date.getMonth() + 1)).slice(-2);
    const dateNow = ('0' + (date.getDate() + 1)).slice(-2);
    return yearNow + '/' + monthNow + '/' + dateNow;
  }

  get fullMonthNow(): any {
    const date = new Date();
    const monthNow = date.getMonth();
    return monthNow;
  }

  get fullYearNow(): any {
    const date = new Date();
    const yearNow = date.getFullYear();
    return yearNow;
  }

  async fullMonthName(date: any): Promise<any> {
    const monthNumber = date.getMonth();
    console.log('monthNumber', monthNumber);
    const listMonth = await this.listMonth();

    return listMonth[monthNumber];
  }

  fullDateNowParam(date: any): any {
    date = new Date(date);
    const yearNow = date.getFullYear();
    const monthNow = ('0' + (date.getMonth() + 1)).slice(-2);
    const dateNow = ('0' + (date.getDate() + 1)).slice(-2);
    return yearNow + '/' + monthNow + '/' + dateNow;
  }

  async fullDateNameTimezone(date: any): Promise<any> {
    console.log('fullDateName', date);
    const yearNow = date.getFullYear();
    const monthNowNumber = date.getMonth();
    const dateNowNumber = date.getDate() + 1;

    const listMonth = await this.listMonth();

    return yearNow + ' ' + listMonth[monthNowNumber] + ' ' + dateNowNumber;
  }

  async fullDateName(date: any): Promise<any> {
    console.log('fullDateName', date);
    const yearNow = date.getFullYear();
    const monthNowNumber = date.getMonth();
    const dateNowNumber = date.getDate();

    const listMonth = await this.listMonth();

    return yearNow + ' ' + listMonth[monthNowNumber] + ' ' + dateNowNumber;
  }

  async onlyMonth(date: any): Promise<any> {
    console.log('fullDateName', date);
    const monthNumber = date.getMonth();

    return monthNumber;
  }

  async onlyYear(date: any): Promise<any> {
    const yearNumber = date.getFullYear();

    return yearNumber;
  }

  async fullDateNameNow(): Promise<any> {
    const date = new Date();
    const yearNow = date.getFullYear();
    const monthNow = ('0' + (date.getMonth() + 1)).slice(-2);
    const monthNowNumber = date.getMonth();
    const dateNow = ('0' + (date.getDate() + 1)).slice(-2);
    const dateNowNumber = date.getDate();

    const listMonth = await this.listMonth();

    return yearNow + ' ' + listMonth[monthNowNumber] + ' ' + dateNowNumber;
  }

  async dayNameNow(): Promise<any> {
    const date = new Date();
    const dayNow = date.getDay();

    const listDay = await this.listDay();

    return listDay[dayNow];
  }

  get dayNow(): any {
    const date = new Date();
    const dayNow = date.getDay();
    return dayNow;
  }

  get fullTimeNow(): any {
    const date = new Date();
    const hourNow = ('0' + (date.getHours() + 1)).slice(-2);
    const minuteNow = ('0' + (date.getMinutes() + 1)).slice(-2);
    const secondNow = ('0' + (date.getSeconds() + 1)).slice(-2);
    return hourNow + ':' + minuteNow + ':' + secondNow;
  }

  fullTimeNowParam(date: any): any {
    date = new Date(date);
    const hourNow = ('0' + (date.getHours() + 1)).slice(-2);
    const minuteNow = ('0' + (date.getMinutes() + 1)).slice(-2);
    const secondNow = ('0' + (date.getSeconds() + 1)).slice(-2);
    return hourNow + ':' + minuteNow + ':' + secondNow;
  }

  get rawFullDate(): any {
    return this.dayNow + ', ' + this.fullDateNow + ' ' + this.fullTimeNow;
  }

  async rawFullDateName(): Promise<any> {
    return (
      (await this.dayNameNow()) +
      ', ' +
      (await this.fullDateNameNow()) +
      '\n' +
      this.fullTimeNow
    );
  }

  async rawFullDateNameSpace(): Promise<any> {
    return (
      (await this.dayNameNow()) +
      ', ' +
      (await this.fullDateNameNow()) +
      ' ' +
      this.fullTimeNow
    );
  }

  get rawOnlyDateTime(): any {
    return this.fullDateNow + ' ' + this.fullTimeNow;
  }

  rawOnlyDateDividerTime(date: any): any {
    return this.fullDateNowParam(date) + '\n' + this.fullTimeNowParam(date);
  }

  async rawOnlyMonthName(date: any): Promise<any> {
    return await this.fullMonthName(date);
  }

  rawOnlyDate(date: any): any {
    return this.fullDateNowParam(date);
  }

  rawOnlyDateDividerTimeSpace(date: any): any {
    return this.fullDateNowParam(date) + ' ' + this.fullTimeNowParam(date);
  }

  async listMonth(): Promise<any> {
    const month = new Array();
    month[0] = await this.getTranslate('January');
    month[1] = await this.getTranslate('February');
    month[2] = await this.getTranslate('March');
    month[3] = await this.getTranslate('April');
    month[4] = await this.getTranslate('May');
    month[5] = await this.getTranslate('June');
    month[6] = await this.getTranslate('July');
    month[7] = await this.getTranslate('August');
    month[8] = await this.getTranslate('September');
    month[9] = await this.getTranslate('October');
    month[10] = await this.getTranslate('November');
    month[11] = await this.getTranslate('December');
    return month;
  }

  async listDay(): Promise<any> {
    const day = new Array();
    day[0] = await this.getTranslate('Sunday');
    day[1] = await this.getTranslate('Monday');
    day[2] = await this.getTranslate('Tuesday');
    day[3] = await this.getTranslate('Wednesday');
    day[4] = await this.getTranslate('Thursday');
    day[5] = await this.getTranslate('Friday');
    day[6] = await this.getTranslate('Saturday');
    return day;
  }

  getTranslate(text: any): Promise<any> {
    return new Promise((resolve, reject) => {
      this._translateService.get(text).subscribe((response: any) => {
        const result = response;
        resolve(result);
      });
    });
  }

  textToBase64Barcode(text: string) {
    const canvas = document.createElement('canvas');
    JsBarcode(canvas, text, { format: 'CODE128', displayValue: false });
    return canvas.toDataURL('image/png');
  }

  getBase64(file: any): Promise<any> {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = async () => {
        resolve(reader.result);
      };
      reader.onerror = (error) => {
        console.log('Error: ', error);
      };
    });
  }

  removeBase64tag(base64string: any): any {
    const base64Files = base64string.split(',');
    return base64Files.length > 1 ? base64Files[1] : base64Files[0];
  }

  makeRequest(url: any, method = 'GET'): Promise<any> {
    const request = new XMLHttpRequest();
    return new Promise(async (resolve, reject) => {
      request.onload = async () => {
        if (request.readyState !== 4) return;
        if (request.status >= 200 && request.status < 300) {
          const base64 = await this.getBase64(request.response);
          resolve(base64);
        } else {
          reject({
            status: request.status,
            statusText: request.statusText,
          });
        }
      };
      request.responseType = 'blob';
      request.open(method || 'GET', url, true);
      request.send();
    });
  }

  setFileName(title: any): any {
    title = title.toLowerCase();
    title = title.replace(' ', '-');

    const randomName = this.randomName('alphaNumeric', 6);

    title = title + '-' + this.fullDateFileNameNow + '-' + randomName;

    return title;
  }

  titleCase(str: any): any {
    str = str.toLowerCase();

    str = str.split(' ');
    for (let i = 0; i < str.length; i++) {
      str[i] = str[i].charAt(0).toUpperCase() + str[i].slice(1);
    }

    return str.join(' ');
  }

  randomName(type: any, length: number): string {
    let charset = '';
    let randomName = '';

    const numeric = '0123456789';
    const alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLNMOPQRSTUPWXYZ';
    const alphaNumberic = numeric + alphabet;

    if (type === 'numeric') {
      charset = numeric;
    } else if (type === 'alphabet') {
      charset = alphabet;
    } else if (type === 'alphaNumeric') {
      charset = alphaNumberic;
    }

    for (let i = 0; i < length; i++) {
      randomName += charset.charAt(Math.floor(Math.random() * charset.length));
    }
    return randomName;
  }
}
