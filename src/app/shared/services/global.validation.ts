import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';

import * as _ from 'lodash';

@Injectable()
export class GlobalValidation {
  constructor(private http: HttpClient) {}

  public checkValidationAll(schemas: any, result: boolean = false): boolean {
    if (schemas && schemas.length > 0) {
      schemas.forEach((schema: any) => {
        if (schema.hasOwnProperty('validationInput')) {
          if (!result) {
            result = schema.validationInput;
          }
        }
        if (schema.schemas && schema.schemas.length > 0) {
          result = this.checkValidationAll(schema.schemas, result);
        }
      });
    }
    return result;
  }

  public checkValidation(validation: any, value: any): any {
    let result = { status: true, text: '' };
    if (validation.required) {
      result = this.isRequired(validation, value);
      if (!result.status) {
        return result;
      }
    }

    if (validation.minLength) {
      result = this.isMinLength(validation, value);
      if (!result.status) {
        return result;
      }
    }

    if (result.status) {
      return result;
    }
  }

  public checkPipeValidation(pipe: any, value: any, values?: any): any {
    const result = { status: true, text: '' };

    pipe.forEach((itemPipe: any) => {
      if (itemPipe.key === 'email') {
        if (!this.validateEmail(value)) {
          result.status = false;
          result.text = 'Email not valid, Example: example@gmail.com';
          return result;
        }
      }
      if (itemPipe.key === 'url') {
        if (!this.validateURL(value)) {
          result.status = false;
          result.text = 'URL not valid, Example: https://www.example.com';
          return result;
        }
      }
      if (itemPipe.key === 'sameValue') {
        if (!this.validateSameValue(itemPipe, value, values)) {
          result.status = false;
          result.text = itemPipe.text;
          return result;
        }
      }
    });

    return result;
  }

  validateEmail(email: any) {
    const regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regex.test(email);
  }

  validateURL(url: any) {
    const regex = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)?/gi;
    return regex.test(url);
  }

  validateSameValue(pipe: any, value: any, values?: any) {
    if (
      values[pipe.parentBinding] &&
      values[pipe.parentBinding][pipe.bindingObject] &&
      value
    ) {
      if (values[pipe.parentBinding][pipe.bindingObject] !== value) {
        return false;
      }
    }
    return true;
  }

  isMinLength(validation: any, value: any): any {
    let result = { status: true, text: '' };
    if ((value && validation.minLength > value.length) || !value) {
      result = {
        status: false,
        text: 'Min Length ' + validation.minLength + ' Character',
      };
    }
    return result;
  }

  isRequired(validation: any, value: any): any {
    let result = { status: true, text: '' };
    if (value === undefined && value === null) {
      result = {
        status: false,
        text: 'Required Field',
      };
    }
    return result;
  }
}
