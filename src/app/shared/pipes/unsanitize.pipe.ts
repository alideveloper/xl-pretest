import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

@Pipe({
  name: 'unsanitize',
})
export class UnsanitizePipe implements PipeTransform {
  constructor(private domSanitizer: DomSanitizer) {}

  transform(html: string): SafeHtml {
    return this.domSanitizer.bypassSecurityTrustHtml(html);
  }

  url(url: any): SafeHtml {
    return this.domSanitizer.bypassSecurityTrustResourceUrl(url);
  }

  resourceURL(url: any): any {
    const result: any = this.domSanitizer.bypassSecurityTrustResourceUrl(url);
    return result.changingThisBreaksApplicationSecurity;
  }
}
