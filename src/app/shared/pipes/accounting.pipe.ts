import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'accounting',
})
export class AccountingPipe implements PipeTransform {
  constructor() {}
  transform(
    value: string,
    align: string = 'right',
    precision: number = 2,
    decimal: string = '.',
    prefix: string = 'Rp',
    suffix: string = '',
    thousands: string = ',',
    nullable: boolean = false
  ): any {
    value = value.toString().replace(/,/g, '').replace(/^0+/, '');
    let [integer, fraction = ''] = (value || '').toString().split('.');
    fraction =
      precision > 0
        ? decimal + (fraction + '0000000000000000').substring(0, precision)
        : '';
    integer = integer.replace(/\B(?=(\d{3})+(?!\d))/g, thousands);
    console.log('prefix', prefix);
    console.log('integer', integer);
    console.log('fraction', fraction);
    return prefix + (integer ? integer : 0) + fraction;
  }
}
