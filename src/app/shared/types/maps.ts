import { Label } from './label';
import { State } from './state';

export class Maps {
  component?: string;
  position?: string;
  label?: Label;
  labelSubtitle?: Label;
  zoom?: number;
  minZoom?: number;
  maxZoom?: number;
  parentBinding?: any;
  latitude?: any;
  required?: boolean;
  longitude?: any;
  search?: Search;
  address?: Address;
  style?: any;
  styleClass?: string;
  state?: State;
  callbackMaps?: Callback;
  isView?: boolean;
}

export interface Address {
  active?: boolean;
  bindingObject?: any;
}

export interface Search {
  active?: boolean;
  placeholder?: any;
}

export interface Callback {
  accuracy?: any;
  altitude?: any;
  altitudeAccuracy?: any;
  heading?: any;
  speed?: any;
  latitude?: any;
  longitude?: any;
}

export interface Coordinates {
  accuracy?: number;
  altitude?: number | null;
  altitudeAccuracy?: number | null;
  heading?: number | null;
  latitude?: number;
  longitude?: number;
  speed?: number | null;
}
