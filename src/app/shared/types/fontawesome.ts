export interface FontAwesome {
  icon?: any;
  style?: any;
  styleClass?: string;
  size?: string;
}
