import { Label } from './label';
import { State } from './state';

export interface Text {
  label?: Label;
  style?: any;
  styleClass?: string;
  state?: State;
}
