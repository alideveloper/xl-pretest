import {
  trigger,
  state,
  style,
  transition,
  animate,
  group,
  query,
  stagger,
  keyframes,
} from '@angular/animations';

export const SlideInOutAnimation = [
  trigger('slideInOut', [
    state(
      'in',
      style({
        'max-height': '500px',
        opacity: '1',
        visibility: 'visible',
      })
    ),
    state(
      'out',
      style({
        'max-height': '0px',
        opacity: '0',
        visibility: 'hidden',
      })
    ),
    transition('in => out', [
      group([
        animate(
          '5000ms ease-in-out',
          style({
            opacity: '0',
          })
        ),
        animate(
          '600ms ease-in-out',
          style({
            'max-height': '0px',
          })
        ),
        animate(
          '700ms ease-in-out',
          style({
            visibility: 'hidden',
          })
        ),
      ]),
    ]),
    transition('out => in', [
      group([
        animate(
          '5000ms ease-in-out',
          style({
            visibility: 'visible',
          })
        ),
        animate(
          '600ms ease-in-out',
          style({
            'max-height': '500px',
          })
        ),
        animate(
          '800ms ease-in-out',
          style({
            opacity: '1',
          })
        ),
      ]),
    ]),
  ]),
];

export const EnterLeaveAnimation = [
  trigger('enterLeave', [
    state('flyIn', style({ transform: 'translateX(0)' })),
    transition(':enter', [
      style({ transform: 'translateX(-100%)' }),
      animate('0.5s 300ms ease-in'),
    ]),
    transition(':leave', [
      animate('0.3s ease-out', style({ transform: 'translateX(100%)' })),
    ]),
  ]),
];

export const SlideAnimation = [
  trigger('slide', [
    state('left', style({ transform: 'translateX(0)' })),
    state('right', style({ transform: 'translateX(0)' })),
    transition('* => *', animate(500)),
  ]),
];

export const InOutAnimation = [
  trigger('inOutAnimation', [
    transition(':enter', [
      style({ height: 0, opacity: 0 }),
      animate('1s ease-out', style({ width: 300, opacity: 1 })),
    ]),
    transition(':leave', [
      style({ height: 300, opacity: 1 }),
      animate('1s ease-in', style({ width: 0, opacity: 0 })),
    ]),
  ]),
];

export const SidenavAnimation = [
  trigger('sidenavAnimation', [
    transition(':enter', [
      style({ width: 0, opacity: 0 }),
      animate('300ms ease-out', style({ width: 280, opacity: 1 })),
    ]),
    transition(':leave', [
      style({ opacity: 0.5 }),
      animate('300ms ease-in', style({ width: 0, opacity: 0 })),
    ]),
  ]),
];

export const SidenavContentAnimation = [
  trigger('sidenavContentAnimation', [
    transition(':enter', [
      animate('300ms ease-out', style({ width: 280, opacity: 1 })),
    ]),
    transition(':leave', [
      animate('300ms ease-in', style({ width: 0, opacity: 0 })),
    ]),
  ]),
];
