export interface Toolbar {
  title?: string;
  styleClass?: string;
}
