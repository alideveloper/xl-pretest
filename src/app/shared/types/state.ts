import { Condition } from './condition';

export interface State {
  hide?: boolean;
  disabled?: boolean;
  hideCondition?: Condition[];
  disabledCondition?: Condition[];
}
