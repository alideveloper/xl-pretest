import { Label } from './label';
import { State } from './state';

import { Validation } from './validation';

export interface Select {
  label?: Label;
  labelSubtitle?: Label;
  parentBinding?: any;
  bindingObject?: any;
  bindingSearch?: any;
  bindingData?: any;
  bindingDataGlobal?: any;
  primaryKey?: any;
  businessKey?: any;
  style?: any;
  styleClass?: string;
  state?: State;
  data: [];
  bindLabel?: string;
  bindingWhere?: any;
  bindingWhereFrom?: any;
  labelForId?: any;
  bindValue?: string;
  searchable?: boolean;
  grouping?: any;
  multiple?: boolean;
  required?: boolean;
  hideSelected?: boolean;
  search?: boolean;
  maxSelectedItems?: number;
  closeOnSelect?: boolean;
  groupBy?: string;
  selectableGroup?: any;
  selectableGroupAsModel?: any;
  defaultValue?: boolean;
  customSearch?: boolean;
  selectedAll?: boolean;
  customTemplate?: CustomTemplate;
  placeholder?: string;
  append?: Append;
  clearable?: boolean;
  formText?: FormText;
  validation?: Validation;
  validationInput?: boolean;
  selectedBusiness?: any;
}

export interface CustomTemplate {
  active?: boolean;
  type?: any;
  bindingName?: any;
  bindingPhoto?: any;
}

export interface FormText {
  valid?: FormTextItem;
  invalid?: FormTextItem;
  info?: FormTextItem;
  infoInnerHTML?: FormTextItem;
}

export interface FormTextItem {
  active?: boolean;
  text?: string;
}

export interface Append {
  icon?: IconAppend;
  text?: TextAppend;
  button?: ButtonAppend;
  position?: string;
}

export interface IconAppend {
  active?: boolean;
  position: 'left' | 'right';
  src?: any;
  size?: any;
  pulse?: boolean;
  spin?: boolean;
}

export interface TextAppend {
  active?: boolean;
  text?: string;
  style?: any;
  styleClass?: string;
}

export interface ButtonAppend {
  active?: boolean;
  text?: string;
  style?: any;
  styleClass?: string;
}
