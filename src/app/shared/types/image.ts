import { SVG } from './svg';
import { FontAwesome } from './fontawesome';
import { FirebaseStorage } from './firebase-storage';

export interface Image {
  type?: 'SVG' | 'image' | 'FontAwesome' | 'FirebaseStorage' | any;
  image?: SVG | URL | FontAwesome | FirebaseStorage | any;
  positionImage?: 'left' | 'right';
  bindingObject?: any;
  style?: any;
  active?: boolean;
  position?: 'left' | 'right';
  styleClass?: string;
  binding?: boolean;
}

export interface URL {
  downloadURL?: string;
}
