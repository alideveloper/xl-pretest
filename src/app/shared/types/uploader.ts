import { Label } from './label';
import { State } from './state';

import { Validation } from './validation';

export interface Uploader {
  component?: string;
  position?: string;
  label?: Label;
  labelSubtitle?: Label;
  isImage?: boolean;
  formText?: FormText;
  validation?: Validation;
  validationInput?: boolean;
  style?: any;
  styleClass?: string;
  state?: State;
  isDisabled?: boolean;
  multiple?: boolean;
  animation?: boolean;
  required?: boolean;
  minsize?: number;
  maxsize?: number;
  accept?: any;
  parentBinding?: any;
  bindingObject?: any;
  bindingObjectBlob?: any;
}

export interface FormText {
  valid?: FormTextItem;
  invalid?: FormTextItem;
  info?: FormTextItem;
  infoInnerHTML?: FormTextItem;
}

export interface FormTextItem {
  active?: boolean;
  text?: string;
}
