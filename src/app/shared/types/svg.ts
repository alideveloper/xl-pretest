export interface SVG {
  src?: string;
  srcParent?: string;
  style?: any;
  styleClass?: string;
  binding?: boolean;
}
