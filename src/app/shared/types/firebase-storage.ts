export interface FirebaseStorage {
  downloadURL?: boolean;
  size?: number;
  fileType?: any;
  filePath?: string;
  fileName?: string;
  style?: any;
  styleClass?: string;
}
