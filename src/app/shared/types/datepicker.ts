import { Label } from './label';
import { State } from './state';

import { Validation } from './validation';

export interface DatePicker {
  label?: Label;
  labelSubtitle?: Label;
  style?: any;
  styleClass?: string;
  styleClassDate?: string;
  state?: State;
  type: string;
  readonly?: boolean;
  required?: boolean;
  pipe?: any;
  isRange?: boolean;
  minDate?: any;
  maxDate?: any;
  disabledDates?: any;
  dateInputFormat?: any;
  roundedInput?: boolean;
  placeholder?: string;
  bindingObject?: any;
  bindingObjectRange?: any;
  parentBinding?: any;
  defaultValue?: any;
  append?: Append;
  formText?: FormText;
  validation?: Validation;
  validationInput?: boolean;
}

export interface FormText {
  valid: FormTextItem;
  invalid: FormTextItem;
  info: FormTextItem;
  infoInnerHTML: FormTextItem;
}

export interface FormTextItem {
  active?: boolean;
  text?: string;
}

export interface Append {
  icon?: IconAppend;
  text?: any;
  button?: ButtonAppend;
  position?: string;
}

export interface IconAppend {
  active?: boolean;
  position: 'left' | 'right';
  src?: any;
  size?: any;
  pulse?: boolean;
  spin?: boolean;
}

export interface TextAppend {
  active?: boolean;
  text?: string;
  style?: any;
  styleClass?: string;
}

export interface ButtonAppend {
  active?: boolean;
  text?: string;
  style?: any;
  styleClass?: string;
}
