export interface Sidenav {
  visible: boolean;
  style: any;
  styleClass: string;
  scroll: boolean;
  isMobile: boolean;
  position: string;
  background: string;
}
