import { Firebase } from './firebase';

export interface Action {
  type?: string;
  modalClass?: string;
  path?: any;
  firebase?: Firebase;
}
