export interface Firebase {
  status?: boolean;
  detail?: any;
  isObject?: boolean;
  firebase?: FirebaseItem[];
  relations?: Relation[];
}

export interface FirebaseItem {
  key: string;
  bindingKey: any;
  method?:
    | 'POST'
    | 'GET'
    | 'PUT'
    | 'DELETE'
    | 'UPLOADSTORAGE'
    | 'DELETESTORAGE';
  collection?: Collection;
  query?: Query[];
  orderBy?: OrderBy;
  relation?: Relation[];
}

export interface Collection {
  status?: boolean;
  name?: string;
  query?: Query[];
  orderBy?: OrderBy;
  relation?: Relation[];
  document?: Document;
}

export interface Relation {
  bindingFirebase?: string;
  bindingKey?: string;
  bindingObject?: string;
}

export interface Document {
  status?: boolean;
  name?: string;
  collection?: Collection;
}

export interface Query {
  object?: string;
  condition?: string;
  valueParent?: string;
  value?: string;
}

export interface OrderBy {
  object?: string;
  order?: 'asc' | 'desc';
}

export interface Relation {
  key?: string;
}
