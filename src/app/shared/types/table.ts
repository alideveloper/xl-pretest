import { State } from './state';

import { Image } from './image';
import { Label } from './label';
import { Select } from './select';
import { Input } from '@angular/core';

export interface Table {
  style?: any;
  styleClass?: string;
  state?: State;
  pagination?: boolean;
  filter?: Filter;
  emptyState?: EmptyState;
  export?: Export;
  schemaTable?: SchemaTable;
}

export interface EmptyState {
  filter?: boolean;
  filterImage?: Image;
  filterText?: Label;
  data?: boolean;
  dataImage?: Image;
  dataText?: Label;
}

export interface Filter {
  active?: boolean;
  type?: 'Input' | 'Select' | 'InputSelect';
  filterAll?: boolean;
  filterKey?: any;
  select?: Select;
  input?: Input;
}

export interface Export {
  pdf?: boolean;
  excel?: boolean;
}

export interface SchemaTable {
  head?: TableHead[];
  body?: TableBody[];
  style?: any;
  styleClass?: string;
  styleTable?: any;
  styleClassTable?: string;
}

export interface TableHead {
  style?: any;
  styleClass?: string;
  width?: string;
  text?: Label;
  sorting?: boolean;
  colspan?: number;
  rowspan?: number;
}

export interface TableBody {
  style?: any;
  styleClass?: string;
  index?: boolean;
  data?: Label | Image | any;
  colspan?: number;
  rowspan?: number;
}
