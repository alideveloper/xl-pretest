export interface Link {
  type?: any;
  bindingObject?: any;
  style?: any;
  styleClass?: string;
  binding?: boolean;
  target?: any;
}
