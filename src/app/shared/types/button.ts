import { Label } from './label';
import { Image } from './image';
import { State } from './state';
import { Action } from './action';

export interface Button {
  text?: {
    label?: Label;
  };
  image?: Image;
  style?: any;
  styleClass?: string;
  styleClassHost?: string;
  styleButton?: any;
  styleClassButton?: string;
  rounded?: boolean;
  disabledByValidation?: boolean;
  state?: State;
  action?: any;
}
