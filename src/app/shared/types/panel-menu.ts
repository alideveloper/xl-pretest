import { MenuItem } from 'primeng/api';

export interface PanelMenu {
  model?: MenuItem[];
  style?: any;
  styleClass?: string;
  multiple?: boolean;
  transitionOptions?: string;
}

// Styling
// 'ui-panelmenu' | 'ui-panelmenu-header' | 'ui-panelmenu-content' | 'ui-menu-list' | 'ui-menuitem' | 'ui-menuitem-text' |
// 'ui-menuitem-icon' | 'ui-panelmenu-icon'
