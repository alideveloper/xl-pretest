import { State } from './state';
import { Action } from './action';

import { Label } from './label';

export interface Tabs {
  isContainer: boolean;
  isCard?: boolean;
  style?: any;
  styleClass?: string;
  title?: Label;
  pills?: boolean;
  slide?: boolean;
  positionTabs?: string;
  tabs?: Tab[];
  tabsClean?: boolean;
  tabsBordered?: boolean;
}

export interface Icon {
  active?: boolean;
  styleClass?: string;
  image?: any;
}

export interface Tab {
  selected?: boolean;
  path?: any;
  icon?: Icon;
  label: Label;
  state?: State;
  action?: Action;
}
