import { ColorScheme } from '@app/core';

export enum AlertStyle {
  Thin = 'thin',
  Outline = 'outline',
}

export interface Alert {
  scheme?: ColorScheme;
  background?: ColorScheme;
  style?: AlertStyle;
  withIcon?: boolean;
  dismissible?: boolean;
  icon?: any;
}
