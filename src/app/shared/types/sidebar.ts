export interface Sidebar {
  visible?: boolean;
  position?: 'left' | 'right' | 'bottom' | 'top';
  fullScreen?: boolean;
  appendTo?: boolean;
  style?: any;
  styleClass?: string;
  blockScroll?: boolean;
  baseZIndex?: number;
  autoZIndex?: boolean;
  modal?: boolean;
  dismissible?: boolean;
  showCloseIcon?: boolean;
  ariaCloseLabel?: string;
  closeOnEscape?: boolean;
}

// Styling
// 'ui-sidebar' | 'ui-sidebar-left' | 'ui-sidebar-right' | 'ui-sidebar-top' | 'ui-sidebar-bottom' | 'ui-sidebar-full' |
// 'ui-sidebar-active' | 'ui-sidebar-close' | 'ui-sidebar-sm' | 'ui-sidebar-md' | 'ui-sidebar-lg' | 'ui-sidebar-mask'
