import { Label } from './label';
import { State } from './state';

import { Validation } from './validation';

export interface Textarea {
  label?: Label;
  labelSubtitle?: Label;
  style?: any;
  styleClass?: string;
  state?: State;
  json?: boolean;
  rows?: number;
  readonly?: boolean;
  search?: boolean;
  required?: boolean;
  minLength?: number;
  maxLength?: number;
  roundedInput?: boolean;
  placeholder?: string;
  onKeyUpActive?: boolean;
  onBlurActive?: boolean;
  bindingObject?: any;
  parentBinding?: any;
  defaultValue?: any;
  append?: Append;
  formText?: FormText;
  validation?: Validation;
  validationInput?: boolean;
  textEditor?: boolean;
}

export interface FormText {
  valid: FormTextItem;
  invalid: FormTextItem;
  info: FormTextItem;
  infoInnerHTML: FormTextItem;
}

export interface FormTextItem {
  active?: boolean;
  text?: string;
}

export interface Append {
  icon?: IconAppend;
  text?: TextAppend;
  button?: ButtonAppend;
  position?: string;
}

export interface IconAppend {
  active?: boolean;
  position: 'left' | 'right';
  src?: any;
  size?: any;
  pulse?: boolean;
  spin?: boolean;
}

export interface TextAppend {
  active?: boolean;
  text?: string;
  style?: any;
  styleClass?: string;
}

export interface ButtonAppend {
  active?: boolean;
  text?: string;
  style?: any;
  styleClass?: string;
}
