import { State } from './state';

export interface Container {
  styleClass?: string;
  style?: any;
  styleClassHost?: string;
  state?: State;
  bindingLoop?: any;
}
