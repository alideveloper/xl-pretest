import { Label } from './label';
import { State } from './state';

import { Validation } from './validation';

export interface Radio {
  label?: Label;
  labelSubtitle?: Label;
  data?: Data;
  style?: any;
  styleClass?: string;
  state?: State;
  name?: any;
  disabled?: boolean;
  readonly?: boolean;
  required?: boolean;
  bindingObject?: any;
  parentBinding?: any;
  defaultValue?: any;
  shadow?: any;
  formText?: FormText;
  validation?: Validation;
  validationInput?: boolean;
}

export interface Data {
  id?: boolean;
  label?: Label;
  checked?: boolean;
  value?: any;
}

export interface FormText {
  valid?: FormTextItem;
  invalid?: FormTextItem;
  info?: FormTextItem;
  infoInnerHTML?: FormTextItem;
}

export interface FormTextItem {
  active?: boolean;
  text?: string;
}
