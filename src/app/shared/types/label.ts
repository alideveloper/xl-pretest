import { State } from './state';

export interface Label {
  active?: boolean;
  binding?: boolean;
  style?: any;
  styleClass?: string;
  text?: string;
  state?: State;
  type?: 'text' | 'number' | 'currency' | 'boolean' | 'date';
  defaultText?: string;
  defaultType?: 'text' | 'number' | 'currency' | 'boolean' | 'date';
}
