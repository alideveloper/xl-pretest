export interface Condition {
  key?: any;
  operator?: '=' | '!=';
  value?: any;
  logic?: 'OR' | 'AND';
}
