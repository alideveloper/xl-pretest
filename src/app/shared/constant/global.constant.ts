import { ngxLoadingAnimationTypes } from 'ngx-loading';

export class GlobalConstants {
  public static loading: any = {
    isLoading: false,
    text: 'Loading...',
    primaryColour: '',
    secondaryColour: '',
    animation: ngxLoadingAnimationTypes.threeBounce,
    image: 'assets/img/svg/loading-person.svg',
  };
}
