import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { ToastrModule } from 'ngx-toastr';

import { BootstrapModule } from '@app/bootstrap/bootstrap.module';
import { CardsModule } from '@app/components/cards/cards.module';
import { FormControlsModule } from '@app/components/form-controls/form-controls.module';
import { IconsModule } from '@app/components/icons/icons.module';
import { SVGModule } from '@app/components/svg/svg.module';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { FileUploadModule } from '@iplab/ngx-file-upload';
import { AgmCoreModule } from '@agm/core';
import { NgxScrollTopModule } from 'ngx-scrolltop';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgxCurrencyModule } from 'ngx-currency';
import { NgxSkeletonModule } from 'ngx-skeleton';
import { NgPipesModule } from 'ngx-pipes';
import { NgxMaskModule } from 'ngx-mask';
import { NgxPatternModule } from 'ngx-pattern';
import { NgxBarcodeModule } from 'ngx-barcode';
import { NgxBarcodeScannerModule } from '@eisberg-labs/ngx-barcode-scanner';
import { QrCodeModule } from 'ng-qrcode';

import { UnsanitizePipe } from '@shared/pipes/unsanitize.pipe';

import { GlobalService } from '@shared/services/global.service';
import { GlobalAction } from '@shared/services/global.action';
import { GlobalValidation } from '@shared/services/global.validation';
import { MapsService } from '@shared/services/maps/maps.service';
import { LoadingService } from '@app/shared/services/loading.service';
import { PDFService } from '@app/shared/services/pdf.service';
import { ExcelService } from '@app/shared/services/excel.service';
import { AuthService } from '@app/shared/services/auth/services/auth.service';
import { AuthGuard } from '@app/shared/services/auth/guard/auth.guard';
import { AuthNonGuard } from '@app/shared/services/auth/guard/auth-non.guard';
import { AuthFreeGuard } from '@app/shared/services/auth/guard/auth-free.guard';

import { DropzoneDirective } from '@shared/directive/dropzone.directive';
import { AccountingPipe } from '@shared/pipes/accounting.pipe';
import { CountriesHelper } from './helper/countries.helper';

const exportModules = [
  // external modules
  CommonModule,
  HttpClientModule,
  RouterModule,
  TranslateModule,
  PerfectScrollbarModule,
  FileUploadModule,
  AgmCoreModule,
  NgxScrollTopModule,
  NgxPaginationModule,
  ToastrModule,
  NgxCurrencyModule,
  NgxSkeletonModule,
  NgPipesModule,
  NgxMaskModule,
  NgxPatternModule,
  NgxBarcodeModule,
  QrCodeModule,
  NgxBarcodeScannerModule,

  // custom modules
  BootstrapModule,
  CardsModule,
  IconsModule,
  SVGModule,
  FormControlsModule,
];

const serviceModules = [
  AuthService,
  LoadingService,
  PDFService,
  ExcelService,
  GlobalService,
  GlobalAction,
  GlobalValidation,
  MapsService,
  AuthGuard,
  AuthNonGuard,
  AuthFreeGuard,
  CountriesHelper,
];

const pipeModules = [UnsanitizePipe, AccountingPipe];

@NgModule({
  declarations: [...pipeModules, DropzoneDirective],
  imports: [...exportModules],
  exports: [...pipeModules, ...exportModules],
  providers: [...serviceModules],
})
export class SharedModule {}
