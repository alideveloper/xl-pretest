import { Date } from '@shared/model/other/date.model';
import { Business } from '@shared/model/business.model';
import { Role } from './role.model';
import { Warehouse } from './warehouse.model';
import { User } from '@app/components/utils';

export interface Staff {
  created?: Date | any;
  key?: string;
  displayName?: string;
  phoneNumber?: string;
  email?: string | any;
  pin?: string | any;
  status?: string | any;
  businessKey?: string;
  business?: Business | any;
  roleKey?: string;
  roles?: Role | any;
  warehouseKey?: string;
  warehouses?: Warehouse | any;
  updated?: Date | any;
  accuracy?: number;
  address?: string;
  addressed?: string;
  username?: string;
  addressDetail?: string;
  latitude?: number;
  longitude?: number;
  altitude?: any;
  speed?: any;
  userKey?: any;
  users?: User | any;
  altitudeAccuracy?: any;
  lastLogin?: Date | any;
  lastLogout?: Date | any;
}
