export interface CategoryAccount {
  category: string;
  type: string;
  icon: string;
  business: boolean;
}
