import { Date } from '@shared/model/other/date.model';
import { Business } from '@shared/model/business.model';

export interface GoodsUnit {
  created?: Date | any;
  key?: string;
  name?: string;
  businessKey?: string;
  business?: Business | any;
  updated?: Date | any;
}
