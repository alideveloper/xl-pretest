import { Date } from '@shared/model/other/date.model';
import { Warehouse } from '@shared/model/warehouse.model';
import { Supplier } from '@shared/model/supplier.model';
import { Purchaser } from '@shared/model/purchaser.model';
import { Business } from '@shared/model/business.model';

export interface Stock {
  created?: Date | any;
  key?: string;
  type?: 'In' | 'Out';
  businessKey?: string;
  business?: Business | any;
  goods?: any[] | any;
  totalGoods?: number | any;
  totalPrice?: number | any;
  updated?: Date | any;
  warehouseKey?: string;
  warehouses?: Warehouse;
  description?: string;
  date?: Date | any;
  startSerialNumber?: number | any;
  endSerialNumber?: number | any;
  userKey?: string;
  users?: Supplier | Purchaser | any;
  totalOut?: number | any;
  totalTransfer?: number | any;
}
