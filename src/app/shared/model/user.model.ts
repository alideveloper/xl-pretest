import { Date } from '@shared/model/other/date.model';
import { Currency } from '@shared/model/other/currency.model';
import { Storage } from '@shared/model/other/storage.model';

export interface User {
  created?: Date | any;
  currency?: Currency;
  displayName?: string;
  domain?: string;
  email?: string;
  emailVerified?: boolean;
  key?: string;
  lastLogin?: Date | any;
  lastLogout?: Date | any;
  password?: string;
  repeatPassword?: string;
  status?: string | boolean;
  phoneNumber?: string;
  isActiveLogin?: boolean;
  provider?: string;
  photo?: Storage | any;
  photoBlob?: any;
  role?: string;
  roleId?: string;
  uid?: string;
  accuracy?: number;
  address?: string;
  addressed?: string;
  addressDetail?: string;
  latitude?: number;
  longitude?: number;
  altitude?: any;
  speed?: any;
  altitudeAccuracy?: any;
  updated?: Date | any;
  apiKey?: string | any;
  country?: string;
  isMonkey?: boolean;
}
