import { Date } from '@shared/model/other/date.model';
import { Business } from '@shared/model/business.model';

export interface Purchaser {
  created?: Date | any;
  key?: string;
  name?: string;
  phoneNumber?: string;
  businessKey?: string;
  business?: Business | any;
  updated?: Date | any;
  accuracy?: number;
  address?: string;
  addressed?: string;
  addressDetail?: string;
  latitude?: number;
  longitude?: number;
  altitude?: any;
  speed?: any;
  altitudeAccuracy?: any;
}
