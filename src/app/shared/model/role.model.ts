import { Date } from '@shared/model/other/date.model';
import { User } from '@shared/model/user.model';

export interface Role {
  created?: Date | any;
  key?: string;
  name?: string;
  businessKey?: string;
  updated?: Date | any;
  userKey?: any;
  users?: User | any;
  privilegesNgUang?: string | any;
  privilegesxl?: string | any;
  privilegesNgiklan?: string | any;
}
