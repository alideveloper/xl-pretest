import { Date } from '@shared/model/other/date.model';
import { Page } from '@shared/model/panel/page.model';
import { MenuCategory } from './menu-category.model';

export interface Menu {
  created?: Date | any;
  key?: string;
  updated?: Date | any;
  category?: string;
  domain?: string;
  icon?: string;
  page?: any;
  pages?: Page[];
  pagesGlobal?: Page[];
  name?: string;
  module?: string;
  order?: number;
  menuCategory?: MenuCategory[];
  menuCategoryGlobal?: MenuCategory[];
  whitelist?: any[];
  blacklist?: any[];
  privilege?: any[];
}
