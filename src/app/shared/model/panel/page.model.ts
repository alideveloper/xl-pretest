import { Date } from '@shared/model/other/date.model';

export interface Page {
  created?: Date | any;
  key?: string;
  updated?: Date | any;
  constant?: string;
  domain?: string;
  firebase?: string;
  module?: string;
  name?: string;
  path?: string;
  schemas?: string;
  type?: string;
}
