import { Date } from '@shared/model/other/date.model';

export interface Team {
  created?: Date | any;
  key?: string;
  updated?: Date | any;
  facebook?: string;
  instagram?: string;
  name?: string;
  photo?: string;
  position?: string;
}
