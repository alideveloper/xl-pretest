import { Date } from '@shared/model/other/date.model';

export interface Testimonial {
  created?: Date | any;
  key?: string;
  updated?: Date | any;
  photo?: string;
  position?: string;
  review?: string;
  name?: string;
}
