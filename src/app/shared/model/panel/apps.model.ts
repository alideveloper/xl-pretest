import { Date } from '@shared/model/other/date.model';

export interface Apps {
  created?: Date | any;
  key?: string;
  updated?: Date | any;
  projectID?: string;
  projectName?: string;
}
