import { Date } from '@shared/model/other/date.model';
import { Page } from '@shared/model/panel/page.model';

export interface MenuCategory {
  created?: Date | any;
  key?: string;
  updated?: Date | any;
  category?: string;
  domain?: string;
  icon?: string;
  page?: Page;
  order?: number;
}
