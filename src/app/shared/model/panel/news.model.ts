import { Date } from '@shared/model/other/date.model';
import { Category } from '@shared/model/panel/category.model';

export interface News {
  created?: Date | any;
  key?: string;
  updated?: Date | any;
  categories?: Category;
  category?: string;
  content?: string;
  image?: string;
  description?: string;
  title?: string;
  view?: number;
}
