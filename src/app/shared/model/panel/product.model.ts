import { Date } from '@shared/model/other/date.model';

export interface Product {
  created?: Date | any;
  key?: string;
  updated?: Date | any;
  logo?: string;
  name?: string;
  description?: string;
  website?: string;

  path?: string;
  title?: string;
  type?: string;
  img?: string;
}
