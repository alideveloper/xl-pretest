import { Date } from '@shared/model/other/date.model';
import { Category } from '@shared/model/panel/category.model';

export interface Video {
  created?: Date | any;
  key?: string;
  updated?: Date | any;
  categories?: Category;
  category?: string;
  link?: string;
  title?: string;
}
