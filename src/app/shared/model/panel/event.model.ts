import { Date } from '@shared/model/other/date.model';
import { Category } from '@shared/model/panel/category.model';

export interface Event {
  created?: Date | any;
  key?: string;
  updated?: Date | any;
  categories?: Category;
  category?: string;
  description?: string;
  endDate?: Date | any;
  endTime?: Date | any;
  startDate?: Date | any;
  startTime?: Date | any;
  image?: string;
  title?: string;
}
