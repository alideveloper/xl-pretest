import { Date } from '@shared/model/other/date.model';

export interface FAQ {
  created?: Date | any;
  key?: string;
  updated?: Date | any;
  answer?: string;
  question?: string;
  order?: number;
}
