import { Date } from '@shared/model/other/date.model';

export interface Client {
  created?: Date | any;
  key?: string;
  updated?: Date | any;
  client?: string;
  logo?: string;
  website?: string;
}
