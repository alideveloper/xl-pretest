import { Date } from '@shared/model/other/date.model';

export interface Category {
  created?: Date | any;
  key?: string;
  updated?: Date | any;
  category?: string;
  description?: string;
  image?: string;
}
