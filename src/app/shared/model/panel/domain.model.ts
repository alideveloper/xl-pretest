import { Date } from '@shared/model/other/date.model';

export interface Domain {
  created?: Date | any;
  key?: string;
  updated?: Date | any;
  domain?: string;
}
