import { Date } from '@shared/model/other/date.model';
import { Goods } from '@shared/model/goods.model';
import { Warehouse } from '@shared/model/warehouse.model';
import { Business } from '@shared/model/business.model';
import { Stock } from './stock.model';

export interface GoodsWarehouse {
  created?: Date | any;
  key?: string;
  goodsKey?: string;
  goods?: Goods;
  businessKey?: string;
  business?: Business | any;
  serialNumber?: string | any;
  warehouseKey?: string;
  warehouses?: Warehouse;
  barcode?: string;
  stockKey?: string;
  price?: any;
  stocks?: Stock;
  type?: 'In' | 'Out';
  updated?: Date | any;
  stockKeyOut?: string | any;
  dateOut?: Date | any;
  lastStockKey?: any;
  lastWarehouseKey?: any;
  barcodeBase64?: string | any;
  name?: string | any;
}
