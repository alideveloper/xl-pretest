import { Date } from '@shared/model/other/date.model';
import { Currency } from '@shared/model/other/currency.model';
import { User } from '@shared/model/user.model';

export interface Business {
  accuracy?: number;
  address?: string;
  addressed?: string;
  addressDetail?: string;
  latitude?: number;
  longitude?: number;
  altitude?: any;
  speed?: any;
  altitudeAccuracy?: any;
  created?: Date | any;
  key?: string;
  businessDescription?: string;
  businessName?: string;
  currency?: Currency;
  updated?: Date | any;
  logo?: Storage | any;
  logoBlob?: any;
  userKey?: string;
  user?: User | any;
  emailVerified?: boolean;
}
