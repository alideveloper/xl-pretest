export interface Loading {
  isLoading: boolean;
  text: string;
  primaryColour: string;
  secondaryColour: string;
  animation: any;
  image: string;
}
