export interface Date {
  nanoseconds: number;
  seconds: number;
}
