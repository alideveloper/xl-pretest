export interface Currency {
  align?: string;
  allowNegative?: boolean;
  decimal?: string;
  inputMode?: string;
  nullable?: boolean;
  precision?: number;
  prefix?: string;
  suffix?: string;
  thousands?: string;
}
