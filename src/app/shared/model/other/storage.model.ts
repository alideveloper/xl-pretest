export interface Storage {
  downloadURL: string;
  extension: string;
  fileName: string;
  fileType: string;
  fullPath: string;
  originalName: string;
  size: number;
}
