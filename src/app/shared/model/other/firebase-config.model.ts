export interface Storage {
  active?: boolean;
  path?: string;
  keyUpload?: any[];
  keyResult?: any[];
  deletePath?: any;
}

export interface StorageDelete {
  active?: boolean;
  keyPath?: string;
  bindingPath?: string;
}

export interface OrderBy {
  object?: string;
  order?: string;
}

export interface Query {
  object?: string;
  condition?: string;
  value?: any;
}

export interface Collection {
  status?: boolean;
  name?: string;
  document?: Document;
  collection?: Collection;
  orderBy?: OrderBy;
  query?: Query[];
}

export interface Document {
  status?: boolean;
  name?: string;
  collection?: Collection;
  document?: Document;
  orderBy?: OrderBy;
  query?: Query[];
}

export interface FirebaseConfigModel {
  method?: string;
  customKey?: string;
  storage?: Storage;
  storageDelete?: StorageDelete;
  collection?: Collection;
  document?: Document;
  orderBy?: OrderBy;
  query?: Query[];
}
