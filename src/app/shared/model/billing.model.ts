import { Date } from '@shared/model/other/date.model';
import { User } from '@shared/model/user.model';

export interface Billing {
  created?: Date | any;
  key?: string;
  typePayment?: string;
  projectId?: string;
  beforeDate?: Date | any;
  finalDate?: Date | any;
  businessKey?: string;
  users?: User | any;
  userKey?: string | any;
  total?: number | any;
  updated?: Date | any;
}
