import { Date } from '@shared/model/other/date.model';
import { GoodsCategory } from '@shared/model/goods-category.model';
import { GoodsUnit } from '@shared/model/goods-unit.model';
import { Storage } from '@shared/model/other/storage.model';
import { Business } from '@shared/model/business.model';

export interface Goods {
  created?: Date | any;
  key?: string;
  categoryKey?: string;
  categories?: GoodsCategory;
  barcode?: string;
  barcodeBase64?: string;
  businessKey?: string;
  business?: Business | any;
  image?: Storage;
  imageBlob?: any;
  unitKey?: string;
  units?: GoodsUnit;
  description?: string;
  name?: string;
  price?: number | any;
  sellingPrice?: number | any;
  updated?: Date | any;
}
