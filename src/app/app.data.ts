import { Injectable } from '@angular/core';
import { Business } from '@shared/model/business.model';
import { Domain } from '@shared/model/panel/domain.model';
import { CategoryAccount } from '@shared/model/apps/category-account.model';
import { Apps } from '@shared/model/panel/apps.model';

@Injectable({
  providedIn: 'root',
})
export class AppData {
  private _categoryAccount: CategoryAccount[];
  get categoryAccount(): CategoryAccount[] {
    return this._categoryAccount;
  }
  set categoryAccount(value: CategoryAccount[]) {
    if (this._categoryAccount !== value) {
      this._categoryAccount = value;
    }
  }

  private _selectedBusiness: any;
  get selectedBusiness(): any {
    return this._selectedBusiness;
  }
  set selectedBusiness(value: any) {
    if (this._selectedBusiness !== value) {
      this._selectedBusiness = value;
    }
  }

  private _selectedBusinessValue: any;
  get selectedBusinessValue(): any {
    return this._selectedBusinessValue;
  }
  set selectedBusinessValue(value: any) {
    if (this._selectedBusinessValue !== value) {
      this._selectedBusinessValue = value;
    }
  }

  private _business: Business[];
  get business(): Business[] {
    return this._business;
  }
  set business(value: Business[]) {
    if (this._business !== value) {
      this._business = value;
    }
  }

  private _project: Apps;
  get project(): Apps {
    return this._project;
  }
  set project(value: Apps) {
    if (this._project !== value) {
      this._project = value;
    }
  }

  private _domain: Domain;
  get domain(): Domain {
    return this._domain;
  }
  set domain(value: Domain) {
    if (this._domain !== value) {
      this._domain = value;
    }
  }
  constructor() {}
}
