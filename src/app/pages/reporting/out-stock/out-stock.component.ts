import {
  Component,
  OnInit,
  ChangeDetectorRef,
  ViewEncapsulation,
} from '@angular/core';

import { BaseComponent } from '@app/core';
import { environment } from '@environments/environment';

import { FirebaseService } from '@app/@firebase/firebase.service';

import { AppService } from '@app/app.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AccountingPipe } from '@app/shared/pipes/accounting.pipe';
import { FilterByPipe } from 'ngx-pipes';

const moment = require('moment');
moment();
import {
  Query,
  FirebaseConfigModel,
} from '@app/shared/model/other/firebase-config.model';
import { GlobalService } from '@app/shared/services/global.service';
import { PDFService } from '@app/shared/services/pdf.service';
import { ExcelService } from '@app/shared/services/excel.service';
import { Site } from '@app/shared/model/panel/site.model';
import { GlobalAction } from '@app/shared/services/global.action';
import { Staff } from '@app/shared/model/staff.model';
import { Warehouse } from '@app/shared/model/warehouse.model';
import { GoodsUnit } from '@app/shared/model/goods-unit.model';
import { GoodsCategory } from '@app/shared/model/goods-category.model';
import { Goods } from '@app/shared/model/goods.model';
import { Purchaser } from '@app/shared/model/purchaser.model';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { Stock } from '@app/shared/model/stock.model';
import { User } from '@app/shared/model/user.model';

@Component({
  selector: 'app-out-stock',
  templateUrl: './out-stock.component.html',
  styleUrls: ['./out-stock.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [AccountingPipe],
})
export class OutStockComponent extends BaseComponent implements OnInit {
  redirectURL = environment.indexURL.root + environment.indexURL.homeTwo;

  timeout: any;

  warehouse: Account;

  dateNow: any = new Date();

  search: any = {};

  data: Stock[];
  initDataOutStocks: Stock[];

  staff: Staff;

  selectedBusinessValue: any;
  selectedBusiness: any;

  selectedWarehouseValue: any;
  selectedWarehouse: any;
  dataWarehouse: Warehouse[];
  initDataWarehouse: Warehouse[];
  warehouses: Warehouse[];

  units: GoodsUnit[];
  categories: GoodsCategory[];
  goods: Goods[];
  purchasers: Purchaser[];

  user: User;

  private _unsubscribeAll: Subject<any>;

  value: any = {
    form: {},
  };

  schemaPurchaserSelect: any = {
    component: 'select',
    position: 'body',
    label: {
      active: true,
      binding: false,
      text: 'By Purchaser',
      type: 'text',
      state: {
        hide: false,
        disabled: false,
        hideCondition: [],
        disabledCondition: [],
      },
    },
    labelSubtitle: {
      active: false,
    },
    type: 'text',
    placeholder: 'Select Purchaser',
    parentBinding: 'form',
    bindingObject: 'userKey',
    primaryKey: 'key',
    bindingData: 'dataPurchasers',
    labelForId: 'name',
    bindLabel: 'name',
    bindValue: 'key',
    searchable: true,
    search: true,
    closeOnSelect: true,
    defaultValue: true,
    customSearch: true,
    clearable: false,
    bindingSearch: 'dataPurchasers',
    required: false,
    formText: {
      valid: {
        active: false,
        text: 'Data telah Sesuai!',
      },
      invalid: {
        active: false,
        text: 'Data belum Sesuai!',
      },
    },
  };

  schemaWarehouseSelect: any = {
    component: 'select',
    position: 'body',
    label: {
      active: true,
      binding: false,
      text: 'Select Warehouse',
      type: 'text',
      state: {
        hide: false,
        disabled: false,
        hideCondition: [],
        disabledCondition: [],
      },
    },
    labelSubtitle: {
      active: false,
    },
    type: 'text',
    placeholder: 'Select Warehouse',
    parentBinding: 'form',
    bindingObject: 'warehouseKey',
    primaryKey: 'key',
    bindingData: 'dataWarehouse',
    labelForId: 'name',
    bindLabel: 'name',
    bindValue: 'key',
    searchable: true,
    search: true,
    closeOnSelect: true,
    defaultValue: true,
    customSearch: true,
    clearable: false,
    bindingSearch: 'dataWarehouse',
    required: false,
    formText: {
      valid: {
        active: false,
        text: 'Data telah Sesuai!',
      },
      invalid: {
        active: false,
        text: 'Data belum Sesuai!',
      },
    },
  };

  flag: any = {
    load: false,
    loadData: false,
  };

  minBalanceRange: any = 0;
  maxBalanceRange: any = 0;

  totalGoods: any = 0;
  totalPrice: any = 0;

  emptyBusiness: any;

  constructor(
    private _firebaseService: FirebaseService,
    private _cdRef: ChangeDetectorRef,
    private _appService: AppService,
    private _filterByPipe: FilterByPipe,
    private _globalService: GlobalService,
    private _accountingPipe: AccountingPipe,
    private _globalAction: GlobalAction,
    private _pdfService: PDFService,
    private _translateService: TranslateService,
    private _router: Router,
    private _excelService: ExcelService
  ) {
    super();
  }

  async ngOnInit() {
    await this.initStorage();
    await this.initRouting();

    this._unsubscribeAll = new Subject();

    this.initSearch();

    if (this.emptyBusiness === false) {
      this.initBusiness();
      this.initData();

      this._appService.onDataChanges
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe(async (data) => {
          console.log('on changes');
          console.log(data.selectedBusiness);
          if (data.selectedBusinessValue !== this.selectedBusinessValue) {
            if (data.selectedBusinessValue) {
              this.selectedBusinessValue = data.selectedBusinessValue;
            }
          }
          if (
            (data.selectedBusiness &&
              data.selectedBusiness !== this.selectedBusiness) ||
            (data.selectedBusiness &&
              data.selectedBusiness.currency !== this.selectedBusiness.currency)
          ) {
            console.log('MASUK');
            this.selectedBusiness = data.selectedBusiness;
            this.initSearch();
            this.initData();
          }
        });
    }
  }

  async initRouting() {
    this.emptyBusiness = await this._globalAction.isEmptyBusiness();
    this._globalAction.submenuRouting('/reporting/out-stock');
  }

  initSearch() {
    this.search = {
      userKey: '',
      typeDate: 'All',
      rangeDate: [
        new Date(this.dateNow.getFullYear(), this.dateNow.getMonth(), 1),
        new Date(this.dateNow.getFullYear(), this.dateNow.getMonth() + 1, 0),
      ],
      rangeBalanceCallback: [
        {
          oldValue: [this.minBalanceRange, this.maxBalanceRange],
          newValue: [this.minBalanceRange, this.maxBalanceRange],
        },
      ],
      rangeBalance: [this.minBalanceRange, this.maxBalanceRange],
    };
  }

  async initData() {
    this.flag.load = false;
    await this.getWarehouse();
    await this.getPurchaser();
    if (this.selectedWarehouseValue) {
      await this.getOutStock();
    } else {
      this.data = [];
      this.initDataOutStocks = [];
    }

    await this.getCategories();
    await this.getUnits();
    await this.getGoods();
    this.flag.load = true;
  }

  async initStorage(): Promise<any> {
    this.user = this._globalAction.getUserLogin();
  }

  initBusiness() {
    this.selectedBusinessValue = localStorage.getItem(
      environment.domain + '-selectedBusinessValue'
    );
    this.selectedBusiness = JSON.parse(
      localStorage.getItem(environment.domain + '-selectedBusiness')
    );
  }

  async getWarehouse(): Promise<any> {
    console.log(environment.domain);
    console.log(
      JSON.parse(localStorage.getItem(environment.domain + '-selectedBusiness'))
    );
    console.log(this.selectedBusiness, 'selectedBusiness');
    return new Promise(async (resolve, reject) => {
      const configFirebase: FirebaseConfigModel = {
        collection: {
          status: true,
          name: 'apps',
          document: {
            status: true,
            name: 'Database',
            collection: {
              status: true,
              name: 'business',
              document: {
                status: true,
                name: this.selectedBusiness.key,
                collection: {
                  status: true,
                  name: 'warehouses',
                  document: {
                    status: false,
                  },
                  orderBy: {
                    object: 'created',
                    order: 'desc',
                  },
                  query: [],
                },
              },
            },
          },
        },
      };

      const data: any = await this._firebaseService.handleCollection(
        null,
        configFirebase
      );
      data.subscribe((response: Warehouse[]) => {
        if (response) {
          console.log('response warehouse', response);
          let warehouses: Warehouse[] = [];

          warehouses = [...response];
          if (warehouses && warehouses.length > 0) {
            this.selectedWarehouseValue = warehouses[0].key;
            this.selectedWarehouse = warehouses[0];
          }
          this.dataWarehouse = [...warehouses];
          this.initDataWarehouse = [...this.dataWarehouse];
          this.warehouses = [...this.dataWarehouse];
          this.value.dataWarehouse = [...this.initDataWarehouse];
          this._cdRef.detectChanges();
          if (warehouses && warehouses.length === 0) {
            this._globalAction.showToast({
              title: this._translateService.instant('Info!'),
              message: this._translateService.instant('Add Warehouse First!'),
              type: 'error',
            });
            this._router.navigateByUrl('/dashboard');
          }
          resolve(warehouses);
          return;
        }
      });
    });
  }

  async getOutStock(): Promise<any> {
    this.maxBalanceRange = 0;
    this.flag.loadData = false;

    console.log(environment.domain);
    console.log(
      JSON.parse(localStorage.getItem(environment.domain + '-selectedBusiness'))
    );
    console.log(this.selectedBusiness, 'selectedBusiness');

    const where: Query[] = [
      {
        object: 'type',
        condition: '==',
        value: 'Out',
      },
    ];
    return new Promise(async (resolve, reject) => {
      const configFirebase: FirebaseConfigModel = {
        collection: {
          status: true,
          name: 'apps',
          document: {
            status: true,
            name: 'Database',
            collection: {
              status: true,
              name: 'business',
              document: {
                status: true,
                name: this.selectedBusiness.key,
                collection: {
                  status: true,
                  name: 'warehouses',
                  document: {
                    status: true,
                    name: this.selectedWarehouse.key,
                    collection: {
                      status: true,
                      name: 'stocks',
                      document: {
                        status: false,
                      },
                      orderBy: {
                        object: 'created',
                        order: 'desc',
                      },
                      query: [...where],
                    },
                  },
                },
              },
            },
          },
        },
      };

      const data: any = await this._firebaseService.handleCollection(
        null,
        configFirebase
      );
      data.subscribe((response: Stock[]) => {
        console.log('stock', response);
        if (response) {
          this.totalGoods = 0;
          this.totalPrice = 0;
          response.map((stocks: Stock) => {
            this.totalGoods += stocks.totalGoods;
            this.totalPrice += stocks.totalPrice;
            if (this.maxBalanceRange < stocks.totalPrice) {
              this.maxBalanceRange = stocks.totalPrice;
            }

            if (this.purchasers && this.purchasers.length > 0) {
              const users = this.purchasers.filter(
                (purchaserItem: any) => purchaserItem.key === stocks.userKey
              );

              if (users && users.length > 0) {
                stocks.users = users[0];
              } else {
                stocks.userKey = null;
                stocks.users = {};
                stocks.users.key = '';
                stocks.users.name = '-';
              }
            } else {
              stocks.userKey = null;
              stocks.users = {};
              stocks.users.key = '';
              stocks.users.name = '-';
            }
          });

          this.search.rangeBalance = [
            this.minBalanceRange,
            this.maxBalanceRange,
          ];

          this.data = [...response];
          this.initDataOutStocks = [...response];

          this._cdRef.detectChanges();
          this.flag.loadData = true;
          resolve(response);
          return;
        }
      });
    });
  }

  async getGoods(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const configFirebase: FirebaseConfigModel = {
        collection: {
          status: true,
          name: 'apps',
          document: {
            status: true,
            name: 'Database',
            collection: {
              status: true,
              name: 'business',
              document: {
                status: true,
                name: this.selectedBusiness.key,
                collection: {
                  status: true,
                  name: 'goods',
                  document: {
                    status: false,
                  },
                  orderBy: {
                    object: 'created',
                    order: 'desc',
                  },
                  query: [],
                },
              },
            },
          },
        },
      };

      const data: any = await this._firebaseService.handleCollection(
        null,
        configFirebase
      );
      data.subscribe((response: Goods[]) => {
        if (response) {
          response.map((goods: Goods) => {
            if (this.categories && this.categories.length > 0) {
              const categories = this.categories.filter(
                (categoryItem: any) => categoryItem.key === goods.categoryKey
              );

              if (categories && categories.length > 0) {
                goods.categories = categories[0];
              } else {
                goods.categoryKey = null;
                goods.categories = {};
                goods.categories.key = '';
                goods.categories.name = '-';
              }
            } else {
              goods.categoryKey = null;
              goods.categories = {};
              goods.categories.key = '';
              goods.categories.name = '-';
            }

            if (this.units && this.units.length > 0) {
              const units = this.units.filter(
                (unitItem: any) => unitItem.key === goods.unitKey
              );

              if (units && units.length > 0) {
                goods.units = units[0];
              } else {
                goods.unitKey = null;
                goods.units = {};
                goods.units.key = '';
                goods.units.name = '-';
              }
            } else {
              goods.unitKey = null;
              goods.units = {};
              goods.units.key = '';
              goods.units.name = '-';
            }
          });
          this.goods = [...response];
          this._cdRef.detectChanges();
          resolve(response);
          return;
        }
      });
    });
  }

  async getCategories(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      if (this.selectedBusiness) {
        const configFirebase: FirebaseConfigModel = {
          collection: {
            status: true,
            name: 'apps',
            document: {
              status: true,
              name: 'Database',
              collection: {
                status: true,
                name: 'business',
                document: {
                  status: true,
                  name: this.selectedBusiness.key,
                  collection: {
                    status: true,
                    name: 'categories',
                    document: {
                      status: false,
                    },
                    orderBy: {
                      object: 'created',
                      order: 'desc',
                    },
                    query: [],
                  },
                },
              },
            },
          },
        };

        const data: any = await this._firebaseService.handleCollection(
          null,
          configFirebase
        );
        data.subscribe((response: GoodsCategory[]) => {
          if (response) {
            this.categories = [...response];
            console.log('responsecategories', response);
            this._cdRef.detectChanges();
            resolve(response);
            return;
          }
        });
      }
    });
  }

  async getUnits(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      if (this.selectedBusiness) {
        const configFirebase: FirebaseConfigModel = {
          collection: {
            status: true,
            name: 'apps',
            document: {
              status: true,
              name: 'Database',
              collection: {
                status: true,
                name: 'business',
                document: {
                  status: true,
                  name: this.selectedBusiness.key,
                  collection: {
                    status: true,
                    name: 'units',
                    document: {
                      status: false,
                    },
                    orderBy: {
                      object: 'created',
                      order: 'desc',
                    },
                    query: [],
                  },
                },
              },
            },
          },
        };

        const data: any = await this._firebaseService.handleCollection(
          null,
          configFirebase
        );
        data.subscribe((response: GoodsUnit[]) => {
          if (response) {
            this.units = [...response];
            console.log('responseunits', response);
            this._cdRef.detectChanges();
            resolve(response);
            return;
          }
        });
      }
    });
  }

  async getPurchaser(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const configFirebase: FirebaseConfigModel = {
        collection: {
          status: true,
          name: 'apps',
          document: {
            status: true,
            name: 'Database',
            collection: {
              status: true,
              name: 'business',
              document: {
                status: true,
                name: this.selectedBusiness.key,
                collection: {
                  status: true,
                  name: 'purchasers',
                  document: {
                    status: false,
                  },
                  orderBy: {
                    object: 'created',
                    order: 'desc',
                  },
                  query: [],
                },
              },
            },
          },
        },
      };

      const data: any = await this._firebaseService.handleCollection(
        null,
        configFirebase
      );
      data.subscribe((response: Purchaser[]) => {
        if (response) {
          if (response && response.length > 0) {
            response.map((responseItem: any) => {
              console.log('FILTERING', response);
            });
          }

          const dataPurchasers = [];
          const itemAll = {
            key: '',
            name: 'All',
          };
          dataPurchasers.push(itemAll);

          response.map((item: Purchaser) => {
            dataPurchasers.push(item);
          });

          this.value.dataPurchasers = [...dataPurchasers];
          this.purchasers = [...response];
          this._cdRef.detectChanges();
          resolve(response);
          return;
        }
      });
    });
  }

  countingBalance(result: any) {}

  async callbackWarehouse(event: any) {
    console.log('event', event);
    if (event) {
      this.selectedWarehouseValue = event;
    } else {
      this.selectedWarehouseValue = '';
    }

    if (this.initDataWarehouse && this.initDataWarehouse.length > 0) {
      const findWarehouse = this.initDataWarehouse.filter(
        (itemWarehouse: any) => {
          if (itemWarehouse.key === this.selectedWarehouseValue) {
            return itemWarehouse;
          }
        }
      );
      if (findWarehouse && findWarehouse.length > 0) {
        this.selectedWarehouse = findWarehouse[0];
      }
    }

    this.flag.load = false;
    await this.getOutStock();
    this.flag.load = true;
  }

  onChangeSearch(type?: any, event?: any) {
    let result: any = Object.assign([], this.initDataOutStocks);

    this.totalGoods = 0;
    this.totalPrice = 0;

    if (type === 'rangeBalance') {
      this.search.rangeBalance = [event.newValue[0], event.newValue[1]];
    }
    console.log(event, result);

    if (this.search.typeDate === 'All') {
      result = this.onChangeSearchGeneral(result, this.search.stockKey, 'key');
      result = this.onChangeSearchGeneral(
        result,
        this.search.userKey,
        'userKey'
      );
      this.search.rangeDate = [
        new Date(this.dateNow.getFullYear(), this.dateNow.getMonth(), 1),
        new Date(this.dateNow.getFullYear(), this.dateNow.getMonth() + 1, 0),
      ];
      result = this.onChangeSearchRangeBalance(
        result,
        this.search.rangeBalance
      );
    } else {
      result = this.onChangeSearchGeneral(result, this.search.stockKey, 'key');
      result = this.onChangeSearchGeneral(
        result,
        this.search.userKey,
        'userKey'
      );
      result = this.onChangeSearchRangeDate(result, this.search.rangeDate);
      result = this.onChangeSearchRangeBalance(
        result,
        this.search.rangeBalance
      );
    }

    result.map((item: any) => {
      this.totalGoods += item.totalGoods;
      this.totalPrice += item.totalPrice;
    });
    // this.countingBalance(result);
    console.log('final', result);
    this.data = [...result];
  }

  onChangeSearchGeneral(result: any, event: any, field?: any) {
    console.log('onChangeSearchGeneral', result, event, field);
    if (event) {
      const filter = this._filterByPipe.transform(result, [field], [event]);
      console.log('return', filter);
      return filter;
    }
    console.log('empty');
    return result;
  }

  onChangeSearchRangeDate(result: any, event: any) {
    console.log('onChangeSearchRangeDate', result, event);
    if (event) {
      if (result && result.length > 0) {
        result = result.filter((item: any) => {
          const date = moment(item.date.seconds * 1000);
          console.log('date', date);
          if (date > moment(event[0]) && date < moment(event[1])) {
            return item;
          }
        });
      }
    }
    return result;
  }

  onChangeSearchRangeBalance(result: any, event: any) {
    console.log('onChangeSearchRangeBalance', result, event);
    if (event) {
      if (result && result.length > 0) {
        result = result.filter((item: any) => {
          if (item.totalPrice >= event[0] && item.totalPrice <= event[1]) {
            return item;
          }
        });
      }
    }
    return result;
  }

  callbackPurchaser(event: any) {
    console.log('event', event);
    if (event) {
      this.search.userKey = event;
    } else {
      this.search.userKey = '';
    }

    this.onChangeSearch('userKey', event);
  }

  async setPdfData(): Promise<any> {
    const getData: Stock[] = Object.assign([], this.data);
    const data: any[] = [];

    let no = 0;

    const txtName = await this._globalService.getTranslate('Name:');
    const txtPhoneNumber = await this._globalService.getTranslate(
      'Phone Number:'
    );
    const txtAddress = await this._globalService.getTranslate('Address:');
    const txtDetail = await this._globalService.getTranslate('Detail:');
    getData.forEach(async (item) => {
      no++;
      const itemData: Stock = Object.assign({}, item);

      const payload: any = {
        no,
        purchaser:
          txtName +
          itemData.users.name +
          '\n' +
          txtPhoneNumber +
          itemData.users.phoneNumber +
          '\n\n' +
          txtAddress +
          itemData.users.address +
          '\n' +
          txtDetail +
          itemData.users.addressDetail,
        description: itemData.description ? itemData.description : '-',
        totalGoods: itemData.totalGoods,
        totalPrice: this._accountingPipe.transform(itemData.totalPrice),
        date: this._globalService.rawOnlyDateDividerTime(item.date.toDate()),
      };

      data.push(payload);
    });

    const width = ['6%', '24%', '18%', '25%', '25%', '12%'];
    const header = [
      await this._globalService.getTranslate('No'),
      await this._globalService.getTranslate('Purchaser'),
      await this._globalService.getTranslate('Description'),
      await this._globalService.getTranslate('Total Goods'),
      await this._globalService.getTranslate('Total Price'),
      await this._globalService.getTranslate('Date'),
    ];
    const value = [
      'no',
      'purchaser',
      'description',
      'totalGoods',
      'totalPrice',
      'date',
    ];

    const table = {
      header,
      width,
      data: value,
    };

    return { table, data };
  }

  async setExcelData(): Promise<any> {
    const getData: Stock[] = Object.assign([], this.data);
    const data: any[] = [];

    let no = 0;

    getData.forEach(async (item) => {
      no++;
      const itemData: Stock = Object.assign({}, item);

      const payload = {
        no,
        namePurchaser: itemData.users.name,
        phoneNumberPurchaser: itemData.users.phoneNumber,
        addressPurchaser: itemData.users.address,
        addressDetailPurchaser: itemData.users.addressDetail,
        description: itemData.description ? itemData.description : '-',
        totalGoods: itemData.totalGoods,
        totalPrice: this._accountingPipe.transform(itemData.totalPrice),
        date: this._globalService.rawOnlyDateDividerTimeSpace(
          item.date.toDate()
        ),
      };
      data.push(payload);
    });

    const width = [6, 20, 20, 30, 30, 30, 25, 25, 20];
    const header = [
      await this._globalService.getTranslate('No'),
      await this._globalService.getTranslate('Nama Purchaser'),
      await this._globalService.getTranslate('Phone Number Purchaser'),
      await this._globalService.getTranslate('Address Purchaser'),
      await this._globalService.getTranslate('Address Detail Purchaser'),
      await this._globalService.getTranslate('Description'),
      await this._globalService.getTranslate('Total Goods'),
      await this._globalService.getTranslate('Total Price'),
      await this._globalService.getTranslate('Date'),
    ];
    const value = [
      'no',
      'namePurchaser',
      'phoneNumberPurchaser',
      'addressPurchaser',
      'addressDetailPurchaser',
      'description',
      'totalGoods',
      'totalPrice',
      'date',
    ];

    const finalValue = [
      ...data.map((itemValue) => {
        const response: any = [];
        value.forEach((valueItem: any) => {
          response.push(itemValue[valueItem]);
        });
        return response;
      }),
    ];

    const result = {
      header,
      width,
      table: finalValue,
    };

    return result;
  }

  async fileAction(type: string, action: string): Promise<any> {
    console.log(this.selectedBusiness);

    const accounting: any = {
      name: '',
      image: '',
    };

    let rangeDate;
    if (this.search.typeDate === 'Range') {
      rangeDate = await this._globalService.getTranslate('Periode');
      rangeDate += ': ';
      rangeDate += this._globalService.rawOnlyDate(this.search.rangeDate[0]);
      rangeDate += ' - ';
      rangeDate += this._globalService.rawOnlyDate(this.search.rangeDate[1]);
    }

    let total;
    total = await this._globalService.getTranslate('Warehouse');
    total += ': ';
    total += this.selectedWarehouse.name;

    total += ' - ';
    total += await this._globalService.getTranslate('Total xl');
    total += ': ';
    total += this.totalGoods;

    total += ' - ';
    total += await this._globalService.getTranslate('Total Price');
    total += ': ';
    total += this._accountingPipe.transform(this.totalPrice);

    const site: Site = JSON.parse(
      localStorage.getItem(environment.domain + '-site')
    );
    accounting.title = 'Personal';
    accounting.name = await this._globalService.getTranslate(
      'List Goods in Warehouse'
    );
    accounting.image = site.favicon.downloadURL;

    console.log('accounting', accounting);
    const title = await this._globalService.getTranslate(accounting.name);
    if (type === 'pdf') {
      const data = await this.setPdfData();
      this._pdfService.onLoad(
        action,
        title,
        data.table,
        data.data,
        'A4',
        'company',
        'landscape',
        accounting,
        rangeDate,
        total
      );
    } else if (type === 'excel') {
      const data = await this.setExcelData();
      this._excelService.onLoad(
        action,
        title,
        data,
        'company',
        accounting,
        rangeDate,
        total
      );
    }
  }
}
