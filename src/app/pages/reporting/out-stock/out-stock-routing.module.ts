import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OutStockComponent } from './out-stock.component';

import { extract } from '@app/core';

const routes: Routes = [
  {
    path: '',
    component: OutStockComponent,
    data: { title: extract('In Stock') },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OutStockRoutingModule {}
