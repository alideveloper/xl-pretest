import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WarehouseGoodsRoutingModule } from './warehouse-goods-routing.module';

import { SharedModule } from '@app/shared';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { UtilsModule } from '@app/components/utils/utils.module';
import { NavigationsModule } from '@components/navigations/navigations.module';
import { AlertsModule } from '@app/components/alerts/alerts.module';
import { FormControlsModule } from '@app/components/form-controls/form-controls.module';
import { IconsModule } from '@app/components/icons/icons.module';

import { WarehouseGoodsComponent } from './warehouse-goods.component';

import { FilterByPipe } from 'ngx-pipes';
import { SelectsModule } from '@app/components/select/select.module';

const modules = [
  UtilsModule,
  CommonModule,
  NavigationsModule,
  NgxDatatableModule,
  IconsModule,
  AlertsModule,
  FormControlsModule,
  SharedModule,
  SelectsModule,
];

const declarations = [WarehouseGoodsComponent];

@NgModule({
  declarations: [...declarations],
  imports: [WarehouseGoodsRoutingModule, ...modules],
  entryComponents: [...declarations],
  providers: [FilterByPipe],
})
export class WarehouseModule {}
