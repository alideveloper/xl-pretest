import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'warehouse-goods',
        loadChildren: () =>
          import('./warehouse-goods/warehouse-goods.module').then(
            (m) => m.WarehouseModule
          ),
      },
      {
        path: 'in-stock',
        loadChildren: () =>
          import('./in-stock/in-stock.module').then((m) => m.InStockModule),
      },
      {
        path: 'out-stock',
        loadChildren: () =>
          import('./out-stock/out-stock.module').then((m) => m.OutStockModule),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReportingRoutingModule {}
