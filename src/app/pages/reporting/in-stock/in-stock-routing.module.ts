import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InStockComponent } from './in-stock.component';

import { extract } from '@app/core';

const routes: Routes = [
  {
    path: '',
    component: InStockComponent,
    data: { title: extract('In Stock') },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InStockRoutingModule {}
