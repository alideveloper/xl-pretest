import { NgModule } from '@angular/core';
import { ReportingRoutingModule } from './reporting-routing.module';

@NgModule({
  declarations: [],
  imports: [ReportingRoutingModule],
})
export class ReportingModule {}
