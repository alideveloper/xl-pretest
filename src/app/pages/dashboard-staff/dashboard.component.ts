import { Component, OnInit } from '@angular/core';

const moment = require('moment');
moment();

import { BaseComponent } from '@app/core';

import { AccountingPipe } from '@app/shared/pipes/accounting.pipe';

import { Loading } from '@app/shared/model/other/loading.model';
import { ngxLoadingAnimationTypes } from 'ngx-loading';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  providers: [AccountingPipe],
})
export class DashboardComponent extends BaseComponent implements OnInit {
  loading: Loading = {
    isLoading: false,
    text: this._translateService.instant('Loading') + '...',
    primaryColour: '',
    secondaryColour: '',
    animation: ngxLoadingAnimationTypes.threeBounce,
    image: 'assets/img/svg/loading-person.svg',
  };

  constructor(private _translateService: TranslateService) {
    super();
  }

  async ngOnInit() {}
}
