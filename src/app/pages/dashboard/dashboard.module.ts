import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardRoutingModule } from './dashboard-routing.module';

import { SharedModule } from '@app/shared';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { UtilsModule } from '@app/components/utils/utils.module';
import { NavigationsModule } from '@components/navigations/navigations.module';
import { AlertsModule } from '@app/components/alerts/alerts.module';
import { FormControlsModule } from '@app/components/form-controls/form-controls.module';
import { IconsModule } from '@app/components/icons/icons.module';
import { UploaderModule } from '@app/components/uploader/uploader.module';
import { AnalyticsModule } from '@app/components/analytics/analytics.module';

import { FilterByPipe } from 'ngx-pipes';

import { DashboardComponent } from './dashboard.component';
import { SelectsModule } from '@app/components/select/select.module';

const modules = [
  UtilsModule,
  CommonModule,
  NavigationsModule,
  NgxDatatableModule,
  IconsModule,
  UploaderModule,
  AlertsModule,
  FormControlsModule,
  AnalyticsModule,
  SharedModule,
  SelectsModule,
];

const declarations = [DashboardComponent];

@NgModule({
  declarations: [...declarations],
  imports: [DashboardRoutingModule, ...modules],
  entryComponents: [...declarations],
  providers: [FilterByPipe],
})
export class DashboardModule {}
