import {
  Component,
  OnInit,
  ChangeDetectorRef,
  NgZone,
  ViewChild,
  ElementRef,
  OnDestroy,
} from '@angular/core';
import { Router } from '@angular/router';
import { environment } from '@environments/environment';
import { FirebaseService } from '@app/@firebase/firebase.service';

import { GlobalAction } from '@app/shared/services/global.action';

const moment = require('moment');
moment();

import { BaseComponent } from '@app/core';

import { FirebaseConfigModel } from '@app/shared/model/other/firebase-config.model';
import { Query } from '@app/shared/types/firebase';
import { AccountingPipe } from '@app/shared/pipes/accounting.pipe';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AppService } from '@app/app.service';
import { Site } from '@app/shared/model/panel/site.model';

import { Loading } from '@app/shared/model/other/loading.model';
import { ngxLoadingAnimationTypes } from 'ngx-loading';
import { TranslateService } from '@ngx-translate/core';
import { Stock } from '@app/shared/model/stock.model';
import { Warehouse } from '@app/shared/model/warehouse.model';

import { ColorScheme, StyleService } from '@app/core';
import { GlobalService } from '@app/shared/services/global.service';
import { GoodsCategory } from '@app/shared/model/goods-category.model';
import { GoodsUnit } from '@app/shared/model/goods-unit.model';
import { Goods } from '@app/shared/model/goods.model';
import { Supplier } from '@app/shared/model/supplier.model';
import { Purchaser } from '@app/shared/model/purchaser.model';
import { GoodsWarehouse } from '@app/shared/model/goods-warehouse.model';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  providers: [AccountingPipe],
})
export class DashboardComponent extends BaseComponent
  implements OnInit, OnDestroy {
  loading: Loading = {
    isLoading: false,
    text: this._translateService.instant('Loading') + '...',
    primaryColour: '',
    secondaryColour: '',
    animation: ngxLoadingAnimationTypes.threeBounce,
    image: 'assets/img/svg/loading-person.svg',
  };

  emailVerified: any;

  site: Site;
  role: any;
  user: any;

  dayLeft: any;
  isTrialAccount: any;

  warehouses: Warehouse[];
  goodswarehouses: GoodsWarehouse[];
  categories: GoodsCategory[];
  units: GoodsUnit[];
  goods: Goods[];
  suppliers: Supplier[];
  purchasers: Purchaser[];

  selectedBusinessValue: any;
  selectedBusiness: any;

  private _unsubscribeAll: Subject<any>;

  yearNow: any = 2020;

  flag: any = {
    emailVerified: false,
    business: false,
  };

  schemaWarehouseSelect: any = {
    component: 'select',
    position: 'body',
    label: {
      active: true,
      binding: false,
      text: 'Select Warehouse',
      type: 'text',
      state: {
        hide: false,
        disabled: false,
        hideCondition: [],
        disabledCondition: [],
      },
    },
    labelSubtitle: {
      active: false,
    },
    type: 'text',
    placeholder: 'Select Warehouse',
    parentBinding: 'form',
    bindingObject: 'warehouseKey',
    primaryKey: 'key',
    bindingData: 'dataWarehouse',
    labelForId: 'name',
    bindLabel: 'name',
    bindValue: 'key',
    searchable: true,
    search: true,
    closeOnSelect: true,
    defaultValue: true,
    customSearch: true,
    clearable: false,
    bindingSearch: 'dataWarehouse',
    required: false,
    formText: {
      valid: {
        active: false,
        text: 'Data telah Sesuai!',
      },
      invalid: {
        active: false,
        text: 'Data belum Sesuai!',
      },
    },
  };

  value: any = {
    form: {},
  };

  totalGoods: any = 0;
  totalPrice: any = 0;

  stocks: Stock[];
  stocksIn: Stock[];
  stocksOut: Stock[];

  totalStockIn: any = 0;
  totalStockOut: any = 0;

  selectedWarehouseValue: any;
  selectedWarehouse: any;

  statisticsInStock: any = {};
  statisticsOutStock: any = {};
  statisticsGoodsCategory: any = {};
  statisticsGoods: any = {};
  statisticsGoodsUnit: any = {};
  statisticsSuppliers: any = {};
  statisticsPurchasers: any = {};
  statisticsGoodsWarehouse: any = {};

  yearlyBalance: any = {
    inStock: this.initYearlyAccounting,
    outStock: this.initYearlyAccounting,
  };

  constructor(
    private _cdRef: ChangeDetectorRef,
    private _globalAction: GlobalAction,
    private _firebaseService: FirebaseService,
    private _appService: AppService,
    private _router: Router,
    private _translateService: TranslateService,
    private zone: NgZone,
    private _globalService: GlobalService,
    private _accountingPipe: AccountingPipe,
    private _styles: StyleService
  ) {
    super();
  }

  async ngOnInit() {
    await this.initRouting();

    this._unsubscribeAll = new Subject();

    this.site = JSON.parse(localStorage.getItem(environment.domain + '-site'));

    this.initBusiness();
    await this.initData();

    this._appService.onDataChanges
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(async (data) => {
        console.log('on changes');
        console.log(data.selectedBusiness);
        if (data.selectedBusinessValue !== this.selectedBusinessValue) {
          if (data.selectedBusinessValue) {
            this.selectedBusinessValue = data.selectedBusinessValue;
          }
        }
        if (
          (data.selectedBusiness &&
            data.selectedBusiness !== this.selectedBusiness) ||
          (data.selectedBusiness &&
            data.selectedBusiness.currency !== this.selectedBusiness.currency)
        ) {
          console.log('MASUK on Changes');
          this.selectedBusiness = data.selectedBusiness;
          await this.initData();
        }
      });
  }

  async ngOnDestroy() {
    try {
      this._unsubscribeAll.next();
      this._unsubscribeAll.complete();
    } catch (error) {}
  }

  async initRouting() {
    this._globalAction.menuRouting('/dashboard');
  }

  async initData() {
    this.emailVerified = true;
    this.flag.emailVerified = true;

    this.user = this._globalAction.getUserLogin();
    this.role = this._globalAction.getRoleLogin();
    if (this.flag.business) {
      this.loading.isLoading = true;

      await this.getWarehouses();
      this.initYearly();
      if (this.selectedWarehouseValue) {
        this.initStatistic();
        await this.getStocks();
        this.loading.isLoading = false;
        await this.getCategory();
        await this.getUnit();
        await this.getPurchaser();
        await this.getSupplier();
        await this.getGoodsWarehouse();
      }
      if (this.loading.isLoading) this.loading.isLoading = false;
    }
  }

  initBusiness() {
    this.selectedBusinessValue = localStorage.getItem(
      environment.domain + '-selectedBusinessValue'
    );
    this.selectedBusiness = JSON.parse(
      localStorage.getItem(environment.domain + '-selectedBusiness')
    );

    if (this.selectedBusiness) {
      this.flag.business = true;
    }
  }

  get initYearlyAccounting() {
    return [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  }

  async getStocks(): Promise<any> {
    this.flag.loadData = false;

    console.log(environment.domain);
    console.log(
      JSON.parse(localStorage.getItem(environment.domain + '-selectedBusiness'))
    );
    console.log(this.selectedBusiness, 'selectedBusiness');

    return new Promise(async (resolve, reject) => {
      const configFirebase: FirebaseConfigModel = {
        collection: {
          status: true,
          name: 'apps',
          document: {
            status: true,
            name: 'Database',
            collection: {
              status: true,
              name: 'business',
              document: {
                status: true,
                name: this.selectedBusiness.key,
                collection: {
                  status: true,
                  name: 'warehouses',
                  document: {
                    status: true,
                    name: this.selectedWarehouse.key,
                    collection: {
                      status: true,
                      name: 'stocks',
                      document: {
                        status: false,
                      },
                      orderBy: {
                        object: 'created',
                        order: 'desc',
                      },
                      query: [],
                    },
                  },
                },
              },
            },
          },
        },
      };

      const data: any = await this._firebaseService.handleCollection(
        null,
        configFirebase
      );
      data.subscribe(async (response: Stock[]) => {
        console.log('stock', response);
        this.totalStockIn = 0;
        this.totalStockOut = 0;
        if (response) {
          this.totalGoods = 0;
          this.totalPrice = 0;
          response.map(async (stocks: Stock) => {
            this.totalGoods += stocks.totalGoods;
            this.totalPrice += stocks.totalPrice;
          });
          this.stocks = [...response];

          const dataStock = Object.assign([], response);

          if (dataStock && dataStock.length > 0) {
            this.stocksIn = dataStock.filter((item: any) => item.type === 'In');
            this.stocksOut = dataStock.filter(
              (item: any) => item.type === 'Out'
            );
            if (this.stocksIn && this.stocksIn.length > 0) {
              console.log('statistics', this.statisticsInStock);
              this.stocksIn.map(async (item: Stock) => {
                this.totalStockIn += item.totalGoods;

                let year;
                let month;
                try {
                  year = await this._globalService.onlyYear(
                    new Date(item.date.seconds * 1000)
                  );
                  month = await this._globalService.onlyMonth(
                    new Date(item.date.seconds * 1000)
                  );
                } catch (error) {
                  year = await this._globalService.onlyYear(
                    new Date(item.date.toDate())
                  );
                  month = await this._globalService.onlyMonth(
                    new Date(item.date.toDate())
                  );
                }

                if (year === this.yearNow) {
                  console.log('year', year);
                  this.countingYearly(
                    item.totalPrice,
                    this.yearlyBalance.inStock,
                    month
                  );
                }
              });
            }
            if (this.stocksOut && this.stocksOut.length > 0) {
              this.stocksOut.map(async (item: Stock) => {
                this.totalStockOut += item.totalGoods;

                let year;
                let month;
                try {
                  year = await this._globalService.onlyYear(
                    new Date(item.date.seconds * 1000)
                  );
                  month = await this._globalService.onlyMonth(
                    new Date(item.date.seconds * 1000)
                  );
                } catch (error) {
                  year = await this._globalService.onlyYear(
                    new Date(item.date.toDate())
                  );
                  month = await this._globalService.onlyMonth(
                    new Date(item.date.toDate())
                  );
                }

                if (year === this.yearNow) {
                  console.log('year', year);
                  this.countingYearly(
                    item.totalPrice,
                    this.yearlyBalance.outStock,
                    month
                  );
                }
              });
            }
          } else {
            this.stocksIn = [];
            this.stocksOut = [];
          }

          this.statisticsInStock = {
            title: await this._globalService.getTranslate('In Stock'),
            scheme: ColorScheme.Success,
            value: this.totalStockIn + ' Data',
            percent: 100,
            percentActive: false,
          };

          this.statisticsOutStock = {
            title: await this._globalService.getTranslate('Out Stock'),
            scheme: ColorScheme.Danger,
            value: this.totalStockOut + ' Data',
            percent: 100,
            percentActive: false,
          };

          this._cdRef.detectChanges();
          this.flag.loadData = true;
          resolve(response);
          return;
        }
      });
    });
  }

  countingYearly(balance: any, name: any, month: number) {
    name[month] += balance;
    console.log('this.yearlyBalance.outStock', this.yearlyBalance.outStock);
  }

  async getGoodsWarehouse(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const configFirebase: FirebaseConfigModel = {
        collection: {
          status: true,
          name: 'apps',
          document: {
            status: true,
            name: 'Database',
            collection: {
              status: true,
              name: 'business',
              document: {
                status: true,
                name: this.selectedBusiness.key,
                collection: {
                  status: true,
                  name: 'warehouses',
                  document: {
                    status: true,
                    name: this.selectedWarehouse.key,
                    collection: {
                      status: true,
                      name: 'goodswarehouses',
                      document: {
                        status: false,
                      },
                      orderBy: {
                        object: 'created',
                        order: 'desc',
                      },
                      query: [],
                    },
                  },
                },
              },
            },
          },
        },
      };

      const data: any = await this._firebaseService.handleCollection(
        null,
        configFirebase
      );
      data.subscribe(async (response: GoodsCategory[]) => {
        if (response) {
          if (response && response.length > 0) {
            response.map((responseItem: any) => {
              console.log('FILTERING', response);
            });
          }
          this.statisticsGoodsWarehouse = {
            title: await this._globalService.getTranslate('Goods in Warehouse'),
            scheme: '#FFF000',
            value: response.length + ' Data',
            percent: 100,
            percentActive: false,
          };
          this.goodswarehouses = [...response];
          this._cdRef.detectChanges();
          resolve(response);
          return;
        }
      });
    });
  }

  async getSupplier(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const configFirebase: FirebaseConfigModel = {
        collection: {
          status: true,
          name: 'apps',
          document: {
            status: true,
            name: 'Database',
            collection: {
              status: true,
              name: 'business',
              document: {
                status: true,
                name: this.selectedBusiness.key,
                collection: {
                  status: true,
                  name: 'suppliers',
                  document: {
                    status: false,
                  },
                  orderBy: {
                    object: 'created',
                    order: 'desc',
                  },
                  query: [],
                },
              },
            },
          },
        },
      };

      const data: any = await this._firebaseService.handleCollection(
        null,
        configFirebase
      );
      data.subscribe(async (response: GoodsCategory[]) => {
        if (response) {
          if (response && response.length > 0) {
            response.map((responseItem: any) => {
              console.log('FILTERING', response);
            });
          }
          this.statisticsSuppliers = {
            title: await this._globalService.getTranslate('Supplier'),
            scheme: '#FFF000',
            value: response.length + ' Data',
            percent: 100,
            percentActive: false,
          };
          this.suppliers = [...response];
          this._cdRef.detectChanges();
          resolve(response);
          return;
        }
      });
    });
  }

  async getPurchaser(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const configFirebase: FirebaseConfigModel = {
        collection: {
          status: true,
          name: 'apps',
          document: {
            status: true,
            name: 'Database',
            collection: {
              status: true,
              name: 'business',
              document: {
                status: true,
                name: this.selectedBusiness.key,
                collection: {
                  status: true,
                  name: 'purchasers',
                  document: {
                    status: false,
                  },
                  orderBy: {
                    object: 'created',
                    order: 'desc',
                  },
                  query: [],
                },
              },
            },
          },
        },
      };

      const data: any = await this._firebaseService.handleCollection(
        null,
        configFirebase
      );
      data.subscribe(async (response: GoodsCategory[]) => {
        if (response) {
          if (response && response.length > 0) {
            response.map((responseItem: any) => {
              console.log('FILTERING', response);
            });
          }
          this.statisticsPurchasers = {
            title: await this._globalService.getTranslate('Purchaser'),
            scheme: '#FFF000',
            value: response.length + ' Data',
            percent: 100,
            percentActive: false,
          };
          this.purchasers = [...response];
          this._cdRef.detectChanges();
          resolve(response);
          return;
        }
      });
    });
  }

  async getUnit(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const configFirebase: FirebaseConfigModel = {
        collection: {
          status: true,
          name: 'apps',
          document: {
            status: true,
            name: 'Database',
            collection: {
              status: true,
              name: 'business',
              document: {
                status: true,
                name: this.selectedBusiness.key,
                collection: {
                  status: true,
                  name: 'units',
                  document: {
                    status: false,
                  },
                  orderBy: {
                    object: 'created',
                    order: 'desc',
                  },
                  query: [],
                },
              },
            },
          },
        },
      };

      const data: any = await this._firebaseService.handleCollection(
        null,
        configFirebase
      );
      data.subscribe(async (response: GoodsCategory[]) => {
        if (response) {
          if (response && response.length > 0) {
            response.map((responseItem: any) => {
              console.log('FILTERING', response);
            });
          }
          this.statisticsGoodsUnit = {
            title: await this._globalService.getTranslate('Goods Unit'),
            scheme: '#FFF000',
            value: response.length + ' Data',
            percent: 100,
            percentActive: false,
          };
          this.units = [...response];
          this._cdRef.detectChanges();
          resolve(response);
          return;
        }
      });
    });
  }

  async getCategory(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const configFirebase: FirebaseConfigModel = {
        collection: {
          status: true,
          name: 'apps',
          document: {
            status: true,
            name: 'Database',
            collection: {
              status: true,
              name: 'business',
              document: {
                status: true,
                name: this.selectedBusiness.key,
                collection: {
                  status: true,
                  name: 'categories',
                  document: {
                    status: false,
                  },
                  orderBy: {
                    object: 'created',
                    order: 'desc',
                  },
                  query: [],
                },
              },
            },
          },
        },
      };

      const data: any = await this._firebaseService.handleCollection(
        null,
        configFirebase
      );
      data.subscribe(async (response: GoodsCategory[]) => {
        if (response) {
          if (response && response.length > 0) {
            response.map((responseItem: any) => {
              console.log('FILTERING', response);
            });
          }
          this.statisticsGoodsCategory = {
            title: await this._globalService.getTranslate('Goods Category'),
            scheme: '#FFF000',
            value: response.length + ' Data',
            percent: 100,
            percentActive: false,
          };
          this.categories = [...response];
          this._cdRef.detectChanges();
          resolve(response);
          return;
        }
      });
    });
  }

  async getWarehouses(): Promise<any> {
    console.log('getWarehouses', this.selectedBusiness);
    return new Promise(async (resolve, reject) => {
      const configFirebase: FirebaseConfigModel = {
        collection: {
          status: true,
          name: 'apps',
          document: {
            status: true,
            name: 'Database',
            collection: {
              status: true,
              name: 'business',
              document: {
                status: true,
                name: this.selectedBusiness.key,
                collection: {
                  status: true,
                  name: 'warehouses',
                  document: {
                    status: false,
                  },
                  orderBy: {
                    object: 'created',
                    order: 'desc',
                  },
                  query: [],
                },
              },
            },
          },
        },
      };

      console.log('configFirebase', configFirebase);
      const data: any = await this._firebaseService.handleCollection(
        null,
        configFirebase
      );
      data.subscribe((response: Warehouse[]) => {
        if (response) {
          if (response && response.length > 0) {
            this.selectedWarehouseValue = response[0].key;
            this.selectedWarehouse = response[0];
          }
          this.warehouses = [...response];
          this.value.dataWarehouse = [...this.warehouses];
          this._cdRef.detectChanges();
          resolve(response);
          return;
        }
      });
    });
  }

  initYearly() {
    this.yearlyBalance = {
      inStock: Object.assign([], this.initYearlyAccounting),
      outStock: Object.assign([], this.initYearlyAccounting),
    };
  }

  async initStatistic() {}

  checkYearly(yearly: any) {
    console.log('checkyearly', yearly);
    if (yearly[0] > 0) {
      return true;
    } else if (yearly[1] > 0) {
      return true;
    } else if (yearly[2] > 0) {
      return true;
    } else if (yearly[3] > 0) {
      return true;
    } else if (yearly[4] > 0) {
      return true;
    } else if (yearly[5] > 0) {
      return true;
    } else if (yearly[6] > 0) {
      return true;
    } else if (yearly[7] > 0) {
      return true;
    } else if (yearly[8] > 0) {
      return true;
    } else if (yearly[9] > 0) {
      return true;
    } else if (yearly[10] > 0) {
      return true;
    } else if (yearly[11] > 0) {
      return true;
    }
    return false;
  }

  async callbackWarehouse(event: any) {
    console.log('event', event);
    if (event) {
      this.selectedWarehouseValue = event;
    } else {
      this.selectedWarehouseValue = '';
    }

    if (this.warehouses && this.warehouses.length > 0) {
      const findWarehouse = this.warehouses.filter((itemWarehouse: any) => {
        if (itemWarehouse.key === this.selectedWarehouseValue) {
          return itemWarehouse;
        }
      });
      if (findWarehouse && findWarehouse.length > 0) {
        this.selectedWarehouse = findWarehouse[0];
      }
    }

    this.loading.isLoading = true;
    setTimeout(async () => {
      this.flag.load = false;
      this.initYearly();
      await this.getStocks();
      await this.getGoodsWarehouse();
      this.loading.isLoading = false;
      this.flag.load = true;
    }, 3000);
  }

  addBusiness() {
    this._router.navigateByUrl('/settings/business');
  }
}
