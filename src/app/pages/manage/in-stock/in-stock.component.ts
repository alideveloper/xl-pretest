import {
  Component,
  OnInit,
  ChangeDetectorRef,
  ViewEncapsulation,
  HostListener,
  ViewChild,
  OnDestroy,
} from '@angular/core';

import { ColorScheme, BaseComponent } from '@app/core';
import { environment } from '@environments/environment';

import { FirebaseService } from '@app/@firebase/firebase.service';

import { DatatableComponent } from '@swimlane/ngx-datatable';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { SelectionType } from '@swimlane/ngx-datatable';

import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { FilterByPipe } from 'ngx-pipes';

import { AddInStockComponent } from './add/add.component';
import { DeleteInStockComponent } from './delete/delete.component';
import { AppService } from '@app/app.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AccountingPipe } from '@app/shared/pipes/accounting.pipe';
import { Stock } from '@app/shared/model/stock.model';
import {
  Query,
  FirebaseConfigModel,
} from '@app/shared/model/other/firebase-config.model';
import { Warehouse } from '@app/shared/model/warehouse.model';
import { GoodsUnit } from '@app/shared/model/goods-unit.model';
import { GoodsCategory } from '@app/shared/model/goods-category.model';
import { Goods } from '@app/shared/model/goods.model';
import { Supplier } from '@app/shared/model/supplier.model';
import { DetailInStockComponent } from './detail/detail.component';
import { GlobalAction } from '@app/shared/services/global.action';
import { User } from '@app/shared/model/user.model';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Staff } from '@app/shared/model/staff.model';

const moment = require('moment');
moment();

@Component({
  selector: 'app-in-stock',
  templateUrl: './in-stock.component.html',
  styleUrls: ['./in-stock.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [AccountingPipe],
})
export class InStockComponent extends BaseComponent
  implements OnInit, OnDestroy {
  @ViewChild(DatatableComponent, { static: false })
  table: DatatableComponent;

  redirectURL = environment.indexURL.root + environment.indexURL.homeTwo;

  ColorScheme = ColorScheme;
  ColumnMode = ColumnMode;

  messages: any = {
    emptyMessage:
      '<div class="empty-container"><img src="assets/img/home1.png"><div class="empty-table">No results</div></div>',
  };

  timeout: any;

  currentStyles: any = 'table-hover';
  isCard = true;
  isTableView = false;
  isListCollapsed = false;
  responsive = true;
  responsiveExpandable = false;

  headerHeight: any = 'auto';
  rowHeight: any = false;
  footerHeight: any = 'auto';
  scrollbarV = false;
  scrollbarH = true;
  limit = 10;

  selectedActive = false;
  selected: any[] = [];
  SelectionType = SelectionType;
  selectAllRowsOnPage = false;

  selectedMessage: any;

  data: Stock[];
  initDataInStocks: Stock[];

  modalRef: BsModalRef;

  pagination: any = {
    active: true,
    labelTotalMessage: 'Total Data',
  };

  columns: any = [
    {
      width: 50,
      sortable: false,
      canAutoResize: true,
      draggable: false,
      resizeable: false,
      name: 'index',
      header: 'No',
    },
    {
      width: 200,
      sortable: true,
      canAutoResize: true,
      draggable: false,
      resizeable: false,
      name: 'key',
      header: 'Stock Key',
    },
    {
      width: 175,
      sortable: true,
      canAutoResize: true,
      draggable: false,
      resizeable: false,
      name: 'users',
      header: 'Supplier',
    },
    {
      width: 175,
      sortable: true,
      canAutoResize: true,
      draggable: false,
      resizeable: false,
      name: 'totalGoods',
      header: 'Total Goods',
    },
    {
      width: 200,
      sortable: true,
      canAutoResize: true,
      draggable: false,
      resizeable: false,
      name: 'totalPrice',
      header: 'Total Price',
    },
    {
      width: 200,
      sortable: true,
      canAutoResize: true,
      draggable: false,
      resizeable: false,
      name: 'date',
      header: 'Created',
    },
    {
      width: 'auto',
      sortable: false,
      canAutoResize: true,
      draggable: false,
      resizeable: false,
      name: 'action',
      header: 'Action',
    },
  ];

  user: User;

  selectedBusinessValue: any;
  selectedBusiness: any;

  search: any = {};

  widthLayer: any = window.innerWidth;
  isTableResponse: boolean;

  selectedWarehouseValue: any;
  selectedWarehouse: any;
  dataWarehouse: Warehouse[];
  initDataWarehouse: Warehouse[];
  warehouses: Warehouse[];

  units: GoodsUnit[];
  categories: GoodsCategory[];
  goods: Goods[];
  suppliers: Supplier[];

  private _unsubscribeAll: Subject<any>;

  value: any = {
    form: {},
  };

  dateNow: any = new Date();

  schemaSupplierSelect: any = {
    component: 'select',
    position: 'body',
    label: {
      active: true,
      binding: false,
      text: 'By Supplier',
      type: 'text',
      state: {
        hide: false,
        disabled: false,
        hideCondition: [],
        disabledCondition: [],
      },
    },
    labelSubtitle: {
      active: false,
    },
    type: 'text',
    placeholder: 'Select Supplier',
    parentBinding: 'form',
    bindingObject: 'userKey',
    primaryKey: 'key',
    bindingData: 'dataSuppliers',
    labelForId: 'name',
    bindLabel: 'name',
    bindValue: 'key',
    searchable: true,
    search: true,
    closeOnSelect: true,
    defaultValue: true,
    customSearch: true,
    clearable: false,
    bindingSearch: 'dataSuppliers',
    required: false,
    formText: {
      valid: {
        active: false,
        text: 'Data telah Sesuai!',
      },
      invalid: {
        active: false,
        text: 'Data belum Sesuai!',
      },
    },
  };

  schemaWarehouseSelect: any = {
    component: 'select',
    position: 'body',
    label: {
      active: true,
      binding: false,
      text: 'Select Warehouse',
      type: 'text',
      state: {
        hide: false,
        disabled: false,
        hideCondition: [],
        disabledCondition: [],
      },
    },
    labelSubtitle: {
      active: false,
    },
    type: 'text',
    placeholder: 'Select Warehouse',
    parentBinding: 'form',
    bindingObject: 'warehouseKey',
    primaryKey: 'key',
    bindingData: 'dataWarehouse',
    labelForId: 'name',
    bindLabel: 'name',
    bindValue: 'key',
    searchable: true,
    search: true,
    closeOnSelect: true,
    defaultValue: true,
    customSearch: true,
    clearable: false,
    bindingSearch: 'dataWarehouse',
    required: false,
    formText: {
      valid: {
        active: false,
        text: 'Data telah Sesuai!',
      },
      invalid: {
        active: false,
        text: 'Data belum Sesuai!',
      },
    },
  };

  flag: any = {
    load: false,
    loadData: false,
  };

  staff: Staff;

  minBalanceRange: any = 0;
  maxBalanceRange: any = 0;

  totalGoods: any = 0;
  totalPrice: any = 0;

  emptyBusiness: any;

  @HostListener('window:resize', ['$event'])
  onResize(event: any) {
    this.widthLayer = event.target.innerWidth;
    this.checkLayerWidth();
  }

  constructor(
    private _modalService: BsModalService,
    private _firebaseService: FirebaseService,
    private _cdRef: ChangeDetectorRef,
    private _filterByPipe: FilterByPipe,
    private _appService?: AppService,
    private _bsModalService?: BsModalService,
    private _globalAction?: GlobalAction,
    private _router?: Router,
    private _translateService?: TranslateService
  ) {
    super();
  }

  async ngOnInit() {
    await this.initStorage();
    await this.initRouting();

    this._unsubscribeAll = new Subject();
    this.checkLayerWidth();
    this.initSearch();

    if (this.emptyBusiness === false) {
      await this.initBusiness();
      await this.initData();

      this._appService.onDataChanges
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe(async (data) => {
          console.log('on changes');
          console.log(data.selectedBusiness);
          if (data.selectedBusinessValue !== this.selectedBusinessValue) {
            if (data.selectedBusinessValue) {
              this.selectedBusinessValue = data.selectedBusinessValue;
            }
          }
          if (
            (data.selectedBusiness &&
              data.selectedBusiness !== this.selectedBusiness) ||
            (data.selectedBusiness &&
              data.selectedBusiness.currency !== this.selectedBusiness.currency)
          ) {
            console.log('MASUK', data.selectedBusiness);
            this.selectedBusiness = data.selectedBusiness;
            this.initSearch();
            this.initData();
          }
        });
    }
  }

  async ngOnDestroy() {
    try {
      this._unsubscribeAll.next();
      this._unsubscribeAll.complete();
    } catch (error) {}
  }

  async initStorage(): Promise<any> {
    this.user = this._globalAction.getUserLogin();
  }

  checkLayerWidth() {
    if (this.widthLayer < 800) {
      this.isTableResponse = true;
      this.scrollbarV = false;
    } else {
      this.isTableResponse = false;
    }
  }

  initSearch() {
    this.search = {
      stockKey: '',
      userKey: '',
      typeDate: 'All',
      rangeDate: [
        new Date(this.dateNow.getFullYear(), this.dateNow.getMonth(), 1),
        new Date(this.dateNow.getFullYear(), this.dateNow.getMonth() + 1, 0),
      ],
      rangeBalanceCallback: [
        {
          oldValue: [this.minBalanceRange, this.maxBalanceRange],
          newValue: [this.minBalanceRange, this.maxBalanceRange],
        },
      ],
      rangeBalance: [this.minBalanceRange, this.maxBalanceRange],
    };
  }

  async initRouting() {
    this.emptyBusiness = await this._globalAction.isEmptyBusiness();
    this._globalAction.submenuRouting('/manage/in-stock');
  }

  async initData() {
    this.flag.load = false;
    await this.getWarehouse();
    await this.getSupplier();
    if (this.selectedWarehouseValue) {
      await this.getInStock();
    } else {
      this.data = [];
      this.initDataInStocks = [];
    }

    await this.getCategories();
    await this.getUnits();
    await this.getGoods();
    this.flag.load = true;
  }

  async initBusiness() {
    console.log('ini business');
    this.selectedBusinessValue = localStorage.getItem(
      environment.domain + '-selectedBusinessValue'
    );
    this.selectedBusiness = JSON.parse(
      localStorage.getItem(environment.domain + '-selectedBusiness')
    );
  }

  onSelect({ selected }: any) {
    this.selected = [...selected];
  }

  async getWarehouse(): Promise<any> {
    console.log(environment.domain);
    console.log(
      JSON.parse(localStorage.getItem(environment.domain + '-selectedBusiness'))
    );
    console.log(this.selectedBusiness, 'selectedBusiness');
    return new Promise(async (resolve, reject) => {
      const configFirebase: FirebaseConfigModel = {
        collection: {
          status: true,
          name: 'apps',
          document: {
            status: true,
            name: 'Database',
            collection: {
              status: true,
              name: 'business',
              document: {
                status: true,
                name: this.selectedBusiness.key,
                collection: {
                  status: true,
                  name: 'warehouses',
                  document: {
                    status: false,
                  },
                  orderBy: {
                    object: 'created',
                    order: 'desc',
                  },
                  query: [],
                },
              },
            },
          },
        },
      };

      const data: any = await this._firebaseService.handleCollection(
        null,
        configFirebase
      );
      data.subscribe((response: Warehouse[]) => {
        if (response) {
          console.log('response warehouse', response);
          let warehouses: Warehouse[] = [];

          warehouses = [...response];
          if (warehouses && warehouses.length > 0) {
            this.selectedWarehouseValue = warehouses[0].key;
            this.selectedWarehouse = warehouses[0];
          }
          this.dataWarehouse = [...warehouses];
          this.initDataWarehouse = [...this.dataWarehouse];
          this.warehouses = [...this.dataWarehouse];
          this.value.dataWarehouse = [...this.initDataWarehouse];
          this._cdRef.detectChanges();
          if (warehouses && warehouses.length === 0) {
            this._globalAction.showToast({
              title: this._translateService.instant('Info!'),
              message: this._translateService.instant('Add Warehouse First!'),
              type: 'error',
            });
            this._router.navigateByUrl('/dashboard');
          }
          resolve(warehouses);
          return;
        }
      });
    });
  }

  async getInStock(): Promise<any> {
    this.maxBalanceRange = 0;
    this.flag.loadData = false;

    this.messages = {
      emptyMessage:
        '<div class="empty-container"><div class="empty-table">Loading...</div></div>',
    };

    console.log(environment.domain);
    console.log(
      JSON.parse(localStorage.getItem(environment.domain + '-selectedBusiness'))
    );
    console.log(this.selectedBusiness, 'selectedBusiness');

    const where: Query[] = [
      {
        object: 'type',
        condition: '==',
        value: 'In',
      },
    ];
    return new Promise(async (resolve, reject) => {
      const configFirebase: FirebaseConfigModel = {
        collection: {
          status: true,
          name: 'apps',
          document: {
            status: true,
            name: 'Database',
            collection: {
              status: true,
              name: 'business',
              document: {
                status: true,
                name: this.selectedBusiness.key,
                collection: {
                  status: true,
                  name: 'warehouses',
                  document: {
                    status: true,
                    name: this.selectedWarehouse.key,
                    collection: {
                      status: true,
                      name: 'stocks',
                      document: {
                        status: false,
                      },
                      orderBy: {
                        object: 'created',
                        order: 'desc',
                      },
                      query: [...where],
                    },
                  },
                },
              },
            },
          },
        },
      };

      const data: any = await this._firebaseService.handleCollection(
        null,
        configFirebase
      );
      data.subscribe((response: Stock[]) => {
        console.log('stock', response);
        if (response) {
          this.totalGoods = 0;
          this.totalPrice = 0;
          response.map((stocks: Stock) => {
            this.totalGoods += stocks.totalGoods;
            this.totalPrice += stocks.totalPrice;
            if (this.maxBalanceRange < stocks.totalPrice) {
              this.maxBalanceRange = stocks.totalPrice;
            }

            if (this.suppliers && this.suppliers.length > 0) {
              const users = this.suppliers.filter(
                (supplierItem: any) => supplierItem.key === stocks.userKey
              );

              if (users && users.length > 0) {
                stocks.users = users[0];
              } else {
                stocks.userKey = null;
                stocks.users = {};
                stocks.users.key = '';
                stocks.users.name = '-';
              }
            } else {
              stocks.userKey = null;
              stocks.users = {};
              stocks.users.key = '';
              stocks.users.name = '-';
            }
          });

          this.search.rangeBalance = [
            this.minBalanceRange,
            this.maxBalanceRange,
          ];

          this.data = [...response];
          this.initDataInStocks = [...response];

          this.messages = {
            emptyMessage:
              '<div class="empty-container"><img src="assets/img/home1.png"><div class="empty-table">No results</div></div>',
          };
          this._cdRef.detectChanges();
          this.flag.loadData = true;
          resolve(response);
          return;
        }
      });
    });
  }

  async getGoods(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const configFirebase: FirebaseConfigModel = {
        collection: {
          status: true,
          name: 'apps',
          document: {
            status: true,
            name: 'Database',
            collection: {
              status: true,
              name: 'business',
              document: {
                status: true,
                name: this.selectedBusiness.key,
                collection: {
                  status: true,
                  name: 'goods',
                  document: {
                    status: false,
                  },
                  orderBy: {
                    object: 'created',
                    order: 'desc',
                  },
                  query: [],
                },
              },
            },
          },
        },
      };

      const data: any = await this._firebaseService.handleCollection(
        null,
        configFirebase
      );
      data.subscribe((response: Goods[]) => {
        if (response) {
          response.map((goods: Goods) => {
            if (this.categories && this.categories.length > 0) {
              const categories = this.categories.filter(
                (categoryItem: any) => categoryItem.key === goods.categoryKey
              );

              if (categories && categories.length > 0) {
                goods.categories = categories[0];
              } else {
                goods.categoryKey = null;
                goods.categories = {};
                goods.categories.key = '';
                goods.categories.name = '-';
              }
            } else {
              goods.categoryKey = null;
              goods.categories = {};
              goods.categories.key = '';
              goods.categories.name = '-';
            }

            if (this.units && this.units.length > 0) {
              const units = this.units.filter(
                (unitItem: any) => unitItem.key === goods.unitKey
              );

              if (units && units.length > 0) {
                goods.units = units[0];
              } else {
                goods.unitKey = null;
                goods.units = {};
                goods.units.key = '';
                goods.units.name = '-';
              }
            } else {
              goods.unitKey = null;
              goods.units = {};
              goods.units.key = '';
              goods.units.name = '-';
            }
          });
          this.goods = [...response];
          this._cdRef.detectChanges();
          resolve(response);
          return;
        }
      });
    });
  }

  async getCategories(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      if (this.selectedBusiness) {
        const configFirebase: FirebaseConfigModel = {
          collection: {
            status: true,
            name: 'apps',
            document: {
              status: true,
              name: 'Database',
              collection: {
                status: true,
                name: 'business',
                document: {
                  status: true,
                  name: this.selectedBusiness.key,
                  collection: {
                    status: true,
                    name: 'categories',
                    document: {
                      status: false,
                    },
                    orderBy: {
                      object: 'created',
                      order: 'desc',
                    },
                    query: [],
                  },
                },
              },
            },
          },
        };

        const data: any = await this._firebaseService.handleCollection(
          null,
          configFirebase
        );
        data.subscribe((response: GoodsCategory[]) => {
          if (response) {
            this.categories = [...response];
            console.log('responsecategories', response);
            this._cdRef.detectChanges();
            resolve(response);
            return;
          }
        });
      }
    });
  }

  async getUnits(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      if (this.selectedBusiness) {
        const configFirebase: FirebaseConfigModel = {
          collection: {
            status: true,
            name: 'apps',
            document: {
              status: true,
              name: 'Database',
              collection: {
                status: true,
                name: 'business',
                document: {
                  status: true,
                  name: this.selectedBusiness.key,
                  collection: {
                    status: true,
                    name: 'units',
                    document: {
                      status: false,
                    },
                    orderBy: {
                      object: 'created',
                      order: 'desc',
                    },
                    query: [],
                  },
                },
              },
            },
          },
        };

        const data: any = await this._firebaseService.handleCollection(
          null,
          configFirebase
        );
        data.subscribe((response: GoodsUnit[]) => {
          if (response) {
            this.units = [...response];
            console.log('responseunits', response);
            this._cdRef.detectChanges();
            resolve(response);
            return;
          }
        });
      }
    });
  }

  async getSupplier(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const configFirebase: FirebaseConfigModel = {
        collection: {
          status: true,
          name: 'apps',
          document: {
            status: true,
            name: 'Database',
            collection: {
              status: true,
              name: 'business',
              document: {
                status: true,
                name: this.selectedBusiness.key,
                collection: {
                  status: true,
                  name: 'suppliers',
                  document: {
                    status: false,
                  },
                  orderBy: {
                    object: 'created',
                    order: 'desc',
                  },
                  query: [],
                },
              },
            },
          },
        },
      };

      const data: any = await this._firebaseService.handleCollection(
        null,
        configFirebase
      );
      data.subscribe((response: Supplier[]) => {
        if (response) {
          if (response && response.length > 0) {
            response.map((responseItem: any) => {
              console.log('FILTERING', response);
            });
          }

          const dataSuppliers = [];
          const itemAll = {
            key: '',
            name: 'All',
          };
          dataSuppliers.push(itemAll);

          response.map((item: Supplier) => {
            dataSuppliers.push(item);
          });

          this.value.dataSuppliers = [...dataSuppliers];
          this.suppliers = [...response];
          this._cdRef.detectChanges();
          resolve(response);
          return;
        }
      });
    });
  }

  get tableStyles(): string {
    return (this.isTableView ? 'table' : 'listview') + ' ' + this.currentStyles;
  }

  onPage(event: Event) {
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      console.log(event);
    }, 100);
  }

  addInStock(): void {
    const navParam = {
      initialState: {
        stocks: Object.assign([], this.initDataInStocks),
        selectedBusiness: Object.assign([], this.selectedBusiness),
        selectedWarehouse: Object.assign([], this.selectedWarehouse),
        goods: Object.assign([], this.goods),
        units: Object.assign([], this.units),
        categories: Object.assign([], this.categories),
        users: Object.assign([], this.suppliers),
        _bsModalService: this._bsModalService,
        user: this.user,
      },
      ignoreBackdropClick: true,
      class: 'modal-lg modal-dialog-centered modal-scroll',
    };

    this.modalRef = this._modalService.show(AddInStockComponent, navParam);
  }

  detailInStock(value: any): void {
    const navParam = {
      initialState: {
        stocks: this.initDataInStocks,
        formData: value,
        selectedBusiness: this.selectedBusiness,
        selectedWarehouse: this.selectedWarehouse,
      },
      class: 'modal-lg modal-dialog-centered',
    };

    console.log(navParam, 'navParam');
    this.modalRef = this._modalService.show(DetailInStockComponent, navParam);
  }

  deleteInStock(value: Stock): void {
    const navParam = {
      initialState: {
        form: value,
        selectedBusiness: this.selectedBusiness,
        selectedWarehouse: this.selectedWarehouse,
        user: this.user,
      },
      class: 'modal-md modal-dialog-centered',
    };

    console.log(navParam, 'navParam');
    this.modalRef = this._modalService.show(DeleteInStockComponent, navParam);
  }

  async callbackWarehouse(event: any) {
    console.log('event', event);
    if (event) {
      this.selectedWarehouseValue = event;
    } else {
      this.selectedWarehouseValue = '';
    }

    if (this.initDataWarehouse && this.initDataWarehouse.length > 0) {
      const findWarehouse = this.initDataWarehouse.filter(
        (itemWarehouse: any) => {
          if (itemWarehouse.key === this.selectedWarehouseValue) {
            return itemWarehouse;
          }
        }
      );
      if (findWarehouse && findWarehouse.length > 0) {
        this.selectedWarehouse = findWarehouse[0];
      }
    }

    this.flag.load = false;
    await this.getInStock();
    this.flag.load = true;
  }

  onChangeSearch(type?: any, event?: any) {
    let result: any = Object.assign([], this.initDataInStocks);

    this.totalGoods = 0;
    this.totalPrice = 0;

    if (type === 'rangeBalance') {
      this.search.rangeBalance = [event.newValue[0], event.newValue[1]];
    }
    console.log(event, result);

    if (this.search.typeDate === 'All') {
      result = this.onChangeSearchGeneral(result, this.search.stockKey, 'key');
      result = this.onChangeSearchGeneral(
        result,
        this.search.userKey,
        'userKey'
      );
      this.search.rangeDate = [
        new Date(this.dateNow.getFullYear(), this.dateNow.getMonth(), 1),
        new Date(this.dateNow.getFullYear(), this.dateNow.getMonth() + 1, 0),
      ];
      result = this.onChangeSearchRangeBalance(
        result,
        this.search.rangeBalance
      );
    } else {
      result = this.onChangeSearchGeneral(result, this.search.stockKey, 'key');
      result = this.onChangeSearchGeneral(
        result,
        this.search.userKey,
        'userKey'
      );
      result = this.onChangeSearchRangeDate(result, this.search.rangeDate);
      result = this.onChangeSearchRangeBalance(
        result,
        this.search.rangeBalance
      );
    }

    result.map((item: any) => {
      this.totalGoods += item.totalGoods;
      this.totalPrice += item.totalPrice;
    });
    // this.countingBalance(result);
    console.log('final', result);
    this.data = [...result];
  }

  onChangeSearchGeneral(result: any, event: any, field?: any) {
    console.log('onChangeSearchGeneral', result, event, field);
    if (event) {
      const filter = this._filterByPipe.transform(result, [field], [event]);
      console.log('return', filter);
      return filter;
    }
    console.log('empty');
    return result;
  }

  onChangeSearchRangeDate(result: any, event: any) {
    console.log('onChangeSearchRangeDate', result, event);
    if (event) {
      if (result && result.length > 0) {
        result = result.filter((item: any) => {
          const date = moment(item.date.seconds * 1000);
          console.log('date', date);
          if (date > moment(event[0]) && date < moment(event[1])) {
            return item;
          }
        });
      }
    }
    return result;
  }

  onChangeSearchRangeBalance(result: any, event: any) {
    console.log('onChangeSearchRangeBalance', result, event);
    if (event) {
      if (result && result.length > 0) {
        result = result.filter((item: any) => {
          if (item.totalPrice >= event[0] && item.totalPrice <= event[1]) {
            return item;
          }
        });
      }
    }
    return result;
  }

  callbackSupplier(event: any) {
    console.log('event', event);
    if (event) {
      this.search.userKey = event;
    } else {
      this.search.userKey = '';
    }

    this.onChangeSearch('userKey', event);
  }
}
