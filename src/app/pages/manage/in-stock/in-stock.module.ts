import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InStockRoutingModule } from './in-stock-routing.module';

import { SharedModule } from '@app/shared';
import { NgxDatatableModule, ScrollbarHelper } from '@swimlane/ngx-datatable';

import { UtilsModule } from '@app/components/utils/utils.module';
import { NavigationsModule } from '@components/navigations/navigations.module';
import { AlertsModule } from '@app/components/alerts/alerts.module';
import { FormControlsModule } from '@app/components/form-controls/form-controls.module';
import { IconsModule } from '@app/components/icons/icons.module';

import { InStockComponent } from './in-stock.component';
import { AddInStockComponent } from './add/add.component';
import { DetailInStockComponent } from './detail/detail.component';
import { DeleteInStockComponent } from './delete/delete.component';

import { FilterByPipe } from 'ngx-pipes';
import { SelectsModule } from '@app/components/select/select.module';

const modules = [
  UtilsModule,
  CommonModule,
  NavigationsModule,
  NgxDatatableModule,
  IconsModule,
  AlertsModule,
  FormControlsModule,
  SharedModule,
  SelectsModule,
];

const declarations = [
  AddInStockComponent,
  DetailInStockComponent,
  DeleteInStockComponent,
];

@NgModule({
  declarations: [InStockComponent, ...declarations],
  imports: [InStockRoutingModule, ...modules],
  entryComponents: [...declarations],
  providers: [FilterByPipe],
})
export class InStockModule {}
