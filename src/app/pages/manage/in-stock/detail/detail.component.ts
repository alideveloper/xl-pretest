import { Component, HostListener, OnInit, ViewChild } from '@angular/core';

import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

import { TranslateService } from '@ngx-translate/core';

import { ngxLoadingAnimationTypes } from 'ngx-loading';
import { Stock } from '@app/shared/model/stock.model';
import { Loading } from '@app/shared/model/other/loading.model';
import {
  ColumnChangesService,
  ColumnMode,
  DatatableComponent,
  DimensionsHelper,
  ScrollbarHelper,
  SelectionType,
} from '@swimlane/ngx-datatable';
import { ColorScheme } from '@app/core';
import { Warehouse } from '@app/shared/model/warehouse.model';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-detail-in-stock',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
  providers: [ScrollbarHelper, DimensionsHelper, ColumnChangesService],
})
export class DetailInStockComponent implements OnInit {
  @ViewChild(DatatableComponent, { static: false })
  tableGoods: DatatableComponent;

  loading: Loading = {
    isLoading: false,
    text: this._translateService.instant('Detail In Stock') + '...',
    primaryColour: '',
    secondaryColour: '',
    animation: ngxLoadingAnimationTypes.threeBounce,
    image: 'assets/img/svg/save-person.svg',
  };

  ColorScheme = ColorScheme;
  ColumnMode = ColumnMode;

  messages: any = {
    emptyMessage:
      '<div class="empty-container"><div class="empty-table">No results</div></div>',
  };

  timeout: any;

  currentStyles: any = 'table-hover';
  isCard = true;
  isTableView = false;
  isListCollapsed = false;
  responsive = true;
  responsiveExpandable = false;

  headerHeight: any = 'auto';
  rowHeight: any = false;
  footerHeight: any = 'auto';
  scrollbarV = false;
  scrollbarH = true;
  limit = 10;

  selectedActive = false;
  selected: any[] = [];
  SelectionType = SelectionType;
  selectAllRowsOnPage = false;

  selectedMessage: any;

  data: any[];
  initDataGoods: any[];

  modalRefTwo: BsModalRef;

  pagination: any = {
    active: true,
    labelTotalMessage: 'Total Data',
  };

  columns: any = [
    {
      width: 50,
      sortable: false,
      canAutoResize: true,
      draggable: false,
      resizeable: false,
      name: 'index',
      header: 'No',
    },
    {
      width: 175,
      sortable: true,
      canAutoResize: true,
      draggable: false,
      resizeable: false,
      name: 'goods',
      header: 'Name',
    },
    {
      width: 175,
      sortable: true,
      canAutoResize: true,
      draggable: false,
      resizeable: false,
      name: 'qty',
      header: 'QTY',
    },
    {
      width: 200,
      sortable: true,
      canAutoResize: true,
      draggable: false,
      resizeable: false,
      name: 'totalPrice',
      header: 'Total Price',
    },
  ];

  formData: Stock;
  selectedBusiness: any;
  selectedWarehouse: Warehouse;

  maxAddStock: any = 200;
  remainingAddStock: any = 200;

  _bsModalService: any;

  public onClose: Subject<boolean>;

  flag: any = {
    disabledButton: false,
  };

  widthLayer: any = window.innerWidth;
  isTableResponse: boolean;

  @HostListener('window:resize', ['$event'])
  onResize(event: any) {
    this.widthLayer = event.target.innerWidth;
    this.checkLayerWidth();
  }

  constructor(
    private _translateService?: TranslateService,
    private _modalService?: BsModalService
  ) {}

  async ngOnInit(): Promise<any> {
    await this.initData();
  }

  async initData(): Promise<any> {
    if (this.formData) {
      this.data = [...JSON.parse(this.formData.goods)];
      this.initDataGoods = [...this.data];
    }
  }

  checkLayerWidth() {
    if (this.widthLayer < 800) {
      this.isTableResponse = true;
      this.scrollbarV = false;
    } else {
      this.isTableResponse = false;
    }
  }

  onChangeInStock(event: any) {}

  async closeModal(): Promise<any> {
    this._modalService.hide(1);
  }

  onSelect({ selected }: any) {
    this.selected = [...selected];
  }

  onPage(event: Event) {
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      console.log(event);
    }, 100);
  }
}
