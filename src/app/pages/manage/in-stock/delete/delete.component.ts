import { Component, OnInit } from '@angular/core';

import { FirebaseService } from '@firebase/firebase.service';

import { BsModalRef } from 'ngx-bootstrap/modal';

import { TranslateService } from '@ngx-translate/core';

import { GlobalAction } from '@app/shared/services/global.action';

import { ngxLoadingAnimationTypes } from 'ngx-loading';
import { Loading } from '@app/shared/model/other/loading.model';
import { Stock } from '@app/shared/model/stock.model';
import {
  FirebaseConfigModel,
  Query,
} from '@app/shared/model/other/firebase-config.model';
import { AngularFirestore } from '@angular/fire/firestore';
import { User } from '@app/shared/model/user.model';

@Component({
  selector: 'app-delete-in-stock',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.scss'],
})
export class DeleteInStockComponent implements OnInit {
  loading: Loading = {
    isLoading: false,
    text:
      this._translateService.instant(
        'Deleted In Stock, This will take a few moments'
      ) + '...',
    primaryColour: '',
    secondaryColour: '',
    animation: ngxLoadingAnimationTypes.threeBounce,
    image: 'assets/img/svg/save-person.svg',
  };

  selectedBusiness: any;
  selectedWarehouse: any;

  key: any;

  form: Stock;

  user: User;

  constructor(
    private _bsModalRef: BsModalRef,
    private _translateService?: TranslateService,
    private _firebaseService?: FirebaseService,
    private _globalAction?: GlobalAction,
    private _angularFirestore?: AngularFirestore
  ) {}

  async ngOnInit(): Promise<any> {
    console.log(this.form, 'form');
    if (this.form) {
      this.key = this.form.key;
    }
  }

  async close(): Promise<any> {
    this._bsModalRef.hide();
  }

  async deleteGoodsInWarehouse(batch: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      console.log('this.form', this.form);
      const where: Query[] = [
        {
          object: 'stockKey',
          condition: '==',
          value: this.form.key,
        },
      ];

      const firebaseConfig: FirebaseConfigModel = {
        method: 'DELETE',
        collection: {
          status: true,
          name: 'apps',
          document: {
            status: true,
            name: 'Database',
            collection: {
              status: true,
              name: 'business',
              document: {
                status: true,
                name: this.selectedBusiness.key,
                collection: {
                  status: true,
                  name: 'warehouses',
                  document: {
                    status: true,
                    name: this.selectedWarehouse.key,
                    collection: {
                      status: true,
                      name: 'goodswarehouses',
                      document: {
                        status: false,
                      },
                      orderBy: {
                        object: 'created',
                        order: 'desc',
                      },
                      query: [...where],
                    },
                  },
                },
              },
            },
          },
        },
      };

      this._firebaseService
        .deleteBatchSubCollection(null, firebaseConfig, null, batch)
        .then(
          (result: any) => {
            resolve(result);
          },
          (error: any) => {
            this.loading.isLoading = false;
            console.log('error history', error);
            this._globalAction.showToast({
              title: this._translateService.instant('Info!'),
              message: error,
              type: 'error',
            });
            reject(false);
          }
        );
    });
  }

  async submit(): Promise<any> {
    this.loading.isLoading = true;
    const where: Query[] = [
      {
        object: 'key',
        condition: '==',
        value: this.form.key,
      },
    ];

    let batch = await this._angularFirestore.firestore.batch();

    if (batch) batch = await this.deleteGoodsInWarehouse(batch);

    const firebaseConfig: FirebaseConfigModel = {
      method: 'DELETE',
      collection: {
        status: true,
        name: 'apps',
        document: {
          status: true,
          name: 'Database',
          collection: {
            status: true,
            name: 'business',
            document: {
              status: true,
              name: this.selectedBusiness.key,
              collection: {
                status: true,
                name: 'warehouses',
                document: {
                  status: true,
                  name: this.selectedWarehouse.key,
                  collection: {
                    status: true,
                    name: 'stocks',
                    document: {
                      status: false,
                    },
                    orderBy: {
                      object: 'created',
                      order: 'desc',
                    },
                    query: [...where],
                  },
                },
              },
            },
          },
        },
      },
    };

    const payload: Stock = Object.assign({}, this.form);
    this._firebaseService
      .deleteBatchSubCollection(null, firebaseConfig, payload, batch, true)
      .then(
        (result: any) => {
          result.commit().then(
            () => {
              this.loading.isLoading = false;
              this._globalAction.showToast({
                title: this._translateService.instant('Info!'),
                message: this._translateService.instant(
                  'Successfully deleted In Stock!'
                ),
                type: 'success',
              });
              this._bsModalRef.hide();
              return;
            },
            (error: any) => {
              console.log('error transactions 1', error);
              this.loading.isLoading = false;
              this._globalAction.showToast({
                title: this._translateService.instant('Info!'),
                message: error,
                type: 'error',
              });
              return false;
            }
          );
        },
        (error: any) => {
          console.log(error);
          this.loading.isLoading = false;
          this._globalAction.showToast({
            title: this._translateService.instant('Info!'),
            message: error,
            type: 'error',
          });
        }
      );
  }
}
