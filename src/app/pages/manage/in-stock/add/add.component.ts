import {
  Component,
  ElementRef,
  HostListener,
  OnInit,
  TemplateRef,
  ViewChild,
} from '@angular/core';

import { FirebaseService } from '@firebase/firebase.service';

import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

import { TranslateService } from '@ngx-translate/core';

import { GlobalAction } from '@app/shared/services/global.action';

import { ngxLoadingAnimationTypes } from 'ngx-loading';
import { Stock } from '@app/shared/model/stock.model';
import { Loading } from '@app/shared/model/other/loading.model';
import {
  FirebaseConfigModel,
  Query,
} from '@app/shared/model/other/firebase-config.model';
import {
  ColumnChangesService,
  ColumnMode,
  DatatableComponent,
  DimensionsHelper,
  ScrollbarHelper,
  SelectionType,
} from '@swimlane/ngx-datatable';
import { ColorScheme } from '@app/core';
import { Warehouse } from '@app/shared/model/warehouse.model';
import { Goods } from '@app/shared/model/goods.model';
import { GoodsWarehouse } from '@app/shared/model/goods-warehouse.model';
import { GoodsUnit } from '@app/shared/model/goods-unit.model';
import { GoodsCategory } from '@app/shared/model/goods-category.model';
import { Supplier } from '@app/shared/model/supplier.model';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';
import { User } from '@app/shared/model/user.model';

declare let onScan: any;

@Component({
  selector: 'app-add-in-stock',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss'],
  providers: [ScrollbarHelper, DimensionsHelper, ColumnChangesService],
})
export class AddInStockComponent implements OnInit {
  @ViewChild(DatatableComponent, { static: false })
  tableGoods: DatatableComponent;

  loading: Loading = {
    isLoading: false,
    text:
      this._translateService.instant(
        'Added In Stock, This will take a few moments'
      ) + '...',
    primaryColour: '',
    secondaryColour: '',
    animation: ngxLoadingAnimationTypes.threeBounce,
    image: 'assets/img/svg/save-person.svg',
  };

  ColorScheme = ColorScheme;
  ColumnMode = ColumnMode;

  messages: any = {
    emptyMessage:
      '<div class="empty-container"><div class="empty-table">No Goods</div></div>',
  };

  timeout: any;

  currentStyles: any = 'table-hover';
  isCard = true;
  isTableView = false;
  isListCollapsed = false;
  responsive = true;
  responsiveExpandable = false;

  headerHeight: any = 'auto';
  rowHeight: any = false;
  footerHeight: any = 'auto';
  scrollbarV = false;
  scrollbarH = true;
  limit = 5;

  selectedActive = false;
  selected: any[] = [];
  SelectionType = SelectionType;
  selectAllRowsOnPage = false;

  selectedMessage: any;

  data: any[];
  initDataGoods: any[];

  modalRefTwo: BsModalRef;
  modalRefThree: BsModalRef;
  modalRefFour: BsModalRef;

  pagination: any = {
    active: true,
    labelTotalMessage: 'Total Data',
  };

  columns: any = [
    {
      width: 50,
      sortable: false,
      canAutoResize: true,
      draggable: false,
      resizeable: false,
      name: 'index',
      header: 'No',
    },
    {
      width: 175,
      sortable: true,
      canAutoResize: true,
      draggable: false,
      resizeable: false,
      name: 'goods',
      header: 'Name',
    },
    {
      width: 175,
      sortable: true,
      canAutoResize: true,
      draggable: false,
      resizeable: false,
      name: 'qty',
      header: 'QTY',
    },
    {
      width: 200,
      sortable: true,
      canAutoResize: true,
      draggable: false,
      resizeable: false,
      name: 'totalPrice',
      header: 'Total Price',
    },
    {
      width: 'auto',
      sortable: false,
      canAutoResize: true,
      draggable: false,
      resizeable: false,
      name: 'action',
      header: 'Action',
    },
  ];

  units: GoodsUnit[];
  categories: GoodsCategory[];
  goods: Goods[];
  stocks: Stock[] = [];
  users: Supplier[];
  selectedBusiness: any;
  selectedWarehouse: Warehouse;

  schemaSupplierSelect: any = {
    component: 'select',
    position: 'body',
    label: {
      active: true,
      binding: false,
      text: 'Select Supplier',
      type: 'text',
      state: {
        hide: false,
        disabled: false,
        hideCondition: [],
        disabledCondition: [],
      },
    },
    labelSubtitle: {
      active: false,
    },
    type: 'text',
    placeholder: 'Select Supplier',
    parentBinding: 'form',
    bindingObject: 'userKey',
    primaryKey: 'key',
    bindingData: 'users',
    labelForId: 'name',
    bindLabel: 'name',
    bindValue: 'key',
    searchable: true,
    search: true,
    closeOnSelect: true,
    defaultValue: true,
    customSearch: true,
    clearable: false,
    bindingSearch: 'users',
    required: false,
    formText: {
      valid: {
        active: false,
        text: 'Data telah Sesuai!',
      },
      invalid: {
        active: false,
        text: 'Data belum Sesuai!',
      },
    },
  };

  value: any = {
    form: {},
  };

  maxAddStock: any = 200;
  remainingAddStock: any = 200;

  _bsModalService: any;

  selectedGoods: Goods;
  selectedUsers: Supplier;
  form: FormGroup;
  formStock: FormGroup;

  schemaGoodsSelect: any = {
    component: 'select',
    position: 'body',
    label: {
      active: true,
      binding: false,
      text: 'Goods',
      type: 'text',
      state: {
        hide: false,
        disabled: false,
        hideCondition: [],
        disabledCondition: [],
      },
    },
    labelSubtitle: {
      active: false,
    },
    type: 'text',
    placeholder: 'Select Goods',
    parentBinding: 'form',
    bindingObject: 'goodsKey',
    primaryKey: 'key',
    bindingData: 'goods',
    labelForId: 'name',
    bindLabel: 'name',
    bindValue: 'key',
    searchable: true,
    search: true,
    closeOnSelect: true,
    defaultValue: true,
    clearable: false,
    customSearch: true,
    bindingSearch: 'goods',
    required: true,
    formText: {
      valid: {
        active: true,
        text: 'Data telah Sesuai!',
      },
      invalid: {
        active: true,
        text: 'Data tidak Sesuai!',
      },
    },
  };

  public onClose: Subject<boolean>;

  flag: any = {
    disabledButton: false,
    loadCamera: false,
  };

  barcode: any;

  widthLayer: any = window.innerWidth;
  isTableResponse: boolean;

  @HostListener('window:resize', ['$event'])
  onResize(event: any) {
    this.widthLayer = event.target.innerWidth;
    this.checkLayerWidth();
  }

  constructor(
    private _translateService?: TranslateService,
    private _firebaseService?: FirebaseService,
    private _globalAction?: GlobalAction,
    private _modalService?: BsModalService,
    private _angularFirestore?: AngularFirestore
  ) {}

  async ngOnInit(): Promise<any> {
    await this.initFormStock();
    await this.initData();
  }

  checkLayerWidth() {
    if (this.widthLayer < 800) {
      this.isTableResponse = true;
      this.scrollbarV = false;
    } else {
      this.isTableResponse = false;
    }
  }

  async initData(): Promise<any> {
    this.stocks = [];

    this.data = [];
    if (this.goods && this.goods.length > 0) {
      this.data = [...this.stocks];
      this.initDataGoods = [...this.goods];
    } else {
      this.data = [];
      this.initDataGoods = [];
    }
    this.value.users = [...this.users];
  }

  callbackSupplier(event: any) {
    console.log('event', event);
    if (event) {
      this.formStock.get('userKey').setValue(event);
    } else {
      this.formStock.get('userKey').setValue('');
    }
  }

  async addInStockxl(template: TemplateRef<any>): Promise<any> {
    console.log('masuk add');
    this.onClose = new Subject();
    console.log(this.value);
    this.value.form.goodsKey = '';
    if (this.goods && this.goods.length > 0) {
      this.value.goods = [...this.goods];
    } else {
      this.value.goods = [];
    }
    this.initForm();
    const navParam: any = {
      initialState: {
        goods: this.goods,
        selectedBusiness: this.selectedBusiness,
        remainingAddStock: this.remainingAddStock,
        _bsModalService: this._bsModalService,
      },
      class: 'modal-md mo dal-dialog-centered',
    };

    this.modalRefTwo = this._modalService.show(template, navParam);
  }

  onChangeInStock(event: any) {}

  initFormStock() {
    if (this.users && this.users.length > 0) {
      this.selectedUsers = this.users[0];
    }

    this.formStock = new FormGroup({
      description: new FormControl(''),
      userKey: new FormControl(this.selectedUsers, [Validators.required]),
      date: new FormControl(new Date(), [Validators.required]),
      totalGoods: new FormControl(0, [Validators.required]),
      totalPrice: new FormControl(0, [Validators.required]),
      account: new FormControl(''),
      balance: new FormControl(0),
    });
  }

  initForm() {
    this.form = new FormGroup({
      goodsKey: new FormControl('', [Validators.required]),
      qty: new FormControl('', [Validators.required]),
      price: new FormControl('', [Validators.required]),
      totalPrice: new FormControl('', [Validators.required]),
    });

    if (this.goods && this.goods.length > 0) {
      this.selectedGoods = this.goods[0];
    }

    this.changeGoods();
    this.changeQTY();
  }

  changeGoods() {
    this.form.get('goodsKey').valueChanges.subscribe(async (value: any) => {
      this.form.get('qty').setValue(0);
      this.form.get('price').setValue(0);
      this.form.get('totalPrice').setValue(0);
    });
  }

  callbackGoods(event: any) {
    console.log('event', event);
    if (event) {
      this.form.get('goodsKey').setValue(event);
    } else {
      this.form.get('goodsKey').setValue(null);
    }

    const findGoods = this.goods.filter((item: any) => {
      if (item.key === event) {
        return item;
      }
    });

    if (findGoods && findGoods.length > 0) {
      this.selectedGoods = findGoods[0];
      this.form.get('price').setValue(this.selectedGoods.sellingPrice);
      this.form
        .get('totalPrice')
        .setValue(this.form.get('qty').value * this.selectedGoods.sellingPrice);
    } else {
      this.form.get('price').setValue(0);
      this.form.get('totalPrice').setValue(0);
    }
  }

  changeQTY() {
    this.form.get('qty').valueChanges.subscribe(async (value: any) => {
      if (value > this.remainingAddStock) {
        this.flag.disabledButton = true;
        setTimeout(() => {
          this.form.get('qty').setValue(this.remainingAddStock);
          value = this.remainingAddStock;
          this.form.get('price').setValue(this.selectedGoods.sellingPrice);
          this.form
            .get('totalPrice')
            .setValue(value * this.selectedGoods.sellingPrice);
          setTimeout(() => {
            this.flag.disabledButton = false;
          }, 1000);
        }, 1000);
      } else {
        this.form.get('price').setValue(this.selectedGoods.sellingPrice);
        this.form
          .get('totalPrice')
          .setValue(value * this.selectedGoods.sellingPrice);
      }
    });
  }

  async closeModal(): Promise<any> {
    this._modalService.hide(1);
  }

  async closeAddInStockModal(): Promise<any> {
    this.modalRefTwo.hide();
  }

  async closeScanWithDevice(): Promise<any> {
    onScan.detachFrom(document);
    this.modalRefThree.hide();
  }

  async closeScanWithCamera(): Promise<any> {
    this.modalRefFour.hide();
  }

  async addStockToWarehouse(
    batch: any,
    stockKey: string,
    stock: GoodsWarehouse
  ): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const firebaseConfig: FirebaseConfigModel = {
        method: 'POST',
        collection: {
          status: true,
          name: 'apps',
          document: {
            status: true,
            name: 'Database',
            collection: {
              status: true,
              name: 'business',
              document: {
                status: true,
                name: this.selectedBusiness.key,
                collection: {
                  status: true,
                  name: 'warehouses',
                  document: {
                    status: true,
                    name: this.selectedWarehouse.key,
                    collection: {
                      status: true,
                      name: 'goodswarehouses',
                      document: {
                        status: false,
                      },
                      orderBy: {
                        object: 'created',
                        order: 'desc',
                      },
                      query: [],
                    },
                  },
                },
              },
            },
          },
        },
      };

      const payload: GoodsWarehouse = {
        stockKey,
        goodsKey: stock.goodsKey,
        businessKey: this.selectedBusiness.key,
        warehouseKey: this.selectedWarehouse.key,
        serialNumber: stock.serialNumber,
        barcode: stock.goodsKey + '-' + stock.serialNumber,
        type: 'In',
        price: stock.price,
      };

      console.log('payload', payload);
      console.log('addStockToWarehouse', firebaseConfig);

      this._firebaseService
        .addBatchSubCollection(null, firebaseConfig, payload, null, null, batch)
        .then(
          (result: any) => {
            console.log('result?', result);
            resolve(result);
          },
          (error: any) => {
            console.log(error);
            this.loading.isLoading = false;
            this._globalAction.showToast({
              title: this._translateService.instant('Info!'),
              message: error,
              type: 'error',
            });
            reject(false);
          }
        );
    });
  }

  async updateWarehouse(batch: any, endSerialNumber: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const where: Query[] = [
        {
          object: 'key',
          condition: '==',
          value: this.selectedWarehouse.key,
        },
      ];

      const firebaseConfig: FirebaseConfigModel = {
        method: 'PUT',
        collection: {
          status: true,
          name: 'apps',
          document: {
            status: true,
            name: 'Database',
            collection: {
              status: true,
              name: 'business',
              document: {
                status: true,
                name: this.selectedBusiness.key,
                collection: {
                  status: true,
                  name: 'warehouses',
                  document: {
                    status: false,
                  },
                  orderBy: {
                    object: 'created',
                    order: 'desc',
                  },
                  query: [],
                },
              },
            },
          },
        },
      };

      const payload: Warehouse = {};
      payload.lastSerialNumber = endSerialNumber;

      this._firebaseService
        .updateBatchCollection(
          null,
          firebaseConfig,
          payload,
          null,
          null,
          null,
          batch,
          where
        )
        .then(
          (result: any) => {
            resolve(result);
          },
          (error: any) => {
            console.log(error);
            this.loading.isLoading = false;
            this._globalAction.showToast({
              title: this._translateService.instant('Info!'),
              message: error,
              type: 'error',
            });
            reject(false);
          }
        );
    });
  }

  async generateAddStockToWarehouse(
    batch: any,
    stockKey: any,
    totalGoods: any
  ): Promise<any> {
    return new Promise(async (resolve, reject) => {
      let serialNumber = Number(this.selectedWarehouse.lastSerialNumber);
      let lengthData = 0;
      console.log('this.data', this.data.length);
      this.data.forEach(async (item: any) => {
        const endNumber =
          Number(totalGoods) - Number(this.selectedWarehouse.lastSerialNumber);
        console.log('endNumber', endNumber);
        console.log('Number(item.qty)', Number(item.qty));

        console.log('totalGoods', totalGoods);
        for (let i = 0; i < Number(item.qty); i++) {
          console.log('loop', i);
          serialNumber += 1;
          const payloadData = {
            goodsKey: item.goodsKey,
            price: item.price,
            serialNumber: this.selectedWarehouse.code + '-' + serialNumber,
          };
          if (batch) {
            batch = await this.addStockToWarehouse(
              batch,
              stockKey,
              payloadData
            );
          }
          lengthData++;

          console.log('final', totalGoods, lengthData);
          if (totalGoods === lengthData) {
            resolve(batch);
          }
        }
      });
    });
  }

  async submit(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      this.loading.isLoading = true;
      const stockKey = this._firebaseService.customKey;
      const totalGoods = this.formStock.get('totalGoods').value;
      const totalPrice = this.formStock.get('totalPrice').value;
      const userKey = this.formStock.get('userKey').value;
      const endSerialNumber =
        Number(this.selectedWarehouse.lastSerialNumber) + Number(totalGoods);

      const key = this._firebaseService.customKey;
      const subKey = this._firebaseService.customKey;

      console.log(stockKey);
      console.log(totalGoods);
      console.log(totalPrice);
      console.log(userKey);
      console.log(endSerialNumber);

      let batch = await this._angularFirestore.firestore.batch();

      if (batch) batch = await this.updateWarehouse(batch, endSerialNumber);
      if (batch) {
        batch = await this.generateAddStockToWarehouse(
          batch,
          stockKey,
          totalGoods
        );
      }
      console.log('datapayload', this.data.length);

      if (batch) {
        const firebaseConfig: FirebaseConfigModel = {
          method: 'POST',
          collection: {
            status: true,
            name: 'apps',
            document: {
              status: true,
              name: 'Database',
              collection: {
                status: true,
                name: 'business',
                document: {
                  status: true,
                  name: this.selectedBusiness.key,
                  collection: {
                    status: true,
                    name: 'warehouses',
                    document: {
                      status: true,
                      name: this.selectedWarehouse.key,
                      collection: {
                        status: true,
                        name: 'stocks',
                        document: {
                          status: false,
                        },
                        orderBy: {
                          object: 'created',
                          order: 'desc',
                        },
                        query: [],
                      },
                    },
                  },
                },
              },
            },
          },
        };

        const payload: Stock = {
          key: stockKey,
          type: 'In',
          businessKey: this.selectedBusiness.key,
          warehouseKey: this.selectedWarehouse.key,
          startSerialNumber:
            this.selectedWarehouse.code +
            '-' +
            this.selectedWarehouse.lastSerialNumber,
          endSerialNumber: this.selectedWarehouse.code + '-' + endSerialNumber,
          description: this.formStock.get('description').value,
          date: this.formStock.get('date').value,
          totalPrice,
          goods: JSON.stringify(this.data),
          totalGoods,
          userKey,
          totalOut: 0,
          totalTransfer: 0,
        };

        this._firebaseService
          .addBatchSubCollection(
            null,
            firebaseConfig,
            payload,
            stockKey,
            null,
            batch
          )
          .then(
            (result: any) => {
              result.commit().then(
                () => {
                  this.loading.isLoading = false;
                  this._globalAction.showToast({
                    title: this._translateService.instant('Info!'),
                    message: this._translateService.instant(
                      'Successfully Added In Stocks!'
                    ),
                    type: 'success',
                  });
                  this.closeModal();
                  return;
                },
                (error: any) => {
                  console.log('error transactions', error);
                  this.loading.isLoading = false;
                  this._globalAction.showToast({
                    title: this._translateService.instant('Info!'),
                    message: error,
                    type: 'error',
                  });
                  return false;
                }
              );
            },
            (error: any) => {
              console.log(error);
              this.loading.isLoading = false;
              this._globalAction.showToast({
                title: this._translateService.instant('Info!'),
                message: error,
                type: 'error',
              });
              reject(false);
            }
          );
      }
    });
  }

  async add(): Promise<any> {
    const payload: any = {
      goodsKey: this.form.get('goodsKey').value,
      qty: Number(this.form.get('qty').value),
      price: this.form.get('price').value,
      totalPrice: this.form.get('totalPrice').value,
    };
    console.log(payload);
    this.stocks.push(payload);

    this.data = [...this.stocks];
    this.data.map((stocks: any) => {
      const goods = this.initDataGoods.filter(
        (goodsItem: any) => goodsItem.key === stocks.goodsKey
      );

      if (goods && goods.length > 0) {
        stocks.goods = goods[0];
      } else {
        stocks.goodsKey = null;
        stocks.goods = {};
        stocks.goods.key = '';
        stocks.goods.name = '-';
      }
    });

    this.remainingAddStock -= Number(payload.qty);
    const totalGoods =
      this.formStock.get('totalGoods').value + Number(payload.qty);
    console.log(this.formStock.get('totalGoods').value);
    this.formStock.get('totalGoods').setValue(totalGoods);

    const totalPrice =
      this.formStock.get('totalPrice').value + payload.totalPrice;
    this.formStock.get('totalPrice').setValue(totalPrice);
    const findIndex = this.goods.findIndex(
      (item: Goods) => item.key === payload.goodsKey
    );
    this.goods.splice(findIndex, 1);
    this.closeAddInStockModal();
  }

  async delete(stock): Promise<any> {
    const findIndex = this.data.findIndex(
      (item: any) => item.key === stock.key
    );
    this.stocks.splice(findIndex, 1);
    this.data = [...this.stocks];
    this.remainingAddStock += Number(stock.qty);

    const totalGoods =
      this.formStock.get('totalGoods').value - Number(stock.qty);
    this.formStock.get('totalGoods').setValue(totalGoods);

    const totalPrice =
      this.formStock.get('totalPrice').value - stock.totalPrice;
    this.formStock.get('totalPrice').setValue(totalPrice);

    this.goods.push(stock.goods);
  }

  onSelect({ selected }: any) {
    this.selected = [...selected];
  }

  onPage(event: Event) {
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      console.log(event);
    }, 100);
  }

  onError(event: any) {}
}
