import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'in-stock',
        loadChildren: () =>
          import('./in-stock/in-stock.module').then((m) => m.InStockModule),
      },
      {
        path: 'out-stock',
        loadChildren: () =>
          import('./out-stock/out-stock.module').then((m) => m.OutStockModule),
      },
    ],
  },
  {
    path: '',
    redirectTo: 'manage/in-stock',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ManageRoutingModule {}
