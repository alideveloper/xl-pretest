import {
  Component,
  ElementRef,
  HostListener,
  OnInit,
  TemplateRef,
  ViewChild,
} from '@angular/core';

import { FirebaseService } from '@firebase/firebase.service';

import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

import { TranslateService } from '@ngx-translate/core';

import { GlobalAction } from '@app/shared/services/global.action';

import { ngxLoadingAnimationTypes } from 'ngx-loading';
import { Stock } from '@app/shared/model/stock.model';
import { Loading } from '@app/shared/model/other/loading.model';
import {
  FirebaseConfigModel,
  Query,
} from '@app/shared/model/other/firebase-config.model';
import {
  ColumnChangesService,
  ColumnMode,
  DatatableComponent,
  DimensionsHelper,
  ScrollbarHelper,
  SelectionType,
} from '@swimlane/ngx-datatable';
import { ColorScheme } from '@app/core';
import { Warehouse } from '@app/shared/model/warehouse.model';
import { Goods } from '@app/shared/model/goods.model';
import { GoodsWarehouse } from '@app/shared/model/goods-warehouse.model';
import { GoodsUnit } from '@app/shared/model/goods-unit.model';
import { GoodsCategory } from '@app/shared/model/goods-category.model';
import { Purchaser } from '@app/shared/model/purchaser.model';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';
import { User } from '@app/shared/model/user.model';

declare let onScan: any;

@Component({
  selector: 'app-add-out-stock',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss'],
  providers: [ScrollbarHelper, DimensionsHelper, ColumnChangesService],
})
export class AddOutStockComponent implements OnInit {
  @ViewChild(DatatableComponent, { static: false })
  tableGoods: DatatableComponent;

  loading: Loading = {
    isLoading: false,
    text:
      this._translateService.instant(
        'Added Out Stock, This will take a few moments'
      ) + '...',
    primaryColour: '',
    secondaryColour: '',
    animation: ngxLoadingAnimationTypes.threeBounce,
    image: 'assets/img/svg/save-person.svg',
  };

  ColorScheme = ColorScheme;
  ColumnMode = ColumnMode;

  messages: any = {
    emptyMessage:
      '<div class="empty-container"><div class="empty-table">No Goods</div></div>',
  };

  timeout: any;

  currentStyles: any = 'table-hover';
  isCard = true;
  isTableView = false;
  isListCollapsed = false;
  responsive = true;
  responsiveExpandable = false;

  headerHeight: any = 'auto';
  rowHeight: any = false;
  footerHeight: any = 'auto';
  scrollbarV = false;
  scrollbarH = true;
  limit = 5;

  selectedActive = false;
  selected: any[] = [];
  SelectionType = SelectionType;
  selectAllRowsOnPage = false;

  selectedMessage: any;

  data: any[];
  initDataGoods: any[];

  modalRefTwo: BsModalRef;
  modalRefThree: BsModalRef;
  modalRefFour: BsModalRef;

  pagination: any = {
    active: true,
    labelTotalMessage: 'Total Data',
  };

  columns: any = [
    {
      width: 50,
      sortable: false,
      canAutoResize: true,
      draggable: false,
      resizeable: false,
      name: 'index',
      header: 'No',
    },
    {
      width: 175,
      sortable: true,
      canAutoResize: true,
      draggable: false,
      resizeable: false,
      name: 'name',
      header: 'Goods',
    },
    {
      width: 175,
      sortable: true,
      canAutoResize: true,
      draggable: false,
      resizeable: false,
      name: 'serialNumber',
      header: 'Serial Number',
    },
    {
      width: 200,
      sortable: true,
      canAutoResize: true,
      draggable: false,
      resizeable: false,
      name: 'price',
      header: 'Price',
    },
    {
      width: 'auto',
      sortable: false,
      canAutoResize: true,
      draggable: false,
      resizeable: false,
      name: 'action',
      header: 'Action',
    },
  ];

  units: GoodsUnit[];
  categories: GoodsCategory[];
  goods: Goods[];
  stocks: Stock[] = [];
  users: Purchaser[];
  selectedBusiness: any;
  selectedWarehouse: Warehouse;

  schemaPurchaserSelect: any = {
    component: 'select',
    position: 'body',
    label: {
      active: true,
      binding: false,
      text: 'Select Purchaser',
      type: 'text',
      state: {
        hide: false,
        disabled: false,
        hideCondition: [],
        disabledCondition: [],
      },
    },
    labelSubtitle: {
      active: false,
    },
    type: 'text',
    placeholder: 'Select Purchaser',
    parentBinding: 'form',
    bindingObject: 'userKey',
    primaryKey: 'key',
    bindingData: 'users',
    labelForId: 'name',
    bindLabel: 'name',
    bindValue: 'key',
    searchable: true,
    search: true,
    closeOnSelect: true,
    defaultValue: true,
    customSearch: true,
    hideSelected: true,
    clearable: false,
    bindingSearch: 'users',
    required: false,
    formText: {
      valid: {
        active: false,
        text: 'Data telah Sesuai!',
      },
      invalid: {
        active: false,
        text: 'Data belum Sesuai!',
      },
    },
  };

  value: any = {
    form: {},
  };

  maxAddStock: any = 200;
  remainingAddStock: any = 200;

  _bsModalService: any;

  selectedGoods: Goods;
  form: FormGroup;
  formStock: FormGroup;

  schemaWarehouseGoodsSelect: any = {
    component: 'select',
    position: 'body',
    label: {
      active: true,
      binding: false,
      text: 'Goods in Warehouse',
      type: 'text',
      state: {
        hide: false,
        disabled: false,
        hideCondition: [],
        disabledCondition: [],
      },
    },
    labelSubtitle: {
      active: false,
    },
    selectedAll: false,
    type: 'text',
    placeholder: 'Select Goods in Warehouse',
    parentBinding: 'form',
    multiple: true,
    bindingObject: 'warehousegoodsKey',
    primaryKey: 'key',
    bindingData: 'warehousegoods',
    labelForId: 'name',
    styleClass: 'select-warehouse-goods',
    bindLabel: 'name',
    bindValue: 'key',
    searchable: true,
    search: true,
    closeOnSelect: false,
    defaultValue: false,
    clearable: false,
    customSearch: true,
    customTemplate: {
      active: true,
      type: 'warehousegoods',
    },
    bindingSearch: 'warehousegoods',
    required: true,
    formText: {
      valid: {
        active: true,
        text: 'Data telah Sesuai!',
      },
      invalid: {
        active: true,
        text: 'Data tidak Sesuai!',
      },
    },
  };

  public onClose: Subject<boolean>;

  user: User;
  transaction: Account;
  warehousegoods: GoodsWarehouse[];
  initDataWarehousegoods: GoodsWarehouse[];
  inStocks: Stock[];

  flag: any = {
    disabledButton: false,
    loadCamera: false,
  };

  barcode: any;

  widthLayer: any = window.innerWidth;
  isTableResponse: boolean;

  @HostListener('window:resize', ['$event'])
  onResize(event: any) {
    this.widthLayer = event.target.innerWidth;
    this.checkLayerWidth();
  }

  constructor(
    private _translateService?: TranslateService,
    private _firebaseService?: FirebaseService,
    private _globalAction?: GlobalAction,
    private _modalService?: BsModalService,
    private _angularFirestore?: AngularFirestore
  ) {}

  async ngOnInit(): Promise<any> {
    await this.initFormStock();
    await this.initData();
  }

  getOnScanSettings() {
    let sFormatedErrorString = 'Scanner Settings: \n';
    const aJSONArray = JSON.stringify(onScan.getOptions(document)).split(',');
    for (let prop = 0; prop < aJSONArray.length - 1; prop++) {
      if (aJSONArray[prop + 1][0] === '"') {
        sFormatedErrorString += aJSONArray[prop] + ',' + '\n';
      } else {
        sFormatedErrorString += aJSONArray[prop] + ',';
      }
    }
    sFormatedErrorString += aJSONArray[aJSONArray.length - 1];

    console.log('sFormatedErrorString', sFormatedErrorString);
  }

  checkLayerWidth() {
    if (this.widthLayer < 800) {
      this.isTableResponse = true;
      this.scrollbarV = false;
    } else {
      this.isTableResponse = false;
    }
  }

  async initData(): Promise<any> {
    this.stocks = [];

    this.initDataWarehousegoods = Object.assign([], this.warehousegoods);

    this.data = [];
    if (this.goods && this.goods.length > 0) {
      this.data = [...this.stocks];
      this.initDataGoods = [...this.goods];
    } else {
      this.data = [];
      this.initDataGoods = [];
    }
    this.value.users = [...this.users];
  }

  callbackPurchaser(event: any) {
    console.log('event', event);
    if (event) {
      this.formStock.get('userKey').setValue(event);
    } else {
      this.formStock.get('userKey').setValue('');
    }
  }

  async addOutStockxl(template: TemplateRef<any>): Promise<any> {
    console.log('masuk add');
    this.onClose = new Subject();
    console.log(this.value);
    this.value.form.goodsKey = '';
    if (this.warehousegoods && this.warehousegoods.length > 0) {
      this.value.warehousegoods = [...this.warehousegoods];
    } else {
      this.value.warehousegoods = [];
    }
    this.schemaWarehouseGoodsSelect.selectedBusiness = this.selectedBusiness;
    this.initForm();
    const navParam: any = {
      initialState: {
        goods: this.goods,
        selectedBusiness: this.selectedBusiness,
        remainingAddStock: this.remainingAddStock,
        _bsModalService: this._bsModalService,
        warehousegoods: this.warehousegoods,
        user: this.user,
      },
      class: 'modal-md modal-dialog-centered',
    };

    this.modalRefTwo = this._modalService.show(template, navParam);
  }

  onChangeOutStock(event: any) {}

  initFormStock() {
    if (this.goods && this.goods.length > 0) {
      this.selectedGoods = this.goods[0];
    }

    this.formStock = new FormGroup({
      description: new FormControl(''),
      userKey: new FormControl(this.selectedGoods, [Validators.required]),
      date: new FormControl(new Date(), [Validators.required]),
      totalGoods: new FormControl(0, [Validators.required]),
      totalPrice: new FormControl(0, [Validators.required]),
      account: new FormControl(''),
      balance: new FormControl(0),
    });
  }

  initForm() {
    this.value.form.warehousegoodsKey = [];
    this.form = new FormGroup({
      stockKey: new FormControl([], [Validators.required]),
    });
  }

  callbackWarehouseGoods(event: any) {
    console.log('event', event);
    if (event) {
      this.form.get('stockKey').setValue(event);
    } else {
      this.form.get('stockKey').setValue(null);
    }
  }

  async closeModal(): Promise<any> {
    this._modalService.hide(1);
  }

  async closeAddOutStockModal(): Promise<any> {
    this.modalRefTwo.hide();
  }

  async updateStock(batch: any, stock: Stock, total: number): Promise<any> {
    console.log('updateStock', stock, total);
    return new Promise(async (resolve, reject) => {
      const where: Query[] = [
        {
          object: 'key',
          condition: '==',
          value: stock.key,
        },
      ];
      const firebaseConfig: FirebaseConfigModel = {
        method: 'PUT',
        collection: {
          status: true,
          name: 'apps',
          document: {
            status: true,
            name: 'Database',
            collection: {
              status: true,
              name: 'business',
              document: {
                status: true,
                name: this.selectedBusiness.key,
                collection: {
                  status: true,
                  name: 'warehouses',
                  document: {
                    status: true,
                    name: this.selectedWarehouse.key,
                    collection: {
                      status: true,
                      name: 'stocks',
                      document: {
                        status: false,
                      },
                      orderBy: {
                        object: 'created',
                        order: 'desc',
                      },
                      query: [...where],
                    },
                  },
                },
              },
            },
          },
        },
      };

      const payload: Stock = {};
      payload.key = stock.key;
      payload.totalOut = stock.totalOut
        ? Number(stock.totalOut) + Number(total)
        : Number(total);

      console.log('payload', payload);
      console.log('updateStockToWarehouse', firebaseConfig);

      this._firebaseService
        .updatedBatchSubCollection(
          null,
          firebaseConfig,
          payload,
          null,
          null,
          null,
          batch,
          where
        )
        .then(
          (result: any) => {
            console.log('result?', result);
            resolve(result);
          },
          (error: any) => {
            console.log(error);
            this.loading.isLoading = false;
            this._globalAction.showToast({
              title: this._translateService.instant('Info!'),
              message: error,
              type: 'error',
            });
            reject(false);
          }
        );
    });
  }

  async updateStockToWarehouse(
    batch: any,
    stockKey: string,
    stock: GoodsWarehouse
  ): Promise<any> {
    return new Promise(async (resolve, reject) => {
      console.log('updateStockToWarehousekey', stock.key);
      const where: Query[] = [
        {
          object: 'key',
          condition: '==',
          value: stock.key,
        },
      ];
      const firebaseConfig: FirebaseConfigModel = {
        method: 'PUT',
        collection: {
          status: true,
          name: 'apps',
          document: {
            status: true,
            name: 'Database',
            collection: {
              status: true,
              name: 'business',
              document: {
                status: true,
                name: this.selectedBusiness.key,
                collection: {
                  status: true,
                  name: 'warehouses',
                  document: {
                    status: true,
                    name: this.selectedWarehouse.key,
                    collection: {
                      status: true,
                      name: 'goodswarehouses',
                      document: {
                        status: false,
                      },
                      orderBy: {
                        object: 'created',
                        order: 'desc',
                      },
                      query: [...where],
                    },
                  },
                },
              },
            },
          },
        },
      };

      const payload: GoodsWarehouse = {};
      payload.stockKeyOut = stockKey;
      payload.type = 'Out';
      payload.dateOut = this._firebaseService.timestampPost;

      console.log('payload', payload);
      console.log('updateStockToWarehouse', firebaseConfig);

      this._firebaseService
        .updatedBatchSubCollection(
          null,
          firebaseConfig,
          payload,
          null,
          null,
          null,
          batch,
          where
        )
        .then(
          (result: any) => {
            console.log('result?', result);
            resolve(result);
          },
          (error: any) => {
            console.log(error);
            this.loading.isLoading = false;
            this._globalAction.showToast({
              title: this._translateService.instant('Info!'),
              message: error,
              type: 'error',
            });
            reject(false);
          }
        );
    });
  }

  async updateWarehouse(batch: any, endSerialNumber: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const where: Query[] = [
        {
          object: 'key',
          condition: '==',
          value: this.selectedWarehouse.key,
        },
      ];

      const firebaseConfig: FirebaseConfigModel = {
        method: 'PUT',
        collection: {
          status: true,
          name: 'apps',
          document: {
            status: true,
            name: 'Database',
            collection: {
              status: true,
              name: 'business',
              document: {
                status: true,
                name: this.selectedBusiness.key,
                collection: {
                  status: true,
                  name: 'warehouses',
                  document: {
                    status: false,
                  },
                  orderBy: {
                    object: 'created',
                    order: 'desc',
                  },
                  query: [],
                },
              },
            },
          },
        },
      };

      const payload: Warehouse = {};
      payload.lastSerialNumber = endSerialNumber;

      this._firebaseService
        .updateBatchCollection(
          null,
          firebaseConfig,
          payload,
          null,
          null,
          null,
          batch,
          where
        )
        .then(
          (result: any) => {
            resolve(result);
          },
          (error: any) => {
            console.log(error);
            this.loading.isLoading = false;
            this._globalAction.showToast({
              title: this._translateService.instant('Info!'),
              message: error,
              type: 'error',
            });
            reject(false);
          }
        );
    });
  }

  async generateUpdateStockToWarehouse(
    batch: any,
    stockKey: any,
    totalGoods: any
  ): Promise<any> {
    return new Promise(async (resolve, reject) => {
      let lengthData = 0;
      console.log('this.data', this.data.length);
      const stocks: any = [];
      this.data.forEach(async (item: any) => {
        console.log('errpritem', item);
        console.log('totalGoods', totalGoods);
        console.log('updateStockToWarehouse', stockKey);
        console.log('updateStockToWarehouse', item);
        if (batch) {
          batch = await this.updateStockToWarehouse(batch, stockKey, item);
        }
        lengthData++;

        if (stocks && stocks.length === 0) {
          stocks.push({
            key: item.stockKey,
            total: 1,
          });
        } else {
          const findIndex = stocks.findIndex((itemFind: any) => {
            if (itemFind.key === item.stockKey) {
              return itemFind;
            }
          });

          if (findIndex !== -1) {
            stocks[findIndex].total += 1;
          } else {
            stocks.push({
              key: item.stockKey,
              total: 1,
            });
          }
        }

        console.log('final', totalGoods, lengthData);
        if (totalGoods === lengthData) {
          let lengthDataSub = 0;
          stocks.forEach(async (itemStock: any) => {
            console.log('stocks', itemStock);
            const stock = this.inStocks.filter(
              (itemInStock: any) => itemInStock.key === itemStock.key
            );
            console.log('stock', stock);
            console.log('inStocks', this.inStocks);
            if (batch) {
              batch = await this.updateStock(batch, stock[0], itemStock.total);
            }
            lengthDataSub++;

            console.log('sub', stocks.length, lengthDataSub);
            if (stocks.length === lengthDataSub) {
              resolve(batch);
            }
          });
        }
      });
    });
  }

  async submit(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      this.loading.isLoading = true;
      const stockKey = this._firebaseService.customKey;
      const totalGoods = this.formStock.get('totalGoods').value;
      const totalPrice = this.formStock.get('totalPrice').value;
      const userKey = this.formStock.get('userKey').value;

      const key = this._firebaseService.customKey;
      const subKey = this._firebaseService.customKey;

      console.log(stockKey);
      console.log(totalGoods);
      console.log(totalPrice);
      console.log(userKey);

      let batch = await this._angularFirestore.firestore.batch();

      if (batch) {
        batch = await this.generateUpdateStockToWarehouse(
          batch,
          stockKey,
          totalGoods
        );
      }
      console.log('datapayload', this.data.length);

      if (batch) {
        const firebaseConfig: FirebaseConfigModel = {
          method: 'POST',
          collection: {
            status: true,
            name: 'apps',
            document: {
              status: true,
              name: 'Database',
              collection: {
                status: true,
                name: 'business',
                document: {
                  status: true,
                  name: this.selectedBusiness.key,
                  collection: {
                    status: true,
                    name: 'warehouses',
                    document: {
                      status: true,
                      name: this.selectedWarehouse.key,
                      collection: {
                        status: true,
                        name: 'stocks',
                        document: {
                          status: false,
                        },
                        orderBy: {
                          object: 'created',
                          order: 'desc',
                        },
                        query: [],
                      },
                    },
                  },
                },
              },
            },
          },
        };

        const payload: Stock = {
          key: stockKey,
          type: 'Out',
          businessKey: this.selectedBusiness.key,
          warehouseKey: this.selectedWarehouse.key,
          description: this.formStock.get('description').value,
          date: this.formStock.get('date').value,
          totalPrice,
          goods: JSON.stringify(this.data),
          totalGoods,
          userKey,
        };

        this._firebaseService
          .addBatchSubCollection(
            null,
            firebaseConfig,
            payload,
            stockKey,
            null,
            batch
          )
          .then(
            (result: any) => {
              result.commit().then(
                () => {
                  this.loading.isLoading = false;
                  this._globalAction.showToast({
                    title: this._translateService.instant('Info!'),
                    message: this._translateService.instant(
                      'Successfully Added Out Stocks!'
                    ),
                    type: 'success',
                  });
                  this.closeModal();
                  return;
                },
                (error: any) => {
                  console.log('error transactions', error);
                  this.loading.isLoading = false;
                  this._globalAction.showToast({
                    title: this._translateService.instant('Info!'),
                    message: error,
                    type: 'error',
                  });
                  return false;
                }
              );
            },
            (error: any) => {
              console.log(error);
              this.loading.isLoading = false;
              this._globalAction.showToast({
                title: this._translateService.instant('Info!'),
                message: error,
                type: 'error',
              });
              reject(false);
            }
          );
      }
    });
  }

  async add(): Promise<any> {
    const payload: any = {
      warehousegoods: this.form.get('stockKey').value,
    };

    payload.warehousegoods.forEach((itemGoods: GoodsWarehouse) => {
      console.log(itemGoods);
      const warehousegoods = this.initDataWarehousegoods.filter(
        (warehousegoodsItem: any) => warehousegoodsItem.key === itemGoods
      );
      console.log(warehousegoods);

      const findIndex = this.warehousegoods.findIndex(
        (warehousegoodsItem: any) => warehousegoodsItem.key === itemGoods
      );

      if (findIndex !== -1) {
        this.warehousegoods.splice(findIndex, 1);
      }

      if (warehousegoods && warehousegoods.length > 0) {
        console.log('MASUK ADD', warehousegoods);
        this.stocks.push(warehousegoods[0]);
        this.remainingAddStock -= 1;
        const totalGoods = this.formStock.get('totalGoods').value + 1;
        this.formStock.get('totalGoods').setValue(totalGoods);
        const totalPrice =
          Number(this.formStock.get('totalPrice').value) +
          Number(warehousegoods[0].price);
        console.log('warehousegoods[0].price', totalPrice);
        this.formStock
          .get('totalPrice')
          .setValue(Number(totalPrice.toFixed(2)));
      }
    });
    this.data = [...this.stocks];
    this.closeAddOutStockModal();
  }

  async delete(stock): Promise<any> {
    const findIndex = this.data.findIndex(
      (item: any) => item.key === stock.key
    );
    this.stocks.splice(findIndex, 1);
    this.data = [...this.stocks];
    this.remainingAddStock += 1;

    const totalGoods = this.formStock.get('totalGoods').value - 1;
    this.formStock.get('totalGoods').setValue(totalGoods);

    const totalPrice =
      Number(this.formStock.get('totalPrice').value) - Number(stock.price);
    console.log('totalPrice', totalPrice.toFixed(2));
    this.formStock.get('totalPrice').setValue(Number(totalPrice.toFixed(2)));

    this.warehousegoods.push(stock);
  }

  onSelect({ selected }: any) {
    this.selected = [...selected];
  }

  onPage(event: Event) {
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      console.log(event);
    }, 100);
  }

  onError(event: any) {}
}
