import { Component, OnInit } from '@angular/core';

import { FirebaseService } from '@firebase/firebase.service';

import { BsModalRef } from 'ngx-bootstrap/modal';

import { TranslateService } from '@ngx-translate/core';

import { GlobalAction } from '@app/shared/services/global.action';

import { ngxLoadingAnimationTypes } from 'ngx-loading';
import { environment } from '@environments/environment';
import { Loading } from '@app/shared/model/other/loading.model';
import { Stock } from '@app/shared/model/stock.model';
import {
  FirebaseConfigModel,
  Query,
} from '@app/shared/model/other/firebase-config.model';
import { AngularFirestore } from '@angular/fire/firestore';
import { User } from '@app/shared/model/user.model';
import { GoodsWarehouse } from '@app/shared/model/goods-warehouse.model';

@Component({
  selector: 'app-delete-out-stock',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.scss'],
})
export class DeleteOutStockComponent implements OnInit {
  loading: Loading = {
    isLoading: false,
    text:
      this._translateService.instant(
        'Deleted Out Stock, This will take a few moments'
      ) + '...',
    primaryColour: '',
    secondaryColour: '',
    animation: ngxLoadingAnimationTypes.threeBounce,
    image: 'assets/img/svg/save-person.svg',
  };

  selectedBusiness: any;
  selectedWarehouse: any;

  key: any;
  inStocks: Stock[];

  form: Stock;

  user: User;

  constructor(
    private _bsModalRef: BsModalRef,
    private _translateService?: TranslateService,
    private _firebaseService?: FirebaseService,
    private _globalAction?: GlobalAction,
    private _angularFirestore?: AngularFirestore
  ) {}

  async ngOnInit(): Promise<any> {
    console.log(this.form, 'form');
    if (this.form) {
      this.key = this.form.key;
      console.log(JSON.parse(this.form.goods));
    }
  }

  async close(): Promise<any> {
    this._bsModalRef.hide();
  }

  async updateStockToWarehouse(
    batch: any,
    stock: GoodsWarehouse
  ): Promise<any> {
    return new Promise(async (resolve, reject) => {
      console.log('updateStockToWarehousekey', stock);
      const where: Query[] = [
        {
          object: 'stockKeyOut',
          condition: '==',
          value: stock.key,
        },
      ];
      const firebaseConfig: FirebaseConfigModel = {
        method: 'PUT',
        collection: {
          status: true,
          name: 'apps',
          document: {
            status: true,
            name: 'Database',
            collection: {
              status: true,
              name: 'business',
              document: {
                status: true,
                name: this.selectedBusiness.key,
                collection: {
                  status: true,
                  name: 'warehouses',
                  document: {
                    status: true,
                    name: this.selectedWarehouse.key,
                    collection: {
                      status: true,
                      name: 'goodswarehouses',
                      document: {
                        status: false,
                      },
                      orderBy: {
                        object: 'created',
                        order: 'desc',
                      },
                      query: [...where],
                    },
                  },
                },
              },
            },
          },
        },
      };

      const payload: GoodsWarehouse = {};
      payload.stockKeyOut = null;
      payload.type = 'In';
      payload.dateOut = null;

      console.log('payload', payload);
      console.log('updateStockToWarehouse', firebaseConfig);

      this._firebaseService
        .updatedBatchSubCollection(
          null,
          firebaseConfig,
          payload,
          null,
          null,
          null,
          batch,
          where
        )
        .then(
          (result: any) => {
            console.log('result?', result);
            resolve(result);
          },
          (error: any) => {
            console.log(error);
            this.loading.isLoading = false;
            this._globalAction.showToast({
              title: this._translateService.instant('Info!'),
              message: error,
              type: 'error',
            });
            reject(false);
          }
        );
    });
  }

  async updateStock(batch: any, stock: Stock, total: number): Promise<any> {
    console.log('updateStock', stock, total);
    return new Promise(async (resolve, reject) => {
      const where: Query[] = [
        {
          object: 'key',
          condition: '==',
          value: stock.key,
        },
      ];
      const firebaseConfig: FirebaseConfigModel = {
        method: 'PUT',
        collection: {
          status: true,
          name: 'apps',
          document: {
            status: true,
            name: 'Database',
            collection: {
              status: true,
              name: 'business',
              document: {
                status: true,
                name: this.selectedBusiness.key,
                collection: {
                  status: true,
                  name: 'warehouses',
                  document: {
                    status: true,
                    name: this.selectedWarehouse.key,
                    collection: {
                      status: true,
                      name: 'stocks',
                      document: {
                        status: false,
                      },
                      orderBy: {
                        object: 'created',
                        order: 'desc',
                      },
                      query: [...where],
                    },
                  },
                },
              },
            },
          },
        },
      };

      const payload: Stock = {};
      payload.key = stock.key;
      payload.totalOut = Number(stock.totalOut) - Number(total);

      console.log('payload', payload);
      console.log('updateStockToWarehouse', firebaseConfig);

      this._firebaseService
        .updatedBatchSubCollection(
          null,
          firebaseConfig,
          payload,
          null,
          null,
          null,
          batch,
          where
        )
        .then(
          (result: any) => {
            console.log('result?', result);
            resolve(result);
          },
          (error: any) => {
            console.log(error);
            this.loading.isLoading = false;
            this._globalAction.showToast({
              title: this._translateService.instant('Info!'),
              message: error,
              type: 'error',
            });
            reject(false);
          }
        );
    });
  }

  async generateUpdateStockToWarehouse(batch: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const stocks = JSON.parse(this.form.goods);
      const stocksFinal: any = [];
      stocks.forEach(async (item: any) => {
        if (stocksFinal && stocksFinal.length === 0) {
          stocksFinal.push({
            key: item.stockKey,
            total: 1,
          });
        } else {
          const findIndex = stocksFinal.findIndex((itemFind: any) => {
            if (itemFind.key === item.stockKey) {
              return itemFind;
            }
          });

          if (findIndex !== -1) {
            stocksFinal[findIndex].total += 1;
          } else {
            stocksFinal.push({
              key: item.stockKey,
              total: 1,
            });
          }
        }
      });

      console.log('stocksFinal', stocksFinal);
      if (stocksFinal && stocksFinal.length > 0) {
        let lengthDataSub = 0;
        stocksFinal.forEach(async (itemStock: any) => {
          console.log('stocks', itemStock);
          const stock = this.inStocks.filter(
            (itemInStock: any) => itemInStock.key === itemStock.key
          );
          if (batch) {
            batch = await this.updateStock(batch, stock[0], itemStock.total);
          }
          lengthDataSub++;

          console.log('sub', stocksFinal.length, lengthDataSub);
          if (stocksFinal.length === lengthDataSub) {
            resolve(batch);
          }
        });
      }
    });
  }

  async submit(): Promise<any> {
    this.loading.isLoading = true;
    const where: Query[] = [
      {
        object: 'key',
        condition: '==',
        value: this.form.key,
      },
    ];

    let batch = await this._angularFirestore.firestore.batch();

    if (batch) batch = await this.updateStockToWarehouse(batch, this.form);

    if (batch) batch = await this.generateUpdateStockToWarehouse(batch);

    const firebaseConfig: FirebaseConfigModel = {
      method: 'DELETE',
      collection: {
        status: true,
        name: 'apps',
        document: {
          status: true,
          name: 'Database',
          collection: {
            status: true,
            name: 'business',
            document: {
              status: true,
              name: this.selectedBusiness.key,
              collection: {
                status: true,
                name: 'warehouses',
                document: {
                  status: true,
                  name: this.selectedWarehouse.key,
                  collection: {
                    status: true,
                    name: 'stocks',
                    document: {
                      status: false,
                    },
                    orderBy: {
                      object: 'created',
                      order: 'desc',
                    },
                    query: [...where],
                  },
                },
              },
            },
          },
        },
      },
    };

    const payload: Stock = Object.assign({}, this.form);
    this._firebaseService
      .deleteBatchSubCollection(null, firebaseConfig, payload, batch, true)
      .then(
        (result: any) => {
          result.commit().then(
            () => {
              this.loading.isLoading = false;
              this._globalAction.showToast({
                title: this._translateService.instant('Info!'),
                message: this._translateService.instant(
                  'Successfully deleted Out Stock!'
                ),
                type: 'success',
              });
              this._bsModalRef.hide();
              return;
            },
            (error: any) => {
              console.log('error transactions 1', error);
              this.loading.isLoading = false;
              this._globalAction.showToast({
                title: this._translateService.instant('Info!'),
                message: error,
                type: 'error',
              });
              return false;
            }
          );
        },
        (error: any) => {
          console.log(error);
          this.loading.isLoading = false;
          this._globalAction.showToast({
            title: this._translateService.instant('Info!'),
            message: error,
            type: 'error',
          });
        }
      );
  }
}
