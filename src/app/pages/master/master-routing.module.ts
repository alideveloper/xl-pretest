import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'warehouse',
        loadChildren: () =>
          import('./warehouse/warehouse.module').then((m) => m.WarehouseModule),
      },
      {
        path: 'category',
        loadChildren: () =>
          import('./category/category.module').then((m) => m.CategoryModule),
      },
      {
        path: 'goods',
        loadChildren: () =>
          import('./goods/goods.module').then((m) => m.GoodsModule),
      },
      {
        path: 'warehouse-goods',
        loadChildren: () =>
          import('./warehouse-goods/warehouse-goods.module').then(
            (m) => m.WarehouseGoodsModule
          ),
      },
      {
        path: 'supplier',
        loadChildren: () =>
          import('./supplier/supplier.module').then((m) => m.SupplierModule),
      },
      {
        path: 'purchaser',
        loadChildren: () =>
          import('./purchaser/purchaser.module').then((m) => m.PurchaserModule),
      },
      {
        path: 'unit',
        loadChildren: () =>
          import('./unit/unit.module').then((m) => m.CategoryModule),
      },
    ],
  },
  {
    path: '',
    redirectTo: 'master/warehouse',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MasterRoutingModule {}
