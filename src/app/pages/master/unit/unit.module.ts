import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UnitRoutingModule } from './unit-routing.module';

import { SharedModule } from '@app/shared';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { UtilsModule } from '@app/components/utils/utils.module';
import { NavigationsModule } from '@components/navigations/navigations.module';
import { AlertsModule } from '@app/components/alerts/alerts.module';
import { FormControlsModule } from '@app/components/form-controls/form-controls.module';
import { IconsModule } from '@app/components/icons/icons.module';

import { UnitComponent } from './unit.component';
import { AddUnitComponent } from './add/add.component';
import { EditUnitComponent } from './edit/edit.component';
import { DeleteUnitComponent } from './delete/delete.component';

import { FilterByPipe } from 'ngx-pipes';

const modules = [
  UtilsModule,
  CommonModule,
  NavigationsModule,
  NgxDatatableModule,
  IconsModule,
  AlertsModule,
  FormControlsModule,
  SharedModule,
];

const declarations = [AddUnitComponent, EditUnitComponent, DeleteUnitComponent];

@NgModule({
  declarations: [UnitComponent, ...declarations],
  imports: [UnitRoutingModule, ...modules],
  entryComponents: [...declarations],
  providers: [FilterByPipe],
})
export class CategoryModule {}
