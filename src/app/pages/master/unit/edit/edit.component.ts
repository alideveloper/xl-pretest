import { Component, OnInit } from '@angular/core';

import { FirebaseService } from '@firebase/firebase.service';

import { BsModalRef } from 'ngx-bootstrap/modal';

import { TranslateService } from '@ngx-translate/core';

import { GlobalAction } from '@app/shared/services/global.action';

import { ngxLoadingAnimationTypes } from 'ngx-loading';
import { Loading } from '@app/shared/model/other/loading.model';
import { FirebaseConfigModel } from '@app/shared/model/other/firebase-config.model';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { GoodsUnit } from '@app/shared/model/goods-unit.model';

@Component({
  selector: 'app-edit-unit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
})
export class EditUnitComponent implements OnInit {
  loading: Loading = {
    isLoading: false,
    text: this._translateService.instant('Updated Goods Unit') + '...',
    primaryColour: '',
    secondaryColour: '',
    animation: ngxLoadingAnimationTypes.threeBounce,
    image: 'assets/img/svg/save-person.svg',
  };

  selectedBusiness: any;

  formData: GoodsUnit;

  form: FormGroup;

  value: any = {
    form: {},
  };

  categories: any;

  constructor(
    private _bsModalRef: BsModalRef,
    private _translateService?: TranslateService,
    private _firebaseService?: FirebaseService,
    private _globalAction?: GlobalAction
  ) {}

  onChangeUnit(event: any) {}

  async ngOnInit(): Promise<any> {
    console.log(this.formData, 'form');
    if (this.formData) {
      this.initForm();
    }
  }

  initForm() {
    this.form = new FormGroup({
      name: new FormControl(this.formData.name, [
        Validators.required,
        Validators.minLength(2),
      ]),
    });

    this.value.form = Object.assign({}, this.formData);
  }

  async close(): Promise<any> {
    this._bsModalRef.hide();
  }

  async submit(): Promise<any> {
    const firebaseConfig: FirebaseConfigModel = {
      method: 'PUT',
      collection: {
        status: true,
        name: 'apps',
        document: {
          status: true,
          name: 'Database',
          collection: {
            status: true,
            name: 'business',
            document: {
              status: true,
              name: this.selectedBusiness.key,
              collection: {
                status: true,
                name: 'units',
                document: {
                  status: false,
                },
                orderBy: {
                  object: 'created',
                  order: 'desc',
                },
                query: [],
              },
            },
          },
        },
      },
    };

    const payload: GoodsUnit = {};
    payload.key = this.formData.key;
    payload.name = this.form.get('name').value;

    this.loading.isLoading = true;
    this._firebaseService.updateCollection(null, firebaseConfig, payload).then(
      (result: any) => {
        this.loading.isLoading = false;
        this._globalAction.showToast({
          title: this._translateService.instant('Info!'),
          message: this._translateService.instant('Successfully updated Unit!'),
          type: 'success',
        });
        this._bsModalRef.hide();
      },
      (error: any) => {
        console.log(error);
        this.loading.isLoading = false;
        this._globalAction.showToast({
          title: this._translateService.instant('Info!'),
          message: error,
          type: 'error',
        });
      }
    );
  }
}
