import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UnitComponent } from './unit.component';

import { extract } from '@app/core';

const routes: Routes = [
  {
    path: '',
    component: UnitComponent,
    data: { title: extract('Goods Unit') },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UnitRoutingModule {}
