import { Component, OnInit } from '@angular/core';

import { FirebaseService } from '@firebase/firebase.service';

import { BsModalRef } from 'ngx-bootstrap/modal';

import { TranslateService } from '@ngx-translate/core';

import { GlobalAction } from '@app/shared/services/global.action';

import { ngxLoadingAnimationTypes } from 'ngx-loading';
import { Purchaser } from '@app/shared/model/purchaser.model';
import { Loading } from '@app/shared/model/other/loading.model';
import { FirebaseConfigModel } from '@app/shared/model/other/firebase-config.model';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-purchaser',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss'],
})
export class AddPurchaserComponent implements OnInit {
  loading: Loading = {
    isLoading: false,
    text: this._translateService.instant('Added Purchaser') + '...',
    primaryColour: '',
    secondaryColour: '',
    animation: ngxLoadingAnimationTypes.threeBounce,
    image: 'assets/img/svg/save-person.svg',
  };

  purchasers: Purchaser[];
  selectedBusiness: any;

  form: FormGroup;

  value: any = {
    form: {},
  };

  schemaMaps: any = {
    component: 'maps',
    position: 'body',
    style: {},
    styleClass: 'maps-md',
    label: {
      active: true,
      binding: false,
      style: {},
      styleClass: '',
      text: 'Select Maps',
      type: 'text',
      state: {
        hide: false,
        disabled: false,
        hideCondition: [],
        disabledCondition: [],
      },
    },
    labelSubtitle: {
      active: false,
    },
    search: {
      active: true,
      placeholder: 'Search Address',
    },
    address: {
      active: true,
      bindingObject: 'address',
    },
    zoom: 13,
    minZoom: 5,
    maxZoom: 20,
    parentBinding: 'form',
    latitude: 'latitude',
    longitude: 'longitude',
    callbackMaps: {
      latitude: 'latitude',
      longitude: 'longitude',
      accuracy: 'accuracy',
      altitude: 'altitude',
      altitudeAccuracy: 'altitudeAccuracy',
      heading: 'heading',
      speed: 'speed',
    },
  };

  constructor(
    private _bsModalRef: BsModalRef,
    private _translateService?: TranslateService,
    private _firebaseService?: FirebaseService,
    private _globalAction?: GlobalAction
  ) {}

  async ngOnInit(): Promise<any> {
    this.initForm();
  }

  initForm() {
    this.form = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.minLength(3)]),
      phoneNumber: new FormControl('', [
        Validators.required,
        Validators.minLength(11),
      ]),
      address: new FormControl('', [
        Validators.required,
        Validators.minLength(3),
      ]),
      addressDetail: new FormControl('', [Validators.required]),
      addressed: new FormControl('', [Validators.required]),
    });
  }

  callbackMaps(event: any) {
    console.log(event);
    this.form.get('address').setValue(event.address);
    this.form.get('addressed').setValue(event);
  }

  onChangePurchaser(event: any) {}

  async close(): Promise<any> {
    this._bsModalRef.hide();
  }

  async submit(): Promise<any> {
    const firebaseConfig: FirebaseConfigModel = {
      method: 'POST',
      collection: {
        status: true,
        name: 'apps',
        document: {
          status: true,
          name: 'Database',
          collection: {
            status: true,
            name: 'business',
            document: {
              status: true,
              name: this.selectedBusiness.key,
              collection: {
                status: true,
                name: 'purchasers',
                document: {
                  status: false,
                },
                orderBy: {
                  object: 'created',
                  order: 'desc',
                },
                query: [],
              },
            },
          },
        },
      },
    };

    const businessKey = this.selectedBusiness.key;

    const payload: Purchaser = {};
    payload.name = this.form.get('name').value;
    payload.phoneNumber = this.form.get('phoneNumber').value;
    payload.address = this.form.get('address').value;
    payload.addressDetail = this.form.get('addressDetail').value;
    payload.businessKey = businessKey;
    if (this.form.get('addressed').value) {
      payload.accuracy = this.form.get('addressed').value.accuracy;
      payload.altitude = this.form.get('addressed').value.altitude;
      payload.altitudeAccuracy = this.form.get(
        'addressed'
      ).value.altitudeAccuracy;
      payload.latitude = this.form.get('addressed').value.latitude;
      payload.longitude = this.form.get('addressed').value.longitude;
      payload.speed = this.form.get('addressed').value.speed;
    }

    this.loading.isLoading = true;
    this._firebaseService.addCollection(null, firebaseConfig, payload).then(
      (result: any) => {
        this.loading.isLoading = false;
        this._globalAction.showToast({
          title: this._translateService.instant('Info!'),
          message: this._translateService.instant(
            'Successfully added Purchaser!'
          ),
          type: 'success',
        });
        this._bsModalRef.hide();
      },
      (error: any) => {
        console.log(error);
        this.loading.isLoading = false;
        this._globalAction.showToast({
          title: this._translateService.instant('Info!'),
          message: error,
          type: 'error',
        });
      }
    );
  }
}
