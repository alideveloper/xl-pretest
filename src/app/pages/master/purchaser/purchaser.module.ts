import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PurchaserRoutingModule } from './purchaser-routing.module';

import { SharedModule } from '@app/shared';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { UtilsModule } from '@app/components/utils/utils.module';
import { NavigationsModule } from '@components/navigations/navigations.module';
import { AlertsModule } from '@app/components/alerts/alerts.module';
import { FormControlsModule } from '@app/components/form-controls/form-controls.module';
import { IconsModule } from '@app/components/icons/icons.module';
import { MapsModule } from '@app/components/maps/maps.module';

import { PurchaserComponent } from './purchaser.component';
import { AddPurchaserComponent } from './add/add.component';
import { EditPurchaserComponent } from './edit/edit.component';
import { DetailPurchaserComponent } from './detail/detail.component';
import { DeletePurchaserComponent } from './delete/delete.component';

import { FilterByPipe } from 'ngx-pipes';

const modules = [
  UtilsModule,
  CommonModule,
  NavigationsModule,
  NgxDatatableModule,
  IconsModule,
  AlertsModule,
  FormControlsModule,
  SharedModule,
  MapsModule,
];

const declarations = [
  AddPurchaserComponent,
  EditPurchaserComponent,
  DetailPurchaserComponent,
  DeletePurchaserComponent,
];

@NgModule({
  declarations: [PurchaserComponent, ...declarations],
  imports: [PurchaserRoutingModule, ...modules],
  entryComponents: [...declarations],
  providers: [FilterByPipe],
})
export class PurchaserModule {}
