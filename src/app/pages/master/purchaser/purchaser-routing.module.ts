import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PurchaserComponent } from './purchaser.component';

import { extract } from '@app/core';

const routes: Routes = [
  {
    path: '',
    component: PurchaserComponent,
    data: { title: extract('Purchaser') },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PurchaserRoutingModule {}
