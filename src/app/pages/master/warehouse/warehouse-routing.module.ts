import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WarehouseComponent } from './warehouse.component';

import { extract } from '@app/core';

const routes: Routes = [
  {
    path: '',
    component: WarehouseComponent,
    data: { title: extract('Warehouse') },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WarehouseRoutingModule {}
