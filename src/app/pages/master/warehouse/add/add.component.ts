import { Component, OnInit } from '@angular/core';

import { FirebaseService } from '@firebase/firebase.service';

import { BsModalRef } from 'ngx-bootstrap/modal';

import { TranslateService } from '@ngx-translate/core';

import { GlobalAction } from '@app/shared/services/global.action';

import { ngxLoadingAnimationTypes } from 'ngx-loading';
import { Warehouse } from '@app/shared/model/warehouse.model';
import { Loading } from '@app/shared/model/other/loading.model';
import { FirebaseConfigModel } from '@app/shared/model/other/firebase-config.model';

@Component({
  selector: 'app-add-warehouse',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss'],
})
export class AddWarehouseComponent implements OnInit {
  loading: Loading = {
    isLoading: false,
    text: this._translateService.instant('Added Warehouse') + '...',
    primaryColour: '',
    secondaryColour: '',
    animation: ngxLoadingAnimationTypes.threeBounce,
    image: 'assets/img/svg/save-person.svg',
  };

  name: any;
  code: any;

  warehouses: Warehouse[];
  selectedBusiness: any;

  constructor(
    private _bsModalRef: BsModalRef,
    private _translateService?: TranslateService,
    private _firebaseService?: FirebaseService,
    private _globalAction?: GlobalAction
  ) {}

  async ngOnInit(): Promise<any> {}

  onChangeWarehouse(event: any) {}

  async close(): Promise<any> {
    this._bsModalRef.hide();
  }

  async submit(): Promise<any> {
    if (this.warehouses && this.warehouses.length > 0) {
      const findExistCode = this.warehouses.filter(
        (item: any) => item.code === this.code
      );

      if (findExistCode && findExistCode.length > 0) {
        this._globalAction.showToast({
          title: this._translateService.instant('Info!'),
          message: this._translateService.instant('Warehouse Code Exist!'),
          type: 'info',
        });
        return;
      }
    }

    const firebaseConfig: FirebaseConfigModel = {
      method: 'POST',
      collection: {
        status: true,
        name: 'apps',
        document: {
          status: true,
          name: 'Database',
          collection: {
            status: true,
            name: 'business',
            document: {
              status: true,
              name: this.selectedBusiness.key,
              collection: {
                status: true,
                name: 'warehouses',
                document: {
                  status: false,
                },
                orderBy: {
                  object: 'created',
                  order: 'desc',
                },
                query: [],
              },
            },
          },
        },
      },
    };

    const businessKey = this.selectedBusiness.key;

    const payload: any = {
      code: this.code,
      name: this.name,
      lastSerialNumber: 0,
      businessKey,
    };

    this.loading.isLoading = true;
    this._firebaseService.addCollection(null, firebaseConfig, payload).then(
      (result: any) => {
        this.loading.isLoading = false;
        this._globalAction.showToast({
          title: this._translateService.instant('Info!'),
          message: this._translateService.instant(
            'Successfully added Warehouse!'
          ),
          type: 'success',
        });
        this._bsModalRef.hide();
      },
      (error: any) => {
        console.log(error);
        this.loading.isLoading = false;
        this._globalAction.showToast({
          title: this._translateService.instant('Info!'),
          message: error,
          type: 'error',
        });
      }
    );
  }
}
