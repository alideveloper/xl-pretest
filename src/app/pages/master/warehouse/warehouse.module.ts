import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WarehouseRoutingModule } from './warehouse-routing.module';

import { SharedModule } from '@app/shared';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { UtilsModule } from '@app/components/utils/utils.module';
import { NavigationsModule } from '@components/navigations/navigations.module';
import { AlertsModule } from '@app/components/alerts/alerts.module';
import { FormControlsModule } from '@app/components/form-controls/form-controls.module';
import { IconsModule } from '@app/components/icons/icons.module';

import { WarehouseComponent } from './warehouse.component';
import { AddWarehouseComponent } from './add/add.component';
import { EditWarehouseComponent } from './edit/edit.component';
import { DeleteWarehouseComponent } from './delete/delete.component';

import { FilterByPipe } from 'ngx-pipes';

const modules = [
  UtilsModule,
  CommonModule,
  NavigationsModule,
  NgxDatatableModule,
  IconsModule,
  AlertsModule,
  FormControlsModule,
  SharedModule,
];

const declarations = [
  AddWarehouseComponent,
  EditWarehouseComponent,
  DeleteWarehouseComponent,
];

@NgModule({
  declarations: [WarehouseComponent, ...declarations],
  imports: [WarehouseRoutingModule, ...modules],
  entryComponents: [...declarations],
  providers: [FilterByPipe],
})
export class WarehouseModule {}
