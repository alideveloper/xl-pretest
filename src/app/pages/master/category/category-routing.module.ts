import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CategoryComponent } from './category.component';

import { extract } from '@app/core';

const routes: Routes = [
  {
    path: '',
    component: CategoryComponent,
    data: { title: extract('Goods Category') },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CategoryRoutingModule {}
