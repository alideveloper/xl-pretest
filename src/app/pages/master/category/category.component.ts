import {
  Component,
  OnInit,
  ChangeDetectorRef,
  ViewEncapsulation,
  HostListener,
  ViewChild,
  OnDestroy,
} from '@angular/core';

import { ColorScheme, BaseComponent } from '@app/core';
import { environment } from '@environments/environment';

import { FirebaseService } from '@app/@firebase/firebase.service';

import { DatatableComponent } from '@swimlane/ngx-datatable';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { SelectionType } from '@swimlane/ngx-datatable';

import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { FilterByPipe } from 'ngx-pipes';

import { AddCategoryComponent } from './add/add.component';
import { EditCategoryComponent } from './edit/edit.component';
import { DeleteCategoryComponent } from './delete/delete.component';
import { AppService } from '@app/app.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AccountingPipe } from '@app/shared/pipes/accounting.pipe';
import { GoodsCategory } from '@app/shared/model/goods-category.model';
import {
  Query,
  FirebaseConfigModel,
} from '@app/shared/model/other/firebase-config.model';
import { GlobalAction } from '@app/shared/services/global.action';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [AccountingPipe],
})
export class CategoryComponent extends BaseComponent
  implements OnInit, OnDestroy {
  @ViewChild(DatatableComponent, { static: false })
  table: DatatableComponent;

  redirectURL = environment.indexURL.root + environment.indexURL.homeTwo;

  ColorScheme = ColorScheme;
  ColumnMode = ColumnMode;

  messages: any = {
    emptyMessage:
      '<div class="empty-container"><img src="assets/img/inventory.png"><div class="empty-table">No results</div></div>',
  };

  timeout: any;

  currentStyles: any = 'table-hover';
  isCard = true;
  isTableView = false;
  isListCollapsed = false;
  responsive = true;
  responsiveExpandable = false;

  headerHeight: any = 'auto';
  rowHeight: any = false;
  footerHeight: any = 'auto';
  scrollbarV = false;
  scrollbarH = true;
  limit = 10;

  selectedActive = false;
  selected: any[] = [];
  SelectionType = SelectionType;
  selectAllRowsOnPage = false;

  selectedMessage: any;

  data: GoodsCategory[];
  initDataCategory: GoodsCategory[];

  modalRef: BsModalRef;

  pagination: any = {
    active: true,
    labelTotalMessage: 'Total Data',
  };

  columns: any = [
    {
      width: 50,
      sortable: false,
      canAutoResize: true,
      draggable: false,
      resizeable: false,
      name: 'index',
      header: 'No',
    },
    {
      width: 300,
      sortable: true,
      canAutoResize: true,
      draggable: false,
      resizeable: false,
      name: 'name',
      header: 'Name',
    },
    {
      width: 200,
      sortable: true,
      canAutoResize: true,
      draggable: false,
      resizeable: false,
      name: 'created',
      header: 'Created',
    },
    {
      width: 200,
      sortable: true,
      canAutoResize: true,
      draggable: false,
      resizeable: false,
      name: 'updated',
      header: 'Last Modified',
    },
    {
      width: 'auto',
      sortable: false,
      canAutoResize: true,
      draggable: false,
      resizeable: false,
      name: 'action',
      header: 'Action',
    },
  ];

  selectedBusinessValue: any;
  selectedBusiness: any;

  search: any = {};

  emptyBusiness: any;

  widthLayer: any = window.innerWidth;
  isTableResponse: boolean;

  private _unsubscribeAll: Subject<any>;

  @HostListener('window:resize', ['$event'])
  onResize(event: any) {
    this.widthLayer = event.target.innerWidth;
    this.checkLayerWidth();
  }

  constructor(
    private _modalService: BsModalService,
    private _firebaseService: FirebaseService,
    private _cdRef: ChangeDetectorRef,
    private _filterByPipe: FilterByPipe,
    private _appService?: AppService,
    private _globalAction?: GlobalAction
  ) {
    super();
  }

  async ngOnInit() {
    await this.initRouting();

    this._unsubscribeAll = new Subject();

    this.checkLayerWidth();
    this.initSearch();

    if (this.emptyBusiness === false) {
      await this.initBusiness();
      await this.initData();

      this._appService.onDataChanges
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe(async (data) => {
          console.log('on changes');
          console.log(data.selectedBusiness);
          if (data.selectedBusinessValue !== this.selectedBusinessValue) {
            if (data.selectedBusinessValue) {
              this.selectedBusinessValue = data.selectedBusinessValue;
            }
          }
          if (
            (data.selectedBusiness &&
              data.selectedBusiness !== this.selectedBusiness) ||
            (data.selectedBusiness &&
              data.selectedBusiness.currency !== this.selectedBusiness.currency)
          ) {
            console.log('MASUK');
            this.selectedBusiness = data.selectedBusiness;
            this.initSearch();
            this.initData();
          }
        });
    }
  }

  async ngOnDestroy() {
    try {
      this._unsubscribeAll.next();
      this._unsubscribeAll.complete();
    } catch (error) {}
  }

  checkLayerWidth() {
    if (this.widthLayer < 800) {
      this.isTableResponse = true;
      this.scrollbarV = false;
    } else {
      this.isTableResponse = false;
    }
  }

  initSearch() {
    this.search = {
      text: '',
      code: '',
    };
  }

  async initRouting() {
    this.emptyBusiness = await this._globalAction.isEmptyBusiness();
    this._globalAction.submenuRouting('/master/category');
  }

  async initData() {
    await this.getCategory();
  }

  async initBusiness() {
    this.selectedBusinessValue = localStorage.getItem(
      environment.domain + '-selectedBusinessValue'
    );
    this.selectedBusiness = JSON.parse(
      localStorage.getItem(environment.domain + '-selectedBusiness')
    );
  }

  onSelect({ selected }: any) {
    this.selected = [...selected];
  }

  async getCategory(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const configFirebase: FirebaseConfigModel = {
        collection: {
          status: true,
          name: 'apps',
          document: {
            status: true,
            name: 'Database',
            collection: {
              status: true,
              name: 'business',
              document: {
                status: true,
                name: this.selectedBusiness.key,
                collection: {
                  status: true,
                  name: 'categories',
                  document: {
                    status: false,
                  },
                  orderBy: {
                    object: 'created',
                    order: 'desc',
                  },
                  query: [],
                },
              },
            },
          },
        },
      };

      const data: any = await this._firebaseService.handleCollection(
        null,
        configFirebase
      );
      data.subscribe((response: GoodsCategory[]) => {
        if (response) {
          if (response && response.length > 0) {
            response.map((responseItem: any) => {
              console.log('FILTERING', response);
            });
          }
          this.data = [...response];
          this.initDataCategory = [...response];
          this._cdRef.detectChanges();
          resolve(response);
          return;
        }
      });
    });
  }

  get tableStyles(): string {
    return (this.isTableView ? 'table' : 'listview') + ' ' + this.currentStyles;
  }

  onPage(event: Event) {
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      console.log(event);
    }, 100);
  }

  addCategory(): void {
    const navParam = {
      initialState: {
        categories: this.initDataCategory,
        selectedBusiness: this.selectedBusiness,
      },
      class: 'modal-md modal-dialog-centered',
    };

    this.modalRef = this._modalService.show(AddCategoryComponent, navParam);
  }

  editCategory(value: any): void {
    const navParam = {
      initialState: {
        categories: this.initDataCategory,
        formData: value,
        selectedBusiness: this.selectedBusiness,
      },
      class: 'modal-md modal-dialog-centered',
    };

    console.log(navParam, 'navParam');
    this.modalRef = this._modalService.show(EditCategoryComponent, navParam);
  }

  deleteCategory(value: any): void {
    const navParam = {
      initialState: {
        form: value,
        selectedBusiness: this.selectedBusiness,
      },
      class: 'modal-md modal-dialog-centered',
    };

    console.log(navParam, 'navParam');
    this.modalRef = this._modalService.show(DeleteCategoryComponent, navParam);
  }

  onChangeSearch(type?: any, event?: any) {
    let result: any = Object.assign([], this.initDataCategory);
    if (type === 'rangeBalance') {
      this.search.rangeBalance = [event.newValue[0], event.newValue[1]];
    }
    console.log(event, result);

    result = this.onChangeSearchGeneral(result, this.search.name, 'name');
    this.data = [...result];
  }

  onChangeSearchGeneral(result: any, event: any, field?: any) {
    console.log('onChangeSearchGeneral', result, event, field);
    if (event) {
      const filter = this._filterByPipe.transform(result, [field], [event]);
      console.log('return', filter);
      return filter;
    }
    console.log('empty');
    return result;
  }
}
