import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CategoryRoutingModule } from './category-routing.module';

import { SharedModule } from '@app/shared';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { UtilsModule } from '@app/components/utils/utils.module';
import { NavigationsModule } from '@components/navigations/navigations.module';
import { AlertsModule } from '@app/components/alerts/alerts.module';
import { FormControlsModule } from '@app/components/form-controls/form-controls.module';
import { IconsModule } from '@app/components/icons/icons.module';

import { CategoryComponent } from './category.component';
import { AddCategoryComponent } from './add/add.component';
import { EditCategoryComponent } from './edit/edit.component';
import { DeleteCategoryComponent } from './delete/delete.component';

import { FilterByPipe } from 'ngx-pipes';

const modules = [
  UtilsModule,
  CommonModule,
  NavigationsModule,
  NgxDatatableModule,
  IconsModule,
  AlertsModule,
  FormControlsModule,
  SharedModule,
];

const declarations = [
  AddCategoryComponent,
  EditCategoryComponent,
  DeleteCategoryComponent,
];

@NgModule({
  declarations: [CategoryComponent, ...declarations],
  imports: [CategoryRoutingModule, ...modules],
  entryComponents: [...declarations],
  providers: [FilterByPipe],
})
export class CategoryModule {}
