import { NgModule } from '@angular/core';
import { MasterRoutingModule } from './master-routing.module';

@NgModule({
  declarations: [],
  imports: [MasterRoutingModule],
})
export class MasterModule {}
