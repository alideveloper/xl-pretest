import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WarehouseGoodsComponent } from './warehouse-goods.component';

import { extract } from '@app/core';

const routes: Routes = [
  {
    path: '',
    component: WarehouseGoodsComponent,
    data: { title: extract('In Stock') },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WarehouseGoodsRoutingModule {}
