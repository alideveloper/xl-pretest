import { Component, OnInit } from '@angular/core';

import { BsModalRef } from 'ngx-bootstrap/modal';

import { TranslateService } from '@ngx-translate/core';

import { ngxLoadingAnimationTypes } from 'ngx-loading';
import { Loading } from '@app/shared/model/other/loading.model';
import { Supplier } from '@app/shared/model/supplier.model';
import { Maps } from '@app/shared/types/maps';

@Component({
  selector: 'app-detail-supplier',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
})
export class DetailSupplierComponent implements OnInit {
  loading: Loading = {
    isLoading: false,
    text: this._translateService.instant('Detail Supplier') + '...',
    primaryColour: '',
    secondaryColour: '',
    animation: ngxLoadingAnimationTypes.threeBounce,
    image: 'assets/img/svg/save-person.svg',
  };

  value: any = {
    form: {
      logo: {},
      logoBlob: {},
    },
    formData: {},
  };

  numbersOnly = /^[0-9]*$/;

  formData: Supplier;

  schemaMaps: Maps = {
    component: 'maps',
    position: 'body',
    style: {},
    styleClass: 'maps-md',
    isView: true,
    label: {
      active: false,
      binding: false,
      style: {},
      styleClass: '',
      text: 'Select Maps',
      type: 'text',
      state: {
        hide: false,
        disabled: false,
        hideCondition: [],
        disabledCondition: [],
      },
    },
    labelSubtitle: {
      active: false,
    },
    search: {
      active: false,
      placeholder: 'Search Address',
    },
    address: {
      active: false,
      bindingObject: 'address',
    },
    zoom: 13,
    minZoom: 5,
    maxZoom: 20,
    parentBinding: 'formData',
    latitude: 'latitude',
    longitude: 'longitude',
    callbackMaps: {
      latitude: 'latitude',
      longitude: 'longitude',
      accuracy: 'accuracy',
      altitude: 'altitude',
      altitudeAccuracy: 'altitudeAccuracy',
      heading: 'heading',
      speed: 'speed',
    },
  };

  constructor(
    private _bsModalRef: BsModalRef,
    private _translateService?: TranslateService
  ) {}

  async ngOnInit(): Promise<any> {
    this.value.formData = this.formData;
  }

  async close(): Promise<any> {
    this._bsModalRef.hide();
  }
}
