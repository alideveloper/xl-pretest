import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SupplierRoutingModule } from './supplier-routing.module';

import { SharedModule } from '@app/shared';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { UtilsModule } from '@app/components/utils/utils.module';
import { NavigationsModule } from '@components/navigations/navigations.module';
import { AlertsModule } from '@app/components/alerts/alerts.module';
import { FormControlsModule } from '@app/components/form-controls/form-controls.module';
import { IconsModule } from '@app/components/icons/icons.module';
import { MapsModule } from '@app/components/maps/maps.module';

import { SupplierComponent } from './supplier.component';
import { AddSupplierComponent } from './add/add.component';
import { EditSupplierComponent } from './edit/edit.component';
import { DetailSupplierComponent } from './detail/detail.component';
import { DeleteSupplierComponent } from './delete/delete.component';

import { FilterByPipe } from 'ngx-pipes';

const modules = [
  UtilsModule,
  CommonModule,
  NavigationsModule,
  NgxDatatableModule,
  IconsModule,
  AlertsModule,
  FormControlsModule,
  SharedModule,
  MapsModule,
];

const declarations = [
  AddSupplierComponent,
  EditSupplierComponent,
  DetailSupplierComponent,
  DeleteSupplierComponent,
];

@NgModule({
  declarations: [SupplierComponent, ...declarations],
  imports: [SupplierRoutingModule, ...modules],
  entryComponents: [...declarations],
  providers: [FilterByPipe],
})
export class SupplierModule {}
