import { Component, OnInit } from '@angular/core';

import { BsModalRef } from 'ngx-bootstrap/modal';

import { TranslateService } from '@ngx-translate/core';

import { ngxLoadingAnimationTypes } from 'ngx-loading';
import { Loading } from '@app/shared/model/other/loading.model';
import { Goods } from '@app/shared/model/goods.model';

@Component({
  selector: 'app-detail-goods',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
})
export class DetailGoodsComponent implements OnInit {
  loading: Loading = {
    isLoading: false,
    text: this._translateService.instant('Detail Goods') + '...',
    primaryColour: '',
    secondaryColour: '',
    animation: ngxLoadingAnimationTypes.threeBounce,
    image: 'assets/img/svg/save-person.svg',
  };

  value: any = {
    form: {
      image: {},
      imageBlob: {},
    },
    formData: {},
  };

  selectedBusiness: any;

  numbersOnly = /^[0-9]*$/;

  formData: Goods;

  constructor(
    private _bsModalRef: BsModalRef,
    private _translateService?: TranslateService
  ) {}

  async ngOnInit(): Promise<any> {
    this.value.formData = this.formData;
  }

  async close(): Promise<any> {
    this._bsModalRef.hide();
  }
}
