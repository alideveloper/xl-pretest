import {
  Component,
  OnInit,
  ChangeDetectorRef,
  ViewEncapsulation,
  HostListener,
  ViewChild,
  OnDestroy,
} from '@angular/core';

import { ColorScheme, BaseComponent } from '@app/core';
import { environment } from '@environments/environment';

import { FirebaseService } from '@app/@firebase/firebase.service';

import { DatatableComponent } from '@swimlane/ngx-datatable';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { SelectionType } from '@swimlane/ngx-datatable';

import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { FilterByPipe } from 'ngx-pipes';

import { AddGoodsComponent } from './add/add.component';
import { EditGoodsComponent } from './edit/edit.component';
import { DeleteGoodsComponent } from './delete/delete.component';
import { AppService } from '@app/app.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AccountingPipe } from '@app/shared/pipes/accounting.pipe';
import {
  Query,
  FirebaseConfigModel,
} from '@app/shared/model/other/firebase-config.model';
import { Goods } from '@app/shared/model/goods.model';
import { GoodsCategory } from '@app/shared/model/goods-category.model';
import { GoodsUnit } from '@app/shared/model/goods-unit.model';
import { DetailGoodsComponent } from './detail/detail.component';
import { EditImageGoodsComponent } from './edit-image/edit.component';
import { GlobalAction } from '@app/shared/services/global.action';

@Component({
  selector: 'app-goods',
  templateUrl: './goods.component.html',
  styleUrls: ['./goods.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [AccountingPipe],
})
export class GoodsComponent extends BaseComponent implements OnInit, OnDestroy {
  @ViewChild(DatatableComponent, { static: false })
  table: DatatableComponent;

  redirectURL = environment.indexURL.root + environment.indexURL.homeTwo;

  ColorScheme = ColorScheme;
  ColumnMode = ColumnMode;

  messages: any = {
    emptyMessage:
      '<div class="empty-container"><img src="assets/img/inventory.png"><div class="empty-table">No results</div></div>',
  };

  timeout: any;

  currentStyles: any = 'table-hover';
  isCard = true;
  isTableView = false;
  isListCollapsed = false;
  responsive = true;
  responsiveExpandable = false;

  headerHeight: any = 'auto';
  rowHeight: any = false;
  footerHeight: any = 'auto';
  scrollbarV = false;
  scrollbarH = true;
  limit = 10;

  selectedActive = false;
  selected: any[] = [];
  SelectionType = SelectionType;
  selectAllRowsOnPage = false;

  selectedMessage: any;

  data: Goods[];
  initDataGoods: Goods[];
  categories: GoodsCategory[];
  units: GoodsUnit[];

  modalRef: BsModalRef;

  pagination: any = {
    active: true,
    labelTotalMessage: 'Total Data',
  };

  columns: any = [
    {
      width: 50,
      sortable: false,
      canAutoResize: false,
      draggable: false,
      resizeable: false,
      name: 'index',
      header: 'No',
    },
    {
      width: 200,
      sortable: true,
      canAutoResize: true,
      draggable: false,
      resizeable: false,
      name: 'categories',
      header: 'Category',
    },
    {
      width: 175,
      sortable: true,
      canAutoResize: true,
      draggable: false,
      resizeable: false,
      name: 'image',
      header: 'Image',
    },
    {
      width: 175,
      sortable: true,
      canAutoResize: true,
      draggable: false,
      resizeable: false,
      name: 'name',
      header: 'Name',
    },
    {
      width: 100,
      sortable: true,
      canAutoResize: true,
      draggable: false,
      resizeable: false,
      name: 'price',
      header: 'Price',
    },
    {
      width: 100,
      sortable: true,
      canAutoResize: true,
      draggable: false,
      resizeable: false,
      name: 'units',
      header: 'Unit',
    },
    {
      width: 200,
      sortable: true,
      canAutoResize: true,
      draggable: false,
      resizeable: false,
      name: 'updated',
      header: 'Last Modified',
    },
    {
      width: 'auto',
      sortable: false,
      canAutoResize: true,
      draggable: false,
      resizeable: false,
      name: 'action',
      header: 'Action',
    },
  ];

  schemaCategorySelect: any = {
    component: 'select',
    position: 'body',
    label: {
      active: true,
      binding: false,
      text: 'By Category',
      type: 'text',
      state: {
        hide: false,
        disabled: false,
        hideCondition: [],
        disabledCondition: [],
      },
    },
    labelSubtitle: {
      active: false,
    },
    type: 'text',
    placeholder: 'All',
    parentBinding: 'Search by Category',
    bindingObject: 'categoryKey',
    primaryKey: 'key',
    bindingData: 'dataCategoriesSearch',
    labelForId: 'name',
    bindLabel: 'name',
    bindValue: 'key',
    searchable: true,
    search: true,
    closeOnSelect: true,
    defaultValue: false,
    customSearch: true,
    clearable: false,
    bindingSearch: 'dataCategoriesSearch',
    required: false,
    formText: {
      valid: {
        active: false,
        text: 'Data telah Sesuai!',
      },
      invalid: {
        active: false,
        text: 'Data belum Sesuai!',
      },
    },
  };

  schemaUnitSelect: any = {
    component: 'select',
    position: 'body',
    label: {
      active: true,
      binding: false,
      text: 'By Unit',
      type: 'text',
      state: {
        hide: false,
        disabled: false,
        hideCondition: [],
        disabledCondition: [],
      },
    },
    labelSubtitle: {
      active: false,
    },
    type: 'text',
    placeholder: 'Search by Unit',
    parentBinding: 'form',
    bindingObject: 'unitKey',
    primaryKey: 'key',
    bindingData: 'dataUnitsSearch',
    labelForId: 'name',
    bindLabel: 'name',
    bindValue: 'key',
    searchable: true,
    search: true,
    closeOnSelect: true,
    defaultValue: false,
    customSearch: true,
    clearable: false,
    bindingSearch: 'dataUnitsSearch',
    required: false,
    formText: {
      valid: {
        active: false,
        text: 'Data telah Sesuai!',
      },
      invalid: {
        active: false,
        text: 'Data belum Sesuai!',
      },
    },
  };

  selectedBusinessValue: any;
  selectedBusiness: any;

  dataCategoriesSearch: GoodsCategory[];
  dataUnitsSearch: GoodsUnit[];

  search: any = {};

  widthLayer: any = window.innerWidth;
  isTableResponse: boolean;

  value: any = {
    form: {},
  };

  emptyBusiness: any;

  private _unsubscribeAll: Subject<any>;

  @HostListener('window:resize', ['$event'])
  onResize(event: any) {
    this.widthLayer = event.target.innerWidth;
    this.checkLayerWidth();
  }

  constructor(
    private _modalService: BsModalService,
    private _firebaseService: FirebaseService,
    private _cdRef: ChangeDetectorRef,
    private _filterByPipe: FilterByPipe,
    private _appService?: AppService,
    private _globalAction?: GlobalAction
  ) {
    super();
  }

  async ngOnInit() {
    await this.initRouting();

    this._unsubscribeAll = new Subject();

    this.checkLayerWidth();
    this.initSearch();

    if (this.emptyBusiness === false) {
      await this.initBusiness();
      await this.initData();

      this._appService.onDataChanges
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe(async (data) => {
          console.log('on changes');
          console.log(data.selectedBusiness);
          if (data.selectedBusinessValue !== this.selectedBusinessValue) {
            if (data.selectedBusinessValue) {
              this.selectedBusinessValue = data.selectedBusinessValue;
            }
          }
          if (
            (data.selectedBusiness &&
              data.selectedBusiness !== this.selectedBusiness) ||
            (data.selectedBusiness &&
              data.selectedBusiness.currency !== this.selectedBusiness.currency)
          ) {
            console.log('MASUK');
            this.selectedBusiness = data.selectedBusiness;
            this.initSearch();
            this.initData();
          }
        });
    }
  }

  async ngOnDestroy() {
    try {
      this._unsubscribeAll.next();
      this._unsubscribeAll.complete();
    } catch (error) {}
  }

  checkLayerWidth() {
    if (this.widthLayer < 800) {
      this.isTableResponse = true;
      this.scrollbarV = false;
    } else {
      this.isTableResponse = false;
    }
  }

  initSearch() {
    this.search = {
      category: '',
      name: '',
      unit: '',
    };
  }

  async initRouting() {
    this.emptyBusiness = await this._globalAction.isEmptyBusiness();
    this._globalAction.submenuRouting('/master/goods');
  }

  async initData() {
    await this.getCategories();
    await this.getUnits();
    await this.getGoods();
  }

  async initBusiness() {
    this.selectedBusinessValue = localStorage.getItem(
      environment.domain + '-selectedBusinessValue'
    );
    this.selectedBusiness = JSON.parse(
      localStorage.getItem(environment.domain + '-selectedBusiness')
    );
  }

  onSelect({ selected }: any) {
    this.selected = [...selected];
  }

  async getGoods(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const configFirebase: FirebaseConfigModel = {
        collection: {
          status: true,
          name: 'apps',
          document: {
            status: true,
            name: 'Database',
            collection: {
              status: true,
              name: 'business',
              document: {
                status: true,
                name: this.selectedBusiness.key,
                collection: {
                  status: true,
                  name: 'goods',
                  document: {
                    status: false,
                  },
                  orderBy: {
                    object: 'created',
                    order: 'desc',
                  },
                  query: [],
                },
              },
            },
          },
        },
      };

      const data: any = await this._firebaseService.handleCollection(
        null,
        configFirebase
      );
      data.subscribe((response: Goods[]) => {
        if (response) {
          response.map(async (goods: Goods) => {
            if (this.categories && this.categories.length > 0) {
              const categories = this.categories.filter(
                (categoryItem: any) => categoryItem.key === goods.categoryKey
              );

              if (categories && categories.length > 0) {
                goods.categories = categories[0];
              } else {
                goods.categoryKey = null;
                goods.categories = {};
                goods.categories.key = '';
                goods.categories.name = '-';
              }
            } else {
              goods.categoryKey = null;
              goods.categories = {};
              goods.categories.key = '';
              goods.categories.name = '-';
            }

            if (this.units && this.units.length > 0) {
              const units = this.units.filter(
                (unitItem: any) => unitItem.key === goods.unitKey
              );

              if (units && units.length > 0) {
                goods.units = units[0];
              } else {
                goods.unitKey = null;
                goods.units = {};
                goods.units.key = '';
                goods.units.name = '-';
              }
            } else {
              goods.unitKey = null;
              goods.units = {};
              goods.units.key = '';
              goods.units.name = '-';
            }
          });
          console.log('response', response);
          this.data = [...response];
          this.initDataGoods = [...response];
          this._cdRef.detectChanges();
          resolve(response);
          return;
        }
      });
    });
  }

  async getCategories(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      if (this.selectedBusiness) {
        const configFirebase: FirebaseConfigModel = {
          collection: {
            status: true,
            name: 'apps',
            document: {
              status: true,
              name: 'Database',
              collection: {
                status: true,
                name: 'business',
                document: {
                  status: true,
                  name: this.selectedBusiness.key,
                  collection: {
                    status: true,
                    name: 'categories',
                    document: {
                      status: false,
                    },
                    orderBy: {
                      object: 'created',
                      order: 'desc',
                    },
                    query: [],
                  },
                },
              },
            },
          },
        };

        const data: any = await this._firebaseService.handleCollection(
          null,
          configFirebase
        );
        data.subscribe((response: GoodsCategory[]) => {
          if (response) {
            this.dataCategoriesSearch = [];
            this.categories = [...response];
            const itemAll = {
              key: '',
              name: 'All',
            };
            this.dataCategoriesSearch.push(itemAll);

            response.map((item: GoodsCategory) => {
              this.dataCategoriesSearch.push(item);
            });

            this.value.dataCategoriesSearch = [...this.dataCategoriesSearch];
            console.log('responsecategories', response);
            this._cdRef.detectChanges();
            resolve(response);
            return;
          }
        });
      }
    });
  }

  async getUnits(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      if (this.selectedBusiness) {
        const configFirebase: FirebaseConfigModel = {
          collection: {
            status: true,
            name: 'apps',
            document: {
              status: true,
              name: 'Database',
              collection: {
                status: true,
                name: 'business',
                document: {
                  status: true,
                  name: this.selectedBusiness.key,
                  collection: {
                    status: true,
                    name: 'units',
                    document: {
                      status: false,
                    },
                    orderBy: {
                      object: 'created',
                      order: 'desc',
                    },
                    query: [],
                  },
                },
              },
            },
          },
        };

        const data: any = await this._firebaseService.handleCollection(
          null,
          configFirebase
        );
        data.subscribe((response: GoodsUnit[]) => {
          if (response) {
            this.dataUnitsSearch = [];
            this.units = [...response];
            const itemAll = {
              key: '',
              name: 'All',
            };
            this.dataUnitsSearch.push(itemAll);

            response.map((item: GoodsUnit) => {
              this.dataUnitsSearch.push(item);
            });

            this.value.dataUnitsSearch = [...this.dataUnitsSearch];
            this.value.dataUnits = [...response];
            this.value.form.unitKey = '';
            console.log('responseunits', response);
            this._cdRef.detectChanges();
            resolve(response);
            return;
          }
        });
      }
    });
  }

  get tableStyles(): string {
    return (this.isTableView ? 'table' : 'listview') + ' ' + this.currentStyles;
  }

  onPage(event: Event) {
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      console.log(event);
    }, 100);
  }

  addGoods(): void {
    const navParam = {
      initialState: {
        goods: this.initDataGoods,
        categories: this.categories,
        units: this.units,
        selectedBusiness: this.selectedBusiness,
      },
      class: 'modal-lg modal-dialog-centered',
    };

    this.modalRef = this._modalService.show(AddGoodsComponent, navParam);
  }

  editGoods(value: any): void {
    const navParam = {
      initialState: {
        goods: this.initDataGoods,
        categories: this.categories,
        units: this.units,
        formData: value,
        selectedBusiness: this.selectedBusiness,
      },
      class: 'modal-lg modal-dialog-centered',
    };

    console.log(navParam, 'navParam');
    this.modalRef = this._modalService.show(EditGoodsComponent, navParam);
  }

  detailGoods(value: any): void {
    const navParam = {
      initialState: {
        formData: value,
        selectedBusiness: this.selectedBusiness,
      },
      class: 'modal-lg modal-dialog-centered',
    };

    console.log(navParam, 'navParam');
    this.modalRef = this._modalService.show(DetailGoodsComponent, navParam);
  }

  editImageGoods(value: any): void {
    const navParam = {
      initialState: {
        formData: value,
        selectedBusiness: this.selectedBusiness,
      },
      class: 'modal-md modal-dialog-centered',
    };

    console.log(navParam, 'navParam');
    this.modalRef = this._modalService.show(EditImageGoodsComponent, navParam);
  }

  deleteGoods(value: any): void {
    const navParam = {
      initialState: {
        form: value,
        selectedBusiness: this.selectedBusiness,
      },
      class: 'modal-md modal-dialog-centered',
    };

    console.log(navParam, 'navParam');
    this.modalRef = this._modalService.show(DeleteGoodsComponent, navParam);
  }

  onChangeSearch(type?: any, event?: any) {
    let result: any = Object.assign([], this.initDataGoods);
    if (type === 'rangeBalance') {
      this.search.rangeBalance = [event.newValue[0], event.newValue[1]];
    }
    console.log(event, result);

    if (this.search.typeDate === 'All') {
      result = this.onChangeSearchGeneral(
        result,
        this.search.category,
        'categoryKey'
      );
      result = this.onChangeSearchGeneral(result, this.search.name, 'name');
      result = this.onChangeSearchGeneral(result, this.search.unit, 'unitKey');
    } else {
      result = this.onChangeSearchGeneral(
        result,
        this.search.category,
        'categoryKey'
      );
      result = this.onChangeSearchGeneral(result, this.search.name, 'name');
      result = this.onChangeSearchGeneral(result, this.search.unit, 'unitKey');
    }

    console.log('final', result);
    this.data = [...result];
  }

  onChangeSearchGeneral(result: any, event: any, field?: any) {
    console.log('onChangeSearchGeneral', result, event, field);
    if (event) {
      const filter = this._filterByPipe.transform(result, [field], [event]);
      console.log('return', filter);
      return filter;
    }
    console.log('empty');
    return result;
  }

  callbackUnit(event: any) {
    console.log('event', event);
    if (event) {
      this.search.unitKey = event;
    } else {
      this.search.unitKey = '';
    }
    this.onChangeSearch('unitKey', event);
  }

  callbackCategory(event: any) {
    console.log('event', event);
    if (event) {
      this.search.category = event;
    } else {
      this.search.category = '';
    }
    this.onChangeSearch('categoryKey', event);
  }
}
