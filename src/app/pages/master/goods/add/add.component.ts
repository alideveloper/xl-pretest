import { Component, OnInit } from '@angular/core';

import { FirebaseService } from '@firebase/firebase.service';

import { BsModalRef } from 'ngx-bootstrap/modal';

import { TranslateService } from '@ngx-translate/core';

import { GlobalAction } from '@app/shared/services/global.action';

import { ngxLoadingAnimationTypes } from 'ngx-loading';
import { Loading } from '@app/shared/model/other/loading.model';
import { FirebaseConfigModel } from '@app/shared/model/other/firebase-config.model';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { GoodsCategory } from '@app/shared/model/goods-category.model';
import { GlobalService } from '@app/shared/services/global.service';
import { Goods } from '@app/shared/model/goods.model';
import { Uploader } from '@app/shared/types/uploader';
import { GoodsUnit } from '@app/shared/model/goods-unit.model';

@Component({
  selector: 'app-add-goods',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss'],
})
export class AddGoodsComponent implements OnInit {
  loading: Loading = {
    isLoading: false,
    text: this._translateService.instant('Added Goods') + '...',
    primaryColour: '',
    secondaryColour: '',
    animation: ngxLoadingAnimationTypes.threeBounce,
    image: 'assets/img/svg/save-person.svg',
  };

  categories: GoodsCategory[];
  units: GoodsUnit[];
  selectedBusiness: any;

  form: FormGroup;

  value: any = {
    form: {
      image: {},
      imageBlob: {},
    },
  };

  schemaCategorySelect: any = {
    component: 'select',
    position: 'body',
    label: {
      active: true,
      binding: false,
      text: 'Category',
      type: 'text',
      state: {
        hide: false,
        disabled: false,
        hideCondition: [],
        disabledCondition: [],
      },
    },
    labelSubtitle: {
      active: false,
    },
    type: 'text',
    placeholder: 'Select Category',
    parentBinding: 'form',
    bindingObject: 'categoryKey',
    primaryKey: 'key',
    bindingData: 'dataCategories',
    labelForId: 'name',
    bindLabel: 'name',
    bindValue: 'key',
    searchable: true,
    search: true,
    closeOnSelect: true,
    defaultValue: true,
    customSearch: true,
    bindingSearch: 'dataCategories',
    required: true,
    formText: {
      valid: {
        active: true,
        text: 'Data telah Sesuai!',
      },
      invalid: {
        active: true,
        text: 'Data tidak Sesuai!',
      },
    },
  };

  schemaUnitSelect: any = {
    component: 'select',
    position: 'body',
    label: {
      active: true,
      binding: false,
      text: 'Unit',
      type: 'text',
      state: {
        hide: false,
        disabled: false,
        hideCondition: [],
        disabledCondition: [],
      },
    },
    labelSubtitle: {
      active: false,
    },
    type: 'text',
    placeholder: 'Select Unit',
    parentBinding: 'form',
    bindingObject: 'unitKey',
    primaryKey: 'key',
    bindingData: 'dataUnits',
    labelForId: 'name',
    bindLabel: 'name',
    bindValue: 'key',
    searchable: true,
    search: true,
    closeOnSelect: true,
    defaultValue: true,
    customSearch: true,
    bindingSearch: 'dataUnits',
    required: true,
    formText: {
      valid: {
        active: true,
        text: 'Data telah Sesuai!',
      },
      invalid: {
        active: true,
        text: 'Data tidak Sesuai!',
      },
    },
  };

  schemaUploader: Uploader = {
    component: 'uploader',
    position: 'body',
    label: {
      active: true,
      binding: false,
      text: 'Image',
      type: 'text',
      state: {
        hide: false,
        disabled: false,
        hideCondition: [],
        disabledCondition: [],
      },
    },
    labelSubtitle: {
      active: false,
    },
    parentBinding: 'form',
    bindingObject: 'image',
    bindingObjectBlob: 'imageBlob',
    accept: 'image/*',
    minsize: 0.01,
    maxsize: 10,
    isDisabled: false,
    animation: false,
    multiple: false,
    state: {
      hide: false,
      disabled: false,
      hideCondition: [],
      disabledCondition: [],
    },
    required: true,
    formText: {
      valid: {
        active: true,
        text: 'Data telah Sesuai!',
      },
      invalid: {
        active: true,
        text: 'Data Belum Sesuai',
      },
    },
  };

  constructor(
    private _bsModalRef: BsModalRef,
    private _translateService?: TranslateService,
    private _firebaseService?: FirebaseService,
    private _globalAction?: GlobalAction,
    private _globalService?: GlobalService
  ) {}

  async ngOnInit(): Promise<any> {
    await this.initForm();
    await this.initData();
  }

  initForm() {
    this.form = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.minLength(3)]),
      description: new FormControl('', []),
      categoryKey: new FormControl('', [Validators.required]),
      image: new FormControl('', [Validators.required]),
      imageBlob: new FormControl('', [Validators.required]),
      unitKey: new FormControl('', [Validators.required]),
      price: new FormControl(0, [Validators.required]),
      sellingPrice: new FormControl(0, [Validators.required]),
    });
  }

  async initData() {
    console.log(this.value);
    this.value.dataCategories = [...this.categories];
    this.value.form.categoryKey = '';

    this.value.dataUnits = [...this.units];
    this.value.form.unitKey = '';
  }

  onChangeCategory(event: any) {}

  callbackUploaders(event: any) {
    console.log('event', event);
    if (event) {
      this.form.get('image').setValue(event.image);
      this.form.get('imageBlob').setValue(event.imageBlob);
    } else {
      this.form.get('image').setValue(null);
      this.form.get('imageBlob').setValue(null);
    }
  }

  callbackCategory(event: any) {
    console.log('event', event);
    if (event) {
      this.form.get('categoryKey').setValue(event);
    } else {
      this.form.get('categoryKey').setValue(null);
    }
  }

  callbackUnit(event: any) {
    console.log('event', event);
    if (event) {
      this.form.get('unitKey').setValue(event);
    } else {
      this.form.get('unitKey').setValue(null);
    }
  }

  async close(): Promise<any> {
    this._bsModalRef.hide();
  }

  async submit(): Promise<any> {
    const firebaseConfig: FirebaseConfigModel = {
      method: 'POST',
      storage: {
        active: true,
        path: `/apps/Database/business/${this.selectedBusiness.key}/goods/`,
        keyUpload: ['imageBlob'],
        keyResult: ['image'],
      },
      collection: {
        status: true,
        name: 'apps',
        document: {
          status: true,
          name: 'Database',
          collection: {
            status: true,
            name: 'business',
            document: {
              status: true,
              name: this.selectedBusiness.key,
              collection: {
                status: true,
                name: 'goods',
                document: {
                  status: false,
                },
                orderBy: {
                  object: 'created',
                  order: 'desc',
                },
                query: [],
              },
            },
          },
        },
      },
    };

    const businessKey = this.selectedBusiness.key;
    const qrCode =
      'goods-' + this._globalService.randomName('alphaNumeric', 32);

    const payload: Goods = {};
    payload.categoryKey = this.form.get('categoryKey').value;
    payload.barcode = qrCode;
    payload.businessKey = businessKey;
    payload.name = this.form.get('name').value;
    payload.image = this.form.get('image').value;
    payload.imageBlob = this.form.get('imageBlob').value;
    payload.unitKey = this.form.get('unitKey').value;
    payload.description = this.form.get('name').value;
    payload.price = this.form.get('price').value;
    payload.sellingPrice = this.form.get('sellingPrice').value;

    this.loading.isLoading = true;
    this._firebaseService.addCollection(null, firebaseConfig, payload).then(
      (result: any) => {
        this.loading.isLoading = false;
        this._globalAction.showToast({
          title: this._translateService.instant('Info!'),
          message: this._translateService.instant('Successfully added Goods!'),
          type: 'success',
        });
        this._bsModalRef.hide();
      },
      (error: any) => {
        console.log(error);
        this.loading.isLoading = false;
        this._globalAction.showToast({
          title: this._translateService.instant('Info!'),
          message: error,
          type: 'error',
        });
      }
    );
  }
}
