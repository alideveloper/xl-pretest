import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GoodsRoutingModule } from './goods-routing.module';

import { SharedModule } from '@app/shared';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { UtilsModule } from '@app/components/utils/utils.module';
import { NavigationsModule } from '@components/navigations/navigations.module';
import { AlertsModule } from '@app/components/alerts/alerts.module';
import { FormControlsModule } from '@app/components/form-controls/form-controls.module';
import { IconsModule } from '@app/components/icons/icons.module';

import { GoodsComponent } from './goods.component';
import { AddGoodsComponent } from './add/add.component';
import { EditGoodsComponent } from './edit/edit.component';
import { DeleteGoodsComponent } from './delete/delete.component';

import { FilterByPipe } from 'ngx-pipes';
import { SelectsModule } from '@app/components/select/select.module';
import { UploaderModule } from '@app/components/uploader/uploader.module';
import { DetailGoodsComponent } from './detail/detail.component';
import { EditImageGoodsComponent } from './edit-image/edit.component';

const modules = [
  UtilsModule,
  CommonModule,
  NavigationsModule,
  NgxDatatableModule,
  IconsModule,
  AlertsModule,
  FormControlsModule,
  SharedModule,
  SelectsModule,
  UploaderModule,
];

const declarations = [
  AddGoodsComponent,
  EditGoodsComponent,
  DeleteGoodsComponent,
  DetailGoodsComponent,
  EditImageGoodsComponent,
];

@NgModule({
  declarations: [GoodsComponent, ...declarations],
  imports: [GoodsRoutingModule, ...modules],
  entryComponents: [...declarations],
  providers: [FilterByPipe],
})
export class GoodsModule {}
