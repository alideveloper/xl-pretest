import { Component, OnInit } from '@angular/core';

import { FirebaseService } from '@firebase/firebase.service';

import { BsModalRef } from 'ngx-bootstrap/modal';

import { TranslateService } from '@ngx-translate/core';

import { GlobalAction } from '@app/shared/services/global.action';

import { FormGroup, FormControl, Validators } from '@angular/forms';

import { ngxLoadingAnimationTypes } from 'ngx-loading';
import { Uploader } from '@app/shared/types/uploader';
import { Loading } from '@app/shared/model/other/loading.model';
import { FirebaseConfigModel } from '@app/shared/model/other/firebase-config.model';
import { Goods } from '@app/shared/model/goods.model';

@Component({
  selector: 'app-edit-image-goods',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
})
export class EditImageGoodsComponent implements OnInit {
  loading: Loading = {
    isLoading: false,
    text: this._translateService.instant('Updated Image Goods') + '...',
    primaryColour: '',
    secondaryColour: '',
    animation: ngxLoadingAnimationTypes.threeBounce,
    image: 'assets/img/svg/save-person.svg',
  };

  value: any = {
    form: {
      image: {},
      imageBlob: {},
    },
  };

  numbersOnly = /^[0-9]*$/;

  form: FormGroup;
  formData: Goods;

  selectedBusiness: any;

  schemaUploader: Uploader = {
    component: 'uploader',
    position: 'body',
    label: {
      active: true,
      binding: false,
      text: 'Image',
      type: 'text',
      state: {
        hide: false,
        disabled: false,
        hideCondition: [],
        disabledCondition: [],
      },
    },
    labelSubtitle: {
      active: false,
    },
    parentBinding: 'form',
    bindingObject: 'image',
    bindingObjectBlob: 'imageBlob',
    accept: 'image/*',
    minsize: 0.01,
    maxsize: 10,
    isDisabled: false,
    animation: false,
    multiple: false,
    state: {
      hide: false,
      disabled: false,
      hideCondition: [],
      disabledCondition: [],
    },
    required: true,
    formText: {
      valid: {
        active: true,
        text: 'Data telah Sesuai!',
      },
      invalid: {
        active: true,
        text: 'Data Belum Sesuai',
      },
    },
  };

  constructor(
    private _bsModalRef: BsModalRef,
    private _translateService?: TranslateService,
    private _firebaseService?: FirebaseService,
    private _globalAction?: GlobalAction
  ) {}

  onChangeBusiness(event: any) {}

  async ngOnInit(): Promise<any> {
    this.form = new FormGroup({
      image: new FormControl('', [Validators.required]),
      imageBlob: new FormControl('', [Validators.required]),
    });
  }

  async close(): Promise<any> {
    this._bsModalRef.hide();
  }

  callbackUploaders(event: any) {
    console.log(event);
    if (event) {
      this.form.get('image').setValue(event.image);
      this.form.get('imageBlob').setValue(event.imageBlob);
    } else {
      this.form.get('image').setValue(null);
      this.form.get('imageBlob').setValue(null);
    }
  }

  async submit(): Promise<any> {
    const firebaseConfig: FirebaseConfigModel = {
      method: 'PUT',
      storage: {
        active: true,
        path: `/apps/Database/business/${this.selectedBusiness.key}/goods/`,
        keyUpload: ['imageBlob'],
        keyResult: ['image'],
        deletePath: '',
      },
      collection: {
        status: true,
        name: 'apps',
        document: {
          status: true,
          name: 'Database',
          collection: {
            status: true,
            name: 'business',
            document: {
              status: true,
              name: this.selectedBusiness.key,
              collection: {
                status: true,
                name: 'goods',
                document: {
                  status: false,
                },
                orderBy: {
                  object: 'created',
                  order: 'desc',
                },
                query: [],
              },
            },
          },
        },
      },
    };

    if (this.formData.image) {
      firebaseConfig.storage.deletePath = this.formData.image.fullPath;
    }

    const payload: Goods = {};
    payload.key = this.formData.key;
    payload.image = this.form.get('image').value;
    payload.imageBlob = this.form.get('imageBlob').value;

    this.loading.isLoading = true;
    this._firebaseService.updateCollection(null, firebaseConfig, payload).then(
      (result: any) => {
        this.loading.isLoading = false;
        this._globalAction.showToast({
          title: this._translateService.instant('Info!'),
          message: this._translateService.instant(
            'Successfully edit image Goods!'
          ),
          type: 'success',
        });
        this._bsModalRef.hide();
      },
      (error: any) => {
        console.log(error);
        this.loading.isLoading = false;
        this._globalAction.showToast({
          title: this._translateService.instant('Info!'),
          message: error,
          type: 'error',
        });
      }
    );
  }
}
