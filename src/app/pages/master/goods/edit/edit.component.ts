import { Component, OnInit } from '@angular/core';

import { FirebaseService } from '@firebase/firebase.service';

import { BsModalRef } from 'ngx-bootstrap/modal';

import { TranslateService } from '@ngx-translate/core';

import { GlobalAction } from '@app/shared/services/global.action';

import { ngxLoadingAnimationTypes } from 'ngx-loading';
import { Loading } from '@app/shared/model/other/loading.model';
import { FirebaseConfigModel } from '@app/shared/model/other/firebase-config.model';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Goods } from '@app/shared/model/goods.model';
import { GoodsCategory } from '@app/shared/model/goods-category.model';
import { GoodsUnit } from '@app/shared/model/goods-unit.model';

@Component({
  selector: 'app-edit-goods',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
})
export class EditGoodsComponent implements OnInit {
  loading: Loading = {
    isLoading: false,
    text: this._translateService.instant('Updated Goods') + '...',
    primaryColour: '',
    secondaryColour: '',
    animation: ngxLoadingAnimationTypes.threeBounce,
    image: 'assets/img/svg/save-person.svg',
  };

  selectedBusiness: any;

  formData: Goods;
  categories: GoodsCategory[];
  units: GoodsUnit[];

  form: FormGroup;

  value: any = {
    form: {},
  };

  schemaCategorySelect: any = {
    component: 'select',
    position: 'body',
    label: {
      active: true,
      binding: false,
      text: 'Category',
      type: 'text',
      state: {
        hide: false,
        disabled: false,
        hideCondition: [],
        disabledCondition: [],
      },
    },
    labelSubtitle: {
      active: false,
    },
    type: 'text',
    placeholder: 'Select Category',
    parentBinding: 'form',
    bindingObject: 'categoryKey',
    primaryKey: 'key',
    bindingData: 'dataCategories',
    labelForId: 'name',
    bindLabel: 'name',
    bindValue: 'key',
    searchable: true,
    search: true,
    closeOnSelect: true,
    defaultValue: false,
    customSearch: true,
    bindingSearch: 'dataCategories',
    required: true,
    formText: {
      valid: {
        active: true,
        text: 'Data telah Sesuai!',
      },
      invalid: {
        active: true,
        text: 'Data tidak Sesuai!',
      },
    },
  };

  schemaUnitSelect: any = {
    component: 'select',
    position: 'body',
    label: {
      active: true,
      binding: false,
      text: 'Unit',
      type: 'text',
      state: {
        hide: false,
        disabled: false,
        hideCondition: [],
        disabledCondition: [],
      },
    },
    labelSubtitle: {
      active: false,
    },
    type: 'text',
    placeholder: 'Select Unit',
    parentBinding: 'form',
    bindingObject: 'unitKey',
    primaryKey: 'key',
    bindingData: 'dataUnits',
    labelForId: 'name',
    bindLabel: 'name',
    bindValue: 'key',
    searchable: true,
    search: true,
    closeOnSelect: true,
    defaultValue: false,
    customSearch: true,
    bindingSearch: 'dataUnits',
    required: true,
    formText: {
      valid: {
        active: true,
        text: 'Data telah Sesuai!',
      },
      invalid: {
        active: true,
        text: 'Data tidak Sesuai!',
      },
    },
  };

  constructor(
    private _bsModalRef: BsModalRef,
    private _translateService?: TranslateService,
    private _firebaseService?: FirebaseService,
    private _globalAction?: GlobalAction
  ) {}

  onChangeCategory(event: any) {}

  async ngOnInit(): Promise<any> {
    console.log(this.formData, 'form');
    if (this.formData) {
      this.initForm();
    }
    this.initData();
  }

  initForm() {
    this.form = new FormGroup({
      name: new FormControl(this.formData.name, [
        Validators.required,
        Validators.minLength(3),
      ]),
      description: new FormControl(this.formData.description, []),
      categoryKey: new FormControl(this.formData.categoryKey, [
        Validators.required,
      ]),
      unitKey: new FormControl(this.formData.unitKey, [Validators.required]),
      price: new FormControl(this.formData.price, [Validators.required]),
      sellingPrice: new FormControl(this.formData.sellingPrice, [
        Validators.required,
      ]),
    });

    this.value.form = Object.assign({}, this.formData);
  }

  async initData() {
    console.log(this.value);
    this.value.dataCategories = [...this.categories];

    this.value.dataUnits = [...this.units];
  }

  async close(): Promise<any> {
    this._bsModalRef.hide();
  }

  async submit(): Promise<any> {
    const firebaseConfig: FirebaseConfigModel = {
      method: 'PUT',
      collection: {
        status: true,
        name: 'apps',
        document: {
          status: true,
          name: 'Database',
          collection: {
            status: true,
            name: 'business',
            document: {
              status: true,
              name: this.selectedBusiness.key,
              collection: {
                status: true,
                name: 'goods',
                document: {
                  status: false,
                },
                orderBy: {
                  object: 'created',
                  order: 'desc',
                },
                query: [],
              },
            },
          },
        },
      },
    };

    const payload: Goods = {};
    payload.key = this.formData.key;
    payload.categoryKey = this.form.get('categoryKey').value;
    payload.unitKey = this.form.get('unitKey').value;
    payload.name = this.form.get('name').value;
    payload.description = this.form.get('description').value;
    payload.price = this.form.get('price').value;
    payload.sellingPrice = this.form.get('sellingPrice').value;

    this.loading.isLoading = true;
    this._firebaseService.updateCollection(null, firebaseConfig, payload).then(
      (result: any) => {
        this.loading.isLoading = false;
        this._globalAction.showToast({
          title: this._translateService.instant('Info!'),
          message: this._translateService.instant(
            'Successfully updated Goods!'
          ),
          type: 'success',
        });
        this._bsModalRef.hide();
      },
      (error: any) => {
        console.log(error);
        this.loading.isLoading = false;
        this._globalAction.showToast({
          title: this._translateService.instant('Info!'),
          message: error,
          type: 'error',
        });
      }
    );
  }

  callbackCategory(event: any) {
    console.log('event', event);
    if (event) {
      this.form.get('categoryKey').setValue(event);
    } else {
      this.form.get('categoryKey').setValue(null);
    }
  }

  callbackUnit(event: any) {
    console.log('event', event);
    if (event) {
      this.form.get('unitKey').setValue(event);
    } else {
      this.form.get('unitKey').setValue(null);
    }
  }
}
