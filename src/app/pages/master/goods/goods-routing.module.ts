import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GoodsComponent } from './goods.component';

import { extract } from '@app/core';

const routes: Routes = [
  {
    path: '',
    component: GoodsComponent,
    data: { title: extract('Goods') },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GoodsRoutingModule {}
