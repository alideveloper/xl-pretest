import { NgModule } from '@angular/core';
import { SharedModule } from '@app/shared';
import { UtilsModule } from '@app/components/utils';

import { HomeRoutingModule } from '@pages/home/home-routing.module';
import { HomeComponent } from '@pages/home/components/home/home.component';
import { WizardsModule } from '@app/components/wizards/wizards.module';
import { WelcomeComponent } from '@pages/home/components/welcome/welcome.component';
import { GetStartedComponent } from '@pages/home/components/get-started/get-started.component';
import { HomeStepComponent } from '@pages/home/components/home-step/home-step.component';

import { SkipService } from '@pages/home/services/skip.service';

@NgModule({
  imports: [SharedModule, UtilsModule, WizardsModule, HomeRoutingModule],
  declarations: [
    HomeComponent,
    WelcomeComponent,
    HomeStepComponent,
    GetStartedComponent,
  ],
  providers: [SkipService],
})
export class HomeModule {}
