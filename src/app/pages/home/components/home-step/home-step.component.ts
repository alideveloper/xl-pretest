import { Component, OnInit, Input } from '@angular/core';
import { StepModel } from '@pages/home/models/step';

@Component({
  selector: 'app-home-step',
  templateUrl: './home-step.component.html',
  styleUrls: ['./home-step.component.scss'],
})
export class HomeStepComponent implements OnInit {
  @Input() step: StepModel;

  constructor() {}

  ngOnInit() {}
}
