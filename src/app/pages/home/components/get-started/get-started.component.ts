import { Component, OnInit, Input } from '@angular/core';

import { StepModel } from '@pages/home/models/step';

@Component({
  selector: 'app-get-started',
  templateUrl: './get-started.component.html',
  styleUrls: ['./get-started.component.scss'],
})
export class GetStartedComponent implements OnInit {
  @Input() steps: StepModel;

  constructor() {}

  ngOnInit() {}
}
