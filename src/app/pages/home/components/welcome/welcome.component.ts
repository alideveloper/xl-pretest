import { Component, OnInit, Input } from '@angular/core';
import { Site } from '@app/shared/model/panel/site.model';
import { Content } from '@app/shared/model/panel/content.model';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss'],
})
export class WelcomeComponent implements OnInit {
  @Input() starter: Content;
  @Input() site: Site;

  constructor() {}

  ngOnInit() {}
}
