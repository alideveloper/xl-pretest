import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { StepModel } from '@pages/home/models/step';
import { SkipService } from '@pages/home/services/skip.service';
import { RedirectService } from '@app/core/services/redirect.service';

import { environment } from '@environments/environment';

import { FirebaseService } from '@app/@firebase/firebase.service';
import { FirebaseConfigModel } from '@app/shared/model/other/firebase-config.model';
import { Category } from '@app/shared/model/panel/category.model';
import { Content } from '@app/shared/model/panel/content.model';
import { Site } from '@app/shared/model/panel/site.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  categoryStarter: Category;
  steps: StepModel[] = [
    {
      title: 'Manage Stock In and Out',
      description: 'Stock In and Out Stock More controlled',
      image: 'assets/img/home1.png',
    },
    {
      title: 'Multiple Warehouse',
      description:
        'Manage Stock from Various Warehouses and Transfer Stock to Other Warehouses',
      image: 'assets/img/home2.png',
    },
    {
      title: 'Multiple Business',
      description: 'Support Manage Inventory of Various Businesses',
      image: 'assets/img/home3.png',
    },
    {
      title: 'Reporting',
      description: 'Complete Reporting Features Support PDF & Excel',
      image: 'assets/img/home4.png',
    },
  ];
  starter: Content = {
    title: 'Manage Inventory',
    description:
      'Manage your stock in your warehouse with this inventory management application.',
  };

  site: Site;

  showOnStartupCheckbox: any;

  showOnStartup: any;

  redirectURL = environment.indexURL.root + environment.indexURL.homeTwo;

  constructor(
    private router: Router,
    private _redirect: RedirectService,
    private _firebaseService: FirebaseService
  ) {
    // Skipping the home in case the user has selected to skip it always
    const role = localStorage.getItem(environment.domain + '-LoginRole');
    if (role === 'Staff') {
      this.router.navigate([this.redirectURL], { replaceUrl: true });
      return;
    }

    this.showOnStartup = localStorage.getItem(
      environment.domain + '-showOnStartup'
    );
    if (!this.showOnStartup) {
      this.showOnStartup = false;
      localStorage.setItem(environment.domain + '-showOnStartup', 'active');
    }

    if (this.showOnStartup === 'active') {
      this.showOnStartup = true;
    }

    if (this.showOnStartup === 'nonActive') {
      this.showOnStartup = false;
    }

    if (this.showOnStartup === 'skip') {
      this.router.navigate([this.redirectURL], { replaceUrl: true });
    }
  }

  ngOnInit() {
    this.initData();
  }

  Skip() {
    if (!this.showOnStartupCheckbox) {
      localStorage.setItem(environment.domain + '-showOnStartup', 'skip');
    } else {
      localStorage.setItem(environment.domain + '-showOnStartup', 'active');
    }
    this.router.navigate([this.redirectURL], { replaceUrl: true });
  }

  redirect(params: Params) {
    if (params.redirect) {
      this._redirect.to(params.redirect, { replaceUrl: true });
    } else {
      this._redirect.to(this.redirectURL);
    }
  }

  async initData() {
    this.site = JSON.parse(localStorage.getItem(environment.domain + '-site'));
  }

  onChangeCheckbox(event: any) {
    if (event) {
      this.showOnStartup = true;
      localStorage.setItem(environment.domain + '-showOnStartup', 'active');
    } else {
      this.showOnStartup = false;
      localStorage.setItem(environment.domain + '-showOnStartup', 'nonActive');
    }
  }
}
