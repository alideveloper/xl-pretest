export interface StepModel {
  title: string;
  description: string;
  image: string;
  path?: string;
}
