import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccountingRoutingModule } from './accounting-routing.module';

import { SharedModule } from '@app/shared';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { UtilsModule } from '@app/components/utils/utils.module';
import { NavigationsModule } from '@components/navigations/navigations.module';
import { AlertsModule } from '@app/components/alerts/alerts.module';
import { FormControlsModule } from '@app/components/form-controls/form-controls.module';
import { IconsModule } from '@app/components/icons/icons.module';
import { MapsModule } from '@app/components/maps/maps.module';
import { UploaderModule } from '@app/components/uploader/uploader.module';
import { SelectBusinessModule } from '@app/components/select-business/select-business.module';

import { AccountingComponent } from './accounting.component';
import { EditAccountingComponent } from './edit/edit.component';

import { FilterByPipe } from 'ngx-pipes';

const modules = [
  UtilsModule,
  CommonModule,
  NavigationsModule,
  NgxDatatableModule,
  IconsModule,
  AlertsModule,
  FormControlsModule,
  SharedModule,
  MapsModule,
  UploaderModule,
  SelectBusinessModule,
];

@NgModule({
  declarations: [AccountingComponent, EditAccountingComponent],
  imports: [AccountingRoutingModule, ...modules],
  entryComponents: [EditAccountingComponent],
  providers: [FilterByPipe],
})
export class AccountingModule {}
