import { Component, OnInit } from '@angular/core';

import { FirebaseService } from '@firebase/firebase.service';

import { BsModalRef } from 'ngx-bootstrap/modal';

import { TranslateService } from '@ngx-translate/core';

import { GlobalAction } from '@app/shared/services/global.action';

import { ngxLoadingAnimationTypes } from 'ngx-loading';

import { FormGroup, FormControl, Validators } from '@angular/forms';
import { environment } from '@environments/environment';
import { pairwise } from 'rxjs/operators';
import { AppService } from '@app/app.service';
import { AppData } from '@app/app.data';
import { User } from '@app/shared/model/user.model';
import { Business } from '@app/shared/model/business.model';
import { FirebaseConfigModel } from '@app/shared/model/other/firebase-config.model';

@Component({
  selector: 'app-edit-accounting',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
})
export class EditAccountingComponent implements OnInit {
  loading = {
    isLoading: false,
    text: this._translateService.instant('Updated Format') + '...',
    primaryColour: '',
    secondaryColour: '',
    animation: ngxLoadingAnimationTypes.threeBounce,
    image: 'assets/img/svg/save-person.svg',
  };

  form: FormGroup;
  formData: User | Business;

  value: any = {
    form: {},
  };

  selectedBusinessValue: any;

  constructor(
    private _bsModalRef: BsModalRef,
    private _translateService?: TranslateService,
    private _firebaseService?: FirebaseService,
    private _globalAction?: GlobalAction,
    private _appData?: AppData,
    private _appService?: AppService
  ) {}

  async ngOnInit(): Promise<any> {
    console.log(this.formData, 'form');
    if (this.formData) {
      this.initForm();
      this.changesPrecision();
    }
  }

  initForm() {
    const currency = Object.assign({}, this.formData.currency);
    this.form = new FormGroup({
      align: new FormControl(currency.align, [Validators.required]),
      precision: new FormControl(currency.precision, [Validators.required]),
      decimal: new FormControl(currency.decimal, [Validators.required]),
      prefix: new FormControl(currency.prefix, [Validators.required]),
      suffix: new FormControl(currency.suffix),
      thousands: new FormControl(currency.thousands, [Validators.required]),
    });
  }

  changesPrecision() {
    this.form
      .get('precision')
      .valueChanges.pipe(pairwise())
      .subscribe(([prev, next]: [any, any]) => {
        console.log(prev, next);
        if (next > 5) {
          this.form.get('precision').setValue(5);
        }
      });
  }

  async close(): Promise<any> {
    this._bsModalRef.hide();
  }

  async submit(): Promise<any> {
    console.log(this.formData);
    let firebaseConfig: FirebaseConfigModel;
    if (this.selectedBusinessValue === 'Personal') {
      console.log('MASUK');
      firebaseConfig = {
        method: 'PUT',
        collection: {
          status: true,
          name: 'globals',
          document: {
            status: true,
            name: 'Users',
            collection: {
              status: true,
              name: 'users',
              document: {
                status: false,
                name: null,
                document: null,
              },
              orderBy: {
                object: 'created',
                order: 'desc',
              },
              query: [],
            },
          },
        },
      };
    } else {
      firebaseConfig = {
        method: 'PUT',
        collection: {
          status: true,
          name: 'apps',
          document: {
            status: true,
            name: 'Database',
            collection: {
              status: true,
              name: 'business',
              document: {
                status: false,
                name: null,
                document: null,
              },
              orderBy: {
                object: 'created',
                order: 'desc',
              },
              query: [],
            },
          },
        },
      };
    }

    const payload: User | Business = {};
    payload.key = this.formData.key;
    payload.currency.align = this.form.get('align').value;
    payload.currency.precision = Number(this.form.get('precision').value);
    payload.currency.decimal = this.form.get('decimal').value;
    payload.currency.prefix = this.form.get('prefix').value;
    payload.currency.suffix = this.form.get('suffix').value;
    payload.currency.thousands = this.form.get('thousands').value;

    console.log('payload', payload);
    this.formData.currency.align = this.form.get('align').value;
    this.formData.currency.precision = Number(this.form.get('precision').value);
    this.formData.currency.decimal = this.form.get('decimal').value;
    this.formData.currency.prefix = this.form.get('prefix').value;
    this.formData.currency.suffix = this.form.get('suffix').value;
    this.formData.currency.thousands = this.form.get('thousands').value;

    const payloadNew = Object.assign({}, this.formData);
    this.loading.isLoading = true;
    this._firebaseService.updateCollection(null, firebaseConfig, payload).then(
      (result: any) => {
        console.log('result');
        const accounting = JSON.parse(
          localStorage.getItem(environment.domain + '-selectedBusiness')
        );
        console.log(payload);
        console.log(accounting);
        if (payload.key === accounting.key) {
          console.log('Sama');

          this._appData.selectedBusiness = payload;
          this._appService.onDataChanges.next(this._appData);
          localStorage.setItem(
            environment.domain + '-selectedBusiness',
            JSON.stringify(payloadNew)
          );
        }

        if (this.selectedBusinessValue === 'Personal') {
          localStorage.setItem(
            environment.domain + '-Login',
            JSON.stringify(payloadNew)
          );
        }

        this.loading.isLoading = false;
        this._globalAction.showToast({
          title: this._translateService.instant('Info!'),
          message: this._translateService.instant(
            'Successfully updated Format!'
          ),
          type: 'success',
        });
        this._bsModalRef.hide();
      },
      (error: any) => {
        console.log(error);
        this.loading.isLoading = false;
        this._globalAction.showToast({
          title: this._translateService.instant('Info!'),
          message: error,
          type: 'error',
        });
      }
    );
  }
}
