import {
  Component,
  OnInit,
  ChangeDetectorRef,
  ViewChild,
  OnDestroy,
} from '@angular/core';
import { environment } from '@environments/environment';
import { FirebaseService } from '@app/@firebase/firebase.service';

import { DatatableComponent } from '@swimlane/ngx-datatable';

import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { EditAccountingComponent } from './edit/edit.component';

import { BaseComponent } from '@app/core';
import { AccountingPipe } from '@app/shared/pipes/accounting.pipe';
import { AppService } from '@app/app.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Currency } from '@app/shared/model/other/currency.model';
import { User } from '@app/shared/model/user.model';
import { Business } from '@app/shared/model/business.model';
import {
  Query,
  FirebaseConfigModel,
} from '@app/shared/model/other/firebase-config.model';
import { GlobalAction } from '@app/shared/services/global.action';

@Component({
  selector: 'app-accounting',
  templateUrl: './accounting.component.html',
  styleUrls: ['./accounting.component.scss'],
  providers: [AccountingPipe],
})
export class AccountingComponent extends BaseComponent
  implements OnInit, OnDestroy {
  @ViewChild(DatatableComponent, { static: false })
  table: DatatableComponent;

  redirectURL = environment.indexURL.root + environment.indexURL.homeTwo;

  messages: any = {
    emptyMessage:
      '<div class="empty-container"><img src="assets/img/history-accounting.png"><div class="empty-table">No results</div></div>',
  };

  timeout: any;

  isCard = true;

  selectedMessage: any;
  selectedBusinessValue: any;
  accounting: User | Business;
  currency: Currency;

  modalRef: BsModalRef;

  private _unsubscribeAll: Subject<any>;

  constructor(
    private _modalService: BsModalService,
    private _firebaseService: FirebaseService,
    private _cdRef: ChangeDetectorRef,
    private _appService?: AppService,
    private _globalAction?: GlobalAction
  ) {
    super();
  }

  async ngOnInit() {
    await this.initRouting();

    this._unsubscribeAll = new Subject();

    this.onChangeAccounting();

    this._appService.onDataChanges
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(async (data) => {
        console.log('on changes');
        console.log(data.selectedBusiness);
        console.log(this.accounting);
        if (data.selectedBusinessValue !== this.selectedBusinessValue) {
          if (data.selectedBusinessValue) {
            this.selectedBusinessValue = data.selectedBusinessValue;
          }
        }
        if (
          data.selectedBusiness !== this.accounting ||
          (this.accounting &&
            data.selectedBusiness.currency !== this.accounting.currency)
        ) {
          console.log('MASUK');
          await this.initData();
        }
      });
  }

  async ngOnDestroy() {
    try {
      this._unsubscribeAll.next();
      this._unsubscribeAll.complete();
    } catch (error) {}
  }

  async initRouting() {
    this._globalAction.isEmptyBusiness();
  }

  async initData() {
    await this.getAccounting();
  }

  async getAccounting(): Promise<any> {
    const where: Query[] = [
      {
        object: 'key',
        condition: '==',
        value: JSON.parse(
          localStorage.getItem(environment.domain + '-selectedBusiness')
        ).key,
      },
    ];

    return new Promise(async (resolve, reject) => {
      let configFirebase: FirebaseConfigModel;
      console.log('selectedBusinessValue', this.selectedBusinessValue);
      if (this.selectedBusinessValue === 'Personal') {
        where[0].object = 'key';
        configFirebase = {
          collection: {
            status: true,
            name: 'globals',
            document: {
              status: true,
              name: 'Users',
              collection: {
                status: true,
                name: 'users',
                document: {
                  status: false,
                  name: null,
                  document: null,
                },
                orderBy: {
                  object: 'created',
                  order: 'desc',
                },
                query: [...where],
              },
            },
          },
        };
      } else {
        where[0].object = 'userKey';
        configFirebase = {
          collection: {
            status: true,
            name: 'apps',
            document: {
              status: true,
              name: 'Database',
              collection: {
                status: true,
                name: 'business',
                document: {
                  status: false,
                  name: null,
                  document: null,
                },
                orderBy: {
                  object: 'created',
                  order: 'desc',
                },
                query: [...where],
              },
            },
          },
        };
      }

      console.log('configFirebase', configFirebase);
      const data: any = await this._firebaseService.handleCollection(
        null,
        configFirebase
      );
      data.subscribe((response: any) => {
        if (response) {
          console.log('response', response);
          this.accounting = Object.assign({}, response[0]);
          this.currency = Object.assign({}, this.accounting.currency);
          this._cdRef.detectChanges();
          resolve(response);
          return;
        }
      });
    });
  }

  editAccounting(value: any): void {
    const navParam = {
      initialState: {
        formData: value,
        selectedBusinessValue: this.selectedBusinessValue,
      },
      class: 'modal-md modal-dialog-centered',
    };

    console.log(navParam, 'navParam');
    this.modalRef = this._modalService.show(EditAccountingComponent, navParam);
  }

  onChangeAccounting() {
    this.accounting = Object.assign(
      {},
      JSON.parse(localStorage.getItem(environment.domain + '-selectedBusiness'))
    );
    this.currency = Object.assign({}, this.accounting.currency);
    this.selectedBusinessValue = localStorage.getItem(
      environment.domain + '-selectedBusinessValue'
    );
  }
}
