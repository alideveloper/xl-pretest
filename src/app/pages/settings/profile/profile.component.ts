import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { environment } from '@environments/environment';
import { FirebaseService } from '@app/@firebase/firebase.service';

import { DatatableComponent } from '@swimlane/ngx-datatable';

import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { EditProfileComponent } from './edit/edit.component';

import { BaseComponent } from '@app/core';
import { EditPhotoProfileComponent } from './edit-image/edit.component';
import { GlobalAction } from '@app/shared/services/global.action';
import { LoadingService } from '@app/shared/services/loading.service';
import { TranslateService } from '@ngx-translate/core';
import { User } from '@app/shared/model/user.model';
import {
  Query,
  FirebaseConfigModel,
} from '@app/shared/model/other/firebase-config.model';
import { Site } from '@app/shared/model/panel/site.model';
import { EditAddressedComponent } from './edit-addressed/edit-addressed.component';
import { Router } from '@angular/router';
import { Staff } from '@app/shared/model/staff.model';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent extends BaseComponent implements OnInit {
  @ViewChild(DatatableComponent, { static: false })
  table: DatatableComponent;

  redirectURL = environment.indexURL.root + environment.indexURL.homeTwo;

  messages: any = {
    emptyMessage:
      '<div class="empty-container"><img src="assets/img/history-accounting.png"><div class="empty-table">No results</div></div>',
  };

  timeout: any;

  isCard = true;

  value: any = {
    form: '',
  };

  selectedMessage: any;

  dayLeft: any;
  isTrialAccount: any;

  isStaff: boolean;

  user: User | Staff | any;

  modalRef: BsModalRef;

  schemaMaps: any = {
    component: 'maps',
    position: 'body',
    style: {},
    styleClass: 'maps-md',
    isView: true,
    label: {
      active: false,
    },
    labelSubtitle: {
      active: false,
    },
    search: {
      active: false,
      placeholder: 'Search Address',
    },
    address: {
      active: true,
      bindingObject: 'address',
    },
    zoom: 13,
    minZoom: 5,
    maxZoom: 20,
    parentBinding: 'form',
    latitude: 'latitude',
    longitude: 'longitude',
    callbackMaps: {
      latitude: 'latitude',
      longitude: 'longitude',
      accuracy: 'accuracy',
      altitude: 'altitude',
      altitudeAccuracy: 'altitudeAccuracy',
      heading: 'heading',
      speed: 'speed',
    },
  };

  constructor(
    private _modalService: BsModalService,
    private _firebaseService: FirebaseService,
    private _cdRef: ChangeDetectorRef,
    private _loadingService?: LoadingService,
    private _globalAction?: GlobalAction,
    private _translateService?: TranslateService,
    private _router?: Router
  ) {
    super();
  }

  async ngOnInit() {
    await this.initRouting();
    this.initData();
  }

  async initRouting() {
    this._globalAction.submenuRouting('/settings/profile');
  }

  initData() {
    this.getProfile();
  }

  async getProfileStaff(): Promise<any> {
    const user: Staff = JSON.parse(
      localStorage.getItem(environment.domain + '-LoginStaff')
    );
    const where: Query[] = [
      {
        object: 'key',
        condition: '==',
        value: user.key,
      },
    ];

    this.value.form = Object.assign({}, user);

    return new Promise(async (resolve, reject) => {
      const configFirebase: FirebaseConfigModel = {
        collection: {
          status: true,
          name: 'apps',
          document: {
            status: true,
            name: environment.projectId,
            collection: {
              status: true,
              name: 'staffs',
              document: {
                status: false,
                name: null,
                document: null,
              },
              orderBy: {
                object: 'created',
                order: 'desc',
              },
              query: [...where],
            },
          },
        },
      };

      console.log('configFirebase', configFirebase);
      const data: any = await this._firebaseService.handleCollection(
        null,
        configFirebase
      );
      data.subscribe((response: User[]) => {
        if (response) {
          console.log('response', response);
          this.user = Object.assign({}, response[0]);

          localStorage.setItem(
            environment.domain + '-LoginStaff',
            JSON.stringify(response[0])
          );
          this._cdRef.detectChanges();
          resolve(response);
          return;
        }
      });
    });
  }

  async getProfile(): Promise<any> {
    const user: Site = JSON.parse(
      localStorage.getItem(environment.domain + '-Login')
    );
    const where: Query[] = [
      {
        object: 'key',
        condition: '==',
        value: user.key,
      },
    ];

    this.value.form = Object.assign({}, user);

    return new Promise(async (resolve, reject) => {
      const configFirebase: FirebaseConfigModel = {
        collection: {
          status: true,
          name: 'globals',
          document: {
            status: true,
            name: 'Users',
            collection: {
              status: true,
              name: 'users',
              document: {
                status: false,
                name: null,
                document: null,
              },
              orderBy: {
                object: 'created',
                order: 'desc',
              },
              query: [...where],
            },
          },
        },
      };

      console.log('configFirebase', configFirebase);
      const data: any = await this._firebaseService.handleCollection(
        null,
        configFirebase
      );
      data.subscribe((response: User[]) => {
        if (response) {
          console.log('response', response);
          this.user = Object.assign({}, response[0]);

          localStorage.setItem(
            environment.domain + '-Login',
            JSON.stringify(response[0])
          );
          this._cdRef.detectChanges();
          resolve(response);
          return;
        }
      });
    });
  }

  editPersonalInfo(value: any): void {
    const navParam = {
      initialState: {
        formData: value,
      },
      class: 'modal-md modal-dialog-centered',
    };

    console.log(navParam, 'navParam');
    this.modalRef = this._modalService.show(EditProfileComponent, navParam);
  }

  editAddressed(value: any): void {
    const navParam = {
      initialState: {
        formData: value,
      },
      class: 'modal-md modal-dialog-centered',
    };

    console.log(navParam, 'navParam');
    this.modalRef = this._modalService.show(EditAddressedComponent, navParam);
  }

  editPhotoProfile(value: any): void {
    const navParam = {
      initialState: {
        formData: value,
      },
      class: 'modal-md modal-dialog-centered',
    };

    console.log(navParam, 'navParam');
    this.modalRef = this._modalService.show(
      EditPhotoProfileComponent,
      navParam
    );
  }

  async resetPassword(email: any) {
    this._globalAction.showToast({
      title: 'Info',
      type: 'error',
      message: await this._translateService.instant(
        'To Reset Pin, Please Contact Your Supervisor!'
      ),
    });
  }

  callback(event: any) {
    console.log('event', event);
    if (event) {
      this.getProfile();
    }
  }

  upgradeBusiness() {
    this._router.navigateByUrl('/upgrade');
  }

  goToBilling() {}
}
