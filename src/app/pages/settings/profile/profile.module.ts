import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfileRoutingModule } from './profile-routing.module';

import { SharedModule } from '@app/shared';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { UtilsModule } from '@app/components/utils/utils.module';
import { NavigationsModule } from '@components/navigations/navigations.module';
import { AlertsModule } from '@app/components/alerts/alerts.module';
import { FormControlsModule } from '@app/components/form-controls/form-controls.module';
import { IconsModule } from '@app/components/icons/icons.module';
import { MapsModule } from '@app/components/maps/maps.module';
import { UploaderModule } from '@app/components/uploader/uploader.module';
import { SelectCountryModule } from '@app/components/select-country/select-country.component.module';

import { ProfileComponent } from './profile.component';
import { EditProfileComponent } from './edit/edit.component';
import { EditPhotoProfileComponent } from './edit-image/edit.component';
import { EditAddressedComponent } from './edit-addressed/edit-addressed.component';

import { FilterByPipe } from 'ngx-pipes';

const modules = [
  UtilsModule,
  CommonModule,
  NavigationsModule,
  NgxDatatableModule,
  IconsModule,
  AlertsModule,
  FormControlsModule,
  SharedModule,
  MapsModule,
  UploaderModule,
  SelectCountryModule,
];

const declarations = [
  EditProfileComponent,
  EditPhotoProfileComponent,
  EditAddressedComponent,
];

@NgModule({
  declarations: [ProfileComponent, ...declarations],
  imports: [ProfileRoutingModule, ...modules],
  entryComponents: [...declarations],
  providers: [FilterByPipe],
})
export class ProfileModule {}
