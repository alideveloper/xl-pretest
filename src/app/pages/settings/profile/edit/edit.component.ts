import { Component, OnInit } from '@angular/core';

import { FirebaseService } from '@firebase/firebase.service';

import { BsModalRef } from 'ngx-bootstrap/modal';

import { TranslateService } from '@ngx-translate/core';

import { GlobalAction } from '@app/shared/services/global.action';

import { ngxLoadingAnimationTypes } from 'ngx-loading';

import { FormGroup, FormControl, Validators } from '@angular/forms';
import { User } from '@app/shared/model/user.model';
import { Loading } from '@app/shared/model/other/loading.model';
import { FirebaseConfigModel } from '@app/shared/model/other/firebase-config.model';
import { environment } from '@environments/environment';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
})
export class EditProfileComponent implements OnInit {
  loading: Loading = {
    isLoading: false,
    text: this._translateService.instant('Updated Personal Info') + '...',
    primaryColour: '',
    secondaryColour: '',
    animation: ngxLoadingAnimationTypes.threeBounce,
    image: 'assets/img/svg/save-person.svg',
  };

  form: FormGroup;
  formData: User;

  value: any = {
    form: {},
  };

  isStaff: boolean;

  constructor(
    private _bsModalRef: BsModalRef,
    private _translateService?: TranslateService,
    private _firebaseService?: FirebaseService,
    private _globalAction?: GlobalAction
  ) {}

  async ngOnInit(): Promise<any> {
    console.log(this.formData, 'form');
    if (this.formData) {
      this.initForm();
    }
  }

  initForm() {
    this.form = new FormGroup({
      displayName: new FormControl(this.formData.displayName, [
        Validators.required,
        Validators.minLength(3),
      ]),
      phoneNumber: new FormControl(this.formData.phoneNumber, [
        Validators.required,
        Validators.minLength(11),
      ]),
    });
  }

  async close(): Promise<any> {
    this._bsModalRef.hide();
  }

  async submit(): Promise<any> {
    console.log(this.formData);
    const firebaseConfig: FirebaseConfigModel = {
      method: 'PUT',
      collection: {
        status: true,
        name: 'globals',
        document: {
          status: true,
          name: 'Users',
          collection: {
            status: true,
            name: 'users',
            document: {
              status: false,
              name: null,
              document: null,
            },
            orderBy: {
              object: 'created',
              order: 'desc',
            },
            query: [],
          },
        },
      },
    };

    const payload: User = {};
    payload.key = this.formData.key;
    payload.displayName = this.form.get('displayName').value;
    payload.phoneNumber = this.form.get('phoneNumber').value;

    console.log('payload', payload);
    this.loading.isLoading = true;
    this._firebaseService.updateCollection(null, firebaseConfig, payload).then(
      (result: any) => {
        console.log('result', result);
        this.loading.isLoading = false;
        this._globalAction.showToast({
          title: this._translateService.instant('Info!'),
          message: this._translateService.instant(
            'Successfully updated Personal Info!'
          ),
          type: 'success',
        });
        this._bsModalRef.hide();
      },
      (error: any) => {
        console.log(error);
        this.loading.isLoading = false;
        this._globalAction.showToast({
          title: this._translateService.instant('Info!'),
          message: error,
          type: 'error',
        });
      }
    );
  }
}
