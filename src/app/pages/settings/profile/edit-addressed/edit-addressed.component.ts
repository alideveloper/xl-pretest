import { Component, OnInit } from '@angular/core';

import { FirebaseService } from '@firebase/firebase.service';

import { BsModalRef } from 'ngx-bootstrap/modal';

import { TranslateService } from '@ngx-translate/core';

import { GlobalAction } from '@app/shared/services/global.action';

import { ngxLoadingAnimationTypes } from 'ngx-loading';

import { FormGroup, FormControl, Validators } from '@angular/forms';
import { User } from '@app/shared/model/user.model';
import { Loading } from '@app/shared/model/other/loading.model';
import { FirebaseConfigModel } from '@app/shared/model/other/firebase-config.model';
import { environment } from '@environments/environment';

@Component({
  selector: 'app-edit-addressed',
  templateUrl: './edit-addressed.component.html',
  styleUrls: ['./edit-addressed.component.scss'],
})
export class EditAddressedComponent implements OnInit {
  loading: Loading = {
    isLoading: false,
    text: this._translateService.instant('Updated Addressed') + '...',
    primaryColour: '',
    secondaryColour: '',
    animation: ngxLoadingAnimationTypes.threeBounce,
    image: 'assets/img/svg/save-person.svg',
  };

  form: FormGroup;
  formData: User;

  value: any = {
    form: {},
  };

  schemaMaps: any = {
    component: 'maps',
    position: 'body',
    style: {},
    styleClass: 'maps-md',
    label: {
      active: true,
      binding: false,
      style: {},
      styleClass: '',
      text: 'Select Maps',
      type: 'text',
      state: {
        hide: false,
        disabled: false,
        hideCondition: [],
        disabledCondition: [],
      },
    },
    labelSubtitle: {
      active: false,
    },
    search: {
      active: true,
      placeholder: 'Search Address',
    },
    address: {
      active: true,
      bindingObject: 'address',
    },
    zoom: 13,
    minZoom: 5,
    maxZoom: 20,
    parentBinding: 'form',
    latitude: 'latitude',
    longitude: 'longitude',
    callbackMaps: {
      latitude: 'latitude',
      longitude: 'longitude',
      accuracy: 'accuracy',
      altitude: 'altitude',
      altitudeAccuracy: 'altitudeAccuracy',
      heading: 'heading',
      speed: 'speed',
    },
  };

  isStaff: boolean;

  constructor(
    private _bsModalRef: BsModalRef,
    private _translateService?: TranslateService,
    private _firebaseService?: FirebaseService,
    private _globalAction?: GlobalAction
  ) {}

  async ngOnInit(): Promise<any> {
    console.log(this.formData, 'form');
    if (this.formData) {
      this.initForm();
    }
  }

  initForm() {
    this.form = new FormGroup({
      address: new FormControl(this.formData.address, [
        Validators.required,
        Validators.minLength(3),
      ]),
      addressDetail: new FormControl(this.formData.addressDetail, [
        Validators.required,
      ]),
      addressed: new FormControl(this.formData.addressed),
      country: new FormControl(this.formData.country, [Validators.required]),
    });

    this.form.get('country').setValidators([]);
    this.value.form = Object.assign({}, this.formData);
  }

  callbackMaps(event: any) {
    console.log(event);
    this.form.get('address').setValue(event.address);
    this.form.get('addressed').setValue(event);
  }

  callbackCountry(event: any) {
    console.log('callbackCountry', event);
    this.form.get('country').setValue(event);
  }

  async close(): Promise<any> {
    this._bsModalRef.hide();
  }

  async submit(): Promise<any> {
    console.log(this.formData);
    const firebaseConfig: FirebaseConfigModel = {
      method: 'PUT',
      collection: {
        status: true,
        name: 'globals',
        document: {
          status: true,
          name: 'Users',
          collection: {
            status: true,
            name: 'users',
            document: {
              status: false,
              name: null,
              document: null,
            },
            orderBy: {
              object: 'created',
              order: 'desc',
            },
            query: [],
          },
        },
      },
    };

    const payload: User = {};
    payload.key = this.formData.key;
    payload.address = this.form.get('address').value;
    payload.addressDetail = this.form.get('addressDetail').value;
    if (this.form.get('addressed').value) {
      payload.accuracy = this.form.get('addressed').value.accuracy;
      payload.altitude = this.form.get('addressed').value.altitude;
      payload.altitudeAccuracy = this.form.get(
        'addressed'
      ).value.altitudeAccuracy;
      payload.latitude = this.form.get('addressed').value.latitude;
      payload.longitude = this.form.get('addressed').value.longitude;
      payload.speed = this.form.get('addressed').value.speed;
    }
    payload.country = this.form.get('country').value;

    console.log('payload', payload);
    this.loading.isLoading = true;
    this._firebaseService.updateCollection(null, firebaseConfig, payload).then(
      (result: any) => {
        console.log('result', result);
        this.loading.isLoading = false;
        this._globalAction.showToast({
          title: this._translateService.instant('Info!'),
          message: this._translateService.instant(
            'Successfully updated Addressed!'
          ),
          type: 'success',
        });
        this._bsModalRef.hide();
      },
      (error: any) => {
        console.log(error);
        this.loading.isLoading = false;
        this._globalAction.showToast({
          title: this._translateService.instant('Info!'),
          message: error,
          type: 'error',
        });
      }
    );
  }
}
