import { Component, OnInit } from '@angular/core';

import { FirebaseService } from '@firebase/firebase.service';

import { BsModalRef } from 'ngx-bootstrap/modal';

import { TranslateService } from '@ngx-translate/core';

import { GlobalAction } from '@app/shared/services/global.action';

import { FormGroup, FormControl, Validators } from '@angular/forms';

import { ngxLoadingAnimationTypes } from 'ngx-loading';
import { User } from '@app/shared/model/user.model';
import { FirebaseConfigModel } from '@app/shared/model/other/firebase-config.model';
import { Uploader } from '@app/shared/types/uploader';
import { Loading } from '@app/shared/model/other/loading.model';

@Component({
  selector: 'app-edit-image-profile',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
})
export class EditPhotoProfileComponent implements OnInit {
  loading: Loading = {
    isLoading: false,
    text: this._translateService.instant('Updated Photo Profile') + '...',
    primaryColour: '',
    secondaryColour: '',
    animation: ngxLoadingAnimationTypes.threeBounce,
    image: 'assets/img/svg/save-person.svg',
  };

  value: any = {
    form: {
      photo: {},
      photoBlob: {},
    },
  };

  numbersOnly = /^[0-9]*$/;

  form: FormGroup;
  formData: User;

  schemaUploader: Uploader = {
    component: 'uploader',
    position: 'body',
    label: {
      active: true,
      binding: false,
      text: 'Photo',
      type: 'text',
      state: {
        hide: false,
        disabled: false,
        hideCondition: [],
        disabledCondition: [],
      },
    },
    labelSubtitle: {
      active: false,
    },
    parentBinding: 'form',
    bindingObject: 'photo',
    bindingObjectBlob: 'photoBlob',
    accept: 'image/*',
    minsize: 0.01,
    maxsize: 10,
    isDisabled: false,
    animation: false,
    multiple: false,
    state: {
      hide: false,
      disabled: false,
      hideCondition: [],
      disabledCondition: [],
    },
    required: true,
    formText: {
      valid: {
        active: true,
        text: 'Data telah Sesuai!',
      },
      invalid: {
        active: true,
        text: 'Data Belum Sesuai',
      },
    },
  };

  constructor(
    private _bsModalRef: BsModalRef,
    private _translateService?: TranslateService,
    private _firebaseService?: FirebaseService,
    private _globalAction?: GlobalAction
  ) {}

  async ngOnInit(): Promise<any> {
    this.form = new FormGroup({
      photo: new FormControl('', [Validators.required]),
      photoBlob: new FormControl('', [Validators.required]),
    });
  }

  async close(): Promise<any> {
    this._bsModalRef.hide();
  }

  callbackUploaders(event: any) {
    console.log(event);
    if (event) {
      this.form.get('photo').setValue(event.photo);
      this.form.get('photoBlob').setValue(event.photoBlob);
    } else {
      this.form.get('photo').setValue(null);
      this.form.get('photoBlob').setValue(null);
    }
  }

  async submit(): Promise<any> {
    const firebaseConfig: FirebaseConfigModel = {
      method: 'PUT',
      storage: {
        active: true,
        path: '/globals/Users/users/',
        keyUpload: ['photoBlob'],
        keyResult: ['photo'],
        deletePath: '',
      },
      collection: {
        status: true,
        name: 'globals',
        document: {
          status: true,
          name: 'Users',
          collection: {
            status: true,
            name: 'users',
            document: {
              status: false,
              name: null,
              document: null,
            },
            orderBy: {
              object: 'created',
              order: 'desc',
            },
            query: [],
          },
        },
      },
    };

    if (this.formData.photo) {
      firebaseConfig.storage.deletePath = this.formData.photo.fullPath;
    }

    const payload: User = {};
    payload.key = this.formData.key;
    payload.photo = this.form.get('photo').value;
    payload.photoBlob = this.form.get('photoBlob').value;

    this.loading.isLoading = true;
    this._firebaseService.updateCollection(null, firebaseConfig, payload).then(
      (result: any) => {
        this.loading.isLoading = false;
        this._globalAction.showToast({
          title: this._translateService.instant('Info!'),
          message: this._translateService.instant(
            'Successfully edit photo profile!'
          ),
          type: 'success',
        });
        this._bsModalRef.hide();
      },
      (error: any) => {
        console.log(error);
        this.loading.isLoading = false;
        this._globalAction.showToast({
          title: this._translateService.instant('Info!'),
          message: error,
          type: 'error',
        });
      }
    );
  }
}
