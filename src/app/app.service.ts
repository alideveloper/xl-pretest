import { Injectable } from '@angular/core';

import { AppData } from './app.data';
import { FirebaseService } from '@firebase/firebase.service';
import { BehaviorSubject } from 'rxjs';

import * as _ from 'lodash';

import { environment } from '@environments/environment';
import { FirebaseConfigModel } from '@shared/model/other/firebase-config.model';
import { Business } from '@shared/model/business.model';
import { Apps } from '@shared/model/panel/apps.model';
import { Query } from '@shared/types/firebase';
import { Domain } from '@shared/model/panel/domain.model';
import { User } from './shared/model/user.model';
import { Router } from '@angular/router';
import { GlobalAction } from './shared/services/global.action';
import { TranslateService } from '@ngx-translate/core';
import { Staff } from './shared/model/staff.model';

@Injectable({
  providedIn: 'root',
})
export class AppService {
  whatsapp: any = {
    phone: '6285280871948',
    title: 'Your Message...',
  };

  onDataChanges: BehaviorSubject<any>;

  staff: Staff;

  constructor(
    private appData: AppData,
    private _firebaseService: FirebaseService,
    private router: Router,
    private _globalAction: GlobalAction,
    private _translateService: TranslateService
  ) {
    this.onDataChanges = new BehaviorSubject({});
  }

  async initData(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      await this.getData();
    });
  }

  async getData(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      await this.getCompanies();
      resolve(true);
    });
  }

  getCompanies(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const user: User = JSON.parse(
        localStorage.getItem(environment.domain + '-Login')
      );

      const configFirebase: FirebaseConfigModel = {
        collection: {
          status: true,
          name: 'apps',
          document: {
            status: true,
            name: 'Database',
            collection: {
              status: true,
              name: 'business',
              document: {
                status: false,
                name: null,
                document: null,
              },
              orderBy: {
                object: 'created',
                order: 'desc',
              },
              query: [
                {
                  object: 'userKey',
                  condition: '==',
                  value: user.key,
                },
              ],
            },
          },
        },
      };

      const data: any = await this._firebaseService.handleCollection(
        null,
        configFirebase
      );
      data.subscribe(
        (response: Business[]) => {
          console.log('getCompanies', response);

          this.appData.business = [...response];
          console.log(this.appData.business);

          if (response && response.length === 0) {
            try {
              localStorage.removeItem(environment.domain + '-business');
              localStorage.removeItem(environment.domain + '-selectedBusiness');
              localStorage.removeItem(
                environment.domain + '-selectedBusinessValue'
              );
              console.log('remove localstorage');
            } catch (error) {}
            this.appData.selectedBusinessValue = null;
            this.appData.selectedBusiness = {};
            this.onDataChanges.next(this.appData);
            console.log('go to dashboard');
            this._globalAction.showToast({
              title: this._translateService.instant('Info!'),
              message: this._translateService.instant('Add Business First!'),
              type: 'error',
            });
            this.router.navigateByUrl('/dashboard');
          }

          localStorage.setItem(
            environment.domain + '-business',
            JSON.stringify(this.appData.business)
          );
          this.onDataChanges.next(this.appData);
          resolve(true);
        },
        (error: any) => {
          console.log('getCompanies', error);
          let value: any = localStorage.getItem(
            environment.domain + '-business'
          );
          if (value) {
            value = JSON.parse(value);
          } else {
            value = [];
          }

          this.appData.business = [...value];
          this.onDataChanges.next(this.appData);
        }
      );
    });
  }

  async getProjects(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const configFirebase: FirebaseConfigModel = {
        collection: {
          status: true,
          name: 'apps',
          document: {
            status: false,
          },
        },
        orderBy: {
          object: 'created',
          order: 'desc',
        },
        query: [
          {
            object: 'key',
            condition: '==',
            value: environment.projectId,
          },
        ],
      };

      const result = await this._firebaseService.handleCollection(
        null,
        configFirebase
      );
      result.subscribe(
        (response: Apps[]) => {
          if (response && response.length > 0) {
            this.appData.project = response[0];
            localStorage.setItem(
              environment.domain + '-project',
              JSON.stringify(response[0])
            );
            this.onDataChanges.next(this.appData);
          }
          resolve(true);
        },
        (error: any) => {
          let value: any = localStorage.getItem(
            environment.domain + '-project'
          );
          if (value) {
            value = JSON.parse(value);
          } else {
            value = {};
          }
          this.appData.project = value;
          this.onDataChanges.next(this.appData);
          resolve(true);
        }
      );
    });
  }

  async getDomains(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const whereDomain: Query = {
        object: 'domain',
        condition: '==',
        value: environment.domain,
      };
      const configFirebase: FirebaseConfigModel = {
        collection: {
          status: true,
          name: 'apps',
          document: {
            status: true,
            name: 'PageBuilderConsole',
            collection: {
              status: true,
              name: 'domains',
              document: {
                status: false,
                name: null,
                document: null,
              },
              orderBy: {
                object: 'created',
                order: 'desc',
              },
              query: [whereDomain],
            },
          },
        },
      };
      const result = await this._firebaseService.handleCollection(
        null,
        configFirebase
      );
      result.subscribe(
        async (response: Domain[]) => {
          if (response && response.length > 0) {
            localStorage.setItem(
              environment.domain + '-domain',
              JSON.stringify(response[0])
            );
            this.appData.domain = response[0];
            this.onDataChanges.next(this.appData);
            resolve(true);
            return;
          } else {
            const resultReturn = await this.getDomainsGlobal();
            resolve(resultReturn);
          }
        },
        (error: any) => {
          let value: any = localStorage.getItem(environment.domain + '-domain');
          if (value) {
            value = JSON.parse(value);
          } else {
            value = {};
          }
          this.appData.domain = value;
          this.onDataChanges.next(this.appData);
          resolve(true);
        }
      );
    });
  }

  async getDomainsGlobal(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const whereDomain: Query = {
        object: 'domain',
        condition: '==',
        value: environment.domainGlobal,
      };
      const configFirebase: FirebaseConfigModel = {
        collection: {
          status: true,
          name: 'globals',
          document: {
            status: true,
            name: 'Builder',
            collection: {
              status: true,
              name: 'domains',
              document: {
                status: false,
                name: null,
                document: null,
              },
              orderBy: {
                object: 'created',
                order: 'desc',
              },
              query: [whereDomain],
            },
          },
        },
      };
      const result = await this._firebaseService.handleCollection(
        null,
        configFirebase
      );
      result.subscribe(
        async (response: Domain[]) => {
          if (response && response.length > 0) {
            localStorage.setItem(
              environment.domain + '-domain',
              JSON.stringify(response[0])
            );
            this.appData.domain = response[0];
            this.onDataChanges.next(this.appData);
            resolve(true);
            return;
          } else {
            resolve(true);
          }
        },
        (error: any) => {
          let value: any = localStorage.getItem(environment.domain + '-domain');
          if (value) {
            value = JSON.parse(value);
          } else {
            value = {};
          }
          this.appData.domain = value;
          this.onDataChanges.next(this.appData);
          resolve(true);
        }
      );
    });
  }
}
