import { Component, ViewEncapsulation } from '@angular/core';

import { Router } from '@angular/router';

import { environment } from '@environments/environment';

@Component({
  selector: 'app-error-500',
  templateUrl: './error-500.component.html',
  styleUrls: ['./error-500.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class Error500Component {
  constructor(private _router: Router) {}

  back(): void {
    this._router.navigateByUrl(
      environment.indexURL.root + environment.indexURL.home
    );
  }
}
