import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '@app/shared';

import { Error500Component } from '@app/errors/500/error-500.component';

const routes = [
  {
    path: '',
    component: Error500Component,
  },
];

@NgModule({
  declarations: [Error500Component],
  imports: [
    RouterModule.forChild(routes),

    CommonModule,
    SharedModule,
    TranslateModule,
  ],
})
export class Error500Module {}
