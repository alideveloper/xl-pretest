import { Component, ViewEncapsulation, OnInit } from '@angular/core';

import { Router } from '@angular/router';

import { environment } from '@environments/environment';
import { Site } from '@app/shared/model/panel/site.model';

@Component({
  selector: 'app-error-404',
  templateUrl: './error-404.component.html',
  styleUrls: ['./error-404.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class Error404Component implements OnInit {
  site: Site;

  constructor(private _router: Router) {}

  ngOnInit(): void {
    console.log('masuk');
    this.site = JSON.parse(localStorage.getItem(environment.domain + '-site'));
    console.log(this.site);
  }

  back(): void {
    this._router.navigateByUrl(
      environment.indexURL.root + environment.indexURL.home
    );
  }
}
