import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '@app/shared';

import { Error404Component } from '@app/errors/404/error-404.component';

const routes = [
  {
    path: '',
    component: Error404Component,
  },
];

@NgModule({
  declarations: [Error404Component],
  imports: [
    RouterModule.forChild(routes),

    CommonModule,
    SharedModule,
    TranslateModule,
  ],
})
export class Error404Module {}
