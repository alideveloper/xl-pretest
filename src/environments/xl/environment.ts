export const environment = {
  production: false,
  hmr: false,
  version: '1.0.3',
  projectId: 'xl',
  domainGlobal: 'Panel',
  domain: 'www.xl.ngodings.com.id',
  serverUrl: '',
  defaultLanguage: 'id',
  logo: 'logoWhite',
  theme: {
    primary: 'dark',
    secondary: 'dark',
  },
  supportedLanguages: [
    {
      value: 'us',
      label: 'English',
      flag: 'assets/flags/us.svg',
    },
    {
      value: 'id',
      label: 'Bahasa',
      flag: 'assets/flags/id.svg',
    },
  ],
  indexURL: {
    rootRoute: '',
    root: '/',
    login: '/login',
    rootLogin: 'login',
    home: '/home',
    homeTwo: '/dashboard',
  },
  layout: {
    type: 'horizontal',
    header: {
      language: true,
      myapps: false,
      inbox: false,
      notification: false,
    },
    whatsapp: false,
    scrollToTop: {
      active: false,
      background: '#2867b2',
    },
  },
};
