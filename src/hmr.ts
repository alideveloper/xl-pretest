import { enableProdMode, NgModuleRef, ApplicationRef } from '@angular/core';
import { createNewHosts } from '@angularclass/hmr';

import { environment } from '@environments/environment';

export function hmrBootstrap(
  module: any,
  bootstrap: () => Promise<NgModuleRef<any>>
) {
  let ngModule: NgModuleRef<any>;
  module.hot.accept();
  if (environment.hmr) {
    if (module.hot) {
      module.hot.accept();
      hmrBootstrap(module, bootstrap);
    } else {
      console.error('HMR is not enabled for webpack-dev-server!');
      console.log('Are you using the --hmr flag for ng serve?');
    }
  } else {
    console.log('hot');
    bootstrap().catch((err) => console.log(err));
  }

  bootstrap()
    .then((mod) => (ngModule = mod))
    .catch((err) => console.error(err));

  module.hot.dispose(() => {
    const appRef: ApplicationRef = ngModule.injector.get(ApplicationRef);
    const elements = appRef.components.map((c) => c.location.nativeElement);
    const makeVisible = createNewHosts(elements);
    ngModule.destroy();
    makeVisible();
  });
}
